#include <Arduino.h>

class MCP4021{
    int csPin,udPin;
    public:
        MCP4021(int cspin,int udpin){
            csPin = cspin;
            csPin = udpin;
        }
    
    void Setup(){
         pinMode(udPin, OUTPUT);
	    pinMode(csPin, OUTPUT);
    }
    void IncrementTimingWaveform(int repeat)
    {
        IncrementTimingWaveform(csPin,udPin,repeat);
    }
    void DecrementTimingWaveform(int repeat){
        DecrementTimingWaveform(csPin,udPin,repeat);
    }
    static void IncrementTimingWaveform(int cspin,int udpin,int repeat);
    static void DecrementTimingWaveform(int cspin,int udpin,int repeat);

};
