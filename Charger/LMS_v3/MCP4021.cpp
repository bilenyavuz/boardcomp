#include <Arduino.h>
#include "MCP4021.h"
/* please check documentation at http://ww1.microchip.com/downloads/en/DeviceDoc/20001945F.pdf figure 1-1 */
void MCP4021::IncrementTimingWaveform(int csPin,int udPin,int repeat)
{
	/** set cs and ud for initial values */
	//  cs  high
	digitalWrite(csPin, HIGH);
    // ud  low
	digitalWrite(udPin, LOW);

	/* starting to procedure */
    // ud high
    digitalWrite(udPin,HIGH);
	// tLUC 0.5ms
	delayMicroseconds(1);
	// cs low
	digitalWrite(csPin, LOW);
	// tLCUF 0.5ms
	delayMicroseconds(1);
	/* signal increment */
	// ud low
	digitalWrite(udPin,LOW);
	// tLCUR 3ms or tLO 500ns
	delayMicroseconds(1);
	// ud high this will increment wave
	digitalWrite(udPin,HIGH);
	/* repeat increment */
	for (int i = 0; i < repeat; i++)
	{
			// tS wait to settle wave form 2kOhm 0.5ms - 50 kOhm 10ms
			//delayMicroseconds(10);
			// tHI 0.5ms
			delayMicroseconds(1);
			// ud low
			digitalWrite(udPin,LOW);
			// 1/fUD 1ms
			delayMicroseconds(1);
			// ud high this will increment wave
			digitalWrite(udPin,HIGH);
	}
	 /* end procedure */

	// tLUC 0.5ms
	delayMicroseconds(1);
	// cs high
	digitalWrite(csPin, HIGH);
	// tLCUF 0.5ms
	delayMicroseconds(1);
	// ud low
	digitalWrite(udPin,LOW);
	// tCSHI 0.5ms
	delayMicroseconds(1);
	// ud low
	digitalWrite(csPin,LOW);
}

/* please check documentation at http://ww1.microchip.com/downloads/en/DeviceDoc/20001945F.pdf figure 1-2 */
void MCP4021::DecrementTimingWaveform(int csPin,int udPin,int repeat)
{
 	/** set cs and ud for initial values */
	//  cs  high
	digitalWrite(csPin, HIGH);

    // ud  high
	digitalWrite(udPin, HIGH);

	/* starting to procedure */

    // ud low
    digitalWrite(udPin,LOW);
	// tLUC 0.5ms
	delayMicroseconds(1);
	// cs low
	digitalWrite(csPin, LOW);
	// tLCUR 3ms
	delayMicroseconds(3);
	/* signal decrement */
	// ud high this will decrement wave
	digitalWrite(udPin,HIGH);

    /* repeat decrement */
	for (int i = 0; i < repeat; i++)
	{
			// tS wait to settle wave form 2kOhm 0.5ms - 50 kOhm 10ms
			//delayMicroseconds(10);
			// tHI 0.5ms
			delayMicroseconds(1);
			// ud low
			digitalWrite(udPin,LOW);
			// 1/fUD 1ms
			delayMicroseconds(1);
			// ud high this will decrement wave
			digitalWrite(udPin,HIGH);
	}
 	/* end procedure */

	// tLUC 0.5ms
	delayMicroseconds(1);
	// cs high
	digitalWrite(csPin, HIGH);
	// tLCUF 0.5ms
	delayMicroseconds(1);
	// ud low
	digitalWrite(udPin,LOW);
	// tCSHI 0.5ms
	delayMicroseconds(1);
	// ud low
	digitalWrite(csPin,LOW);
}


