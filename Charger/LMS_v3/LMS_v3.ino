#include <MapleFreeRTOS900.h>
#include <Wire.h>
#include <SPI.h>
#include <inttypes.h>
#include "MCP4021.h"

#ifndef pdMS_TO_TICKS_CST
#define pdMS_TO_TICKS_CST (xTimeInMs)(((TickType_t)(((TickType_t)(xTimeInMs)*(TickType_t)configTICK_RATE_HZ*(TickType_t)5)/(TickType_t)1000)))
#define PIN_DATA_OUT PA7   // MOSI (Master Out / Slave In)
#define PIN_DATA_IN PA6    // MISO (Master In / Slave Out)
#define PIN_SPI_CLOCK PA5  // SCK (Serial Clock) 
#define PIN_SPI_SS PA4 		//SS (Slave Select)
#endif




/*
TODOS:
	- Check BMS Communication, especially sent data!
*/


// Relais to connect phase to power supply 
// { PH1->LD1, PH1->LD2, PH2->LD2, PH2->LD3, PH3->LD3, PH3->LD4 }
int Relais[] = {PE7, PB1, PC5, PC4, PB0, PB2};

// Relais to PowerSupply Mapping
int LDToRelais[][2] = { {0,0}, {1,2}, {3,4} , {5,0} };

// MCP4021 digital Poti to reduce voltage, { LD1, LD2, LD3, LD4 }
//int MCP4021_UD[] = { PC6, PC7, PC8, PC9 };
//int MCP4021_CS[] = { PA12, PA10, PA14, PA8 };
// create potentiometers
MCP4021 digiPots[] = {MCP4021(PC6,PA12),MCP4021(PC7,PA10),MCP4021(PC8,PA14),MCP4021(PC9,PA8)};

// I sensor, { PH1, PH2, PH3 }
int Imeasure[] = { PC0, PC1, PC3 };


int toBMS[15] = { 0 };
uint8_t bmsData[89] = { 0 };



/*
 * Temperature:  Range -20 and +80 (Celsius)
 *       100 / 255 = 0.39  (We used Range -20° to 80° = 100 in total)
 *       0.39 * byte Data = Temperature - 20° (Top is 80°)
 * Voltage (Spannung):   2V until 4V
 *		1. Option (more precise) [Range between 2-4V]
 *      	2V / 255 = 0.0078V (each Bit!)
 *      	0.0078V * byte Data = Voltage (Total of 2V)
 *		2. Option
 *			4V / 255 = 0.0156V (each Bit!)
 *      	0.0156V * byte Data = Voltage (Total of 4V)
*/
double temp_offset = -20;
double temp_const = 100 / 255;
double volt_offset = 2;
double volt_const = 2 / 255;


void setup(){
    for(int i = 0; i < (sizeof(Relais)/sizeof(int)); i++) {
        pinMode(Relais[i], OUTPUT);
        digitalWrite(Relais[i], LOW);
    }
	// setup potentiometers
	for (int i = 0; i < 4; i++) {
		// pinMode(MCP4021_UD[i], OUTPUT);
		// pinMode(MCP4021_CS[i], OUTPUT);
		// digitalWrite(MCP4021_CS[i], HIGH);
		digiPots[i].Setup();
	}
	for (int i = 0; i < 3; i++) {
		pinMode(Imeasure[i], INPUT);
	}
    //Task - Prio 1-3, Stackgr��e 100
	xTaskCreate(BMScommunication, "Communication with BMS", 100, NULL, 2, NULL);
	xTaskCreate(TemperaturShutdown, "Emergency shutdown because of high temperatur", 100, NULL, 1, NULL);
	xTaskCreate(OvervoltageShutdown, "Emergency shutdown becuse of overvoltage", 100, NULL, 1, NULL);
	xTaskCreate(VoltageReduction, "Voltage Reduction via MCP4021 when BMS is balancing", 100, NULL, 2, NULL);
	xTaskCreate(DefaultLoading, "Default loading cycle", 100, NULL, 3, NULL);
	xTaskCreate(MeasureCurrent, "Measure current on the 3 phases", 100, NULL, 3, NULL);
    vTaskStartScheduler();

    // Initialize Serial for debugging
    Serial.begin(115200);
    
	pinMode(PIN_SPI_SS, INPUT);
    SPI.beginTransactionSlave(SPISettings(4500000, MSBFIRST, SPI_MODE0, DATA_SIZE_8BIT));
}

/*
 *OPTION 1:
 * RECEIVED DATASTRING
 *    01,    02   ,    03   ,    04   ,     05    ,   06   ,  07 , ...
 *    ID, Spannung, Spannung, Spannung, Temperatur, Balance, ID-2, ...
 *	  08,    09   ,    10   ,    11   ,     12    ,   13   ,  14 , ...
 *    15,    16   ,    17   ,    18   ,     19    ,   20   ,  21 , ...

 *OPTION 2 (CHOSEN ONE):
 * RECEIVED DATASTRING
 *    	  00  ,    01   ,    02   ,    03   ,    04   ,    05   ,  ... ,    59   ,
 *    Spannung, Spannung, Spannung, Spannung, Spannung, Spannung,  ... , Spannung,
 *
 *	       60    ,      62    ,      63    ,      64    , ... ,      79    ,
 *    Temperature, Temperature, Temperature, Temperature, ... , Temperature,
 *
 *	     80  ,    81  ,    82  ,    83  ,    84  , ... ,   87
 *    Balance, Balance, Balance, Balance, Balance, ... , Balance
*/
void BMScommunication(void *params) {
	while (true) {
		for (int i = 0; i < 89; i++) {
			if (i < 13) {
				bmsData[i] = SPI.transfer(toBMS[i]);        // do a transfer
			}
			else { 
				bmsData[i] = SPI.transfer(0);				// do a transfer with dummy data
			}        
		}
		vTaskDelay(100);
	}
}

void TemperaturShutdown(void *params) {
	double temp_value;
	while (true) {
		//20x Temperature: pro Modul 5x Temperatur
		for (int i = 60; i < 80; i++) {
			temp_value = (bmsData[i] * temp_const) + temp_offset;
			int modulNr = (int)((i - 60) / 5);
			bool cT = checkTemp(temp_value);
			for (int j = 0; j < (sizeof(LDToRelais[modulNr]) / LDToRelais[modulNr][0]); j++) {
				if (cT) pinMode(Relais[LDToRelais[modulNr][j]], INPUT);
				else pinMode(Relais[LDToRelais[modulNr][j]], OUTPUT);
			}
			toBMS[modulNr + 4] = (int)cT;
		}
		vTaskDelay(100);
	}		
}

void OvervoltageShutdown(void *params) {
	double volt_value;
	while (true) {
		//60x Cell Voltage: 15x per Module
		for (int i = 0; i < 60; i++) {
			volt_value = (bmsData[i] * volt_const) + volt_offset;
			int modulNr = (int)(i / 15);
			bool cV = checkVolt(volt_value);
			for (int j = 0; j < (sizeof(LDToRelais[modulNr]) / LDToRelais[modulNr][0]); j++) {
				if (cV) pinMode(Relais[LDToRelais[modulNr][j]], INPUT);
				else pinMode(Relais[LDToRelais[modulNr][j]], OUTPUT);
			}
			toBMS[modulNr] = (int)cV;
		}
		vTaskDelay(100);
	}
}


void VoltageReduction(void *params) {
	while (true) {
		for (int i = 80; i < 88; i+2) {
			int modulNr = (int)((i - 80) / 2);
			if ((bmsData[i] >= 1 ? toBMS[modulNr + 8] == 0 : toBMS[modulNr + 8] >= 1)) {
				toBMS[modulNr + 8] = bmsData[i];
				if(bmsData[i] >= 1)
					digiPots[i].DecrementTimingWaveform(63);
				else
					digiPots[i].IncrementTimingWaveform(63);

				//handleVReduction(modulNr, (bmsData[i] >= 1));
			}
		}
		vTaskDelay(100);
	}
}

// void handleVReduction(int modulNr, bool reduce) {
// 	digitalWrite(MCP4021_CS[modulNr], LOW);
// 	digitalWrite(MCP4021_UD[modulNr], reduce);

// 	for (int i = 0; i < 64; i++) {
// 		digitalWrite(MCP4021_UD[modulNr], !reduce);
// 		delayMicroseconds(2);
// 		digitalWrite(MCP4021_UD[modulNr], reduce);
// 		delayMicroseconds(1);
// 	}
// 	digitalWrite(MCP4021_UD[modulNr], !reduce);
// 	delayMicroseconds(10);
// 	digitalWrite(MCP4021_CS[modulNr], HIGH);
// }

void DefaultLoading(void *params) {
	while (true) {
		for (int i = 0; i < 4; i++) {
			powerArraysByCycle(i, HIGH);
			vTaskDelay(3000000);
			powerArraysByCycle(i, LOW);
			vTaskDelay(1000);
		}
	}
}

void MeasureCurrent(void *params) {	
	while (true) {
		for (int i = 0; i < 3; i++) {
			// divide analog value by x ?
			toBMS[i + 12] = analogRead(Imeasure[i]);
		}
		vTaskDelay(100);
	}
}

//Check if the voltage is > 3.8V (batteries are full - stop charging)
bool checkVolt(double x) {
	return (x > 3.8);
}

//Check the Temperature between -18� and +81�
bool checkTemp(double x) {
	return (x < -18 || x > 75);
}


void powerArraysByCycle(int cycle, uint8_t powered) {
  switch(cycle) {
    case 0:
        digitalWrite(Relais[0], powered);
        digitalWrite(Relais[2], powered);
        digitalWrite(Relais[4], powered);
        break;
    case 1:
        digitalWrite(Relais[0], powered);
        digitalWrite(Relais[3], powered);
        digitalWrite(Relais[5], powered);
        break;
    case 2:
        digitalWrite(Relais[0], powered);
        digitalWrite(Relais[2], powered);
        digitalWrite(Relais[5], powered);
        break;
    case 3:
        digitalWrite(Relais[1], powered);
        digitalWrite(Relais[3], powered);
        digitalWrite(Relais[5], powered);
        break;
  }
}

void loop() {}
