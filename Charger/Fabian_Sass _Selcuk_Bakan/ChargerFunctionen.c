#include <Arduino_FreeRTOS.h>
#ifndef pdMS_TO_TICKS_CST
#define pdMS_TO_TICKS_CST( xTimeInMs ) ( (( TickType_t ) ( ( ( TickType_t ) ( xTimeInMs ) * ( TickType_t ) configTICK_RATE_HZ  * ( TickType_t )5)  / ( TickType_t )1000)) )
#endif

//int Relais[] = {7, 6, 5, 4, 3, 2};
//int SpannungsRelais[] = {1, 2, 3, 4}
int Relais[] = {20, 21, 19, 18, 25, 31};
int cycle = 0;

//Mödlichkeit 2: Blacklisting
int Blacklisting[6];

void setup() {

  // Initialize D7 - D2 as OUTPUTS and setting them to LOW
  for(int i = 0; i < (sizeof(Relais)/sizeof(int)); i++) {
    pinMode(Relais[i], OUTPUT);
    digitalWrite(Relais[i], LOW);
  }  

  /* 
   * There should be at least one more task to handle the states:  
   * battery full (receiving voltage meter)
   * battery/system overheat (temp. sensor) 
   * 
   * these tasks should receive the taskhandle for handleLoadingCycle, and interrupt / stop the task if necessary
   * 
   * handleLoadingCycle should have a smaller priority since loading safely is more important
   * 
   * The voltage of each of the 4 battery blocks should be stored independantly to handle the loading cycle 
   * in case 1 block is full while the other ones are not. (comparable to blacklisting)
   */
  xTaskCreate(handleLoadingCycle, "Handle loading cycle", 100, NULL, 2, NULL);

  vTaskStartScheduler();

  Serial.begin(9600);
}



void handleLoadingCycle() {
  for(;;) {
    powerArraysByCycle(HIGH);
    Serial.print("Cycle = ");
    Serial.print(cycle);
    Serial.println(", HIGH");
    vTaskDelay(pdMS_TO_TICKS_CST(10000));
    powerArraysByCycle(LOW);
    Serial.print("Cycle = ");
    Serial.print(cycle);
    Serial.println(", LOW");
    vTaskDelay(pdMS_TO_TICKS_CST(500));
    if (cycle < 3)  { cycle++; }
    else cycle = 0;
  }
}
/*
//Exception once the BatteryStatus is over an certain value.
//Need Temperatur Sensor
void overloadedBattery() {
  if(BatteryPower is Higher than ...) {
    //Stops Tasks, while reading from handler.
    for(int i = 0; i < (sizeof(Relais)/sizeof(int)); i++) {
        digitalWrite(Relais[i], LOW);
        
    }  
  }
}
*/
/*
//TODO: Blacklisten von den Batterien die voll aufgeladen sind auf 3.6V und ein Strom von 10%
void blacklisting() {
    int j = 0;
    for(int i = 0; i < (sizeof(Relais)/sizeof(int)); i++) {
      if(Relais[i] > 3.6 (VOLT)) {
          //Mödlichkeit 1:
          //TESTEN ob pinMode(Relais[i], INPUT) in der Laufzeit änderbar?
          pinMode(pinMode[i], INPUT);
          
          //Mödlichkeit 2:
          Blacklisting[j] = Relais[i];
          j++;
      } else {
          pinMode(pinMode[i], OUTPUT);
      }
   } 
}
*/

//TODO: Zellen innerhalb eines Batteriemoduls haben unterschiedliche Spannungen, wenn BMS Balanced, dann Ladegerät (Spannung) reduzieren...
/*
void spannungsRegler() {
  if(BMS[i] Balancing is ON bei 3.62V) {
    //SpannungsRelais[x] OFF:ON //REGLER
    digitalWrite(SpannungsRelais[i], powered);
  } else {
    digitalWrite(SpannungsRelais[i], unpowered);
  }
}
*/

//TODO: Zellen innerhalb eines Batteriemoduls haben unterschiedliche Spannungen, wenn BMS Balanced, dann Ladegerät (Spannung) reduzieren...
//Über ein entsprechenden PIN ansprechen für Transistorschlatung! (DIGITAL-PIN)



void powerArraysByCycle(uint8_t powered) {
  /*
   * occurences ?!?!?:
   * 0 -> 3x
   * 1 -> 1x
   * 2 -> 2x
   * 3 -> 2x
   * 4 -> 1x
   * 5 -> 3x
   * 
   */
  
  switch(cycle) {
    /*
    case 0:
        digitalWrite(Relais[0], powered);
        digitalWrite(Relais[2], powered);
        digitalWrite(Relais[3], powered);
        //digitalWrite(Relais[3], powered);
        //if (canLoadAll4Modules) digitalWrite(bla,powered);
        break;
    case 1:
        digitalWrite(Relais[2], powered);
        digitalWrite(Relais[3], powered);
        digitalWrite(Relais[5], powered);
        break;
    case 2:
        digitalWrite(Relais[4], powered);
        digitalWrite(Relais[0], powered);
        digitalWrite(Relais[5], powered);
        break;
    case 3:
        digitalWrite(Relais[1], powered);
        digitalWrite(Relais[0], powered);
        digitalWrite(Relais[5], powered);
      break;
    */
    case 0:
        digitalWrite(Relais[0], powered);
        digitalWrite(Relais[2], powered);
        digitalWrite(Relais[4], powered);
        //digitalWrite(Relais[3], powered);
        //if (canLoadAll4Modules) digitalWrite(bla,powered);
        break;
    case 1:
        digitalWrite(Relais[0], powered);
        digitalWrite(Relais[3], powered);
        digitalWrite(Relais[5], powered);
        break;
    case 2:
        digitalWrite(Relais[0], powered);
        digitalWrite(Relais[2], powered);
        digitalWrite(Relais[5], powered);
        break;
    case 3:
        digitalWrite(Relais[1], powered);
        digitalWrite(Relais[3], powered);
        digitalWrite(Relais[5], powered);
        break;
  }
}


void loop() {}
