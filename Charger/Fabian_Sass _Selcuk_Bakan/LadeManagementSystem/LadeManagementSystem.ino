#include <MapleFreeRTOS900.h>
#include <Wire.h>
#include <SPI.h>
#include <inttypes.h>

#ifndef pdMS_TO_TICKS_CST
#define pdMS_TO_TICKS_CST (xTimeInMs)(((TickType_t)(((TickType_t)(xTimeInMs)*(TickType_t)configTICK_RATE_HZ*(TickType_t)5)/(TickType_t)1000)))
#define PIN_DATA_OUT 11   // MOSI (Master Out / Slave In)
#define PIN_DATA_IN 12    // MISO (Master In / Slave Out)
#define PIN_SPI_CLOCK 13  // SCK (Serial Clock) 
#endif


//int Relais[] = {7, 6, 5, 4, 3, 2};
//int SpannungsRelais[] = {1, 2, 3, 4}
int Relais[] = {20, 21, 19, 18, 25, 31};
int cycle = 0;

//Moeglichkeit 1: Relais[i] von OUTPUT zu INPUT setzen womit geloest waere!
//Moeglichkeit 2: Blacklisting (Explizit die Relais wÃ¤hlen die rausgenommen werden sollen und dann entsprechend ignorieren)
int Blacklisting[6];


// Creating an object to handle the 2nd SPI interface
SPIClass SPI_5(5);

void setup(){
    // Initialize D7 - D2 as OUTPUTS and setting them to LOW
    for(int i = 0; i < (sizeof(Relais)/sizeof(int)); i++) {
        pinMode(Relais[i], OUTPUT);
        digitalWrite(Relais[i], LOW);
    }  

    xTaskCreate(handleLoadingCycle, "Handle loading cycle", 100, NULL, 2, NULL);
    vTaskStartScheduler();
    // Initialize Serial for debugging
    Serial.begin(115200);
    
    //Defining SS as output -> GETS Data from Master!
    pinMode(PB12, INPUT);

    //Default-Wert should be always on HIGH, when data is send put on LOW and
    //  set again on HIGH when the transaction is over.
    digitalWrite(PB12, HIGH);
    // Initializing SPI 2 interface on 4.5MHz
    SPI_5.beginTransactionSlave(SPISettings(4500000, MSBFIRST, SPI_MODE0, DATA_SIZE_8BIT));
}


uint8_t count = 0;

void loop(){
    count++;
    // EXCHANGE SPI REGISTERS
    uint8_t msg = SPI_5.transfer(count);
    Serial.print("Count: ");
    Serial.print(count);
    Serial.print(", Received: ");
    Serial.println(msg, HEX);
}

/*
 * Temperature:  Range -20° until 80°
 *       100 / 255 = 0.39  (We used Range -20° to 80° = 100 in total)
 *       0.39 * byte Data = Temperature - 20° (Top is 80°)
 * Voltage (Spannung):   2V until 4V
 *       4V / 255 = 0.0156V (each Bit!)
 *       0.0156V * byte Data = Voltage (Total of 4V)
*/

/*
 * RECEIVED DATASTRING
 *    01,    02   ,    03   ,    04   ,     05    , 06 , ...
 *    ID, Spannung, Spannung, Spannung, Temperatur, ID2, ...
*/

void sendData(){
    int i = 0;
    int x = 0;

    //SPI.beginTransaction (SPISettings (4500000, MSBFIRST, SPI_MODE0));
    digitalWrite (SS, LOW);                       // assert Slave Select
    char checkedData[88];
    uint8_t receivedData[88];
    
    for(i = 0, i < 88, i++) {
        receivedData[i] = SPI.transfer(0);        // do a transfer
        if(i%(2+x) == 0 || i%(3+x) == 0 || i%(4+x) == 0) {
           if(i%(4+x) == 0= {
                x++;
           }
           receivedData[i] 
        }
     }
    
    digitalWrite (SS, HIGH);                      // de-assert Slave Select
    SPI.endTransaction ();                        // transaction over
}


int binaryToInteger(uint8_t x) {
    
}


void handleLoadingCycle(void* pv) {
    for(;;) {
        powerArraysByCycle(HIGH);
        Serial.print("Cycle = ");
        Serial.print(cycle);
        Serial.println(", HIGH");
        vTaskDelay(pdMS_TO_TICKS_CST(10000));
        powerArraysByCycle(LOW);
        Serial.print("Cycle = ");
        Serial.print(cycle);
        Serial.println(", LOW");
        vTaskDelay(pdMS_TO_TICKS_CST(500));
        
    if (cycle < 3)  { cycle++; }
        else cycle = 0;
    }
}

//TODO: Zellen innerhalb eines Batteriemoduls haben unterschiedliche Spannungen, 
//        wenn BMS Balanced, dann Ladegeraet (Spannung) reduzieren...
void spannungsRegler() {
    if(BMS[i] Balancing is ON bei 3.62V) {
        //SpannungsRelais[x] OFF:ON //REGLER
        digitalWrite(SpannungsRelais[i], powered);
    } else {
        digitalWrite(SpannungsRelais[i], unpowered);
    }
}

void powerArraysByCycle(uint8_t powered) {
  /*
   * occurences ?!?!?:
   * 0 -> 3x
   * 1 -> 1x
   * 2 -> 2x
   * 3 -> 2x
   * 4 -> 1x
   * 5 -> 3x
   * 
   */
  
  switch(cycle) {
    /*
    case 0:
        digitalWrite(Relais[0], powered);
        digitalWrite(Relais[2], powered);
        digitalWrite(Relais[3], powered);
        //digitalWrite(Relais[3], powered);
        //if (canLoadAll4Modules) digitalWrite(bla,powered);
        break;
    case 1:
        digitalWrite(Relais[2], powered);
        digitalWrite(Relais[3], powered);
        digitalWrite(Relais[5], powered);
        break;
    case 2:
        digitalWrite(Relais[4], powered);
        digitalWrite(Relais[0], powered);
        digitalWrite(Relais[5], powered);
        break;
    case 3:
        digitalWrite(Relais[1], powered);
        digitalWrite(Relais[0], powered);
        digitalWrite(Relais[5], powered);
      break;
    */
    
    //GELADENE BATTERIEBLOECKE
    //TODO: Sende Informationen der geladenen Batteriebloecke an das BMS!!!
    case 0:
        digitalWrite(Relais[0], powered);
        digitalWrite(Relais[2], powered);
        digitalWrite(Relais[4], powered);
        //digitalWrite(Relais[3], powered);
        //if (canLoadAll4Modules) digitalWrite(bla,powered);
        break;
    case 1:
        digitalWrite(Relais[0], powered);
        digitalWrite(Relais[3], powered);
        digitalWrite(Relais[5], powered);
        break;
    case 2:
        digitalWrite(Relais[0], powered);
        digitalWrite(Relais[2], powered);
        digitalWrite(Relais[5], powered);
        break;
    case 3:
        digitalWrite(Relais[1], powered);
        digitalWrite(Relais[3], powered);
        digitalWrite(Relais[5], powered);
        break;
  }
}

#
