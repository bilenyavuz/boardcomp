#include <Arduino_FreeRTOS.h>
#include <Wire.h>
#include <SPI.h>
#include <inttypes.h>

#ifndef pdMS_TO_TICKS_CST
#define pdMS_TO_TICKS_CST (xTimeInMs)(((TickType_t)(((TickType_t)(xTimeInMs)*(TickType_t)configTICK_RATE_HZ*(TickType_t)5)/(TickType_t)1000)))
#define PIN_DATA_OUT 11   // MOSI (Master Out / Slave In)
#define PIN_DATA_IN 12    // MISO (Master In / Slave Out)
#define PIN_SPI_CLOCK 13  // SCK (Serial Clock) 
#endif


//int Relais[] = {7, 6, 5, 4, 3, 2};
//int SpannungsRelais[] = {1, 2, 3, 4}
int Relais[] = {20, 21, 19, 18, 25, 31};
int cycle = 0;

//Moeglichkeit 1: Relais[i] von OUTPUT zu INPUT setzen womit geloest waere!
//Moeglichkeit 2: Blacklisting (Explizit die Relais wÃ¤hlen die rausgenommen werden sollen und dann entsprechend ignorieren)
int Blacklisting[6];

/*
double volt_const = 100/255;
double temp_const = 2/255;
double volt_value[64];
double temp_value[20];
uint8_t balancing_value[8];
uint8_t balancing_temp_value[3];
char checkedData[88];
uint8_t bmsData[88];
*/

// Creating an object to handle the 2nd SPI interface
SPIClass SPI_5(5);

void setup(){
    // Initialize D7 - D2 as OUTPUTS and setting them to LOW
    for(int i = 0; i < (sizeof(Relais)/sizeof(int)); i++) {
        pinMode(Relais[i], OUTPUT);
        digitalWrite(Relais[i], LOW);
    }  

    xTaskCreate(handleLoadingCycle, "Handle loading cycle", 100, NULL, 2, NULL);
    vTaskStartScheduler();
    // Initialize Serial for debugging
    Serial.begin(115200);
    
    //Defining SS as output -> GETS Data from Master!
    pinMode(PB12, INPUT);

    //Default-Wert should be always on HIGH, when data is send put on LOW and
    //  set again on HIGH when the transaction is over.
    //pinMode(PB12, HIGH);
    // Initializing SPI 2 interface on 4.5MHz
    SPI_5.beginTransactionSlave(SPISettings(4500000, MSBFIRST, SPI_MODE0, DATA_SIZE_8BIT));
}


uint8_t count = 0;

void loop(){
    count++;
    // EXCHANGE SPI REGISTERS
    uint8_t msg = SPI_5.transfer(count);
    Serial.print("Count: ");
    Serial.print(count);
    Serial.print(", Received: ");
    Serial.println(msg, HEX);
}

/*
 * Temperature:  Range -20 and +80 (Celsius)
 *       100 / 255 = 0.39  (We used Range -20° to 80° = 100 in total)
 *       0.39 * byte Data = Temperature - 20° (Top is 80°)
 * Voltage (Spannung):   2V until 4V
 *		1. Option (more precise) [Range between 2-4V]
 *      	2V / 255 = 0.0078V (each Bit!)
 *      	0.0078V * byte Data = Voltage (Total of 2V)     
 *		2. Option
 *			4V / 255 = 0.0156V (each Bit!)
 *      	0.0156V * byte Data = Voltage (Total of 4V)      
*/

/*
 *OPTION 1:
 * RECEIVED DATASTRING
 *    01,    02   ,    03   ,    04   ,     05    ,   06   ,  07 , ...
 *    ID, Spannung, Spannung, Spannung, Temperatur, Balance, ID-2, ...
 *	  08,    09   ,    10   ,    11   ,     12    ,   13   ,  14 , ...
 *    15,    16   ,    17   ,    18   ,     19    ,   20   ,  21 , ...
 
 *OPTION 2:
 * RECEIVED DATASTRING
 *    	  00  ,    01   ,    02   ,    03   ,    04   ,    05   ,  ... ,    59   ,
 *    Spannung, Spannung, Spannung, Spannung, Spannung, Spannung,  ... , Spannung,
 *	      
 *	       60    ,      62    ,      63    ,      64    , ... ,      80    ,
 *    Temperature, Temperature, Temperature, Temperature, ... , Temperature, 
 *	      
 *	     81  ,    82  ,    83  ,    84  ,    85  , ... ,   88
 *    Balance, Balance, Balance, Balance, Balance, ... , Balance
*/

void receiveData(){
    int i=1;
    
    double volt_value;
    double volt_const = 100/255;
    double temp_value;
    double temp_const = 2/255;
	int balanced;
    int balance[12];
    uint8_t bmsData[89];
    
    
    //Sends the BALANCING Values to the BMS and after that ONLY zeros are send!
    for(i=0, i<89, i++){
    	if(i<13) {
    		bmsData[i] = SPI.transfer(balance[i]);        // do a transfer
		}
		bmsData[i] = SPI.transfer(0);        // do a transfer
	}
    
    
    //SPI.beginTransaction (SPISettings (4500000, MSBFIRST, SPI_MODE0));
    //digitalWrite (SS, LOW);                       // assert Slave Select
    for(i=0, i<89, i++) {        
        ///Option 2
        //60x Spannung, 20x Temperatur, 8x Byte Balance = 88 Byte Insgesamt        
        
        //60x Spannung: pro Modul 15x Spannung
		if(i<14) {
			volt_value = (bmsData[i] * volt_const) + 2;
			if(checkVolt(volt_value) == 1)			
					{		balance[0] = 1;		}
		} else if(i>14 && i<30) {
			volt_value = (bmsData[i] * volt_const) + 2;
			if(checkVolt(volt_value) == 1)			
					{		balance[1] = 1;		}
		} else if(i>29 && i<45) {
			volt_value = (bmsData[i] * volt_const) + 2;
			if(checkVolt(volt_value) == 1)			
					{		balance[2] = 1;		}
		} else if(i>44 && i<60) {
			volt_value = (bmsData[i] * volt_const) + 2;
			if(checkVolt(volt_value) == 1)			
					{		balance[3] = 1;		}
		}
		
		//20x Temperature: pro Modul 5x Temperatur
		else if(i>59 && i<65) {
			temp_value = (bmsData[i] * temp_const) - 20;
			if(checkTemp(temp_value) == 1)			
					{		balance[4] = 1;		}
		} else if(i>64 && i<70) {
			temp_value = (bmsData[i] * temp_const) - 20;
			if(checkTemp(temp_value) == 1)			
					{		balance[5] = 1;		}
		} else if(i>69 && i<75) {
			temp_value = (bmsData[i] * temp_const) - 20;
			if(checkTemp(temp_value) == 1)			
					{		balance[6] = 1;		}
		} else if(i>74 && i<80) {
			temp_value = (bmsData[i] * temp_const) - 20;
			if(checkTemp(temp_value) == 1)			
					{		balance[7] = 1;		}
		}
		
		//BMS sends -1 when NOT Balanced so Value is 79 in Total
		//BMS sends +1 when IT IS Balanced so Value is 81 in Total
		//8x Balance: pro Modul 2x Balance
		else if(i>79 && i<82) {
			if(checkBalance(bmsData[i]) == 1)	
					{		balance[8] = 1;		}
		} else if(i>81 && i<84) {
			if(checkBalance(bmsData[i]) == 1)	
					{		balance[9] = 1;		}
		} else if(i>83 && i<86) {
			if(checkBalance(bmsData[i]) == 1)	
					{		balance[10] = 1;	}
		} else if(i>85 && i<87) {
			if(checkBalance(bmsData[i]) == 1)	
					{		balance[11] = 1;	}
		}
    }
    
    
    for(i=0, i<12, i++) {
    	if(balance[i] == 1) {
    		if(i==0 || i==4 || i==8) {
    			balancing_handler(i);			//BALANCING MODULE Nr.1			//i= 0, 4, 8
			} else if(i==1 || i==5 || i==9) {
				balancing_handler(i);			//BALANCING MODULE Nr.2			//i= 1, 5, 9
			} else if(i==2 || i==6 || i==10) {
				balancing_handler(i);			//BALANCING MODULE Nr.3			//i= 2, 6, 10
			} else if(i==3 ||i==7 || i==11) { 
				balancing_handler(i);			//BALANCING MODULE Nr.4			//i= 3, 7, 11
			}
		} else {
			balance[i] = 0;							//DEFAULT-Value for RESET sends back to BMS!
		}
	}
}



//Check the Voltage (Spannung) between 1.8V and 3.9V
int checkVolt(double x) {
	if(x<1.8 || x>3.9)  {	return 1;	} 
	return 0;
}

//Check the Temperature between -18� and +81�
int checkTemp(double x) {
	if(x<-18 || x>81) {		return 1;	} 
	return 0;
}

//Check Balance from BMS if it is Value 1 or Value -1
int checkBalance(int x) {
	if(x == 1) {	return 1;	}
	return 0;
}



//BALANCING-Handler
//		Zellen innerhalb eines Batteriemoduls haben unterschiedliche 
//		Spannungen & Temperaturen wenn BMS Balanced, dann muss das 
//		Ladegeraet (LMS) ebenfalls die Spannung reduzieren... und die Relais auf LOW setzen!
void balancing_handler(int x) {
	//Balancing Module Nr.1
	if(x==0 || x==4 || x==8) {
		digitalWrite(Relais[0], LOW);
        digitalWrite(Relais[2], LOW);
        digitalWrite(Relais[4], LOW);
	} 
	
	//Balancing Module Nr.2
	else if(x==1 || x==5 || x==9) {
		digitalWrite(Relais[0], LOW);
        digitalWrite(Relais[3], LOW);
        digitalWrite(Relais[5], LOW);
	}
	
	//Balancing Module Nr.3
	else if(x==2 || x==6 || x==10) {
		digitalWrite(Relais[0], LOW);
        digitalWrite(Relais[2], LOW);
        digitalWrite(Relais[5], LOW);
	}
	
	//Balancing Module Nr.4
	else if(x==3 || x==7 || x==11) {
		digitalWrite(Relais[1], LOW);
        digitalWrite(Relais[3], LOW);
        digitalWrite(Relais[5], LOW);
	}
}



void handleLoadingCycle() {
    for(;;) {
        powerArraysByCycle(HIGH);
        Serial.print("Cycle = ");
        Serial.print(cycle);
        Serial.println(", HIGH");
        vTaskDelay(pdMS_TO_TICKS_CST(10000));
        powerArraysByCycle(LOW);
        Serial.print("Cycle = ");
        Serial.print(cycle);
        Serial.println(", LOW");
        vTaskDelay(pdMS_TO_TICKS_CST(500));
        
    if (cycle < 3)  { cycle++; }
        else cycle = 0;
    }
}

void powerArraysByCycle(uint8_t powered) {
  /*
   * occurences ?!?!?:
   * 0 -> 3x
   * 1 -> 1x
   * 2 -> 2x
   * 3 -> 2x
   * 4 -> 1x
   * 5 -> 3x
   * 
   */
  
  switch(cycle) {
    /*
    case 0:
        digitalWrite(Relais[0], powered);
        digitalWrite(Relais[2], powered);
        digitalWrite(Relais[3], powered);
        //digitalWrite(Relais[3], powered);
        //if (canLoadAll4Modules) digitalWrite(bla,powered);
        break;
    case 1:
        digitalWrite(Relais[2], powered);
        digitalWrite(Relais[3], powered);
        digitalWrite(Relais[5], powered);
        break;
    case 2:
        digitalWrite(Relais[4], powered);
        digitalWrite(Relais[0], powered);
        digitalWrite(Relais[5], powered);
        break;
    case 3:
        digitalWrite(Relais[1], powered);
        digitalWrite(Relais[0], powered);
        digitalWrite(Relais[5], powered);
      break;
    */
    
    //GELADENE BATTERIEBLOECKE
    //TODO: Sende Informationen der geladenen Batteriebloecke an das BMS!!!
    case 0:
        digitalWrite(Relais[0], powered);
        digitalWrite(Relais[2], powered);
        digitalWrite(Relais[4], powered);
        //digitalWrite(Relais[3], powered);
        //if (canLoadAll4Modules) digitalWrite(bla,powered);
        break;
    case 1:
        digitalWrite(Relais[0], powered);
        digitalWrite(Relais[3], powered);
        digitalWrite(Relais[5], powered);
        break;
    case 2:
        digitalWrite(Relais[0], powered);
        digitalWrite(Relais[2], powered);
        digitalWrite(Relais[5], powered);
        break;
    case 3:
        digitalWrite(Relais[1], powered);
        digitalWrite(Relais[3], powered);
        digitalWrite(Relais[5], powered);
        break;
  }
}

#
