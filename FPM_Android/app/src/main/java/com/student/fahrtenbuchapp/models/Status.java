package com.student.fahrtenbuchapp.models;

import com.google.gson.annotations.SerializedName;
import io.realm.RealmObject;


public class Status extends RealmObject {


    //public Status(){}


/*    public Status(String id, String model, String chargedkWh, int battery, Boolean charging) {
        this.user = id;
        this.model = model;
        this.chargedkWh = chargedkWh;
        this.battery = battery;
        this.charging = charging;
    }*/

    @SerializedName("_id")
    private String user;

    @SerializedName("model")
    private String model;

    @SerializedName("charging")
    private Boolean charging;

    @SerializedName("chargedkWh")
    private String chargedkWh;

    @SerializedName("battery")
    private int battery;

    public String getUser() {
        return user;
    }
    public String getModel() {
        return model;
    }
    public Integer getBattery() {
        return battery;
    }
    public Boolean getCharging() {
        return charging;
    }
    public String getChargedkWh() {
        return chargedkWh;
    }
}

