package com.student.fahrtenbuchapp.logic;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.student.fahrtenbuchapp.R;
import com.student.fahrtenbuchapp.login.LoginActivity;
import com.student.fahrtenbuchapp.models.Status;
import com.student.fahrtenbuchapp.models.Token;

import io.realm.Realm;
import io.realm.RealmResults;
import static com.student.fahrtenbuchapp.dataSync.RestClient.realm;

public class LoadingActivity extends AppCompatActivity {
    public TextView charged, user, model;
    public ProgressBar statusbar;
    private boolean ifStopIsPressed = false;
    private boolean ifSartIsPressed = false;
    private boolean ifStartAddress;
    private boolean ifStopAddress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loaded);
        // Textview deklarieren
        user = (TextView) findViewById(R.id.userField);
        model = (TextView) findViewById(R.id.carField);
        charged = (TextView) findViewById(R.id.kWhfield);
        statusbar = (ProgressBar) findViewById(R.id.progressBar);

        // Datenbankzugriff
        realm = Realm.getDefaultInstance();
     //   final Token myToken = realm.where(Token.class).findFirst();
        final RealmResults<Status> carstatus = realm.where(Status.class).findAll();
        // Netzwerkverbindung
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (wifi.isConnected()) {

            if (!carstatus.isEmpty()) {
// Test zum hochladen.....
                for (int i = 0; i < carstatus.size(); i++) {
                    if (carstatus.get(i).getUser() != null
                            && carstatus.get(i).getModel() != null
                            && carstatus.get(i).getChargedkWh() != null
                            && carstatus.get(i).getBattery() != 0
                            && carstatus.get(i).getCharging()){
                          String userID = carstatus.get(i).getUser();
                          String carModel = carstatus.get(i).getModel();
                        int battery = carstatus.get(i).getBattery();
                        String chargedkWh = carstatus.get(i).getChargedkWh();
                        boolean charging = carstatus.get(i).getCharging();

                        // Textview füllen
                        user.setText(userID);
                        charged.setText(chargedkWh);
                        statusbar.setProgress(battery);
                        model.setText(carModel);

                       //final Status Status = new Status(userID, carModel, chargedkWh, battery, charging);
                    }else{
                        System.exit(0);
                    }
                }
            }else{
                System.exit(0);
            }
        }else{
            System.exit(0);

        }
    }


// Menü oben rechts
// TODO LoadingActivity einbinden - fehlerhaft
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        for (int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            SpannableString spanString = new SpannableString(menu.getItem(i).getTitle().toString());
            int end = spanString.length();
            spanString.setSpan(new RelativeSizeSpan(1.5f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            item.setTitle(spanString);
        }
        return true;
    }

    private void logout() {
        finish();
        startActivity(new Intent(LoadingActivity.this, LoginActivity.class));
    }
    private void status(){
        finish();
        startActivity(new Intent(LoadingActivity.this, LoadingActivity.class));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.carstatus:
                    status();

            case R.id.logout:

                if(ifStopIsPressed == false && ifSartIsPressed == false)
                {
                    logout();
                }
                else
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoadingActivity.this);
                    builder
                            .setTitle("Bitte zuerst die Fahrt beenden!")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }

                return true;

            case R.id.exit:

                if(ifStopIsPressed == false && ifSartIsPressed == false)
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoadingActivity.this);
                    builder.setTitle("Beenden?").setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finishAffinity();
                            System.exit(0);
                        }
                    }).setNegativeButton("Nein", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.cancel();
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                else
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoadingActivity.this);
                    builder
                            .setTitle("Bitte zuerst die Fahrt beenden!")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }



// Zurückbutton
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        AlertDialog.Builder builder = new AlertDialog.Builder(LoadingActivity.this);
        builder.setTitle("Zurück?").setPositiveButton("Ja", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finishAffinity();
                overridePendingTransition(R.anim.zoom_out, 0);
                System.exit(0);
            }
        }).setNegativeButton("Nein", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}

// User - Token hinzufügen.