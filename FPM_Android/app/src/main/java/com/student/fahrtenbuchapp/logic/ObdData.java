package com.student.fahrtenbuchapp.logic;

import java.io.Serializable;

/**
 * Created by user on 17.08.2017.
 */

public class ObdData implements Serializable{
    public String distanceInKm;
    public String stateOfCharge;
    public ObdData(String distanceInKm, String stateOfCharge){
        this.distanceInKm = distanceInKm;
        this.stateOfCharge = stateOfCharge;
    }
}
