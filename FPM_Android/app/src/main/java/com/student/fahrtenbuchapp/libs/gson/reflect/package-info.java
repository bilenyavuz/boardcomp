/**
 * This package provides utility classes for finding type information for generic types.
 *  
 * @author Inderjeet Singh, Joel Leitch
 */
package com.student.fahrtenbuchapp.libs.gson.reflect;