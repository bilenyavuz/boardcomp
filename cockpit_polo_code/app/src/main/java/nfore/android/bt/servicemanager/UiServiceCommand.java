package nfore.android.bt.servicemanager;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface UiServiceCommand extends IInterface {

    public static abstract class Stub extends Binder implements UiServiceCommand {
        private static final String DESCRIPTOR = "nfore.android.bt.servicemanager.UiServiceCommand";
        static final int TRANSACTION_avrcpPlayOrPause = 211;
        static final int TRANSACTION_enterBluetoothTestMode = 239;
        static final int TRANSACTION_getA2dpConnectedDeviceAddress = 107;
        static final int TRANSACTION_getA2dpCurrentState = 108;
        static final int TRANSACTION_getAppleDeviceAuthState = 184;
        static final int TRANSACTION_getAutoConnectionStatus = 201;
        static final int TRANSACTION_getAvrcpConnectedDeviceAddress = 112;
        static final int TRANSACTION_getAvrcpCurrentState = 109;
        static final int TRANSACTION_getBluetoothCurrentState = 18;
        static final int TRANSACTION_getBluetoothLocalAdapterAddress = 15;
        static final int TRANSACTION_getBluetoothLocalAdapterName = 13;
        static final int TRANSACTION_getBluetoothRemoteDeviceName = 14;
        static final int TRANSACTION_getCalibrationClick = 236;
        static final int TRANSACTION_getCalibrationState = 235;
        static final int TRANSACTION_getCallTime = 205;
        static final int TRANSACTION_getCallerId = 206;
        static final int TRANSACTION_getConnectedHfpDeviceCount = 174;
        static final int TRANSACTION_getCurrentCallNumber = 202;
        static final int TRANSACTION_getCurrentImei = 237;
        static final int TRANSACTION_getCurrentSource = 212;
        static final int TRANSACTION_getCurrentSyncState = 209;
        static final int TRANSACTION_getCurrentSyncType = 208;
        static final int TRANSACTION_getDeviceX = 220;
        static final int TRANSACTION_getDeviceY = 221;
        static final int TRANSACTION_getHCaliX1 = 226;
        static final int TRANSACTION_getHCaliX2 = 227;
        static final int TRANSACTION_getHCaliY1 = 228;
        static final int TRANSACTION_getHCaliY2 = 229;
        static final int TRANSACTION_getHXPhoneLen = 222;
        static final int TRANSACTION_getHYPhoneLen = 223;
        static final int TRANSACTION_getHfpCallCounts = 200;
        static final int TRANSACTION_getHfpCallState = 199;
        static final int TRANSACTION_getHfpConnectedDeviceAddress = 32;
        static final int TRANSACTION_getHfpConnectedDeviceVendorID = 185;
        static final int TRANSACTION_getHfpCurrentScoState = 34;
        static final int TRANSACTION_getHfpCurrentState = 33;
        static final int TRANSACTION_getHfpMicSelection = 197;
        static final int TRANSACTION_getHfpRemoteDeviceBatteryStatus = 53;
        static final int TRANSACTION_getHfpRemoteDeviceName = 47;
        static final int TRANSACTION_getHfpRemoteDeviceSignalStatus = 54;
        static final int TRANSACTION_getHidCurrentState = 169;
        static final int TRANSACTION_getHspConnectedDeviceAddress = 64;
        static final int TRANSACTION_getHspCurrentScoState = 66;
        static final int TRANSACTION_getHspCurrentState = 65;
        static final int TRANSACTION_getImportVCardState = 210;
        static final int TRANSACTION_getLastCallNumber = 203;
        static final int TRANSACTION_getMapCurrentState = 101;
        static final int TRANSACTION_getMapRegisterState = 102;
        static final int TRANSACTION_getPCaliX1 = 230;
        static final int TRANSACTION_getPCaliX2 = 231;
        static final int TRANSACTION_getPCaliY1 = 232;
        static final int TRANSACTION_getPCaliY2 = 233;
        static final int TRANSACTION_getPXPhoneLen = 224;
        static final int TRANSACTION_getPYPhoneLen = 225;
        static final int TRANSACTION_getPanConnectedDeviceAddress = 161;
        static final int TRANSACTION_getPanCurrentState = 162;
        static final int TRANSACTION_getPbapCurrentState = 89;
        static final int TRANSACTION_getPhoneResolution = 219;
        static final int TRANSACTION_getPhoneScreenState = 217;
        static final int TRANSACTION_getPhoneScreenType = 218;
        static final int TRANSACTION_getPinCode = 193;
        static final int TRANSACTION_getSoftwareVersion = 194;
        static final int TRANSACTION_getSppCurrentState = 178;
        static final int TRANSACTION_getTargetAddress = 171;
        static final int TRANSACTION_getThreeWayCallNumber = 204;
        static final int TRANSACTION_getThreeWayCallTime = 207;
        static final int TRANSACTION_isA2dpNativeRunning = 155;
        static final int TRANSACTION_isA2dpPlaying = 215;
        static final int TRANSACTION_isAlreadyReadPairedDevices = 187;
        static final int TRANSACTION_isAppleDeviceFinishAuth = 183;
        static final int TRANSACTION_isAvrcp13Supported = 113;
        static final int TRANSACTION_isAvrcp14Supported = 114;
        static final int TRANSACTION_isAvrcpBrowsingChannelEstablished = 151;
        static final int TRANSACTION_isBluetoothAutoReconnectEnable = 23;
        static final int TRANSACTION_isBluetoothDiscoverable = 20;
        static final int TRANSACTION_isBluetoothEnable = 17;
        static final int TRANSACTION_isBluetoothScanning = 19;
        static final int TRANSACTION_isCalibration = 234;
        static final int TRANSACTION_isCallingFlag = 242;
        static final int TRANSACTION_isFirstBootUp = 177;
        static final int TRANSACTION_isForiPodDisconnectA2DP = 188;
        static final int TRANSACTION_isHfpAutoAnswer = 186;
        static final int TRANSACTION_isHfpConnectedAppleDevice = 182;
        static final int TRANSACTION_isHfpRemoteDeviceOnRoaming = 52;
        static final int TRANSACTION_isHfpRemoteDeviceTelecomServiceOn = 51;
        static final int TRANSACTION_isHfpVoiceControlOn = 35;
        static final int TRANSACTION_isMapNotificationRegistered = 96;
        static final int TRANSACTION_isMicMute = 243;
        static final int TRANSACTION_isPhonebookAutoSync = 189;
        static final int TRANSACTION_isSppConnected = 78;
        static final int TRANSACTION_isTwinConnectEnable = 190;
        static final int TRANSACTION_registerA2dpAvrcpCallback = 103;
        static final int TRANSACTION_registerHfpCallback = 28;
        static final int TRANSACTION_registerHidCallback = 163;
        static final int TRANSACTION_registerHspCallback = 60;
        static final int TRANSACTION_registerMapCallback = 90;
        static final int TRANSACTION_registerNforeServiceCallback = 1;
        static final int TRANSACTION_registerPanCallback = 157;
        static final int TRANSACTION_registerPbapCallback = 80;
        static final int TRANSACTION_registerSppCallback = 73;
        static final int TRANSACTION_reqA2dpConnect = 105;
        static final int TRANSACTION_reqA2dpDisconnect = 106;
        static final int TRANSACTION_reqAvrcpAbortMetadataReceiving = 135;
        static final int TRANSACTION_reqAvrcpAddToNowPlaying = 150;
        static final int TRANSACTION_reqAvrcpBackward = 119;
        static final int TRANSACTION_reqAvrcpChangePath = 146;
        static final int TRANSACTION_reqAvrcpForward = 118;
        static final int TRANSACTION_reqAvrcpGetCapabilitiesSupportEvent = 126;
        static final int TRANSACTION_reqAvrcpGetElementAttributesPlaying = 131;
        static final int TRANSACTION_reqAvrcpGetFolderItems = 145;
        static final int TRANSACTION_reqAvrcpGetItemAttributes = 147;
        static final int TRANSACTION_reqAvrcpGetPlayStatus = 132;
        static final int TRANSACTION_reqAvrcpGetPlayerSettingAttributeText = 139;
        static final int TRANSACTION_reqAvrcpGetPlayerSettingAttributesList = 127;
        static final int TRANSACTION_reqAvrcpGetPlayerSettingCurrentValues = 129;
        static final int TRANSACTION_reqAvrcpGetPlayerSettingValueText = 140;
        static final int TRANSACTION_reqAvrcpGetPlayerSettingValuesList = 128;
        static final int TRANSACTION_reqAvrcpInformBatteryStatusOfCt = 142;
        static final int TRANSACTION_reqAvrcpInformDisplayableCharset = 141;
        static final int TRANSACTION_reqAvrcpNextGroup = 136;
        static final int TRANSACTION_reqAvrcpPause = 117;
        static final int TRANSACTION_reqAvrcpPlay = 115;
        static final int TRANSACTION_reqAvrcpPlayPause = 175;
        static final int TRANSACTION_reqAvrcpPlaySelectedItem = 148;
        static final int TRANSACTION_reqAvrcpPreviousGroup = 137;
        static final int TRANSACTION_reqAvrcpRegisterEventWatcher = 133;
        static final int TRANSACTION_reqAvrcpSearch = 149;
        static final int TRANSACTION_reqAvrcpSetAbsoluteVolume = 152;
        static final int TRANSACTION_reqAvrcpSetAddressedPlayer = 143;
        static final int TRANSACTION_reqAvrcpSetBrowsedPlayer = 144;
        static final int TRANSACTION_reqAvrcpSetMetadataCompanyId = 138;
        static final int TRANSACTION_reqAvrcpSetPlayerSettingValue = 130;
        static final int TRANSACTION_reqAvrcpStartFastForward = 122;
        static final int TRANSACTION_reqAvrcpStartRewind = 124;
        static final int TRANSACTION_reqAvrcpStop = 116;
        static final int TRANSACTION_reqAvrcpStopFastForward = 123;
        static final int TRANSACTION_reqAvrcpStopRewind = 125;
        static final int TRANSACTION_reqAvrcpUnregisterEventWatcher = 134;
        static final int TRANSACTION_reqAvrcpUpdateSongStatus = 156;
        static final int TRANSACTION_reqAvrcpVolumeDown = 121;
        static final int TRANSACTION_reqAvrcpVolumeUp = 120;
        static final int TRANSACTION_reqBluetoothConnectAll = 24;
        static final int TRANSACTION_reqBluetoothDisconnectAll = 25;
        static final int TRANSACTION_reqBluetoothFoundDevices = 172;
        static final int TRANSACTION_reqBluetoothPair = 7;
        static final int TRANSACTION_reqBluetoothPairedDevices = 11;
        static final int TRANSACTION_reqBluetoothPairedDevicesCount = 173;
        static final int TRANSACTION_reqBluetoothPairedDevicesExtend = 12;
        static final int TRANSACTION_reqBluetoothRemoteUuids = 27;
        static final int TRANSACTION_reqBluetoothScanning = 6;
        static final int TRANSACTION_reqBluetoothUnpair = 8;
        static final int TRANSACTION_reqBluetoothUnpairAll = 9;
        static final int TRANSACTION_reqBluetoothUnpairAllNextTime = 10;
        static final int TRANSACTION_reqCancelPbapDownload = 83;
        static final int TRANSACTION_reqDisconnectAll = 26;
        static final int TRANSACTION_reqHfpAnswerCall = 39;
        static final int TRANSACTION_reqHfpAtDownloadPhonebook = 56;
        static final int TRANSACTION_reqHfpAtDownloadSMS = 57;
        static final int TRANSACTION_reqHfpAudioTransfer = 44;
        static final int TRANSACTION_reqHfpConnect = 30;
        static final int TRANSACTION_reqHfpDialCall = 36;
        static final int TRANSACTION_reqHfpDisconnect = 31;
        static final int TRANSACTION_reqHfpMemoryDial = 38;
        static final int TRANSACTION_reqHfpMicMute = 198;
        static final int TRANSACTION_reqHfpMicSelection = 196;
        static final int TRANSACTION_reqHfpMultiCallControl = 42;
        static final int TRANSACTION_reqHfpReDial = 37;
        static final int TRANSACTION_reqHfpRejectIncomingCall = 40;
        static final int TRANSACTION_reqHfpRemoteDeviceInfo = 46;
        static final int TRANSACTION_reqHfpRemoteDeviceNetworkOperator = 49;
        static final int TRANSACTION_reqHfpRemoteDevicePhoneNumber = 48;
        static final int TRANSACTION_reqHfpRemoteDeviceSubscriberNumber = 50;
        static final int TRANSACTION_reqHfpRemoteDeviceVolumeSync = 58;
        static final int TRANSACTION_reqHfpSendDtmf = 43;
        static final int TRANSACTION_reqHfpTerminateOngoingCall = 41;
        static final int TRANSACTION_reqHfpVoiceDial = 59;
        static final int TRANSACTION_reqHidConnect = 165;
        static final int TRANSACTION_reqHidDisconnect = 166;
        static final int TRANSACTION_reqHspAnswerCall = 68;
        static final int TRANSACTION_reqHspAudioTransfer = 71;
        static final int TRANSACTION_reqHspConnect = 62;
        static final int TRANSACTION_reqHspDisconnect = 63;
        static final int TRANSACTION_reqHspReDial = 67;
        static final int TRANSACTION_reqHspRejectIncomingCall = 69;
        static final int TRANSACTION_reqHspRemoteDeviceVolumeSync = 72;
        static final int TRANSACTION_reqHspTerminateOngoingCall = 70;
        static final int TRANSACTION_reqMapCleanDatabase = 100;
        static final int TRANSACTION_reqMapDatabaseAvailable = 98;
        static final int TRANSACTION_reqMapDeleteDatabaseByAddress = 99;
        static final int TRANSACTION_reqMapDownloadAllMessages = 92;
        static final int TRANSACTION_reqMapDownloadInterrupt = 97;
        static final int TRANSACTION_reqMapDownloadSingleMessage = 93;
        static final int TRANSACTION_reqMapRegisterNotification = 94;
        static final int TRANSACTION_reqMapUnregisterNotification = 95;
        static final int TRANSACTION_reqPanConnect = 159;
        static final int TRANSACTION_reqPanDisconnect = 160;
        static final int TRANSACTION_reqPbapCleanDatabase = 87;
        static final int TRANSACTION_reqPbapDatabaseAvailable = 84;
        static final int TRANSACTION_reqPbapDeleteDatabaseByAddress = 86;
        static final int TRANSACTION_reqPbapDownloadAllVcards = 82;
        static final int TRANSACTION_reqPbapDownloadInterrupt = 88;
        static final int TRANSACTION_reqPbapQueryName = 85;
        static final int TRANSACTION_reqSoftwareUpdate = 176;
        static final int TRANSACTION_reqSppConnect = 75;
        static final int TRANSACTION_reqSppConnectedDeviceAddressList = 77;
        static final int TRANSACTION_reqSppDisconnect = 76;
        static final int TRANSACTION_reqSppSendData = 79;
        static final int TRANSACTION_requestAudioFocus = 213;
        static final int TRANSACTION_requestBTAudioChannel = 214;
        static final int TRANSACTION_sendHidMouseCommand = 167;
        static final int TRANSACTION_sendHidVirtualKeyCommand = 168;
        static final int TRANSACTION_sendSppMessage = 216;
        static final int TRANSACTION_setA2dpLocalMute = 111;
        static final int TRANSACTION_setA2dpLocalVolume = 110;
        static final int TRANSACTION_setA2dpNativeStart = 153;
        static final int TRANSACTION_setA2dpNativeStop = 154;
        static final int TRANSACTION_setActivePhone = 180;
        static final int TRANSACTION_setBluetoothAutoReconnect = 21;
        static final int TRANSACTION_setBluetoothAutoReconnectForceOor = 22;
        static final int TRANSACTION_setBluetoothDiscoverableDuration = 4;
        static final int TRANSACTION_setBluetoothDiscoverablePermanent = 5;
        static final int TRANSACTION_setBluetoothEnable = 3;
        static final int TRANSACTION_setBluetoothLocalAdapterName = 16;
        static final int TRANSACTION_setCalibration = 238;
        static final int TRANSACTION_setHfpAtNewSmsNotify = 55;
        static final int TRANSACTION_setHfpAutoAnswer = 179;
        static final int TRANSACTION_setHfpLocalRingEnable = 45;
        static final int TRANSACTION_setOutputFrequencyChannel = 240;
        static final int TRANSACTION_setOutputFrequencyChannelExtended = 241;
        static final int TRANSACTION_setPhonebookAutoSyncEnable = 195;
        static final int TRANSACTION_setPinCode = 192;
        static final int TRANSACTION_setTargetAddress = 170;
        static final int TRANSACTION_setTouchesOptions = 181;
        static final int TRANSACTION_setTwinConnectEnable = 191;
        static final int TRANSACTION_unregisterA2dpAvrcpCallback = 104;
        static final int TRANSACTION_unregisterHfpCallback = 29;
        static final int TRANSACTION_unregisterHidCallback = 164;
        static final int TRANSACTION_unregisterHspCallback = 61;
        static final int TRANSACTION_unregisterMapCallback = 91;
        static final int TRANSACTION_unregisterNforeServiceCallback = 2;
        static final int TRANSACTION_unregisterPanCallback = 158;
        static final int TRANSACTION_unregisterPbapCallback = 81;
        static final int TRANSACTION_unregisterSppCallback = 74;

        private static class Proxy implements UiServiceCommand {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            public boolean registerNforeServiceCallback(UiServiceCallback cb) throws RemoteException {
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean unregisterNforeServiceCallback(UiServiceCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setBluetoothEnable(boolean isEnable) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (isEnable) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(3, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setBluetoothDiscoverableDuration(int duration) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(duration);
                    this.mRemote.transact(4, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setBluetoothDiscoverablePermanent(boolean isEnable) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (isEnable) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(5, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqBluetoothScanning(boolean isEnable) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (isEnable) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(6, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqBluetoothPair(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(7, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqBluetoothUnpair(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(8, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqBluetoothUnpairAll() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(9, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqBluetoothUnpairAllNextTime() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(10, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqBluetoothPairedDevices() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(11, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqBluetoothPairedDevicesExtend() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(12, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getBluetoothLocalAdapterName() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(13, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getBluetoothRemoteDeviceName(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(14, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getBluetoothLocalAdapterAddress() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(15, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setBluetoothLocalAdapterName(String name) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(name);
                    this.mRemote.transact(16, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isBluetoothEnable() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(17, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getBluetoothCurrentState() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(18, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isBluetoothScanning() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(19, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isBluetoothDiscoverable() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(20, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setBluetoothAutoReconnect(boolean isEnable) throws RemoteException {
                int i = 0;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (isEnable) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(21, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setBluetoothAutoReconnectForceOor(boolean isEnable) throws RemoteException {
                int i = 0;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (isEnable) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(22, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isBluetoothAutoReconnectEnable() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(23, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int reqBluetoothConnectAll(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(24, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int reqBluetoothDisconnectAll() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(25, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int reqDisconnectAll(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(26, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int reqBluetoothRemoteUuids(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(27, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean registerHfpCallback(UiHfpCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(28, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean unregisterHfpCallback(UiHfpCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(29, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpConnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(30, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpDisconnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(31, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getHfpConnectedDeviceAddress() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(32, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getHfpCurrentState(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(33, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getHfpCurrentScoState(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(34, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isHfpVoiceControlOn(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(35, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpDialCall(String address, String callNumber) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(callNumber);
                    this.mRemote.transact(36, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpReDial(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(37, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpMemoryDial(String address, String memoryLocation) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(memoryLocation);
                    this.mRemote.transact(38, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpAnswerCall(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(39, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpRejectIncomingCall(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(40, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpTerminateOngoingCall(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(41, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpMultiCallControl(String address, byte opCode) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(opCode);
                    this.mRemote.transact(42, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpSendDtmf(String address, String dtmfNumber) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(dtmfNumber);
                    this.mRemote.transact(43, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpAudioTransfer(String address, byte opCode) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(opCode);
                    this.mRemote.transact(44, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setHfpLocalRingEnable(boolean isEnable) throws RemoteException {
                int i = 0;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (isEnable) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(45, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void reqHfpRemoteDeviceInfo(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(46, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getHfpRemoteDeviceName(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(47, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpRemoteDevicePhoneNumber(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(48, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpRemoteDeviceNetworkOperator(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(49, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpRemoteDeviceSubscriberNumber(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(50, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isHfpRemoteDeviceTelecomServiceOn(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(51, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isHfpRemoteDeviceOnRoaming(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(52, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getHfpRemoteDeviceBatteryStatus(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(53, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getHfpRemoteDeviceSignalStatus(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(54, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setHfpAtNewSmsNotify(String address, boolean isEnable) throws RemoteException {
                int i = 0;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    if (isEnable) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(55, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpAtDownloadPhonebook(String address, int storage) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(storage);
                    this.mRemote.transact(56, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpAtDownloadSMS(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(57, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpRemoteDeviceVolumeSync(String address, byte attributeId, int volume) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(attributeId);
                    _data.writeInt(volume);
                    this.mRemote.transact(58, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpVoiceDial(String address, boolean isEnable) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    if (isEnable) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(59, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean registerHspCallback(UiHspCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(60, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean unregisterHspCallback(UiHspCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(61, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHspConnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(62, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHspDisconnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(63, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getHspConnectedDeviceAddress() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(64, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getHspCurrentState(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(65, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getHspCurrentScoState(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(66, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHspReDial(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(67, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHspAnswerCall(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(68, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHspRejectIncomingCall(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(69, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHspTerminateOngoingCall(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(70, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHspAudioTransfer(String address, byte opCode) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(opCode);
                    this.mRemote.transact(71, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHspRemoteDeviceVolumeSync(String address, byte attributeId, int volume) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(attributeId);
                    _data.writeInt(volume);
                    this.mRemote.transact(72, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean registerSppCallback(UiSppCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(73, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean unregisterSppCallback(UiSppCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(74, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqSppConnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(75, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqSppDisconnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(76, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void reqSppConnectedDeviceAddressList() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_reqSppConnectedDeviceAddressList, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isSppConnected(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(Stub.TRANSACTION_isSppConnected, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void reqSppSendData(String address, byte[] sppData) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByteArray(sppData);
                    this.mRemote.transact(Stub.TRANSACTION_reqSppSendData, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean registerPbapCallback(UiPbapCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(80, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean unregisterPbapCallback(UiPbapCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(81, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqPbapDownloadAllVcards(String address, int storage, int notifyFreq, boolean isUseContactsProvider) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(storage);
                    _data.writeInt(notifyFreq);
                    if (isUseContactsProvider) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(Stub.TRANSACTION_reqPbapDownloadAllVcards, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqCancelPbapDownload(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(Stub.TRANSACTION_reqCancelPbapDownload, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void reqPbapDatabaseAvailable(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(Stub.TRANSACTION_reqPbapDatabaseAvailable, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void reqPbapQueryName(String address, String phoneNumber) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(phoneNumber);
                    this.mRemote.transact(Stub.TRANSACTION_reqPbapQueryName, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void reqPbapDeleteDatabaseByAddress(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(Stub.TRANSACTION_reqPbapDeleteDatabaseByAddress, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void reqPbapCleanDatabase() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_reqPbapCleanDatabase, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqPbapDownloadInterrupt(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(Stub.TRANSACTION_reqPbapDownloadInterrupt, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getPbapCurrentState(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(Stub.TRANSACTION_getPbapCurrentState, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean registerMapCallback(UiMapCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(90, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean unregisterMapCallback(UiMapCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(91, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqMapDownloadAllMessages(String address, int folder, int notifyFreq, boolean isContentDownload) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(folder);
                    _data.writeInt(notifyFreq);
                    if (isContentDownload) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(92, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqMapDownloadSingleMessage(String address, int folder, String handle) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(folder);
                    _data.writeString(handle);
                    this.mRemote.transact(Stub.TRANSACTION_reqMapDownloadSingleMessage, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqMapRegisterNotification(String address, boolean downloadNewMessage) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    if (downloadNewMessage) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(Stub.TRANSACTION_reqMapRegisterNotification, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void reqMapUnregisterNotification(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(95, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isMapNotificationRegistered(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(96, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqMapDownloadInterrupt(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(97, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void reqMapDatabaseAvailable(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(98, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void reqMapDeleteDatabaseByAddress(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(99, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void reqMapCleanDatabase() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(100, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getMapCurrentState(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(101, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getMapRegisterState(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(102, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean registerA2dpAvrcpCallback(UiA2dpCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(103, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean unregisterA2dpAvrcpCallback(UiA2dpCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(104, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqA2dpConnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(105, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqA2dpDisconnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(106, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getA2dpConnectedDeviceAddress() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(107, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getA2dpCurrentState() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(108, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getAvrcpCurrentState() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(109, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setA2dpLocalVolume(int vol) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(vol);
                    this.mRemote.transact(110, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setA2dpLocalMute(boolean isMute) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (isMute) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(111, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getAvrcpConnectedDeviceAddress() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(112, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isAvrcp13Supported(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(113, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isAvrcp14Supported(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(114, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpPlay(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(115, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpStop(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(116, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpPause(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(117, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpForward(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(118, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpBackward(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(119, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpVolumeUp(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(120, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpVolumeDown(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(121, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpStartFastForward(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(122, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpStopFastForward(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(123, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpStartRewind(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(124, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpStopRewind(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(125, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpGetCapabilitiesSupportEvent(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(126, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpGetPlayerSettingAttributesList(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(127, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpGetPlayerSettingValuesList(String address, byte attributeId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(attributeId);
                    this.mRemote.transact(128, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpGetPlayerSettingCurrentValues(String address, byte attributeId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(attributeId);
                    this.mRemote.transact(129, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpSetPlayerSettingValue(String address, byte attributeId, byte valueId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(attributeId);
                    _data.writeByte(valueId);
                    this.mRemote.transact(130, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpGetElementAttributesPlaying(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(131, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpGetPlayStatus(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(132, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpRegisterEventWatcher(String address, byte eventId, long interval) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(eventId);
                    _data.writeLong(interval);
                    this.mRemote.transact(133, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpUnregisterEventWatcher(String address, byte eventId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(eventId);
                    this.mRemote.transact(134, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpAbortMetadataReceiving(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(135, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpNextGroup(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(136, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpPreviousGroup(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(137, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpSetMetadataCompanyId(String address, int companyId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(companyId);
                    this.mRemote.transact(138, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpGetPlayerSettingAttributeText(String address, byte attributeId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(attributeId);
                    this.mRemote.transact(139, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpGetPlayerSettingValueText(String address, byte attributeId, byte valueId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(attributeId);
                    _data.writeByte(valueId);
                    this.mRemote.transact(140, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpInformDisplayableCharset(String address, int[] charsetId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeIntArray(charsetId);
                    this.mRemote.transact(141, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpInformBatteryStatusOfCt(String address, byte batteryStatusId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(batteryStatusId);
                    this.mRemote.transact(142, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpSetAddressedPlayer(String address, int playerId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(playerId);
                    this.mRemote.transact(143, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpSetBrowsedPlayer(String address, int playerId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(playerId);
                    this.mRemote.transact(144, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpGetFolderItems(String address, byte scopeId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(scopeId);
                    this.mRemote.transact(145, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpChangePath(String address, int uidCounter, long uid, byte direction) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(uidCounter);
                    _data.writeLong(uid);
                    _data.writeByte(direction);
                    this.mRemote.transact(146, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpGetItemAttributes(String address, byte scopeId, int uidCounter, long uid) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(scopeId);
                    _data.writeInt(uidCounter);
                    _data.writeLong(uid);
                    this.mRemote.transact(147, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpPlaySelectedItem(String address, byte scopeId, int uidCounter, long uid) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(scopeId);
                    _data.writeInt(uidCounter);
                    _data.writeLong(uid);
                    this.mRemote.transact(148, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpSearch(String address, String text) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(text);
                    this.mRemote.transact(149, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpAddToNowPlaying(String address, byte scopeId, int uidCounter, long uid) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(scopeId);
                    _data.writeInt(uidCounter);
                    _data.writeLong(uid);
                    this.mRemote.transact(150, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isAvrcpBrowsingChannelEstablished(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(151, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpSetAbsoluteVolume(String address, byte volume) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(volume);
                    this.mRemote.transact(Stub.TRANSACTION_reqAvrcpSetAbsoluteVolume, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setA2dpNativeStart() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(153, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setA2dpNativeStop() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(154, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isA2dpNativeRunning() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(155, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void reqAvrcpUpdateSongStatus() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_reqAvrcpUpdateSongStatus, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean registerPanCallback(UiPanCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(157, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean unregisterPanCallback(UiPanCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(158, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqPanConnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(159, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqPanDisconnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(160, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getPanConnectedDeviceAddress() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(161, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getPanCurrentState() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(162, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean registerHidCallback(UiHidCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(163, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean unregisterHidCallback(UiHidCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(164, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHidConnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(165, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHidDisconnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(166, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int sendHidMouseCommand(int button, int offset_x, int offset_y, int wheel) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(button);
                    _data.writeInt(offset_x);
                    _data.writeInt(offset_y);
                    _data.writeInt(wheel);
                    this.mRemote.transact(Stub.TRANSACTION_sendHidMouseCommand, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int sendHidVirtualKeyCommand(int key_1, int key_2) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(key_1);
                    _data.writeInt(key_2);
                    this.mRemote.transact(Stub.TRANSACTION_sendHidVirtualKeyCommand, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getHidCurrentState() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getHidCurrentState, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setTargetAddress(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(170, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getTargetAddress() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getTargetAddress, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqBluetoothFoundDevices() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(172, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int reqBluetoothPairedDevicesCount() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_reqBluetoothPairedDevicesCount, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getConnectedHfpDeviceCount() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getConnectedHfpDeviceCount, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpPlayPause() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_reqAvrcpPlayPause, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqSoftwareUpdate() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_reqSoftwareUpdate, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isFirstBootUp() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_isFirstBootUp, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getSppCurrentState(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(Stub.TRANSACTION_getSppCurrentState, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setHfpAutoAnswer(boolean auto) throws RemoteException {
                int i = 0;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (auto) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(179, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setActivePhone(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(180, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setTouchesOptions(boolean enable) throws RemoteException {
                int i = 0;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (enable) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(Stub.TRANSACTION_setTouchesOptions, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isHfpConnectedAppleDevice() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(182, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isAppleDeviceFinishAuth() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(183, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getAppleDeviceAuthState() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getAppleDeviceAuthState, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getHfpConnectedDeviceVendorID() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(185, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isHfpAutoAnswer() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_isHfpAutoAnswer, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isAlreadyReadPairedDevices() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_isAlreadyReadPairedDevices, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isForiPodDisconnectA2DP() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_isForiPodDisconnectA2DP, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isPhonebookAutoSync() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_isPhonebookAutoSync, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isTwinConnectEnable() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(190, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setTwinConnectEnable(boolean enable) throws RemoteException {
                int i = 0;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (enable) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(Stub.TRANSACTION_setTwinConnectEnable, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setPinCode(String pincode) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(pincode);
                    this.mRemote.transact(Stub.TRANSACTION_setPinCode, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getPinCode() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getPinCode, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getSoftwareVersion() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getSoftwareVersion, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setPhonebookAutoSyncEnable(boolean enable) throws RemoteException {
                int i = 0;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (enable) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(195, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpMicSelection() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_reqHfpMicSelection, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getHfpMicSelection() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getHfpMicSelection, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpMicMute() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_reqHfpMicMute, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getHfpCallState() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getHfpCallState, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getHfpCallCounts() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(200, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getAutoConnectionStatus() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getAutoConnectionStatus, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getCurrentCallNumber() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getCurrentCallNumber, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getLastCallNumber() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getLastCallNumber, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getThreeWayCallNumber() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getThreeWayCallNumber, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getCallTime(String number) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(number);
                    this.mRemote.transact(Stub.TRANSACTION_getCallTime, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getCallerId(String number) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(number);
                    this.mRemote.transact(Stub.TRANSACTION_getCallerId, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getThreeWayCallTime() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getThreeWayCallTime, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getCurrentSyncType() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getCurrentSyncType, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getCurrentSyncState() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getCurrentSyncState, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean getImportVCardState() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(210, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean avrcpPlayOrPause() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_avrcpPlayOrPause, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getCurrentSource() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(212, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void requestAudioFocus() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_requestAudioFocus, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void requestBTAudioChannel() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(214, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isA2dpPlaying() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_isA2dpPlaying, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean sendSppMessage(String address, String message) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(message);
                    this.mRemote.transact(Stub.TRANSACTION_sendSppMessage, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getPhoneScreenState() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getPhoneScreenState, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getPhoneScreenType() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getPhoneScreenType, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getPhoneResolution() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getPhoneResolution, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getDeviceX() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(220, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getDeviceY() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getDeviceY, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getHXPhoneLen() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(222, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getHYPhoneLen() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getHYPhoneLen, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getPXPhoneLen() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(224, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getPYPhoneLen() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getPYPhoneLen, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getHCaliX1() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getHCaliX1, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getHCaliX2() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getHCaliX2, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getHCaliY1() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getHCaliY1, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getHCaliY2() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getHCaliY2, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getPCaliX1() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(230, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getPCaliX2() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getPCaliX2, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getPCaliY1() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getPCaliY1, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getPCaliY2() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getPCaliY2, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isCalibration() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_isCalibration, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getCalibrationState() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getCalibrationState, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getCalibrationClick() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getCalibrationClick, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getCurrentImei() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getCurrentImei, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setCalibration(boolean state) throws RemoteException {
                int i = 0;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (state) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(Stub.TRANSACTION_setCalibration, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean enterBluetoothTestMode() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_enterBluetoothTestMode, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setOutputFrequencyChannel(int mode, int channelRange) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(mode);
                    _data.writeInt(channelRange);
                    this.mRemote.transact(240, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setOutputFrequencyChannelExtended(int mode) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(mode);
                    this.mRemote.transact(Stub.TRANSACTION_setOutputFrequencyChannelExtended, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isCallingFlag() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(242, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isMicMute() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_isMicMute, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static UiServiceCommand asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
            if (iin == null || !(iin instanceof UiServiceCommand)) {
                return new Proxy(obj);
            }
            return (UiServiceCommand) iin;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result = registerNforeServiceCallback(nfore.android.bt.servicemanager.UiServiceCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result ? 1 : 0);
                    return true;
                case 2:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result2 = unregisterNforeServiceCallback(nfore.android.bt.servicemanager.UiServiceCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result2 ? 1 : 0);
                    return true;
                case 3:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result3 = setBluetoothEnable(data.readInt() != 0);
                    reply.writeNoException();
                    reply.writeInt(_result3 ? 1 : 0);
                    return true;
                case 4:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result4 = setBluetoothDiscoverableDuration(data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result4 ? 1 : 0);
                    return true;
                case 5:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result5 = setBluetoothDiscoverablePermanent(data.readInt() != 0);
                    reply.writeNoException();
                    reply.writeInt(_result5 ? 1 : 0);
                    return true;
                case 6:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result6 = reqBluetoothScanning(data.readInt() != 0);
                    reply.writeNoException();
                    reply.writeInt(_result6 ? 1 : 0);
                    return true;
                case 7:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result7 = reqBluetoothPair(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result7 ? 1 : 0);
                    return true;
                case 8:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result8 = reqBluetoothUnpair(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result8 ? 1 : 0);
                    return true;
                case 9:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result9 = reqBluetoothUnpairAll();
                    reply.writeNoException();
                    reply.writeInt(_result9 ? 1 : 0);
                    return true;
                case 10:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result10 = reqBluetoothUnpairAllNextTime();
                    reply.writeNoException();
                    reply.writeInt(_result10 ? 1 : 0);
                    return true;
                case 11:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result11 = reqBluetoothPairedDevices();
                    reply.writeNoException();
                    reply.writeInt(_result11 ? 1 : 0);
                    return true;
                case 12:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result12 = reqBluetoothPairedDevicesExtend();
                    reply.writeNoException();
                    reply.writeInt(_result12 ? 1 : 0);
                    return true;
                case 13:
                    data.enforceInterface(DESCRIPTOR);
                    String _result13 = getBluetoothLocalAdapterName();
                    reply.writeNoException();
                    reply.writeString(_result13);
                    return true;
                case 14:
                    data.enforceInterface(DESCRIPTOR);
                    String _result14 = getBluetoothRemoteDeviceName(data.readString());
                    reply.writeNoException();
                    reply.writeString(_result14);
                    return true;
                case 15:
                    data.enforceInterface(DESCRIPTOR);
                    String _result15 = getBluetoothLocalAdapterAddress();
                    reply.writeNoException();
                    reply.writeString(_result15);
                    return true;
                case 16:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result16 = setBluetoothLocalAdapterName(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result16 ? 1 : 0);
                    return true;
                case 17:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result17 = isBluetoothEnable();
                    reply.writeNoException();
                    reply.writeInt(_result17 ? 1 : 0);
                    return true;
                case 18:
                    data.enforceInterface(DESCRIPTOR);
                    int _result18 = getBluetoothCurrentState();
                    reply.writeNoException();
                    reply.writeInt(_result18);
                    return true;
                case 19:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result19 = isBluetoothScanning();
                    reply.writeNoException();
                    reply.writeInt(_result19 ? 1 : 0);
                    return true;
                case 20:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result20 = isBluetoothDiscoverable();
                    reply.writeNoException();
                    reply.writeInt(_result20 ? 1 : 0);
                    return true;
                case 21:
                    data.enforceInterface(DESCRIPTOR);
                    setBluetoothAutoReconnect(data.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case 22:
                    data.enforceInterface(DESCRIPTOR);
                    setBluetoothAutoReconnectForceOor(data.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case 23:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result21 = isBluetoothAutoReconnectEnable();
                    reply.writeNoException();
                    reply.writeInt(_result21 ? 1 : 0);
                    return true;
                case 24:
                    data.enforceInterface(DESCRIPTOR);
                    int _result22 = reqBluetoothConnectAll(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result22);
                    return true;
                case 25:
                    data.enforceInterface(DESCRIPTOR);
                    int _result23 = reqBluetoothDisconnectAll();
                    reply.writeNoException();
                    reply.writeInt(_result23);
                    return true;
                case 26:
                    data.enforceInterface(DESCRIPTOR);
                    int _result24 = reqDisconnectAll(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result24);
                    return true;
                case 27:
                    data.enforceInterface(DESCRIPTOR);
                    int _result25 = reqBluetoothRemoteUuids(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result25);
                    return true;
                case 28:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result26 = registerHfpCallback(nfore.android.bt.servicemanager.UiHfpCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result26 ? 1 : 0);
                    return true;
                case 29:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result27 = unregisterHfpCallback(nfore.android.bt.servicemanager.UiHfpCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result27 ? 1 : 0);
                    return true;
                case 30:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result28 = reqHfpConnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result28 ? 1 : 0);
                    return true;
                case 31:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result29 = reqHfpDisconnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result29 ? 1 : 0);
                    return true;
                case 32:
                    data.enforceInterface(DESCRIPTOR);
                    String _result30 = getHfpConnectedDeviceAddress();
                    reply.writeNoException();
                    reply.writeString(_result30);
                    return true;
                case 33:
                    data.enforceInterface(DESCRIPTOR);
                    int _result31 = getHfpCurrentState(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result31);
                    return true;
                case 34:
                    data.enforceInterface(DESCRIPTOR);
                    int _result32 = getHfpCurrentScoState(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result32);
                    return true;
                case 35:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result33 = isHfpVoiceControlOn(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result33 ? 1 : 0);
                    return true;
                case 36:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result34 = reqHfpDialCall(data.readString(), data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result34 ? 1 : 0);
                    return true;
                case 37:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result35 = reqHfpReDial(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result35 ? 1 : 0);
                    return true;
                case 38:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result36 = reqHfpMemoryDial(data.readString(), data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result36 ? 1 : 0);
                    return true;
                case 39:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result37 = reqHfpAnswerCall(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result37 ? 1 : 0);
                    return true;
                case 40:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result38 = reqHfpRejectIncomingCall(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result38 ? 1 : 0);
                    return true;
                case 41:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result39 = reqHfpTerminateOngoingCall(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result39 ? 1 : 0);
                    return true;
                case 42:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result40 = reqHfpMultiCallControl(data.readString(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result40 ? 1 : 0);
                    return true;
                case 43:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result41 = reqHfpSendDtmf(data.readString(), data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result41 ? 1 : 0);
                    return true;
                case 44:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result42 = reqHfpAudioTransfer(data.readString(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result42 ? 1 : 0);
                    return true;
                case 45:
                    data.enforceInterface(DESCRIPTOR);
                    setHfpLocalRingEnable(data.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case 46:
                    data.enforceInterface(DESCRIPTOR);
                    reqHfpRemoteDeviceInfo(data.readString());
                    reply.writeNoException();
                    return true;
                case 47:
                    data.enforceInterface(DESCRIPTOR);
                    String _result43 = getHfpRemoteDeviceName(data.readString());
                    reply.writeNoException();
                    reply.writeString(_result43);
                    return true;
                case 48:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result44 = reqHfpRemoteDevicePhoneNumber(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result44 ? 1 : 0);
                    return true;
                case 49:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result45 = reqHfpRemoteDeviceNetworkOperator(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result45 ? 1 : 0);
                    return true;
                case 50:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result46 = reqHfpRemoteDeviceSubscriberNumber(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result46 ? 1 : 0);
                    return true;
                case 51:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result47 = isHfpRemoteDeviceTelecomServiceOn(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result47 ? 1 : 0);
                    return true;
                case 52:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result48 = isHfpRemoteDeviceOnRoaming(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result48 ? 1 : 0);
                    return true;
                case 53:
                    data.enforceInterface(DESCRIPTOR);
                    int _result49 = getHfpRemoteDeviceBatteryStatus(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result49);
                    return true;
                case 54:
                    data.enforceInterface(DESCRIPTOR);
                    int _result50 = getHfpRemoteDeviceSignalStatus(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result50);
                    return true;
                case 55:
                    data.enforceInterface(DESCRIPTOR);
                    setHfpAtNewSmsNotify(data.readString(), data.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case 56:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result51 = reqHfpAtDownloadPhonebook(data.readString(), data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result51 ? 1 : 0);
                    return true;
                case 57:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result52 = reqHfpAtDownloadSMS(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result52 ? 1 : 0);
                    return true;
                case 58:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result53 = reqHfpRemoteDeviceVolumeSync(data.readString(), data.readByte(), data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result53 ? 1 : 0);
                    return true;
                case 59:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result54 = reqHfpVoiceDial(data.readString(), data.readInt() != 0);
                    reply.writeNoException();
                    reply.writeInt(_result54 ? 1 : 0);
                    return true;
                case 60:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result55 = registerHspCallback(nfore.android.bt.servicemanager.UiHspCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result55 ? 1 : 0);
                    return true;
                case 61:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result56 = unregisterHspCallback(nfore.android.bt.servicemanager.UiHspCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result56 ? 1 : 0);
                    return true;
                case 62:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result57 = reqHspConnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result57 ? 1 : 0);
                    return true;
                case 63:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result58 = reqHspDisconnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result58 ? 1 : 0);
                    return true;
                case 64:
                    data.enforceInterface(DESCRIPTOR);
                    String _result59 = getHspConnectedDeviceAddress();
                    reply.writeNoException();
                    reply.writeString(_result59);
                    return true;
                case 65:
                    data.enforceInterface(DESCRIPTOR);
                    int _result60 = getHspCurrentState(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result60);
                    return true;
                case 66:
                    data.enforceInterface(DESCRIPTOR);
                    int _result61 = getHspCurrentScoState(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result61);
                    return true;
                case 67:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result62 = reqHspReDial(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result62 ? 1 : 0);
                    return true;
                case 68:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result63 = reqHspAnswerCall(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result63 ? 1 : 0);
                    return true;
                case 69:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result64 = reqHspRejectIncomingCall(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result64 ? 1 : 0);
                    return true;
                case 70:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result65 = reqHspTerminateOngoingCall(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result65 ? 1 : 0);
                    return true;
                case 71:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result66 = reqHspAudioTransfer(data.readString(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result66 ? 1 : 0);
                    return true;
                case 72:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result67 = reqHspRemoteDeviceVolumeSync(data.readString(), data.readByte(), data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result67 ? 1 : 0);
                    return true;
                case 73:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result68 = registerSppCallback(nfore.android.bt.servicemanager.UiSppCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result68 ? 1 : 0);
                    return true;
                case 74:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result69 = unregisterSppCallback(nfore.android.bt.servicemanager.UiSppCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result69 ? 1 : 0);
                    return true;
                case 75:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result70 = reqSppConnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result70 ? 1 : 0);
                    return true;
                case 76:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result71 = reqSppDisconnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result71 ? 1 : 0);
                    return true;
                case TRANSACTION_reqSppConnectedDeviceAddressList /*77*/:
                    data.enforceInterface(DESCRIPTOR);
                    reqSppConnectedDeviceAddressList();
                    reply.writeNoException();
                    return true;
                case TRANSACTION_isSppConnected /*78*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result72 = isSppConnected(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result72 ? 1 : 0);
                    return true;
                case TRANSACTION_reqSppSendData /*79*/:
                    data.enforceInterface(DESCRIPTOR);
                    reqSppSendData(data.readString(), data.createByteArray());
                    reply.writeNoException();
                    return true;
                case 80:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result73 = registerPbapCallback(nfore.android.bt.servicemanager.UiPbapCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result73 ? 1 : 0);
                    return true;
                case 81:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result74 = unregisterPbapCallback(nfore.android.bt.servicemanager.UiPbapCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result74 ? 1 : 0);
                    return true;
                case TRANSACTION_reqPbapDownloadAllVcards /*82*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result75 = reqPbapDownloadAllVcards(data.readString(), data.readInt(), data.readInt(), data.readInt() != 0);
                    reply.writeNoException();
                    reply.writeInt(_result75 ? 1 : 0);
                    return true;
                case TRANSACTION_reqCancelPbapDownload /*83*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result76 = reqCancelPbapDownload(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result76 ? 1 : 0);
                    return true;
                case TRANSACTION_reqPbapDatabaseAvailable /*84*/:
                    data.enforceInterface(DESCRIPTOR);
                    reqPbapDatabaseAvailable(data.readString());
                    reply.writeNoException();
                    return true;
                case TRANSACTION_reqPbapQueryName /*85*/:
                    data.enforceInterface(DESCRIPTOR);
                    reqPbapQueryName(data.readString(), data.readString());
                    reply.writeNoException();
                    return true;
                case TRANSACTION_reqPbapDeleteDatabaseByAddress /*86*/:
                    data.enforceInterface(DESCRIPTOR);
                    reqPbapDeleteDatabaseByAddress(data.readString());
                    reply.writeNoException();
                    return true;
                case TRANSACTION_reqPbapCleanDatabase /*87*/:
                    data.enforceInterface(DESCRIPTOR);
                    reqPbapCleanDatabase();
                    reply.writeNoException();
                    return true;
                case TRANSACTION_reqPbapDownloadInterrupt /*88*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result77 = reqPbapDownloadInterrupt(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result77 ? 1 : 0);
                    return true;
                case TRANSACTION_getPbapCurrentState /*89*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result78 = getPbapCurrentState(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result78);
                    return true;
                case 90:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result79 = registerMapCallback(nfore.android.bt.servicemanager.UiMapCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result79 ? 1 : 0);
                    return true;
                case 91:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result80 = unregisterMapCallback(nfore.android.bt.servicemanager.UiMapCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result80 ? 1 : 0);
                    return true;
                case 92:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result81 = reqMapDownloadAllMessages(data.readString(), data.readInt(), data.readInt(), data.readInt() != 0);
                    reply.writeNoException();
                    reply.writeInt(_result81 ? 1 : 0);
                    return true;
                case TRANSACTION_reqMapDownloadSingleMessage /*93*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result82 = reqMapDownloadSingleMessage(data.readString(), data.readInt(), data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result82 ? 1 : 0);
                    return true;
                case TRANSACTION_reqMapRegisterNotification /*94*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result83 = reqMapRegisterNotification(data.readString(), data.readInt() != 0);
                    reply.writeNoException();
                    reply.writeInt(_result83 ? 1 : 0);
                    return true;
                case 95:
                    data.enforceInterface(DESCRIPTOR);
                    reqMapUnregisterNotification(data.readString());
                    reply.writeNoException();
                    return true;
                case 96:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result84 = isMapNotificationRegistered(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result84 ? 1 : 0);
                    return true;
                case 97:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result85 = reqMapDownloadInterrupt(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result85 ? 1 : 0);
                    return true;
                case 98:
                    data.enforceInterface(DESCRIPTOR);
                    reqMapDatabaseAvailable(data.readString());
                    reply.writeNoException();
                    return true;
                case 99:
                    data.enforceInterface(DESCRIPTOR);
                    reqMapDeleteDatabaseByAddress(data.readString());
                    reply.writeNoException();
                    return true;
                case 100:
                    data.enforceInterface(DESCRIPTOR);
                    reqMapCleanDatabase();
                    reply.writeNoException();
                    return true;
                case 101:
                    data.enforceInterface(DESCRIPTOR);
                    int _result86 = getMapCurrentState(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result86);
                    return true;
                case 102:
                    data.enforceInterface(DESCRIPTOR);
                    int _result87 = getMapRegisterState(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result87);
                    return true;
                case 103:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result88 = registerA2dpAvrcpCallback(nfore.android.bt.servicemanager.UiA2dpCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result88 ? 1 : 0);
                    return true;
                case 104:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result89 = unregisterA2dpAvrcpCallback(nfore.android.bt.servicemanager.UiA2dpCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result89 ? 1 : 0);
                    return true;
                case 105:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result90 = reqA2dpConnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result90 ? 1 : 0);
                    return true;
                case 106:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result91 = reqA2dpDisconnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result91 ? 1 : 0);
                    return true;
                case 107:
                    data.enforceInterface(DESCRIPTOR);
                    String _result92 = getA2dpConnectedDeviceAddress();
                    reply.writeNoException();
                    reply.writeString(_result92);
                    return true;
                case 108:
                    data.enforceInterface(DESCRIPTOR);
                    int _result93 = getA2dpCurrentState();
                    reply.writeNoException();
                    reply.writeInt(_result93);
                    return true;
                case 109:
                    data.enforceInterface(DESCRIPTOR);
                    int _result94 = getAvrcpCurrentState();
                    reply.writeNoException();
                    reply.writeInt(_result94);
                    return true;
                case 110:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result95 = setA2dpLocalVolume(data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result95 ? 1 : 0);
                    return true;
                case 111:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result96 = setA2dpLocalMute(data.readInt() != 0);
                    reply.writeNoException();
                    reply.writeInt(_result96 ? 1 : 0);
                    return true;
                case 112:
                    data.enforceInterface(DESCRIPTOR);
                    String _result97 = getAvrcpConnectedDeviceAddress();
                    reply.writeNoException();
                    reply.writeString(_result97);
                    return true;
                case 113:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result98 = isAvrcp13Supported(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result98 ? 1 : 0);
                    return true;
                case 114:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result99 = isAvrcp14Supported(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result99 ? 1 : 0);
                    return true;
                case 115:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result100 = reqAvrcpPlay(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result100 ? 1 : 0);
                    return true;
                case 116:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result101 = reqAvrcpStop(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result101 ? 1 : 0);
                    return true;
                case 117:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result102 = reqAvrcpPause(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result102 ? 1 : 0);
                    return true;
                case 118:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result103 = reqAvrcpForward(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result103 ? 1 : 0);
                    return true;
                case 119:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result104 = reqAvrcpBackward(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result104 ? 1 : 0);
                    return true;
                case 120:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result105 = reqAvrcpVolumeUp(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result105 ? 1 : 0);
                    return true;
                case 121:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result106 = reqAvrcpVolumeDown(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result106 ? 1 : 0);
                    return true;
                case 122:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result107 = reqAvrcpStartFastForward(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result107 ? 1 : 0);
                    return true;
                case 123:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result108 = reqAvrcpStopFastForward(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result108 ? 1 : 0);
                    return true;
                case 124:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result109 = reqAvrcpStartRewind(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result109 ? 1 : 0);
                    return true;
                case 125:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result110 = reqAvrcpStopRewind(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result110 ? 1 : 0);
                    return true;
                case 126:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result111 = reqAvrcpGetCapabilitiesSupportEvent(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result111 ? 1 : 0);
                    return true;
                case 127:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result112 = reqAvrcpGetPlayerSettingAttributesList(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result112 ? 1 : 0);
                    return true;
                case 128:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result113 = reqAvrcpGetPlayerSettingValuesList(data.readString(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result113 ? 1 : 0);
                    return true;
                case 129:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result114 = reqAvrcpGetPlayerSettingCurrentValues(data.readString(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result114 ? 1 : 0);
                    return true;
                case 130:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result115 = reqAvrcpSetPlayerSettingValue(data.readString(), data.readByte(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result115 ? 1 : 0);
                    return true;
                case 131:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result116 = reqAvrcpGetElementAttributesPlaying(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result116 ? 1 : 0);
                    return true;
                case 132:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result117 = reqAvrcpGetPlayStatus(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result117 ? 1 : 0);
                    return true;
                case 133:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result118 = reqAvrcpRegisterEventWatcher(data.readString(), data.readByte(), data.readLong());
                    reply.writeNoException();
                    reply.writeInt(_result118 ? 1 : 0);
                    return true;
                case 134:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result119 = reqAvrcpUnregisterEventWatcher(data.readString(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result119 ? 1 : 0);
                    return true;
                case 135:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result120 = reqAvrcpAbortMetadataReceiving(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result120 ? 1 : 0);
                    return true;
                case 136:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result121 = reqAvrcpNextGroup(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result121 ? 1 : 0);
                    return true;
                case 137:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result122 = reqAvrcpPreviousGroup(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result122 ? 1 : 0);
                    return true;
                case 138:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result123 = reqAvrcpSetMetadataCompanyId(data.readString(), data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result123 ? 1 : 0);
                    return true;
                case 139:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result124 = reqAvrcpGetPlayerSettingAttributeText(data.readString(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result124 ? 1 : 0);
                    return true;
                case 140:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result125 = reqAvrcpGetPlayerSettingValueText(data.readString(), data.readByte(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result125 ? 1 : 0);
                    return true;
                case 141:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result126 = reqAvrcpInformDisplayableCharset(data.readString(), data.createIntArray());
                    reply.writeNoException();
                    reply.writeInt(_result126 ? 1 : 0);
                    return true;
                case 142:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result127 = reqAvrcpInformBatteryStatusOfCt(data.readString(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result127 ? 1 : 0);
                    return true;
                case 143:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result128 = reqAvrcpSetAddressedPlayer(data.readString(), data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result128 ? 1 : 0);
                    return true;
                case 144:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result129 = reqAvrcpSetBrowsedPlayer(data.readString(), data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result129 ? 1 : 0);
                    return true;
                case 145:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result130 = reqAvrcpGetFolderItems(data.readString(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result130 ? 1 : 0);
                    return true;
                case 146:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result131 = reqAvrcpChangePath(data.readString(), data.readInt(), data.readLong(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result131 ? 1 : 0);
                    return true;
                case 147:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result132 = reqAvrcpGetItemAttributes(data.readString(), data.readByte(), data.readInt(), data.readLong());
                    reply.writeNoException();
                    reply.writeInt(_result132 ? 1 : 0);
                    return true;
                case 148:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result133 = reqAvrcpPlaySelectedItem(data.readString(), data.readByte(), data.readInt(), data.readLong());
                    reply.writeNoException();
                    reply.writeInt(_result133 ? 1 : 0);
                    return true;
                case 149:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result134 = reqAvrcpSearch(data.readString(), data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result134 ? 1 : 0);
                    return true;
                case 150:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result135 = reqAvrcpAddToNowPlaying(data.readString(), data.readByte(), data.readInt(), data.readLong());
                    reply.writeNoException();
                    reply.writeInt(_result135 ? 1 : 0);
                    return true;
                case 151:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result136 = isAvrcpBrowsingChannelEstablished(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result136 ? 1 : 0);
                    return true;
                case TRANSACTION_reqAvrcpSetAbsoluteVolume /*152*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result137 = reqAvrcpSetAbsoluteVolume(data.readString(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result137 ? 1 : 0);
                    return true;
                case 153:
                    data.enforceInterface(DESCRIPTOR);
                    setA2dpNativeStart();
                    reply.writeNoException();
                    return true;
                case 154:
                    data.enforceInterface(DESCRIPTOR);
                    setA2dpNativeStop();
                    reply.writeNoException();
                    return true;
                case 155:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result138 = isA2dpNativeRunning();
                    reply.writeNoException();
                    reply.writeInt(_result138 ? 1 : 0);
                    return true;
                case TRANSACTION_reqAvrcpUpdateSongStatus /*156*/:
                    data.enforceInterface(DESCRIPTOR);
                    reqAvrcpUpdateSongStatus();
                    reply.writeNoException();
                    return true;
                case 157:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result139 = registerPanCallback(nfore.android.bt.servicemanager.UiPanCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result139 ? 1 : 0);
                    return true;
                case 158:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result140 = unregisterPanCallback(nfore.android.bt.servicemanager.UiPanCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result140 ? 1 : 0);
                    return true;
                case 159:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result141 = reqPanConnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result141 ? 1 : 0);
                    return true;
                case 160:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result142 = reqPanDisconnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result142 ? 1 : 0);
                    return true;
                case 161:
                    data.enforceInterface(DESCRIPTOR);
                    String _result143 = getPanConnectedDeviceAddress();
                    reply.writeNoException();
                    reply.writeString(_result143);
                    return true;
                case 162:
                    data.enforceInterface(DESCRIPTOR);
                    int _result144 = getPanCurrentState();
                    reply.writeNoException();
                    reply.writeInt(_result144);
                    return true;
                case 163:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result145 = registerHidCallback(nfore.android.bt.servicemanager.UiHidCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result145 ? 1 : 0);
                    return true;
                case 164:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result146 = unregisterHidCallback(nfore.android.bt.servicemanager.UiHidCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result146 ? 1 : 0);
                    return true;
                case 165:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result147 = reqHidConnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result147 ? 1 : 0);
                    return true;
                case 166:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result148 = reqHidDisconnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result148 ? 1 : 0);
                    return true;
                case TRANSACTION_sendHidMouseCommand /*167*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result149 = sendHidMouseCommand(data.readInt(), data.readInt(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result149);
                    return true;
                case TRANSACTION_sendHidVirtualKeyCommand /*168*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result150 = sendHidVirtualKeyCommand(data.readInt(), data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result150);
                    return true;
                case TRANSACTION_getHidCurrentState /*169*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result151 = getHidCurrentState();
                    reply.writeNoException();
                    reply.writeInt(_result151);
                    return true;
                case 170:
                    data.enforceInterface(DESCRIPTOR);
                    setTargetAddress(data.readString());
                    reply.writeNoException();
                    return true;
                case TRANSACTION_getTargetAddress /*171*/:
                    data.enforceInterface(DESCRIPTOR);
                    String _result152 = getTargetAddress();
                    reply.writeNoException();
                    reply.writeString(_result152);
                    return true;
                case 172:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result153 = reqBluetoothFoundDevices();
                    reply.writeNoException();
                    reply.writeInt(_result153 ? 1 : 0);
                    return true;
                case TRANSACTION_reqBluetoothPairedDevicesCount /*173*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result154 = reqBluetoothPairedDevicesCount();
                    reply.writeNoException();
                    reply.writeInt(_result154);
                    return true;
                case TRANSACTION_getConnectedHfpDeviceCount /*174*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result155 = getConnectedHfpDeviceCount();
                    reply.writeNoException();
                    reply.writeInt(_result155);
                    return true;
                case TRANSACTION_reqAvrcpPlayPause /*175*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result156 = reqAvrcpPlayPause();
                    reply.writeNoException();
                    reply.writeInt(_result156 ? 1 : 0);
                    return true;
                case TRANSACTION_reqSoftwareUpdate /*176*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result157 = reqSoftwareUpdate();
                    reply.writeNoException();
                    reply.writeInt(_result157 ? 1 : 0);
                    return true;
                case TRANSACTION_isFirstBootUp /*177*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result158 = isFirstBootUp();
                    reply.writeNoException();
                    reply.writeInt(_result158 ? 1 : 0);
                    return true;
                case TRANSACTION_getSppCurrentState /*178*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result159 = getSppCurrentState(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result159);
                    return true;
                case 179:
                    data.enforceInterface(DESCRIPTOR);
                    setHfpAutoAnswer(data.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case 180:
                    data.enforceInterface(DESCRIPTOR);
                    setActivePhone(data.readString());
                    reply.writeNoException();
                    return true;
                case TRANSACTION_setTouchesOptions /*181*/:
                    data.enforceInterface(DESCRIPTOR);
                    setTouchesOptions(data.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case 182:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result160 = isHfpConnectedAppleDevice();
                    reply.writeNoException();
                    reply.writeInt(_result160 ? 1 : 0);
                    return true;
                case 183:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result161 = isAppleDeviceFinishAuth();
                    reply.writeNoException();
                    reply.writeInt(_result161 ? 1 : 0);
                    return true;
                case TRANSACTION_getAppleDeviceAuthState /*184*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result162 = getAppleDeviceAuthState();
                    reply.writeNoException();
                    reply.writeInt(_result162);
                    return true;
                case 185:
                    data.enforceInterface(DESCRIPTOR);
                    String _result163 = getHfpConnectedDeviceVendorID();
                    reply.writeNoException();
                    reply.writeString(_result163);
                    return true;
                case TRANSACTION_isHfpAutoAnswer /*186*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result164 = isHfpAutoAnswer();
                    reply.writeNoException();
                    reply.writeInt(_result164 ? 1 : 0);
                    return true;
                case TRANSACTION_isAlreadyReadPairedDevices /*187*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result165 = isAlreadyReadPairedDevices();
                    reply.writeNoException();
                    reply.writeInt(_result165 ? 1 : 0);
                    return true;
                case TRANSACTION_isForiPodDisconnectA2DP /*188*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result166 = isForiPodDisconnectA2DP();
                    reply.writeNoException();
                    reply.writeInt(_result166 ? 1 : 0);
                    return true;
                case TRANSACTION_isPhonebookAutoSync /*189*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result167 = isPhonebookAutoSync();
                    reply.writeNoException();
                    reply.writeInt(_result167 ? 1 : 0);
                    return true;
                case 190:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result168 = isTwinConnectEnable();
                    reply.writeNoException();
                    reply.writeInt(_result168 ? 1 : 0);
                    return true;
                case TRANSACTION_setTwinConnectEnable /*191*/:
                    data.enforceInterface(DESCRIPTOR);
                    setTwinConnectEnable(data.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case TRANSACTION_setPinCode /*192*/:
                    data.enforceInterface(DESCRIPTOR);
                    setPinCode(data.readString());
                    reply.writeNoException();
                    return true;
                case TRANSACTION_getPinCode /*193*/:
                    data.enforceInterface(DESCRIPTOR);
                    String _result169 = getPinCode();
                    reply.writeNoException();
                    reply.writeString(_result169);
                    return true;
                case TRANSACTION_getSoftwareVersion /*194*/:
                    data.enforceInterface(DESCRIPTOR);
                    String _result170 = getSoftwareVersion();
                    reply.writeNoException();
                    reply.writeString(_result170);
                    return true;
                case 195:
                    data.enforceInterface(DESCRIPTOR);
                    setPhonebookAutoSyncEnable(data.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case TRANSACTION_reqHfpMicSelection /*196*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result171 = reqHfpMicSelection();
                    reply.writeNoException();
                    reply.writeInt(_result171 ? 1 : 0);
                    return true;
                case TRANSACTION_getHfpMicSelection /*197*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result172 = getHfpMicSelection();
                    reply.writeNoException();
                    reply.writeInt(_result172);
                    return true;
                case TRANSACTION_reqHfpMicMute /*198*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result173 = reqHfpMicMute();
                    reply.writeNoException();
                    reply.writeInt(_result173 ? 1 : 0);
                    return true;
                case TRANSACTION_getHfpCallState /*199*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result174 = getHfpCallState();
                    reply.writeNoException();
                    reply.writeInt(_result174);
                    return true;
                case 200:
                    data.enforceInterface(DESCRIPTOR);
                    int _result175 = getHfpCallCounts();
                    reply.writeNoException();
                    reply.writeInt(_result175);
                    return true;
                case TRANSACTION_getAutoConnectionStatus /*201*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result176 = getAutoConnectionStatus();
                    reply.writeNoException();
                    reply.writeInt(_result176);
                    return true;
                case TRANSACTION_getCurrentCallNumber /*202*/:
                    data.enforceInterface(DESCRIPTOR);
                    String _result177 = getCurrentCallNumber();
                    reply.writeNoException();
                    reply.writeString(_result177);
                    return true;
                case TRANSACTION_getLastCallNumber /*203*/:
                    data.enforceInterface(DESCRIPTOR);
                    String _result178 = getLastCallNumber();
                    reply.writeNoException();
                    reply.writeString(_result178);
                    return true;
                case TRANSACTION_getThreeWayCallNumber /*204*/:
                    data.enforceInterface(DESCRIPTOR);
                    String _result179 = getThreeWayCallNumber();
                    reply.writeNoException();
                    reply.writeString(_result179);
                    return true;
                case TRANSACTION_getCallTime /*205*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result180 = getCallTime(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result180);
                    return true;
                case TRANSACTION_getCallerId /*206*/:
                    data.enforceInterface(DESCRIPTOR);
                    String _result181 = getCallerId(data.readString());
                    reply.writeNoException();
                    reply.writeString(_result181);
                    return true;
                case TRANSACTION_getThreeWayCallTime /*207*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result182 = getThreeWayCallTime();
                    reply.writeNoException();
                    reply.writeInt(_result182);
                    return true;
                case TRANSACTION_getCurrentSyncType /*208*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result183 = getCurrentSyncType();
                    reply.writeNoException();
                    reply.writeInt(_result183);
                    return true;
                case TRANSACTION_getCurrentSyncState /*209*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result184 = getCurrentSyncState();
                    reply.writeNoException();
                    reply.writeInt(_result184);
                    return true;
                case 210:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result185 = getImportVCardState();
                    reply.writeNoException();
                    reply.writeInt(_result185 ? 1 : 0);
                    return true;
                case TRANSACTION_avrcpPlayOrPause /*211*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result186 = avrcpPlayOrPause();
                    reply.writeNoException();
                    reply.writeInt(_result186 ? 1 : 0);
                    return true;
                case 212:
                    data.enforceInterface(DESCRIPTOR);
                    String _result187 = getCurrentSource();
                    reply.writeNoException();
                    reply.writeString(_result187);
                    return true;
                case TRANSACTION_requestAudioFocus /*213*/:
                    data.enforceInterface(DESCRIPTOR);
                    requestAudioFocus();
                    reply.writeNoException();
                    return true;
                case 214:
                    data.enforceInterface(DESCRIPTOR);
                    requestBTAudioChannel();
                    reply.writeNoException();
                    return true;
                case TRANSACTION_isA2dpPlaying /*215*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result188 = isA2dpPlaying();
                    reply.writeNoException();
                    reply.writeInt(_result188 ? 1 : 0);
                    return true;
                case TRANSACTION_sendSppMessage /*216*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result189 = sendSppMessage(data.readString(), data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result189 ? 1 : 0);
                    return true;
                case TRANSACTION_getPhoneScreenState /*217*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result190 = getPhoneScreenState();
                    reply.writeNoException();
                    reply.writeInt(_result190);
                    return true;
                case TRANSACTION_getPhoneScreenType /*218*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result191 = getPhoneScreenType();
                    reply.writeNoException();
                    reply.writeInt(_result191);
                    return true;
                case TRANSACTION_getPhoneResolution /*219*/:
                    data.enforceInterface(DESCRIPTOR);
                    String _result192 = getPhoneResolution();
                    reply.writeNoException();
                    reply.writeString(_result192);
                    return true;
                case 220:
                    data.enforceInterface(DESCRIPTOR);
                    int _result193 = getDeviceX();
                    reply.writeNoException();
                    reply.writeInt(_result193);
                    return true;
                case TRANSACTION_getDeviceY /*221*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result194 = getDeviceY();
                    reply.writeNoException();
                    reply.writeInt(_result194);
                    return true;
                case 222:
                    data.enforceInterface(DESCRIPTOR);
                    int _result195 = getHXPhoneLen();
                    reply.writeNoException();
                    reply.writeInt(_result195);
                    return true;
                case TRANSACTION_getHYPhoneLen /*223*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result196 = getHYPhoneLen();
                    reply.writeNoException();
                    reply.writeInt(_result196);
                    return true;
                case 224:
                    data.enforceInterface(DESCRIPTOR);
                    int _result197 = getPXPhoneLen();
                    reply.writeNoException();
                    reply.writeInt(_result197);
                    return true;
                case TRANSACTION_getPYPhoneLen /*225*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result198 = getPYPhoneLen();
                    reply.writeNoException();
                    reply.writeInt(_result198);
                    return true;
                case TRANSACTION_getHCaliX1 /*226*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result199 = getHCaliX1();
                    reply.writeNoException();
                    reply.writeInt(_result199);
                    return true;
                case TRANSACTION_getHCaliX2 /*227*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result200 = getHCaliX2();
                    reply.writeNoException();
                    reply.writeInt(_result200);
                    return true;
                case TRANSACTION_getHCaliY1 /*228*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result201 = getHCaliY1();
                    reply.writeNoException();
                    reply.writeInt(_result201);
                    return true;
                case TRANSACTION_getHCaliY2 /*229*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result202 = getHCaliY2();
                    reply.writeNoException();
                    reply.writeInt(_result202);
                    return true;
                case 230:
                    data.enforceInterface(DESCRIPTOR);
                    int _result203 = getPCaliX1();
                    reply.writeNoException();
                    reply.writeInt(_result203);
                    return true;
                case TRANSACTION_getPCaliX2 /*231*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result204 = getPCaliX2();
                    reply.writeNoException();
                    reply.writeInt(_result204);
                    return true;
                case TRANSACTION_getPCaliY1 /*232*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result205 = getPCaliY1();
                    reply.writeNoException();
                    reply.writeInt(_result205);
                    return true;
                case TRANSACTION_getPCaliY2 /*233*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result206 = getPCaliY2();
                    reply.writeNoException();
                    reply.writeInt(_result206);
                    return true;
                case TRANSACTION_isCalibration /*234*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result207 = isCalibration();
                    reply.writeNoException();
                    reply.writeInt(_result207 ? 1 : 0);
                    return true;
                case TRANSACTION_getCalibrationState /*235*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result208 = getCalibrationState();
                    reply.writeNoException();
                    reply.writeInt(_result208);
                    return true;
                case TRANSACTION_getCalibrationClick /*236*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result209 = getCalibrationClick();
                    reply.writeNoException();
                    reply.writeInt(_result209);
                    return true;
                case TRANSACTION_getCurrentImei /*237*/:
                    data.enforceInterface(DESCRIPTOR);
                    String _result210 = getCurrentImei();
                    reply.writeNoException();
                    reply.writeString(_result210);
                    return true;
                case TRANSACTION_setCalibration /*238*/:
                    data.enforceInterface(DESCRIPTOR);
                    setCalibration(data.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case TRANSACTION_enterBluetoothTestMode /*239*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result211 = enterBluetoothTestMode();
                    reply.writeNoException();
                    reply.writeInt(_result211 ? 1 : 0);
                    return true;
                case 240:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result212 = setOutputFrequencyChannel(data.readInt(), data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result212 ? 1 : 0);
                    return true;
                case TRANSACTION_setOutputFrequencyChannelExtended /*241*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result213 = setOutputFrequencyChannelExtended(data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result213 ? 1 : 0);
                    return true;
                case 242:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result214 = isCallingFlag();
                    reply.writeNoException();
                    reply.writeInt(_result214 ? 1 : 0);
                    return true;
                case TRANSACTION_isMicMute /*243*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result215 = isMicMute();
                    reply.writeNoException();
                    reply.writeInt(_result215 ? 1 : 0);
                    return true;
                case 1598968902:
                    reply.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    boolean avrcpPlayOrPause() throws RemoteException;

    boolean enterBluetoothTestMode() throws RemoteException;

    String getA2dpConnectedDeviceAddress() throws RemoteException;

    int getA2dpCurrentState() throws RemoteException;

    int getAppleDeviceAuthState() throws RemoteException;

    int getAutoConnectionStatus() throws RemoteException;

    String getAvrcpConnectedDeviceAddress() throws RemoteException;

    int getAvrcpCurrentState() throws RemoteException;

    int getBluetoothCurrentState() throws RemoteException;

    String getBluetoothLocalAdapterAddress() throws RemoteException;

    String getBluetoothLocalAdapterName() throws RemoteException;

    String getBluetoothRemoteDeviceName(String str) throws RemoteException;

    int getCalibrationClick() throws RemoteException;

    int getCalibrationState() throws RemoteException;

    int getCallTime(String str) throws RemoteException;

    String getCallerId(String str) throws RemoteException;

    int getConnectedHfpDeviceCount() throws RemoteException;

    String getCurrentCallNumber() throws RemoteException;

    String getCurrentImei() throws RemoteException;

    String getCurrentSource() throws RemoteException;

    int getCurrentSyncState() throws RemoteException;

    int getCurrentSyncType() throws RemoteException;

    int getDeviceX() throws RemoteException;

    int getDeviceY() throws RemoteException;

    int getHCaliX1() throws RemoteException;

    int getHCaliX2() throws RemoteException;

    int getHCaliY1() throws RemoteException;

    int getHCaliY2() throws RemoteException;

    int getHXPhoneLen() throws RemoteException;

    int getHYPhoneLen() throws RemoteException;

    int getHfpCallCounts() throws RemoteException;

    int getHfpCallState() throws RemoteException;

    String getHfpConnectedDeviceAddress() throws RemoteException;

    String getHfpConnectedDeviceVendorID() throws RemoteException;

    int getHfpCurrentScoState(String str) throws RemoteException;

    int getHfpCurrentState(String str) throws RemoteException;

    int getHfpMicSelection() throws RemoteException;

    int getHfpRemoteDeviceBatteryStatus(String str) throws RemoteException;

    String getHfpRemoteDeviceName(String str) throws RemoteException;

    int getHfpRemoteDeviceSignalStatus(String str) throws RemoteException;

    int getHidCurrentState() throws RemoteException;

    String getHspConnectedDeviceAddress() throws RemoteException;

    int getHspCurrentScoState(String str) throws RemoteException;

    int getHspCurrentState(String str) throws RemoteException;

    boolean getImportVCardState() throws RemoteException;

    String getLastCallNumber() throws RemoteException;

    int getMapCurrentState(String str) throws RemoteException;

    int getMapRegisterState(String str) throws RemoteException;

    int getPCaliX1() throws RemoteException;

    int getPCaliX2() throws RemoteException;

    int getPCaliY1() throws RemoteException;

    int getPCaliY2() throws RemoteException;

    int getPXPhoneLen() throws RemoteException;

    int getPYPhoneLen() throws RemoteException;

    String getPanConnectedDeviceAddress() throws RemoteException;

    int getPanCurrentState() throws RemoteException;

    int getPbapCurrentState(String str) throws RemoteException;

    String getPhoneResolution() throws RemoteException;

    int getPhoneScreenState() throws RemoteException;

    int getPhoneScreenType() throws RemoteException;

    String getPinCode() throws RemoteException;

    String getSoftwareVersion() throws RemoteException;

    int getSppCurrentState(String str) throws RemoteException;

    String getTargetAddress() throws RemoteException;

    String getThreeWayCallNumber() throws RemoteException;

    int getThreeWayCallTime() throws RemoteException;

    boolean isA2dpNativeRunning() throws RemoteException;

    boolean isA2dpPlaying() throws RemoteException;

    boolean isAlreadyReadPairedDevices() throws RemoteException;

    boolean isAppleDeviceFinishAuth() throws RemoteException;

    boolean isAvrcp13Supported(String str) throws RemoteException;

    boolean isAvrcp14Supported(String str) throws RemoteException;

    boolean isAvrcpBrowsingChannelEstablished(String str) throws RemoteException;

    boolean isBluetoothAutoReconnectEnable() throws RemoteException;

    boolean isBluetoothDiscoverable() throws RemoteException;

    boolean isBluetoothEnable() throws RemoteException;

    boolean isBluetoothScanning() throws RemoteException;

    boolean isCalibration() throws RemoteException;

    boolean isCallingFlag() throws RemoteException;

    boolean isFirstBootUp() throws RemoteException;

    boolean isForiPodDisconnectA2DP() throws RemoteException;

    boolean isHfpAutoAnswer() throws RemoteException;

    boolean isHfpConnectedAppleDevice() throws RemoteException;

    boolean isHfpRemoteDeviceOnRoaming(String str) throws RemoteException;

    boolean isHfpRemoteDeviceTelecomServiceOn(String str) throws RemoteException;

    boolean isHfpVoiceControlOn(String str) throws RemoteException;

    boolean isMapNotificationRegistered(String str) throws RemoteException;

    boolean isMicMute() throws RemoteException;

    boolean isPhonebookAutoSync() throws RemoteException;

    boolean isSppConnected(String str) throws RemoteException;

    boolean isTwinConnectEnable() throws RemoteException;

    boolean registerA2dpAvrcpCallback(UiA2dpCallback uiA2dpCallback) throws RemoteException;

    boolean registerHfpCallback(UiHfpCallback uiHfpCallback) throws RemoteException;

    boolean registerHidCallback(UiHidCallback uiHidCallback) throws RemoteException;

    boolean registerHspCallback(UiHspCallback uiHspCallback) throws RemoteException;

    boolean registerMapCallback(UiMapCallback uiMapCallback) throws RemoteException;

    boolean registerNforeServiceCallback(UiServiceCallback uiServiceCallback) throws RemoteException;

    boolean registerPanCallback(UiPanCallback uiPanCallback) throws RemoteException;

    boolean registerPbapCallback(UiPbapCallback uiPbapCallback) throws RemoteException;

    boolean registerSppCallback(UiSppCallback uiSppCallback) throws RemoteException;

    boolean reqA2dpConnect(String str) throws RemoteException;

    boolean reqA2dpDisconnect(String str) throws RemoteException;

    boolean reqAvrcpAbortMetadataReceiving(String str) throws RemoteException;

    boolean reqAvrcpAddToNowPlaying(String str, byte b, int i, long j) throws RemoteException;

    boolean reqAvrcpBackward(String str) throws RemoteException;

    boolean reqAvrcpChangePath(String str, int i, long j, byte b) throws RemoteException;

    boolean reqAvrcpForward(String str) throws RemoteException;

    boolean reqAvrcpGetCapabilitiesSupportEvent(String str) throws RemoteException;

    boolean reqAvrcpGetElementAttributesPlaying(String str) throws RemoteException;

    boolean reqAvrcpGetFolderItems(String str, byte b) throws RemoteException;

    boolean reqAvrcpGetItemAttributes(String str, byte b, int i, long j) throws RemoteException;

    boolean reqAvrcpGetPlayStatus(String str) throws RemoteException;

    boolean reqAvrcpGetPlayerSettingAttributeText(String str, byte b) throws RemoteException;

    boolean reqAvrcpGetPlayerSettingAttributesList(String str) throws RemoteException;

    boolean reqAvrcpGetPlayerSettingCurrentValues(String str, byte b) throws RemoteException;

    boolean reqAvrcpGetPlayerSettingValueText(String str, byte b, byte b2) throws RemoteException;

    boolean reqAvrcpGetPlayerSettingValuesList(String str, byte b) throws RemoteException;

    boolean reqAvrcpInformBatteryStatusOfCt(String str, byte b) throws RemoteException;

    boolean reqAvrcpInformDisplayableCharset(String str, int[] iArr) throws RemoteException;

    boolean reqAvrcpNextGroup(String str) throws RemoteException;

    boolean reqAvrcpPause(String str) throws RemoteException;

    boolean reqAvrcpPlay(String str) throws RemoteException;

    boolean reqAvrcpPlayPause() throws RemoteException;

    boolean reqAvrcpPlaySelectedItem(String str, byte b, int i, long j) throws RemoteException;

    boolean reqAvrcpPreviousGroup(String str) throws RemoteException;

    boolean reqAvrcpRegisterEventWatcher(String str, byte b, long j) throws RemoteException;

    boolean reqAvrcpSearch(String str, String str2) throws RemoteException;

    boolean reqAvrcpSetAbsoluteVolume(String str, byte b) throws RemoteException;

    boolean reqAvrcpSetAddressedPlayer(String str, int i) throws RemoteException;

    boolean reqAvrcpSetBrowsedPlayer(String str, int i) throws RemoteException;

    boolean reqAvrcpSetMetadataCompanyId(String str, int i) throws RemoteException;

    boolean reqAvrcpSetPlayerSettingValue(String str, byte b, byte b2) throws RemoteException;

    boolean reqAvrcpStartFastForward(String str) throws RemoteException;

    boolean reqAvrcpStartRewind(String str) throws RemoteException;

    boolean reqAvrcpStop(String str) throws RemoteException;

    boolean reqAvrcpStopFastForward(String str) throws RemoteException;

    boolean reqAvrcpStopRewind(String str) throws RemoteException;

    boolean reqAvrcpUnregisterEventWatcher(String str, byte b) throws RemoteException;

    void reqAvrcpUpdateSongStatus() throws RemoteException;

    boolean reqAvrcpVolumeDown(String str) throws RemoteException;

    boolean reqAvrcpVolumeUp(String str) throws RemoteException;

    int reqBluetoothConnectAll(String str) throws RemoteException;

    int reqBluetoothDisconnectAll() throws RemoteException;

    boolean reqBluetoothFoundDevices() throws RemoteException;

    boolean reqBluetoothPair(String str) throws RemoteException;

    boolean reqBluetoothPairedDevices() throws RemoteException;

    int reqBluetoothPairedDevicesCount() throws RemoteException;

    boolean reqBluetoothPairedDevicesExtend() throws RemoteException;

    int reqBluetoothRemoteUuids(String str) throws RemoteException;

    boolean reqBluetoothScanning(boolean z) throws RemoteException;

    boolean reqBluetoothUnpair(String str) throws RemoteException;

    boolean reqBluetoothUnpairAll() throws RemoteException;

    boolean reqBluetoothUnpairAllNextTime() throws RemoteException;

    boolean reqCancelPbapDownload(String str) throws RemoteException;

    int reqDisconnectAll(String str) throws RemoteException;

    boolean reqHfpAnswerCall(String str) throws RemoteException;

    boolean reqHfpAtDownloadPhonebook(String str, int i) throws RemoteException;

    boolean reqHfpAtDownloadSMS(String str) throws RemoteException;

    boolean reqHfpAudioTransfer(String str, byte b) throws RemoteException;

    boolean reqHfpConnect(String str) throws RemoteException;

    boolean reqHfpDialCall(String str, String str2) throws RemoteException;

    boolean reqHfpDisconnect(String str) throws RemoteException;

    boolean reqHfpMemoryDial(String str, String str2) throws RemoteException;

    boolean reqHfpMicMute() throws RemoteException;

    boolean reqHfpMicSelection() throws RemoteException;

    boolean reqHfpMultiCallControl(String str, byte b) throws RemoteException;

    boolean reqHfpReDial(String str) throws RemoteException;

    boolean reqHfpRejectIncomingCall(String str) throws RemoteException;

    void reqHfpRemoteDeviceInfo(String str) throws RemoteException;

    boolean reqHfpRemoteDeviceNetworkOperator(String str) throws RemoteException;

    boolean reqHfpRemoteDevicePhoneNumber(String str) throws RemoteException;

    boolean reqHfpRemoteDeviceSubscriberNumber(String str) throws RemoteException;

    boolean reqHfpRemoteDeviceVolumeSync(String str, byte b, int i) throws RemoteException;

    boolean reqHfpSendDtmf(String str, String str2) throws RemoteException;

    boolean reqHfpTerminateOngoingCall(String str) throws RemoteException;

    boolean reqHfpVoiceDial(String str, boolean z) throws RemoteException;

    boolean reqHidConnect(String str) throws RemoteException;

    boolean reqHidDisconnect(String str) throws RemoteException;

    boolean reqHspAnswerCall(String str) throws RemoteException;

    boolean reqHspAudioTransfer(String str, byte b) throws RemoteException;

    boolean reqHspConnect(String str) throws RemoteException;

    boolean reqHspDisconnect(String str) throws RemoteException;

    boolean reqHspReDial(String str) throws RemoteException;

    boolean reqHspRejectIncomingCall(String str) throws RemoteException;

    boolean reqHspRemoteDeviceVolumeSync(String str, byte b, int i) throws RemoteException;

    boolean reqHspTerminateOngoingCall(String str) throws RemoteException;

    void reqMapCleanDatabase() throws RemoteException;

    void reqMapDatabaseAvailable(String str) throws RemoteException;

    void reqMapDeleteDatabaseByAddress(String str) throws RemoteException;

    boolean reqMapDownloadAllMessages(String str, int i, int i2, boolean z) throws RemoteException;

    boolean reqMapDownloadInterrupt(String str) throws RemoteException;

    boolean reqMapDownloadSingleMessage(String str, int i, String str2) throws RemoteException;

    boolean reqMapRegisterNotification(String str, boolean z) throws RemoteException;

    void reqMapUnregisterNotification(String str) throws RemoteException;

    boolean reqPanConnect(String str) throws RemoteException;

    boolean reqPanDisconnect(String str) throws RemoteException;

    void reqPbapCleanDatabase() throws RemoteException;

    void reqPbapDatabaseAvailable(String str) throws RemoteException;

    void reqPbapDeleteDatabaseByAddress(String str) throws RemoteException;

    boolean reqPbapDownloadAllVcards(String str, int i, int i2, boolean z) throws RemoteException;

    boolean reqPbapDownloadInterrupt(String str) throws RemoteException;

    void reqPbapQueryName(String str, String str2) throws RemoteException;

    boolean reqSoftwareUpdate() throws RemoteException;

    boolean reqSppConnect(String str) throws RemoteException;

    void reqSppConnectedDeviceAddressList() throws RemoteException;

    boolean reqSppDisconnect(String str) throws RemoteException;

    void reqSppSendData(String str, byte[] bArr) throws RemoteException;

    void requestAudioFocus() throws RemoteException;

    void requestBTAudioChannel() throws RemoteException;

    int sendHidMouseCommand(int i, int i2, int i3, int i4) throws RemoteException;

    int sendHidVirtualKeyCommand(int i, int i2) throws RemoteException;

    boolean sendSppMessage(String str, String str2) throws RemoteException;

    boolean setA2dpLocalMute(boolean z) throws RemoteException;

    boolean setA2dpLocalVolume(int i) throws RemoteException;

    void setA2dpNativeStart() throws RemoteException;

    void setA2dpNativeStop() throws RemoteException;

    void setActivePhone(String str) throws RemoteException;

    void setBluetoothAutoReconnect(boolean z) throws RemoteException;

    void setBluetoothAutoReconnectForceOor(boolean z) throws RemoteException;

    boolean setBluetoothDiscoverableDuration(int i) throws RemoteException;

    boolean setBluetoothDiscoverablePermanent(boolean z) throws RemoteException;

    boolean setBluetoothEnable(boolean z) throws RemoteException;

    boolean setBluetoothLocalAdapterName(String str) throws RemoteException;

    void setCalibration(boolean z) throws RemoteException;

    void setHfpAtNewSmsNotify(String str, boolean z) throws RemoteException;

    void setHfpAutoAnswer(boolean z) throws RemoteException;

    void setHfpLocalRingEnable(boolean z) throws RemoteException;

    boolean setOutputFrequencyChannel(int i, int i2) throws RemoteException;

    boolean setOutputFrequencyChannelExtended(int i) throws RemoteException;

    void setPhonebookAutoSyncEnable(boolean z) throws RemoteException;

    void setPinCode(String str) throws RemoteException;

    void setTargetAddress(String str) throws RemoteException;

    void setTouchesOptions(boolean z) throws RemoteException;

    void setTwinConnectEnable(boolean z) throws RemoteException;

    boolean unregisterA2dpAvrcpCallback(UiA2dpCallback uiA2dpCallback) throws RemoteException;

    boolean unregisterHfpCallback(UiHfpCallback uiHfpCallback) throws RemoteException;

    boolean unregisterHidCallback(UiHidCallback uiHidCallback) throws RemoteException;

    boolean unregisterHspCallback(UiHspCallback uiHspCallback) throws RemoteException;

    boolean unregisterMapCallback(UiMapCallback uiMapCallback) throws RemoteException;

    boolean unregisterNforeServiceCallback(UiServiceCallback uiServiceCallback) throws RemoteException;

    boolean unregisterPanCallback(UiPanCallback uiPanCallback) throws RemoteException;

    boolean unregisterPbapCallback(UiPbapCallback uiPbapCallback) throws RemoteException;

    boolean unregisterSppCallback(UiSppCallback uiSppCallback) throws RemoteException;
}
