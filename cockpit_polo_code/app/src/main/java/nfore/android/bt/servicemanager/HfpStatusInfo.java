package nfore.android.bt.servicemanager;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class HfpStatusInfo implements Parcelable {
    public static final Creator<HfpStatusInfo> CREATOR = new Creator<HfpStatusInfo>() {
        public HfpStatusInfo createFromParcel(Parcel source) {
            return new HfpStatusInfo(source);
        }

        public HfpStatusInfo[] newArray(int size) {
            return new HfpStatusInfo[size];
        }
    };
    private int battchgState;
    private int callState;
    private int callbackState;
    private int callheldState;
    private int callsetupState;
    private String deviceAddress;
    private String deviceName;
    private int mainState;
    private int roamState;
    private int scoState;
    private int serviceState;
    private int signalState;
    private int singleType;

    public HfpStatusInfo() {
    }

    public HfpStatusInfo(Parcel parcel) {
        readFromParcel(parcel);
    }

    public void readFromParcel(Parcel parcel) {
        this.deviceAddress = parcel.readString();
        this.deviceName = parcel.readString();
        this.mainState = parcel.readInt();
        this.scoState = parcel.readInt();
        this.serviceState = parcel.readInt();
        this.callState = parcel.readInt();
        this.callsetupState = parcel.readInt();
        this.callheldState = parcel.readInt();
        this.signalState = parcel.readInt();
        this.roamState = parcel.readInt();
        this.battchgState = parcel.readInt();
        this.callbackState = parcel.readInt();
        this.singleType = parcel.readInt();
    }

    public String getDeviceAddress() {
        return this.deviceAddress;
    }

    public void setDeviceAddress(String deviceAddress2) {
        this.deviceAddress = deviceAddress2;
    }

    public String getDeviceName() {
        return this.deviceName;
    }

    public void setDeviceName(String deviceName2) {
        this.deviceName = deviceName2;
    }

    public int getMainState() {
        return this.mainState;
    }

    public void setMainState(int mainState2) {
        this.mainState = mainState2;
    }

    public int getScoState() {
        return this.scoState;
    }

    public void setScoState(int scoState2) {
        this.scoState = scoState2;
    }

    public int getServiceState() {
        return this.serviceState;
    }

    public void setServiceState(int serviceState2) {
        this.serviceState = serviceState2;
    }

    public int getCallState() {
        return this.callState;
    }

    public void setCallState(int callState2) {
        this.callState = callState2;
    }

    public int getCallsetupState() {
        return this.callsetupState;
    }

    public void setCallsetupState(int callsetupState2) {
        this.callsetupState = callsetupState2;
    }

    public int getCallheldState() {
        return this.callheldState;
    }

    public void setCallheldState(int callheldState2) {
        this.callheldState = callheldState2;
    }

    public int getSignalState() {
        return this.signalState;
    }

    public void setSignalState(int signalState2) {
        this.signalState = signalState2;
    }

    public int getRoamState() {
        return this.roamState;
    }

    public void setRoamState(int roamState2) {
        this.roamState = roamState2;
    }

    public int getBattchgState() {
        return this.battchgState;
    }

    public void setBattchgState(int battchgState2) {
        this.battchgState = battchgState2;
    }

    public int getCallbackState() {
        return this.callbackState;
    }

    public void setCallbackState(int callbackState2) {
        this.callbackState = callbackState2;
    }

    public int getSingleType() {
        return this.singleType;
    }

    public void setSingleType(int singleType2) {
        this.singleType = singleType2;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(this.deviceAddress);
        parcel.writeString(this.deviceName);
        parcel.writeInt(this.mainState);
        parcel.writeInt(this.scoState);
        parcel.writeInt(this.serviceState);
        parcel.writeInt(this.callState);
        parcel.writeInt(this.callsetupState);
        parcel.writeInt(this.callheldState);
        parcel.writeInt(this.signalState);
        parcel.writeInt(this.roamState);
        parcel.writeInt(this.battchgState);
        parcel.writeInt(this.callbackState);
        parcel.writeInt(this.singleType);
    }
}
