package nfore.android.bt.servicemanager;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface UiHfpCallback extends IInterface {

    public static abstract class Stub extends Binder implements UiHfpCallback {
        private static final String DESCRIPTOR = "nfore.android.bt.servicemanager.UiHfpCallback";
        static final int TRANSACTION_onHfpActiveNumberChanged = 20;
        static final int TRANSACTION_onHfpAtDownloadPhonebookStateChanged = 13;
        static final int TRANSACTION_onHfpAtDownloadSMSStateChanged = 14;
        static final int TRANSACTION_onHfpCallStateChanged = 21;
        static final int TRANSACTION_onHfpErrorResponse = 4;
        static final int TRANSACTION_onHfpIncomingCallNumber = 15;
        static final int TRANSACTION_onHfpMicSelectionChanged = 23;
        static final int TRANSACTION_onHfpMicVolumeChanged = 18;
        static final int TRANSACTION_onHfpMuteStateChanged = 22;
        static final int TRANSACTION_onHfpOutgoingCallNumber = 16;
        static final int TRANSACTION_onHfpRemoteDeviceBatteryStatusChanged = 11;
        static final int TRANSACTION_onHfpRemoteDevicePhoneNumberChanged = 6;
        static final int TRANSACTION_onHfpRemoteDeviceRoamStatusChanged = 10;
        static final int TRANSACTION_onHfpRemoteDeviceServiceStatusChanged = 9;
        static final int TRANSACTION_onHfpRemoteDeviceSignalStatusChanged = 12;
        static final int TRANSACTION_onHfpScoStateChanged = 2;
        static final int TRANSACTION_onHfpSpeakerVolumeChanged = 17;
        static final int TRANSACTION_onHfpStateChanged = 1;
        static final int TRANSACTION_onHfpVoiceControlStateChanged = 3;
        static final int TRANSACTION_retHfpRemoteDeviceInfo = 5;
        static final int TRANSACTION_retHfpRemoteDeviceNetworkOperator = 7;
        static final int TRANSACTION_retHfpRemoteDeviceSubscriberNumber = 8;
        static final int TRANSACTION_retPbapQueryName = 19;

        private static class Proxy implements UiHfpCallback {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            public void onHfpStateChanged(String address, int prevState, int newState) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(prevState);
                    _data.writeInt(newState);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onHfpScoStateChanged(String address, int prevState, int newState) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(prevState);
                    _data.writeInt(newState);
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onHfpVoiceControlStateChanged(String address, boolean isVoiceControlOn) throws RemoteException {
                int i = 0;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    if (isVoiceControlOn) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(3, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onHfpErrorResponse(String address, HfpErrorInfo errorInfo) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    if (errorInfo != null) {
                        _data.writeInt(1);
                        errorInfo.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(4, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retHfpRemoteDeviceInfo(String address, HfpStatusInfo deviceInfo) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    if (deviceInfo != null) {
                        _data.writeInt(1);
                        deviceInfo.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(5, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onHfpRemoteDevicePhoneNumberChanged(String address, List<HfpPhoneNumberInfo> phoneNumberInfo) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeTypedList(phoneNumberInfo);
                    this.mRemote.transact(6, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retHfpRemoteDeviceNetworkOperator(String address, String mode, int format, String operator) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(mode);
                    _data.writeInt(format);
                    _data.writeString(operator);
                    this.mRemote.transact(7, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retHfpRemoteDeviceSubscriberNumber(String address, String number, int type, int service) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(number);
                    _data.writeInt(type);
                    _data.writeInt(service);
                    this.mRemote.transact(8, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onHfpRemoteDeviceServiceStatusChanged(String address, boolean isTelecomServiceOn) throws RemoteException {
                int i = 0;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    if (isTelecomServiceOn) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(9, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onHfpRemoteDeviceRoamStatusChanged(String address, boolean isOnRoaming) throws RemoteException {
                int i = 0;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    if (isOnRoaming) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(10, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onHfpRemoteDeviceBatteryStatusChanged(String address, int prevStatus, int newStatus, int maxStatus, int minStatus) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(prevStatus);
                    _data.writeInt(newStatus);
                    _data.writeInt(maxStatus);
                    _data.writeInt(minStatus);
                    this.mRemote.transact(11, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onHfpRemoteDeviceSignalStatusChanged(String address, int prevStatus, int newStatus, int maxStatus, int minStatus) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(prevStatus);
                    _data.writeInt(newStatus);
                    _data.writeInt(maxStatus);
                    _data.writeInt(minStatus);
                    this.mRemote.transact(12, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onHfpAtDownloadPhonebookStateChanged(String address, int prevState, int newState) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(prevState);
                    _data.writeInt(newState);
                    this.mRemote.transact(13, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onHfpAtDownloadSMSStateChanged(String address, int newState) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(newState);
                    this.mRemote.transact(14, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onHfpIncomingCallNumber(String address, String callNumber) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(callNumber);
                    this.mRemote.transact(15, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onHfpOutgoingCallNumber(String address, String callNumber) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(callNumber);
                    this.mRemote.transact(16, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onHfpSpeakerVolumeChanged(String address, String prevVolume, String newVolume) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(prevVolume);
                    _data.writeString(newVolume);
                    this.mRemote.transact(17, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onHfpMicVolumeChanged(String address, String prevVolume, String newVolume) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(prevVolume);
                    _data.writeString(newVolume);
                    this.mRemote.transact(18, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retPbapQueryName(String address, String phoneNumber, String fullName, boolean isSuccessed) throws RemoteException {
                int i = 0;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(phoneNumber);
                    _data.writeString(fullName);
                    if (isSuccessed) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(19, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onHfpActiveNumberChanged(String address, String activeNumber, String holdNumber) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(activeNumber);
                    _data.writeString(holdNumber);
                    this.mRemote.transact(20, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onHfpCallStateChanged(String address, int prevState, int newState) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(prevState);
                    _data.writeInt(newState);
                    this.mRemote.transact(21, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onHfpMuteStateChanged(String address, int prevState, int newState) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(prevState);
                    _data.writeInt(newState);
                    this.mRemote.transact(22, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onHfpMicSelectionChanged(String address, int prevState, int newState) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(prevState);
                    _data.writeInt(newState);
                    this.mRemote.transact(23, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static UiHfpCallback asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
            if (iin == null || !(iin instanceof UiHfpCallback)) {
                return new Proxy(obj);
            }
            return (UiHfpCallback) iin;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            boolean _arg3;
            boolean _arg1;
            boolean _arg12;
            HfpStatusInfo _arg13;
            HfpErrorInfo _arg14;
            boolean _arg15;
            switch (code) {
                case 1:
                    data.enforceInterface(DESCRIPTOR);
                    onHfpStateChanged(data.readString(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 2:
                    data.enforceInterface(DESCRIPTOR);
                    onHfpScoStateChanged(data.readString(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 3:
                    data.enforceInterface(DESCRIPTOR);
                    String _arg0 = data.readString();
                    if (data.readInt() != 0) {
                        _arg15 = true;
                    } else {
                        _arg15 = false;
                    }
                    onHfpVoiceControlStateChanged(_arg0, _arg15);
                    reply.writeNoException();
                    return true;
                case 4:
                    data.enforceInterface(DESCRIPTOR);
                    String _arg02 = data.readString();
                    if (data.readInt() != 0) {
                        _arg14 = HfpErrorInfo.CREATOR.createFromParcel(data);
                    } else {
                        _arg14 = null;
                    }
                    onHfpErrorResponse(_arg02, _arg14);
                    reply.writeNoException();
                    return true;
                case 5:
                    data.enforceInterface(DESCRIPTOR);
                    String _arg03 = data.readString();
                    if (data.readInt() != 0) {
                        _arg13 = HfpStatusInfo.CREATOR.createFromParcel(data);
                    } else {
                        _arg13 = null;
                    }
                    retHfpRemoteDeviceInfo(_arg03, _arg13);
                    reply.writeNoException();
                    return true;
                case 6:
                    data.enforceInterface(DESCRIPTOR);
                    onHfpRemoteDevicePhoneNumberChanged(data.readString(), data.createTypedArrayList(HfpPhoneNumberInfo.CREATOR));
                    reply.writeNoException();
                    return true;
                case 7:
                    data.enforceInterface(DESCRIPTOR);
                    retHfpRemoteDeviceNetworkOperator(data.readString(), data.readString(), data.readInt(), data.readString());
                    reply.writeNoException();
                    return true;
                case 8:
                    data.enforceInterface(DESCRIPTOR);
                    retHfpRemoteDeviceSubscriberNumber(data.readString(), data.readString(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 9:
                    data.enforceInterface(DESCRIPTOR);
                    String _arg04 = data.readString();
                    if (data.readInt() != 0) {
                        _arg12 = true;
                    } else {
                        _arg12 = false;
                    }
                    onHfpRemoteDeviceServiceStatusChanged(_arg04, _arg12);
                    reply.writeNoException();
                    return true;
                case 10:
                    data.enforceInterface(DESCRIPTOR);
                    String _arg05 = data.readString();
                    if (data.readInt() != 0) {
                        _arg1 = true;
                    } else {
                        _arg1 = false;
                    }
                    onHfpRemoteDeviceRoamStatusChanged(_arg05, _arg1);
                    reply.writeNoException();
                    return true;
                case 11:
                    data.enforceInterface(DESCRIPTOR);
                    onHfpRemoteDeviceBatteryStatusChanged(data.readString(), data.readInt(), data.readInt(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 12:
                    data.enforceInterface(DESCRIPTOR);
                    onHfpRemoteDeviceSignalStatusChanged(data.readString(), data.readInt(), data.readInt(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 13:
                    data.enforceInterface(DESCRIPTOR);
                    onHfpAtDownloadPhonebookStateChanged(data.readString(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 14:
                    data.enforceInterface(DESCRIPTOR);
                    onHfpAtDownloadSMSStateChanged(data.readString(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 15:
                    data.enforceInterface(DESCRIPTOR);
                    onHfpIncomingCallNumber(data.readString(), data.readString());
                    reply.writeNoException();
                    return true;
                case 16:
                    data.enforceInterface(DESCRIPTOR);
                    onHfpOutgoingCallNumber(data.readString(), data.readString());
                    reply.writeNoException();
                    return true;
                case 17:
                    data.enforceInterface(DESCRIPTOR);
                    onHfpSpeakerVolumeChanged(data.readString(), data.readString(), data.readString());
                    reply.writeNoException();
                    return true;
                case 18:
                    data.enforceInterface(DESCRIPTOR);
                    onHfpMicVolumeChanged(data.readString(), data.readString(), data.readString());
                    reply.writeNoException();
                    return true;
                case 19:
                    data.enforceInterface(DESCRIPTOR);
                    String _arg06 = data.readString();
                    String _arg16 = data.readString();
                    String _arg2 = data.readString();
                    if (data.readInt() != 0) {
                        _arg3 = true;
                    } else {
                        _arg3 = false;
                    }
                    retPbapQueryName(_arg06, _arg16, _arg2, _arg3);
                    reply.writeNoException();
                    return true;
                case 20:
                    data.enforceInterface(DESCRIPTOR);
                    onHfpActiveNumberChanged(data.readString(), data.readString(), data.readString());
                    reply.writeNoException();
                    return true;
                case 21:
                    data.enforceInterface(DESCRIPTOR);
                    onHfpCallStateChanged(data.readString(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 22:
                    data.enforceInterface(DESCRIPTOR);
                    onHfpMuteStateChanged(data.readString(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 23:
                    data.enforceInterface(DESCRIPTOR);
                    onHfpMicSelectionChanged(data.readString(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void onHfpActiveNumberChanged(String str, String str2, String str3) throws RemoteException;

    void onHfpAtDownloadPhonebookStateChanged(String str, int i, int i2) throws RemoteException;

    void onHfpAtDownloadSMSStateChanged(String str, int i) throws RemoteException;

    void onHfpCallStateChanged(String str, int i, int i2) throws RemoteException;

    void onHfpErrorResponse(String str, HfpErrorInfo hfpErrorInfo) throws RemoteException;

    void onHfpIncomingCallNumber(String str, String str2) throws RemoteException;

    void onHfpMicSelectionChanged(String str, int i, int i2) throws RemoteException;

    void onHfpMicVolumeChanged(String str, String str2, String str3) throws RemoteException;

    void onHfpMuteStateChanged(String str, int i, int i2) throws RemoteException;

    void onHfpOutgoingCallNumber(String str, String str2) throws RemoteException;

    void onHfpRemoteDeviceBatteryStatusChanged(String str, int i, int i2, int i3, int i4) throws RemoteException;

    void onHfpRemoteDevicePhoneNumberChanged(String str, List<HfpPhoneNumberInfo> list) throws RemoteException;

    void onHfpRemoteDeviceRoamStatusChanged(String str, boolean z) throws RemoteException;

    void onHfpRemoteDeviceServiceStatusChanged(String str, boolean z) throws RemoteException;

    void onHfpRemoteDeviceSignalStatusChanged(String str, int i, int i2, int i3, int i4) throws RemoteException;

    void onHfpScoStateChanged(String str, int i, int i2) throws RemoteException;

    void onHfpSpeakerVolumeChanged(String str, String str2, String str3) throws RemoteException;

    void onHfpStateChanged(String str, int i, int i2) throws RemoteException;

    void onHfpVoiceControlStateChanged(String str, boolean z) throws RemoteException;

    void retHfpRemoteDeviceInfo(String str, HfpStatusInfo hfpStatusInfo) throws RemoteException;

    void retHfpRemoteDeviceNetworkOperator(String str, String str2, int i, String str3) throws RemoteException;

    void retHfpRemoteDeviceSubscriberNumber(String str, String str2, int i, int i2) throws RemoteException;

    void retPbapQueryName(String str, String str2, String str3, boolean z) throws RemoteException;
}
