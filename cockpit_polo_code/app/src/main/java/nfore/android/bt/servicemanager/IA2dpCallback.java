package nfore.android.bt.servicemanager;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IA2dpCallback extends IInterface {

    public static abstract class Stub extends Binder implements IA2dpCallback {
        private static final String DESCRIPTOR = "nfore.android.bt.servicemanager.IA2dpCallback";
        static final int TRANSACTION_onA2dpStateChanged = 1;
        static final int TRANSACTION_onAvrcpBrowsingStateChanged = 3;
        static final int TRANSACTION_onAvrcpErrorResponse = 38;
        static final int TRANSACTION_onAvrcpEventAddressedPlayerChanged = 26;
        static final int TRANSACTION_onAvrcpEventAvailablePlayerChanged = 25;
        static final int TRANSACTION_onAvrcpEventBatteryStatusChanged = 16;
        static final int TRANSACTION_onAvrcpEventNowPlayingContentChanged = 24;
        static final int TRANSACTION_onAvrcpEventPlaybackPosChanged = 15;
        static final int TRANSACTION_onAvrcpEventPlaybackStatusChanged = 11;
        static final int TRANSACTION_onAvrcpEventPlayerSettingChanged = 18;
        static final int TRANSACTION_onAvrcpEventSystemStatusChanged = 17;
        static final int TRANSACTION_onAvrcpEventTrackChanged = 12;
        static final int TRANSACTION_onAvrcpEventTrackReachedEnd = 13;
        static final int TRANSACTION_onAvrcpEventTrackReachedStart = 14;
        static final int TRANSACTION_onAvrcpEventUidsChanged = 27;
        static final int TRANSACTION_onAvrcpEventVolumeChanged = 28;
        static final int TRANSACTION_onAvrcpStateChanged = 2;
        static final int TRANSACTION_retAvrcpAbsoluteVolumeSet = 37;
        static final int TRANSACTION_retAvrcpAddressedPlayerSet = 29;
        static final int TRANSACTION_retAvrcpBatteryStatusOfCtInformed = 23;
        static final int TRANSACTION_retAvrcpBrowsedPlayerSet = 30;
        static final int TRANSACTION_retAvrcpCapabilitiesSupportEvent = 4;
        static final int TRANSACTION_retAvrcpDisplayableCharsetInformed = 22;
        static final int TRANSACTION_retAvrcpElementAttributesPlaying = 9;
        static final int TRANSACTION_retAvrcpFolderItems = 31;
        static final int TRANSACTION_retAvrcpItemAttributes = 33;
        static final int TRANSACTION_retAvrcpMetadataReceivingAborted = 19;
        static final int TRANSACTION_retAvrcpNowPlayingAdded = 36;
        static final int TRANSACTION_retAvrcpPathChanged = 32;
        static final int TRANSACTION_retAvrcpPlayStatus = 10;
        static final int TRANSACTION_retAvrcpPlayerSettingAttributeText = 20;
        static final int TRANSACTION_retAvrcpPlayerSettingAttributesList = 5;
        static final int TRANSACTION_retAvrcpPlayerSettingCurrentValues = 7;
        static final int TRANSACTION_retAvrcpPlayerSettingValueSet = 8;
        static final int TRANSACTION_retAvrcpPlayerSettingValueText = 21;
        static final int TRANSACTION_retAvrcpPlayerSettingValuesList = 6;
        static final int TRANSACTION_retAvrcpSearchResult = 35;
        static final int TRANSACTION_retAvrcpSelectedItemPlayed = 34;

        private static class Proxy implements IA2dpCallback {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            public void onA2dpStateChanged(String address, int prevState, int newState) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(prevState);
                    _data.writeInt(newState);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onAvrcpStateChanged(String address, int prevState, int newState) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(prevState);
                    _data.writeInt(newState);
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onAvrcpBrowsingStateChanged(String address, int prevState, int newState) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(prevState);
                    _data.writeInt(newState);
                    this.mRemote.transact(3, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retAvrcpCapabilitiesSupportEvent(String address, byte[] eventIds) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByteArray(eventIds);
                    this.mRemote.transact(4, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retAvrcpPlayerSettingAttributesList(String address, byte[] attributeIds) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByteArray(attributeIds);
                    this.mRemote.transact(5, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retAvrcpPlayerSettingValuesList(String address, byte attributeId, byte[] valueIds) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(attributeId);
                    _data.writeByteArray(valueIds);
                    this.mRemote.transact(6, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retAvrcpPlayerSettingCurrentValues(String address, byte[] attributeIds, byte[] valueIds) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByteArray(attributeIds);
                    _data.writeByteArray(valueIds);
                    this.mRemote.transact(7, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retAvrcpPlayerSettingValueSet(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(8, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retAvrcpElementAttributesPlaying(String address, int[] metadataAtrributeIds, String[] texts) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeIntArray(metadataAtrributeIds);
                    _data.writeStringArray(texts);
                    this.mRemote.transact(9, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retAvrcpPlayStatus(String address, long songLen, long songPos, byte statusId) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeLong(songLen);
                    _data.writeLong(songPos);
                    _data.writeByte(statusId);
                    this.mRemote.transact(10, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onAvrcpEventPlaybackStatusChanged(String address, byte statusId) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(statusId);
                    this.mRemote.transact(11, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onAvrcpEventTrackChanged(String address, long elementId) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeLong(elementId);
                    this.mRemote.transact(12, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onAvrcpEventTrackReachedEnd(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(13, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onAvrcpEventTrackReachedStart(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(14, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onAvrcpEventPlaybackPosChanged(String address, long songPos) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeLong(songPos);
                    this.mRemote.transact(15, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onAvrcpEventBatteryStatusChanged(String address, byte statusId) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(statusId);
                    this.mRemote.transact(16, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onAvrcpEventSystemStatusChanged(String address, byte statusId) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(statusId);
                    this.mRemote.transact(17, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onAvrcpEventPlayerSettingChanged(String address, byte[] attributeIds, byte[] valueIds) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByteArray(attributeIds);
                    _data.writeByteArray(valueIds);
                    this.mRemote.transact(18, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retAvrcpMetadataReceivingAborted(String address, int opId) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(opId);
                    this.mRemote.transact(19, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retAvrcpPlayerSettingAttributeText(String address, int cause, byte attributeId, byte[] text) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(cause);
                    _data.writeByte(attributeId);
                    _data.writeByteArray(text);
                    this.mRemote.transact(20, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retAvrcpPlayerSettingValueText(String address, int cause, byte valueId, byte[] text) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(cause);
                    _data.writeByte(valueId);
                    _data.writeByteArray(text);
                    this.mRemote.transact(21, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retAvrcpDisplayableCharsetInformed(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(22, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retAvrcpBatteryStatusOfCtInformed(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(23, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onAvrcpEventNowPlayingContentChanged(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(24, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onAvrcpEventAvailablePlayerChanged(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(25, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onAvrcpEventAddressedPlayerChanged(String address, int playerId, int uidCounter) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(playerId);
                    _data.writeInt(uidCounter);
                    this.mRemote.transact(26, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onAvrcpEventUidsChanged(String address, int uidCounter) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(uidCounter);
                    this.mRemote.transact(27, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onAvrcpEventVolumeChanged(String address, byte volume) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(volume);
                    this.mRemote.transact(28, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retAvrcpAddressedPlayerSet(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(29, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retAvrcpBrowsedPlayerSet(String address, int uidCounter, long itemCount) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(uidCounter);
                    _data.writeLong(itemCount);
                    this.mRemote.transact(30, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retAvrcpFolderItems(String address, int uidCounter, long itemCount) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(uidCounter);
                    _data.writeLong(itemCount);
                    this.mRemote.transact(31, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retAvrcpPathChanged(String address, long itemCount) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeLong(itemCount);
                    this.mRemote.transact(32, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retAvrcpItemAttributes(String address, int[] metadataAtrributeIds, String[] texts) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeIntArray(metadataAtrributeIds);
                    _data.writeStringArray(texts);
                    this.mRemote.transact(33, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retAvrcpSelectedItemPlayed(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(34, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retAvrcpSearchResult(String address, int uidCounter, long itemCount) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(uidCounter);
                    _data.writeLong(itemCount);
                    this.mRemote.transact(35, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retAvrcpNowPlayingAdded(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(36, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retAvrcpAbsoluteVolumeSet(String address, byte volume) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(volume);
                    this.mRemote.transact(37, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onAvrcpErrorResponse(String address, int opId, int reason, byte eventId) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(opId);
                    _data.writeInt(reason);
                    _data.writeByte(eventId);
                    this.mRemote.transact(38, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static IA2dpCallback asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
            if (iin == null || !(iin instanceof IA2dpCallback)) {
                return new Proxy(obj);
            }
            return (IA2dpCallback) iin;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface(DESCRIPTOR);
                    onA2dpStateChanged(data.readString(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 2:
                    data.enforceInterface(DESCRIPTOR);
                    onAvrcpStateChanged(data.readString(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 3:
                    data.enforceInterface(DESCRIPTOR);
                    onAvrcpBrowsingStateChanged(data.readString(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 4:
                    data.enforceInterface(DESCRIPTOR);
                    retAvrcpCapabilitiesSupportEvent(data.readString(), data.createByteArray());
                    reply.writeNoException();
                    return true;
                case 5:
                    data.enforceInterface(DESCRIPTOR);
                    retAvrcpPlayerSettingAttributesList(data.readString(), data.createByteArray());
                    reply.writeNoException();
                    return true;
                case 6:
                    data.enforceInterface(DESCRIPTOR);
                    retAvrcpPlayerSettingValuesList(data.readString(), data.readByte(), data.createByteArray());
                    reply.writeNoException();
                    return true;
                case 7:
                    data.enforceInterface(DESCRIPTOR);
                    retAvrcpPlayerSettingCurrentValues(data.readString(), data.createByteArray(), data.createByteArray());
                    reply.writeNoException();
                    return true;
                case 8:
                    data.enforceInterface(DESCRIPTOR);
                    retAvrcpPlayerSettingValueSet(data.readString());
                    reply.writeNoException();
                    return true;
                case 9:
                    data.enforceInterface(DESCRIPTOR);
                    retAvrcpElementAttributesPlaying(data.readString(), data.createIntArray(), data.createStringArray());
                    reply.writeNoException();
                    return true;
                case 10:
                    data.enforceInterface(DESCRIPTOR);
                    retAvrcpPlayStatus(data.readString(), data.readLong(), data.readLong(), data.readByte());
                    reply.writeNoException();
                    return true;
                case 11:
                    data.enforceInterface(DESCRIPTOR);
                    onAvrcpEventPlaybackStatusChanged(data.readString(), data.readByte());
                    reply.writeNoException();
                    return true;
                case 12:
                    data.enforceInterface(DESCRIPTOR);
                    onAvrcpEventTrackChanged(data.readString(), data.readLong());
                    reply.writeNoException();
                    return true;
                case 13:
                    data.enforceInterface(DESCRIPTOR);
                    onAvrcpEventTrackReachedEnd(data.readString());
                    reply.writeNoException();
                    return true;
                case 14:
                    data.enforceInterface(DESCRIPTOR);
                    onAvrcpEventTrackReachedStart(data.readString());
                    reply.writeNoException();
                    return true;
                case 15:
                    data.enforceInterface(DESCRIPTOR);
                    onAvrcpEventPlaybackPosChanged(data.readString(), data.readLong());
                    reply.writeNoException();
                    return true;
                case 16:
                    data.enforceInterface(DESCRIPTOR);
                    onAvrcpEventBatteryStatusChanged(data.readString(), data.readByte());
                    reply.writeNoException();
                    return true;
                case 17:
                    data.enforceInterface(DESCRIPTOR);
                    onAvrcpEventSystemStatusChanged(data.readString(), data.readByte());
                    reply.writeNoException();
                    return true;
                case 18:
                    data.enforceInterface(DESCRIPTOR);
                    onAvrcpEventPlayerSettingChanged(data.readString(), data.createByteArray(), data.createByteArray());
                    reply.writeNoException();
                    return true;
                case 19:
                    data.enforceInterface(DESCRIPTOR);
                    retAvrcpMetadataReceivingAborted(data.readString(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 20:
                    data.enforceInterface(DESCRIPTOR);
                    retAvrcpPlayerSettingAttributeText(data.readString(), data.readInt(), data.readByte(), data.createByteArray());
                    reply.writeNoException();
                    return true;
                case 21:
                    data.enforceInterface(DESCRIPTOR);
                    retAvrcpPlayerSettingValueText(data.readString(), data.readInt(), data.readByte(), data.createByteArray());
                    reply.writeNoException();
                    return true;
                case 22:
                    data.enforceInterface(DESCRIPTOR);
                    retAvrcpDisplayableCharsetInformed(data.readString());
                    reply.writeNoException();
                    return true;
                case 23:
                    data.enforceInterface(DESCRIPTOR);
                    retAvrcpBatteryStatusOfCtInformed(data.readString());
                    reply.writeNoException();
                    return true;
                case 24:
                    data.enforceInterface(DESCRIPTOR);
                    onAvrcpEventNowPlayingContentChanged(data.readString());
                    reply.writeNoException();
                    return true;
                case 25:
                    data.enforceInterface(DESCRIPTOR);
                    onAvrcpEventAvailablePlayerChanged(data.readString());
                    reply.writeNoException();
                    return true;
                case 26:
                    data.enforceInterface(DESCRIPTOR);
                    onAvrcpEventAddressedPlayerChanged(data.readString(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 27:
                    data.enforceInterface(DESCRIPTOR);
                    onAvrcpEventUidsChanged(data.readString(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 28:
                    data.enforceInterface(DESCRIPTOR);
                    onAvrcpEventVolumeChanged(data.readString(), data.readByte());
                    reply.writeNoException();
                    return true;
                case 29:
                    data.enforceInterface(DESCRIPTOR);
                    retAvrcpAddressedPlayerSet(data.readString());
                    reply.writeNoException();
                    return true;
                case 30:
                    data.enforceInterface(DESCRIPTOR);
                    retAvrcpBrowsedPlayerSet(data.readString(), data.readInt(), data.readLong());
                    reply.writeNoException();
                    return true;
                case 31:
                    data.enforceInterface(DESCRIPTOR);
                    retAvrcpFolderItems(data.readString(), data.readInt(), data.readLong());
                    reply.writeNoException();
                    return true;
                case 32:
                    data.enforceInterface(DESCRIPTOR);
                    retAvrcpPathChanged(data.readString(), data.readLong());
                    reply.writeNoException();
                    return true;
                case 33:
                    data.enforceInterface(DESCRIPTOR);
                    retAvrcpItemAttributes(data.readString(), data.createIntArray(), data.createStringArray());
                    reply.writeNoException();
                    return true;
                case 34:
                    data.enforceInterface(DESCRIPTOR);
                    retAvrcpSelectedItemPlayed(data.readString());
                    reply.writeNoException();
                    return true;
                case 35:
                    data.enforceInterface(DESCRIPTOR);
                    retAvrcpSearchResult(data.readString(), data.readInt(), data.readLong());
                    reply.writeNoException();
                    return true;
                case 36:
                    data.enforceInterface(DESCRIPTOR);
                    retAvrcpNowPlayingAdded(data.readString());
                    reply.writeNoException();
                    return true;
                case 37:
                    data.enforceInterface(DESCRIPTOR);
                    retAvrcpAbsoluteVolumeSet(data.readString(), data.readByte());
                    reply.writeNoException();
                    return true;
                case 38:
                    data.enforceInterface(DESCRIPTOR);
                    onAvrcpErrorResponse(data.readString(), data.readInt(), data.readInt(), data.readByte());
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void onA2dpStateChanged(String str, int i, int i2) throws RemoteException;

    void onAvrcpBrowsingStateChanged(String str, int i, int i2) throws RemoteException;

    void onAvrcpErrorResponse(String str, int i, int i2, byte b) throws RemoteException;

    void onAvrcpEventAddressedPlayerChanged(String str, int i, int i2) throws RemoteException;

    void onAvrcpEventAvailablePlayerChanged(String str) throws RemoteException;

    void onAvrcpEventBatteryStatusChanged(String str, byte b) throws RemoteException;

    void onAvrcpEventNowPlayingContentChanged(String str) throws RemoteException;

    void onAvrcpEventPlaybackPosChanged(String str, long j) throws RemoteException;

    void onAvrcpEventPlaybackStatusChanged(String str, byte b) throws RemoteException;

    void onAvrcpEventPlayerSettingChanged(String str, byte[] bArr, byte[] bArr2) throws RemoteException;

    void onAvrcpEventSystemStatusChanged(String str, byte b) throws RemoteException;

    void onAvrcpEventTrackChanged(String str, long j) throws RemoteException;

    void onAvrcpEventTrackReachedEnd(String str) throws RemoteException;

    void onAvrcpEventTrackReachedStart(String str) throws RemoteException;

    void onAvrcpEventUidsChanged(String str, int i) throws RemoteException;

    void onAvrcpEventVolumeChanged(String str, byte b) throws RemoteException;

    void onAvrcpStateChanged(String str, int i, int i2) throws RemoteException;

    void retAvrcpAbsoluteVolumeSet(String str, byte b) throws RemoteException;

    void retAvrcpAddressedPlayerSet(String str) throws RemoteException;

    void retAvrcpBatteryStatusOfCtInformed(String str) throws RemoteException;

    void retAvrcpBrowsedPlayerSet(String str, int i, long j) throws RemoteException;

    void retAvrcpCapabilitiesSupportEvent(String str, byte[] bArr) throws RemoteException;

    void retAvrcpDisplayableCharsetInformed(String str) throws RemoteException;

    void retAvrcpElementAttributesPlaying(String str, int[] iArr, String[] strArr) throws RemoteException;

    void retAvrcpFolderItems(String str, int i, long j) throws RemoteException;

    void retAvrcpItemAttributes(String str, int[] iArr, String[] strArr) throws RemoteException;

    void retAvrcpMetadataReceivingAborted(String str, int i) throws RemoteException;

    void retAvrcpNowPlayingAdded(String str) throws RemoteException;

    void retAvrcpPathChanged(String str, long j) throws RemoteException;

    void retAvrcpPlayStatus(String str, long j, long j2, byte b) throws RemoteException;

    void retAvrcpPlayerSettingAttributeText(String str, int i, byte b, byte[] bArr) throws RemoteException;

    void retAvrcpPlayerSettingAttributesList(String str, byte[] bArr) throws RemoteException;

    void retAvrcpPlayerSettingCurrentValues(String str, byte[] bArr, byte[] bArr2) throws RemoteException;

    void retAvrcpPlayerSettingValueSet(String str) throws RemoteException;

    void retAvrcpPlayerSettingValueText(String str, int i, byte b, byte[] bArr) throws RemoteException;

    void retAvrcpPlayerSettingValuesList(String str, byte b, byte[] bArr) throws RemoteException;

    void retAvrcpSearchResult(String str, int i, long j) throws RemoteException;

    void retAvrcpSelectedItemPlayed(String str) throws RemoteException;
}
