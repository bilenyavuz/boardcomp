package nfore.android.bt.servicemanager;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface UiServiceCallback extends IInterface {

    public static abstract class Stub extends Binder implements UiServiceCallback {
        private static final String DESCRIPTOR = "nfore.android.bt.servicemanager.UiServiceCallback";
        static final int TRANSACTION_onA2dpStateChanged = 10;
        static final int TRANSACTION_onAdapterStateChanged = 2;
        static final int TRANSACTION_onAvrcpStateChanged = 12;
        static final int TRANSACTION_onDeviceFound = 5;
        static final int TRANSACTION_onDevicePairFailed = 8;
        static final int TRANSACTION_onDevicePaired = 6;
        static final int TRANSACTION_onDeviceUnpaired = 7;
        static final int TRANSACTION_onDeviceUuidsGot = 9;
        static final int TRANSACTION_onHfpStateChanged = 11;
        static final int TRANSACTION_onSecureSimplePairingRequest = 13;
        static final int TRANSACTION_onServiceReady = 1;
        static final int TRANSACTION_retBluetoothFoundDevices = 14;
        static final int TRANSACTION_retBluetoothPairedDevices = 3;
        static final int TRANSACTION_retBluetoothPairedDevicesExtend = 4;

        private static class Proxy implements UiServiceCallback {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            public void onServiceReady(int profilesEnable) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(profilesEnable);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onAdapterStateChanged(int newState) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(newState);
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retBluetoothPairedDevices(int elements, String[] address, String[] name, int[] supportProfiles, byte[] category) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(elements);
                    _data.writeStringArray(address);
                    _data.writeStringArray(name);
                    _data.writeIntArray(supportProfiles);
                    _data.writeByteArray(category);
                    this.mRemote.transact(3, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retBluetoothPairedDevicesExtend(int elements, String[] address, String[] name, int[] supportProfiles, byte[] category, boolean[] connected, boolean[] actived) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(elements);
                    _data.writeStringArray(address);
                    _data.writeStringArray(name);
                    _data.writeIntArray(supportProfiles);
                    _data.writeByteArray(category);
                    _data.writeBooleanArray(connected);
                    _data.writeBooleanArray(actived);
                    this.mRemote.transact(4, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onDeviceFound(String address, String name, byte category, boolean isRealName) throws RemoteException {
                int i = 0;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(name);
                    _data.writeByte(category);
                    if (isRealName) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(5, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onDevicePaired(String address, String name, int supportProfile, byte category) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(name);
                    _data.writeInt(supportProfile);
                    _data.writeByte(category);
                    this.mRemote.transact(6, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onDeviceUnpaired(String address, String name) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(name);
                    this.mRemote.transact(7, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onDevicePairFailed(String address, String name) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(name);
                    this.mRemote.transact(8, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onDeviceUuidsGot(String address, String name, int supportProfile) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(name);
                    _data.writeInt(supportProfile);
                    this.mRemote.transact(9, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onA2dpStateChanged(String address, int prevState, int newState) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(prevState);
                    _data.writeInt(newState);
                    this.mRemote.transact(10, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onHfpStateChanged(String address, int prevState, int newState) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(prevState);
                    _data.writeInt(newState);
                    this.mRemote.transact(11, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onAvrcpStateChanged(String address, int prevState, int newState) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(prevState);
                    _data.writeInt(newState);
                    this.mRemote.transact(12, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onSecureSimplePairingRequest(String sspMode, String passkey) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(sspMode);
                    _data.writeString(passkey);
                    this.mRemote.transact(13, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void retBluetoothFoundDevices(int elements, String[] address, String[] name, int[] supportProfile, byte[] category) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(elements);
                    _data.writeStringArray(address);
                    _data.writeStringArray(name);
                    _data.writeIntArray(supportProfile);
                    _data.writeByteArray(category);
                    this.mRemote.transact(14, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static UiServiceCallback asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
            if (iin == null || !(iin instanceof UiServiceCallback)) {
                return new Proxy(obj);
            }
            return (UiServiceCallback) iin;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface(DESCRIPTOR);
                    onServiceReady(data.readInt());
                    reply.writeNoException();
                    return true;
                case 2:
                    data.enforceInterface(DESCRIPTOR);
                    onAdapterStateChanged(data.readInt());
                    reply.writeNoException();
                    return true;
                case 3:
                    data.enforceInterface(DESCRIPTOR);
                    retBluetoothPairedDevices(data.readInt(), data.createStringArray(), data.createStringArray(), data.createIntArray(), data.createByteArray());
                    reply.writeNoException();
                    return true;
                case 4:
                    data.enforceInterface(DESCRIPTOR);
                    retBluetoothPairedDevicesExtend(data.readInt(), data.createStringArray(), data.createStringArray(), data.createIntArray(), data.createByteArray(), data.createBooleanArray(), data.createBooleanArray());
                    reply.writeNoException();
                    return true;
                case 5:
                    data.enforceInterface(DESCRIPTOR);
                    onDeviceFound(data.readString(), data.readString(), data.readByte(), data.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case 6:
                    data.enforceInterface(DESCRIPTOR);
                    onDevicePaired(data.readString(), data.readString(), data.readInt(), data.readByte());
                    reply.writeNoException();
                    return true;
                case 7:
                    data.enforceInterface(DESCRIPTOR);
                    onDeviceUnpaired(data.readString(), data.readString());
                    reply.writeNoException();
                    return true;
                case 8:
                    data.enforceInterface(DESCRIPTOR);
                    onDevicePairFailed(data.readString(), data.readString());
                    reply.writeNoException();
                    return true;
                case 9:
                    data.enforceInterface(DESCRIPTOR);
                    onDeviceUuidsGot(data.readString(), data.readString(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 10:
                    data.enforceInterface(DESCRIPTOR);
                    onA2dpStateChanged(data.readString(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 11:
                    data.enforceInterface(DESCRIPTOR);
                    onHfpStateChanged(data.readString(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 12:
                    data.enforceInterface(DESCRIPTOR);
                    onAvrcpStateChanged(data.readString(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 13:
                    data.enforceInterface(DESCRIPTOR);
                    onSecureSimplePairingRequest(data.readString(), data.readString());
                    reply.writeNoException();
                    return true;
                case 14:
                    data.enforceInterface(DESCRIPTOR);
                    retBluetoothFoundDevices(data.readInt(), data.createStringArray(), data.createStringArray(), data.createIntArray(), data.createByteArray());
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void onA2dpStateChanged(String str, int i, int i2) throws RemoteException;

    void onAdapterStateChanged(int i) throws RemoteException;

    void onAvrcpStateChanged(String str, int i, int i2) throws RemoteException;

    void onDeviceFound(String str, String str2, byte b, boolean z) throws RemoteException;

    void onDevicePairFailed(String str, String str2) throws RemoteException;

    void onDevicePaired(String str, String str2, int i, byte b) throws RemoteException;

    void onDeviceUnpaired(String str, String str2) throws RemoteException;

    void onDeviceUuidsGot(String str, String str2, int i) throws RemoteException;

    void onHfpStateChanged(String str, int i, int i2) throws RemoteException;

    void onSecureSimplePairingRequest(String str, String str2) throws RemoteException;

    void onServiceReady(int i) throws RemoteException;

    void retBluetoothFoundDevices(int i, String[] strArr, String[] strArr2, int[] iArr, byte[] bArr) throws RemoteException;

    void retBluetoothPairedDevices(int i, String[] strArr, String[] strArr2, int[] iArr, byte[] bArr) throws RemoteException;

    void retBluetoothPairedDevicesExtend(int i, String[] strArr, String[] strArr2, int[] iArr, byte[] bArr, boolean[] zArr, boolean[] zArr2) throws RemoteException;
}
