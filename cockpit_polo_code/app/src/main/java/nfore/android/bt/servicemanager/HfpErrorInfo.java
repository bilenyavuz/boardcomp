package nfore.android.bt.servicemanager;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class HfpErrorInfo implements Parcelable {
    public static final Creator<HfpErrorInfo> CREATOR = new Creator<HfpErrorInfo>() {
        public HfpErrorInfo createFromParcel(Parcel source) {
            return new HfpErrorInfo(source);
        }

        public HfpErrorInfo[] newArray(int size) {
            return new HfpErrorInfo[size];
        }
    };
    private int errorCode;
    private String errorDesc;
    private String errorResponse;

    public HfpErrorInfo() {
    }

    public HfpErrorInfo(Parcel parcel) {
        readFromParcel(parcel);
    }

    public void readFromParcel(Parcel parcel) {
        this.errorCode = parcel.readInt();
        this.errorResponse = parcel.readString();
        this.errorDesc = parcel.readString();
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(int errorCode2) {
        this.errorCode = errorCode2;
    }

    public String getErrorResponse() {
        return this.errorResponse;
    }

    public void setErrorResponse(String errorResponse2) {
        this.errorResponse = errorResponse2;
    }

    public String getErrorDesc() {
        return this.errorDesc;
    }

    public void setErrorDesc(String errorDesc2) {
        this.errorDesc = errorDesc2;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int arg1) {
        parcel.writeInt(this.errorCode);
        parcel.writeString(this.errorResponse);
        parcel.writeString(this.errorDesc);
    }
}
