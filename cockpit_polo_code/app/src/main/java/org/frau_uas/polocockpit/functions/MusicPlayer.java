package org.frau_uas.polocockpit.functions;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ScrollingTabContainerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Layout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import org.frau_uas.polocockpit.MainRecyclerAdapter;
import org.frau_uas.polocockpit.R;
import org.frau_uas.polocockpit.api.model.music.PlayList;
import org.frau_uas.polocockpit.api.model.music.Track;
import org.frau_uas.polocockpit.api.service.MusicLibrary;
import org.frau_uas.polocockpit.api.service.MusicPlayerService;

import java.security.Permission;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MusicPlayer extends CockpitFunction {

    Spinner playlistSelectSpinner;
    RecyclerView trackListRecycler;
    TextView textView_current_name;
    SeekBar seekBar;
    Activity activity;
    Timer mTimer;
    ImageView playPauseButton;
    TrackListAdapter trackListAdapter;
    ImageView loadPlayListButton;
    ImageView shufflePlayListButton;
    ImageView skipButton;
    public MusicPlayer(Activity activity) {
        super(activity);
        this.activity = activity;
    }

    @Override
    public void startCockpitFunction() {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity
                , new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 300);
        }

        int i = 0;
        MusicLibrary musicLibrary = MusicLibrary.getCurrent();
        musicLibrary.scan(activity.getContentResolver());

        textView_current_name = cardView.findViewById(R.id.textView_current_name);


        playlistSelectSpinner = cardView.findViewById(R.id.playlist_select_spinner);
        trackListRecycler = cardView.findViewById(R.id.recyclerView_list_tracks);
        trackListRecycler.setLayoutManager(
                new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));
        trackListRecycler.setHasFixedSize(true);
        trackListAdapter = new TrackListAdapter(trackListRecycler);

        ArrayAdapter<String> playListAdaptor = new ArrayAdapter<String>(this.activity,android.R.layout.simple_spinner_item, musicLibrary.getPlayListNames());
        playListAdaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        playlistSelectSpinner.setAdapter(playListAdaptor);
        playlistSelectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
               /* MusicPlayerService playerService = MusicPlayerService.getCurrent();
                trackListAdapter.notifyItemRangeRemoved(0,playerService.getTrackList().size());
                playerService.clear();
                trackListAdapter.notifyDataSetChanged();*/
            }
        });






        trackListRecycler.setAdapter(trackListAdapter);

        trackListAdapter.notifyDataSetChanged();



        loadPlayListButton = cardView.findViewById(R.id.playlist_load_button);
        loadPlayListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MusicPlayerService playerService = MusicPlayerService.getCurrent();

                trackListAdapter.notifyItemRangeRemoved(0,playerService.getTrackList().size());
                MusicLibrary library = MusicLibrary.getCurrent();
                PlayList playList = library.get(playlistSelectSpinner.getSelectedItemPosition());
                playerService.load(playList);
                trackListAdapter.notifyDataSetChanged();
            }
        });
        shufflePlayListButton = cardView.findViewById(R.id.playlist_shuffle_button);
        shufflePlayListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MusicPlayerService playerService = MusicPlayerService.getCurrent();
                playerService.shuffle();
                trackListAdapter.notifyDataSetChanged();
            }
        });




        playPauseButton = cardView.findViewById(R.id.imageView_play_pause);
        playPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MusicPlayerService.getCurrent().togglePlay();
            }
        });

        skipButton = cardView.findViewById(R.id.imageView_skip);
        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MusicPlayerService playerService = MusicPlayerService.getCurrent();
                playerService.skip();
            }
        });
        seekBar = cardView.findViewById(R.id.seekBar);

        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                activity.runOnUiThread(() -> {
                    MusicPlayerService service = MusicPlayerService.getCurrent();
                    MediaPlayer player = service.getMediaPlayer();
                    Track currentTrack = service.getCurrentTrack();
                    if(currentTrack != null)
                    {
                        textView_current_name.setText(currentTrack.getTitle() + " " + currentTrack.getDuration());

                    }
                    else
                    {
                        textView_current_name.setText("");
                    }
                    if(player.isPlaying()){
                        playPauseButton.setImageDrawable(activity.getDrawable(R.drawable.pause));
                        seekBar.setProgress(player.getCurrentPosition());
                        seekBar.setMax(player.getDuration());
                    }
                    else
                    {
                        playPauseButton.setImageDrawable(activity.getDrawable(R.drawable.play));
                    }


                });
            };
        }, 100,100);

    }

    @Override
    public void stopCockpitFunction() {
        mTimer.cancel();
        mTimer = null;
      //  MusicPlayerService.getCurrent().EndPlayer();
    }

    @Override
    public CardView getCardView() {
        return cardView;
    }

    @Override
    public int getLayout() {
        return R.layout.card_view_music_player;
    }







}
