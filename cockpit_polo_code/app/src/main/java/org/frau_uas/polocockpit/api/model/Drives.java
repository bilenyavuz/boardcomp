package org.frau_uas.polocockpit.api.model;

import io.realm.RealmObject;

public class Drives extends RealmObject {

    private String car = null; // VW-ePolo carID
    private String startDate = null;
    private String endDate = null;
    private Integer startMileage = null;
    private Integer endMileage = null;
    private Integer __v = null; //???
    private Address startAddress;
    private Address endAddress;
    private Coordinates startCoord;
    private Coordinates endCoord;
    private Boolean isStored = false;

    public Drives() {

    }

    public Drives(String car, String startDate, String endDate, Integer startMileage, Integer endMileage, Integer __v, Address startAddress, Address endAddress, Coordinates startCoord, Coordinates endCoord) {
        this.car = car;
        this.startDate = startDate;
        this.endDate = endDate;
        this.startMileage = startMileage;
        this.endMileage = endMileage;
        this.__v = __v;
        this.startAddress = startAddress;
        this.endAddress = endAddress;
        this.startCoord = startCoord;
        this.endCoord = endCoord;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getStartMileage() {
        return startMileage;
    }

    public void setStartMileage(Integer startMileage) {
        this.startMileage = startMileage;
    }

    public Integer getEndMileage() {
        return endMileage;
    }

    public void setEndMileage(Integer endMileage) {
        this.endMileage = endMileage;
    }

    public Integer get__v() {
        return __v;
    }

    public void set__v(Integer __v) {
        this.__v = __v;
    }

    public Address getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(Address startAddress) {
        this.startAddress = startAddress;
    }

    public Address getEndAddress() {
        return endAddress;
    }

    public void setEndAddress(Address endAddress) {
        this.endAddress = endAddress;
    }

    public Coordinates getStartCoord() {
        return startCoord;
    }

    public void setStartCoord(Coordinates startCoord) {
        this.startCoord = startCoord;
    }

    public Coordinates getEndCoord() {
        return endCoord;
    }

    public void setEndCoord(Coordinates endCoord) {
        this.endCoord = endCoord;
    }

    public Boolean getStored() {
        return isStored;
    }

    public void setStored(Boolean stored) {
        isStored = stored;
    }
}
