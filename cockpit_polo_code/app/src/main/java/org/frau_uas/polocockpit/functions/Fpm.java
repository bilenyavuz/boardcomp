package org.frau_uas.polocockpit.functions;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.frau_uas.polocockpit.R;
import org.frau_uas.polocockpit.api.model.Address;
import org.frau_uas.polocockpit.api.model.Coordinates;
import org.frau_uas.polocockpit.api.model.Drives;
import org.frau_uas.polocockpit.api.model.UnsafeOkHttpClient;
import org.frau_uas.polocockpit.api.model.User;
import org.frau_uas.polocockpit.api.service.UserClient;

import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Fpm extends CockpitFunction {

    // UI Elements

    // Login cradView

    private TextView textViewAlterMessageLogin = null;
    private EditText usernameText = null;
    private EditText passwordText = null;
    private ImageButton imageButtonSync = null;
    private Button buttonLogin = null;

    // FPM cardView

    private Button startButton = null;
    private EditText editTextstart = null;
    private TextView textViewAlertMessage = null;
    private EditText editTextDate = null;
    private EditText editTextTime = null;
    private EditText editTextGoal = null;
    private EditText editTextStartMilage = null;
    private EditText editTextGoalMilage = null;
    private Button saveButton = null;
    private EditText editTextgoal = null;
    private EditText usernamedisplay = null;

    // Overview cradview

    private EditText startOv = null;
    private EditText endOv = null;
    private EditText startDateOv = null;
    private EditText endDateOv = null;
    private EditText startTimeOv = null;
    private EditText endTimeOv = null;
    private EditText startMileageOv = null;
    private EditText endMileageOv = null;
    private EditText traveledMilageOV = null;

    // Variables

    private Boolean isLoggedIn = false;
    private Boolean isSaved = false;
    private String username = null;
    private String password = null;
    private static String token = null;

    // Variables to store the drives data

    private String startDate = null;
    private String endDate = null;
    private String startDateSplit = null;
    private String endDateSplit = null;
    private String startTime = null;
    private String endTime = null;
    private Integer startMilage = null;
    private Integer endMilage = null;
    private String car = null;
    private Integer __v = 0;
    private Integer traveledMilage = null;

    // Dateformats

    private SimpleDateFormat timeDF = new SimpleDateFormat("HH:mm:ss");
    private SimpleDateFormat dateDF = new SimpleDateFormat("dd.MM.yyyy");

    private SimpleDateFormat startrestdateDF = new SimpleDateFormat( "yyyy-MM-dd");
    private SimpleDateFormat startresttimeDF = new SimpleDateFormat("HH:mm:ss.SSS");
    private SimpleDateFormat endrestdateDF = new SimpleDateFormat( "yyyy-MM-dd");
    private SimpleDateFormat endresttimeDF = new SimpleDateFormat("HH:mm:ss.SSS");

    private String startLocation = null;

    private SharedPreferences sharedPreferences = activity.getSharedPreferences("org.frau_uas.polocockpit.functions", Context.MODE_PRIVATE);

    private String splitStartAdress[] = null;
    private String splitEndAddress[] = null;

    private String locale = null;

    private Realm realm;

    private int i;

    // Variables to store the FPM data

    public Fpm(Activity activity) {
        super(activity);

        /*Realm.init(activity);
        realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });*/

    }



    @Override
    public void startCockpitFunction() {

        if (isLoggedIn ==  false) {
            setUpLogin();
            clickLoginButton();
        }
    }

    @Override
    public void stopCockpitFunction() {

    }

    @Override
    public CardView getCardView() {
        return cardView;
    }

    @Override
    public int getLayout() {

        if (isLoggedIn) {
            if (isSaved) {
                return R.layout.card_view_fpm_overview;
            } else {
                return R.layout.card_view_fpm;
            }
        } else {
            return R.layout.card_view_fpm_login;
        }

    }

    // Switch to FPM after Loging Button ist clicked

    private void showFPM() {
        cardView.removeAllViews();

        int layout = R.layout.card_view_fpm;
        View v = LayoutInflater
                .from(activity)
                .inflate(layout, cardView, false);

        cardView.addView(v);

        initializeVaribalesFPM();
        initializeFPM();
    }

    private void initializeFPM() {
        editTextstart.setText(sharedPreferences.getString("startAddress", ""));
        editTextStartMilage.setText(sharedPreferences.getString("startMileageA", ""));
    }

    // Switch to FPM Overview

    private void showOverview() {
        cardView.removeAllViews();

        int layout = R.layout.card_view_fpm_overview;
        View v = LayoutInflater
                .from(activity)
                .inflate(layout, cardView, false);

        cardView.addView(v);

        initializeVariablesOverview();
        setUpOverview();
        displayOverview();
    }

    private void showLogin() {
        cardView.removeAllViews();

        int layout = R.layout.card_view_fpm_login;
        View v = LayoutInflater
                .from(activity)
                .inflate(layout, cardView, false);

        cardView.addView(v);

        setUpLogin();
        clickLoginButton();
    }

    private void login() {
        setUpFPM();
    }

    // initialze FPM

    private void setUpFPM() {

        showFPM();
        displayUsername();
        dispalyCalenderStart();
        storeStartDate();
        clickStartButton();
        clickStopButton();
        clickSaveButton();
        clickSyncButton();

    }

    private void setUpLogin() {
        passwordText = cardView.findViewById(R.id.editText_password);
        usernameText = cardView.findViewById(R.id.editText_login_username);
        textViewAlterMessageLogin = cardView.findViewById(R.id.textView_login_alert);
        buttonLogin = cardView.findViewById(R.id.btn_start);
    }

    private void setUpOverview() {

        EditText usernamedisplayOverview = cardView.findViewById(R.id.editText_usernamedisplayOverview);

        usernamedisplayOverview.setText(usernameText.getText().toString());

        clickLogoutButton();
    }

    private void initializeVaribalesFPM() {

        startButton = cardView.findViewById(R.id.btn_start);
        editTextstart = cardView.findViewById(R.id.editText_start);
        editTextStartMilage = cardView.findViewById(R.id.editText_mileagestart);
        textViewAlertMessage = cardView.findViewById(R.id.textView_altermessage);
        editTextDate = cardView.findViewById(R.id.editText_date);
        editTextTime = cardView.findViewById(R.id.editText_time);
        editTextGoal = cardView.findViewById(R.id.editText_goal);
        editTextGoalMilage = cardView.findViewById(R.id.editText_mileagegoal);
        saveButton = cardView.findViewById(R.id.btn_save);
        editTextgoal = cardView.findViewById(R.id.editText_goal);
        usernamedisplay = cardView.findViewById(R.id.editText_usernamedisplay);
        imageButtonSync = cardView.findViewById(R.id.imageButton_sync);

    }

    private void initializeVariablesOverview() {
        startOv = cardView.findViewById(R.id.editText_startOv);
        endOv = cardView.findViewById(R.id.editText_endOv);
        startDateOv = cardView.findViewById(R.id.editText_startDateOv);
        endDateOv = cardView.findViewById(R.id.editText_endDateOv);
        startTimeOv = cardView.findViewById(R.id.editText_startTimeOv);
        endTimeOv = cardView.findViewById(R.id.editText_endTimeOv);
        startMileageOv = cardView.findViewById(R.id.editText_traveledMilageOv);
        endMileageOv = getCardView().findViewById(R.id.editText_KwUsage);
    }

    //need help null error exception

    private void displayUsername() {

        usernamedisplay.setText(usernameText.getText().toString());

    }

    private void dispalyCalenderStart() {

        Calendar calendar = Calendar.getInstance();

        String currentDate = dateDF.format(calendar.getTime());
        String currentTime = timeDF.format(calendar.getTime());

        editTextDate.setText(currentDate);
        editTextTime.setText(currentTime);
        startDateSplit = currentDate;
        startTime = currentTime;
    }

    private void dispalyCalenderStop() {
        Calendar calendar = Calendar.getInstance();

        String currentDate = dateDF.format(calendar.getTime());
        String currentTime = timeDF.format(calendar.getTime());

        editTextDate.setText(currentDate);
        editTextTime.setText(currentTime);
        endDateSplit = currentDate;
        endTime = currentTime;
    }

    private void displayOverview() {
        startMileageOv.setText(traveledMilage.toString() + " / Km");
        endMileageOv.setText(traveledMilage.toString() + "/ kWh");
        startTimeOv.setText(startTime);
        endTimeOv.setText(endTime);
        startDateOv.setText(startDateSplit);
        endDateOv.setText(endDateSplit);
        startOv.setText(editTextstart.getText());
        endOv.setText(editTextGoal.getText());
    }

    private void storeStartDate() {

        Calendar calendar = Calendar.getInstance();

        String currentDate = startrestdateDF.format(calendar.getTime());
        String currentTime = startresttimeDF.format(calendar.getTime());

        startDate = currentDate + "T" + currentTime + "Z";
    }

    private void storeEndDate() {

        Calendar calendar = Calendar.getInstance();

        String currentDate = startrestdateDF.format(calendar.getTime());
        String currentTime = startresttimeDF.format(calendar.getTime());

        endDate = currentDate + "T" + currentTime + "Z";
    }

    private void clickStartButton() {

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ((editTextstart.getText().toString().trim().length() > 0) && (editTextStartMilage.getText().toString().trim().length() > 0)) { // check if start address and start milage is entered by the user

                    String unsplitStartAddress = editTextstart.getText().toString();
                    splitStartAdress = unsplitStartAddress.split(",");

                    if (splitStartAdress.length != 4) {

                        textViewAlertMessage.setVisibility(View.VISIBLE);
                        textViewAlertMessage.setText("Start Adresse Format:\n Land,Stadt,PLZ,Straße");

                    } else {

                        startMilage = Integer.parseInt(editTextStartMilage.getText().toString());
                        textViewAlertMessage.setVisibility(View.GONE);
                        startButton.setVisibility(View.GONE);
                        showStopButton();
                        disableEditAfterStart();

                    }

                } else {
                    textViewAlertMessage.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    private void showStopButton() {
        Button stopButton = cardView.findViewById(R.id.btn_stop);

        stopButton.setVisibility(View.VISIBLE);
    }

    // disable the edit of the time and date during the travel

    private void disableEditAfterStart() {

        editTextDate.setEnabled(false);
        editTextTime.setEnabled(false);
        editTextstart.setEnabled(false);
        editTextStartMilage.setEnabled(false);

    }

    private void disableEditAfterSave() {
        editTextGoal.setEnabled(false);
        editTextGoalMilage.setEnabled(false);
        saveButton.setEnabled(false);
        editTextDate.setEnabled(false);
        editTextTime.setEnabled(false);
        saveButton.setVisibility(View.GONE);
    }

    private void enableEdit() {

        editTextDate.setEnabled(true);
        editTextTime.setEnabled(true);
    }

    private void clickStopButton() {
        final Button stopButton = cardView.findViewById(R.id.btn_stop);

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispalyCalenderStop();
                storeEndDate();
                stopButton.setVisibility(View.GONE);
                showSaveButton();
                enableEdit();
                switchImages();
                switchVisibilityEditText();
            }
        });
    }

    private void showSaveButton() {
        Button saveButton = cardView.findViewById(R.id.btn_save);

        saveButton.setVisibility(View.VISIBLE);
    }

    private void switchImages() {
        ImageView imageViewStart = cardView.findViewById(R.id.imageView_start);
        ImageView imageViewGoal = cardView.findViewById(R.id.imageView_ziel);

        imageViewStart.setVisibility(View.GONE);
        imageViewGoal.setVisibility(View.VISIBLE);
    }

    private void switchVisibilityEditText() {

        EditText editTextstart = cardView.findViewById(R.id.editText_start);
        EditText editTextStartMilage = cardView.findViewById(R.id.editText_mileagestart);

        editTextstart.setVisibility(View.GONE);
        editTextStartMilage.setVisibility(View.GONE);
        editTextGoal.setVisibility(View.VISIBLE);
        editTextGoalMilage.setVisibility(View.VISIBLE);

    }

    private void clickSaveButton() {

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if((editTextgoal.getText().toString().trim().length() > 0) && editTextGoalMilage.getText().toString().trim().length() > 0) {

                    endMilage = Integer.parseInt(editTextGoalMilage.getText().toString());
                    startMilage = Integer.parseInt(editTextStartMilage.getText().toString());

                    String unsplitEndAddress = editTextGoal.getText().toString();
                    splitEndAddress = unsplitEndAddress.split(",");

                    if (startMilage > endMilage)
                    {

                        textViewAlertMessage.setVisibility(View.VISIBLE);
                        textViewAlertMessage.setText("End- ist niederiger als Startkilometerstand");

                    } else if (splitEndAddress.length != 4) {

                        textViewAlertMessage.setVisibility(View.VISIBLE);
                        textViewAlertMessage.setText("Ziel Adresse Format:\n Land,Stadt,PLZ,Straße");

                    } else {
                        textViewAlertMessage.setVisibility(View.GONE);

                        traveledMilage = endMilage - startMilage;

                        Address startAddress = new Address(splitStartAdress[0], splitStartAdress[1], splitStartAdress[2], splitStartAdress[3]);
                        Address endAddress = new Address(splitEndAddress[0], splitEndAddress[1], splitEndAddress[2], splitEndAddress[3]);
                        Coordinates startCoord = new Coordinates("50.117772", "8.574122");
                        Coordinates endCoord = new Coordinates("50.164718", "8.551189");

                        Drives drives = new Drives(
                                car,
                                startDate,
                                endDate,
                                startMilage,
                                endMilage,
                                __v,
                                startAddress,
                                endAddress,
                                startCoord,
                                endCoord
                        );

                        sendNetworkReqeustDrives(drives);

                        sharedPreferences.edit().putString("startAddress", unsplitEndAddress).apply(); // End Address is saved as start address for the next drive
                        sharedPreferences.edit().putString("startMileageA", editTextGoalMilage.getText().toString()).apply(); //End milage is saved as start mileage for the next drive

                        disableEditAfterSave();
                        isSaved = true;
                        showOverview();
                    }

                } else {
                    textViewAlertMessage.setVisibility(View.VISIBLE);
                    textViewAlertMessage.setText("Es müssen alle Felder ausgefüllt werden");
                }

            }
        });
    }

    private void clickLogoutButton() {

        Button logoutButton = cardView.findViewById(R.id.btn_logout);

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isLoggedIn = false;
                isSaved = false;

                showLogin();
            }
        });

    }

    private void clickLoginButton() {

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //User user = new User("Administrator", "1234567890");

                    User user = new User(
                            usernameText.getText().toString(),
                            passwordText.getText().toString()
                    );

                sendNetworkRequestLogin(user);
            }
        });
    }

    private void clickSyncButton() {

        imageButtonSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAllUsers();
            }
        });
    }

    // Retrofit Basic Setup

    private void sendNetworkRequestLogin(User user) {

        // create okHttp client all-trusting

        OkHttpClient okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();

        // create Retrofit instance
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://10.18.2.151:443/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient);

        Retrofit retrofit = builder.build();

        // get client & call object for the request
        UserClient client = retrofit.create(UserClient.class);
        Call<User> call = client.loginAdmin(user);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                if (response.isSuccessful()) {
                    isLoggedIn = true;
                    login();
                    token = "Bearer " + response.body().getToken();
                } else {
                    textViewAlterMessageLogin.setText("Benutzername oder Passwort ist falsch.");
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.v("LOGIN", "Es wird versucht Offline Einzuloggen.");
                try {
                    offlineLogin();
                } catch (InvalidKeySpecException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void offlineLogin() throws InvalidKeySpecException, NoSuchAlgorithmException {

        Realm.init(activity);
        realm = Realm.getDefaultInstance();
        final RealmResults<User> userRealmResults = realm.where(User.class).findAll();

        String securedPasswordHash = null;

        try{
            for (int i = 0;  i < userRealmResults.size(); i++) {
                securedPasswordHash = generatePasswordHash(passwordText.getText().toString(), userRealmResults.get(i).getSalt());

                if (userRealmResults.get(i).getUsername().equals(usernameText.getText().toString()) &&
                        userRealmResults.get(i).getHash().equals(securedPasswordHash))
                {
                    isLoggedIn = true;
                    login();
                }

                else
                {
                    textViewAlterMessageLogin.setText("Benutzername oder Passwort ist falsch.");
                }
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
    }

    private void sendNetworkRequestLocalDrives(Drives drives) {

        // create All-Trusting okHttpClient

        OkHttpClient okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();

        // create retorfit instance

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://10.18.2.151:443/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient);

        Retrofit retrofit = builder.build();

        // get client & call object for request

        UserClient client = retrofit.create(UserClient.class);
        Call<ArrayList<Drives>> call = client.postDrives(token, drives);

        call.enqueue(new Callback<ArrayList<Drives>>() {
            @Override
            public void onResponse(Call<ArrayList<Drives>> call, Response<ArrayList<Drives>> response) {
                Log.v("FUN", "Local Data is send successful");
            }

            @Override
            public void onFailure(Call<ArrayList<Drives>> call, Throwable t) {
                if (t instanceof IOException) {
                    Log.v("FUN", "Network failure");
                    saveDriveData(drives);
                } else {
                    Log.v("FUN", "Conversion Problem");
                }
            }
        });

    }

    private  void sendNetworkReqeustDrives(Drives drives) {

        // create All-Trusting okHttpClient

        OkHttpClient okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();

        // create retorfit instance

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://10.18.2.151:443/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient);

        Retrofit retrofit = builder.build();

        // get client & call object for request

        UserClient client = retrofit.create(UserClient.class);
        Call<ArrayList<Drives>> call = client.postDrives(token, drives);

        call.enqueue(new Callback<ArrayList<Drives>>() {
            @Override
            public void onResponse(Call<ArrayList<Drives>> call, Response<ArrayList<Drives>> response) {
                Log.v("FUN", "Data is stored successfully");

                int status = response.code();

                if (status == 201) {
                    uploadLocalDrives();
                    Toast.makeText(activity, "Fahrtenbucheintrag wurde erfolgreich an den Server gesendet", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Drives>> call, Throwable t) {
                if (t instanceof IOException) {
                    Log.v("FUN", "Network failure");
                    saveDriveData(drives);
                } else {
                    Log.v("FUN", "Conversion Problem");
                }
            }
        });
    }

    // Save drive data if connection to the Server

    private void saveDriveData(Drives drives) {

        Log.v("FUN", "Storing Drivedata into realm DB");

        Realm.init(activity);
        realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Drives drivesRealm = new Drives();

                drivesRealm.setEndAddress(drives.getEndAddress());
                drivesRealm.setStartAddress(drives.getStartAddress());
                drivesRealm.set__v(drives.get__v());
                drivesRealm.setCar(drives.getCar());
                drivesRealm.setEndCoord(drives.getEndCoord());
                drivesRealm.setStartCoord(drives.getStartCoord());
                drivesRealm.setEndMileage(drives.getEndMileage());
                drivesRealm.setStartMileage(drives.getStartMileage());
                drivesRealm.setEndDate(drives.getEndDate());
                drivesRealm.setStartDate(drives.getStartDate());

                realm.insertOrUpdate(drivesRealm);

                Toast.makeText(activity, "Fahrtenbucheintrag wurde lokal gespeichert", Toast.LENGTH_SHORT).show();
            }
        });
    }

    // Insert Users into realm DB

    private void getAllUsers() {
        // create All-Trusting okHttpClient

        OkHttpClient okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();

        // create retorfit instance

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://10.18.2.151:443/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient);

        Retrofit retrofit = builder.build();

        // get client & call object for request

        UserClient client = retrofit.create(UserClient.class);
        Call<ArrayList<User>> call = client.getAllUsers(token);

        call.enqueue(new Callback<ArrayList<User>>() {
            @Override
            public void onResponse(Call<ArrayList<User>> call, Response<ArrayList<User>> response) {
                int status = response.code();

                if (status == 200) {

                    deleteLocalUsers();

                    final ArrayList<User> userArrayList = response.body();

                    Realm.init(activity);
                    realm = Realm.getDefaultInstance();
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {

                            int i;

                            for (i = 0; i < userArrayList.size(); i++) {

                                User userRealm = new User();

                                userRealm.setId(userArrayList.get(i).getId());
                                userRealm.setUsername(userArrayList.get(i).getUsername());
                                userRealm.setFristname(userArrayList.get(i).getFristname());
                                userRealm.setLastname(userArrayList.get(i).getLastname());
                                userRealm.setHash(userArrayList.get(i).getHash());
                                userRealm.setSalt(userArrayList.get(i).getSalt());

                                realm.insertOrUpdate(userRealm);
                            }

                            Toast.makeText(activity, i + " Benutzer wurden erfolgreich lokal gespeichert.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ArrayList<User>> call, Throwable t) {
                Toast.makeText(activity, "Es besteht keine Verbindung zum Server", Toast.LENGTH_SHORT).show();
            }
        });
    }

    // Offline Login

    private static String generatePasswordHash(String password, String saltString) throws NoSuchAlgorithmException, InvalidKeySpecException {
        int iterations = 1000;
        char[] chars = password.toCharArray();
        byte[] salt = saltString.getBytes();

        PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] hash = skf.generateSecret(spec).getEncoded();
        return toHex(hash);
    }

    private static String toHex(byte[] array) throws NoSuchAlgorithmException
    {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if(paddingLength > 0)
        {
            return String.format("%0"  +paddingLength + "d", 0) + hex;
        }else{
            return hex;
        }
    }

    private void uploadLocalDrives() {

        Realm.init(activity);
        realm = Realm.getDefaultInstance();
        final RealmResults<Drives> drivesRealmResults = realm.where(Drives.class).findAll();

        if (drivesRealmResults.size() != 0) {

            Log.v("FUN", "I'm Here" + drivesRealmResults.size());

            for (i = 0; i < drivesRealmResults.size(); i++) {

                String localcar = drivesRealmResults.get(i).getCar();
                String localstartDate = drivesRealmResults.get(i).getStartDate();
                String localendDate = drivesRealmResults.get(i).getEndDate();
                Integer localstartMilage = drivesRealmResults.get(i).getStartMileage();
                Integer localendMilage = drivesRealmResults.get(i).getEndMileage();
                Integer local__v = drivesRealmResults.get(i).get__v();
                Address localstartAddress = new Address(
                        drivesRealmResults.get(i).getStartAddress().getCountry(),
                        drivesRealmResults.get(i).getStartAddress().getCity(),
                        drivesRealmResults.get(i).getStartAddress().getZip(),
                        drivesRealmResults.get(i).getStartAddress().getStreet()
                );
                Address localendAddress = new Address(
                        drivesRealmResults.get(i).getEndAddress().getCountry(),
                        drivesRealmResults.get(i).getEndAddress().getCity(),
                        drivesRealmResults.get(i).getEndAddress().getZip(),
                        drivesRealmResults.get(i).getEndAddress().getStreet()
                );
                Coordinates localstartCoord = new Coordinates(
                        drivesRealmResults.get(i).getStartCoord().getLatitude(),
                        drivesRealmResults.get(i).getStartCoord().getLongitude()
                );
                Coordinates localendCoord = new Coordinates(
                        drivesRealmResults.get(i).getEndCoord().getLatitude(),
                        drivesRealmResults.get(i).getEndCoord().getLongitude()
                );

                Drives localDrives = new Drives(
                        localcar,
                        localstartDate,
                        localendDate,
                        localstartMilage,
                        localendMilage,
                        local__v,
                        localstartAddress,
                        localendAddress,
                        localstartCoord,
                        localendCoord
                );

                sendNetworkRequestLocalDrives(localDrives);
            }

            deleteUploadedDrives();
        }

    }

    private void deleteUploadedDrives() {

        Realm.init(activity);
        realm = Realm.getDefaultInstance();
        RealmResults<Drives> drivesRealmResults = realm.where(Drives.class).findAll();
        RealmResults<Address> addressRealmResults = realm.where(Address.class).findAll();
        RealmResults<Coordinates> coordinatesRealmResults = realm.where(Coordinates.class).findAll();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                drivesRealmResults.deleteAllFromRealm();
                addressRealmResults.deleteAllFromRealm();
                coordinatesRealmResults.deleteAllFromRealm();
                realm.insertOrUpdate(drivesRealmResults);
                realm.insertOrUpdate(addressRealmResults);
                realm.insertOrUpdate(coordinatesRealmResults);
            }
        });

    }

    private void deleteLocalUsers() {

        Realm.init(activity);
        realm = Realm.getDefaultInstance();
        RealmResults<User> userRealmResults = realm.where(User.class).findAll();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                userRealmResults.deleteAllFromRealm();
                realm.insertOrUpdate(userRealmResults);
            }
        });

    }
}
