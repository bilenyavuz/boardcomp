package org.frau_uas.polocockpit.statistics;

import android.app.Activity;

import org.frau_uas.polocockpit.MainActivity;
import org.frau_uas.polocockpit.functions.Statistics;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class StatisticsReceiver {

    private Timer statsTimer;
    private MainActivity activity;
    private static StatisticsReceiver instance;

    private StatisticsReceiver(Activity activity) {
        try {
            createTimer();
            this.activity = (MainActivity) activity;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    static StatisticsReceiver getInstance() {
        return instance;
    }

    public static StatisticsReceiver init(Activity activity) {
        instance = new StatisticsReceiver(activity);
        System.out.println("INIT_____________");
        return instance;
    }

    private void createTimer() throws FileNotFoundException {

        BufferedReader br = new BufferedReader(new FileReader("/dev/ttyACM0"));

        statsTimer = new Timer();
        statsTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    String msg = "";
                    while (br.ready()) {
                        msg = msg + br.readLine();
                    }
                    if (!msg.equals("")) {
                        StatsElement currStats = parseString(msg);
                        notifyUI(currStats);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    // TODO show to user that something with stats is wrong
                } catch (Exception e) {
                    // TODO TOAST THAT SAYS THAT BMS INFO IS WRONGLY FORMATTED
                    e.printStackTrace();
                }
            }
        }, 500, 100);
    }


    private void notifyUI(StatsElement e) {
        activity.notifyCarUpdate(e);
    }


    private StatsElement parseString(String msg) throws Exception {
        String[] cellVoltage =
                msg.substring(msg.indexOf("CellVoltage:")+12, msg.indexOf("CellTemp:")).split(";");
        String[] cellTemp =
                msg.substring(msg.indexOf("CellTemp:")+9, msg.indexOf("DrivingAmperage:")).split(";");
        String drivingAmperage =
                msg.substring(msg.indexOf("DrivingAmperage:")+16, msg.indexOf("ChargingAmperage:"));
        String[] chargingAmperage =
                msg.substring(msg.indexOf("ChargingAmperage:")+17, msg.indexOf("ChargerTemp:")).split(";");
        String[] chargerTemp =
                msg.substring(msg.indexOf("ChargerTemp:")+12).split(";");

        return new StatsElement(Calendar.getInstance().getTime()
                , toIntArr(cellVoltage)
                , createCellTempArr(toIntArr(cellTemp))
                , toIntArr(chargingAmperage)
                , toIntArr(chargerTemp)
                , Integer.parseInt(drivingAmperage));
    }

    private int[] toIntArr(String[] input) {
        return Arrays.stream(input).mapToInt(Integer::parseInt).toArray();
    }

    private int[] createCellTempArr(int[] input) {
        int[] res = new int[60];
        int i2 = 0;
        for (int i = 0; i < 20; i++) {
            res[i2] = input[i];
            res[i2+1] = input[i];
            res[i2+2] = input[i];
            i2 = i2 + 3;
        }
        return res;
    }
}
