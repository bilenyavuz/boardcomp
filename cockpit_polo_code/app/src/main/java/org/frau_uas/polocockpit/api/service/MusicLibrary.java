package org.frau_uas.polocockpit.api.service;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import org.frau_uas.polocockpit.api.model.music.*;

import java.util.HashMap;
import java.util.List;

public class MusicLibrary {
    HashMap<String,PlayList> playLists;
    String[] playListKeys;


    public HashMap<String, PlayList> getPlayLists() {
        return playLists;
    }

    private MusicLibrary(){
        playLists = new HashMap<>();
    }

    static MusicLibrary _current;

    public static MusicLibrary getCurrent(){
        if(_current  == null)
            _current = new MusicLibrary();

        return _current;
    }

    public  void scan(ContentResolver contentResolver){

        playLists.clear();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String[] projection = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.ALBUM
        };
        String sortOrder = MediaStore.Audio.Media.DATA + " ASC";
        Cursor cursor = contentResolver.query(uri, projection, selection, null, sortOrder);
        int count = 0;

        if(cursor != null)
        {

            if(cursor.getCount() > 0)
            {
                while(cursor.moveToNext())
                {
                    Track track = new Track();
                    track.setArtist(cursor.getString(1));
                    track.setTitle(cursor.getString(2));
                    track.setFilePath(cursor.getString(3));
                    track.setDuration(Integer.parseInt(cursor.getString(5)));
                    track.setAlbum(cursor.getString(6));

                    String key = track.getArtist() + " - " + track.getAlbum();

                    if(!playLists.containsKey(key))
                        playLists.put(key, new PlayList(track.getArtist(),track.getAlbum()));

                    PlayList playList = playLists.get(key);
                    playList.add(track);
                }

            }
        }

        cursor.close();

        playListKeys = new String[playLists.size()];
        playListKeys = playLists.keySet().toArray(playListKeys);
    }

    public PlayList get(int i) {

        Object key = playLists.keySet().toArray()[i];
        return playLists.get(key);
    }

    public String[] getPlayListNames(){
        return playListKeys;
    }

}
