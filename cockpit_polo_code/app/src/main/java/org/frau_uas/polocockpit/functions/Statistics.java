package org.frau_uas.polocockpit.functions;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.storage.StorageManager;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import org.frau_uas.polocockpit.CarUpdateListener;
import org.frau_uas.polocockpit.DatePicker;
import org.frau_uas.polocockpit.R;
import org.frau_uas.polocockpit.statistics.StatisticsStorage;
import org.frau_uas.polocockpit.statistics.StatsElement;

import java.util.ArrayList;

public class Statistics extends CockpitFunction implements CarUpdateListener {

    private TextView chooseDiagram;
    private TextView chooseFrom;
    private TextView chooseTo;
    private Chart chart;

    public Statistics(Activity activity) {
        super(activity);
    }

    @Override
    public void startCockpitFunction() {
        initUIElements();
    }

    @Override
    public void stopCockpitFunction() {

    }

    @Override
    public CardView getCardView() {
        return cardView;
    }

    @Override
    public int getLayout() {
        return R.layout.card_view_statistics;
    }



    private void initUIElements() {
        setUIVariables();
        createChooseDiagramListener();
        initChart();
        initChartPicker();
        initStartDatePicker();
        initEndDatePicker();
    }

    private void setUIVariables() {
        this.chooseDiagram = cardView.findViewById(R.id.textView_diagram_name);
        this.chooseFrom = cardView.findViewById(R.id.textView_diagram_from);
        this.chooseTo = cardView.findViewById(R.id.textView_diagram_to);
        this.chart = cardView.findViewById(R.id.chart);
    }


    private void initStartDatePicker() {
        this.chooseFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(chooseFrom);
            }
        });
    }

    private void initEndDatePicker() {
        this.chooseTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(chooseTo);
            }
        });
    }

    private void showDatePicker(TextView tv) {
        DatePicker datePicker = new DatePicker(activity, tv.getId());
    }


    private void initChartPicker() {
        this.chooseDiagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showChooseChartDialog();
            }
        });
    }

    private void showChooseChartDialog() {
        String[] diagrams = new String[16];
        diagrams[0] = "Batteriespannung [Gesamt]";
        diagrams[1] = "Batteriespannung [Block 1]";
        diagrams[2] = "Batteriespannung [Block 2]";
        diagrams[3] = "Batteriespannung [Block 3]";
        diagrams[4] = "Batteriespannung [Block 4]";
        diagrams[5] = "Batterietemperatur [Gesamt]";
        diagrams[6] = "Batterietemperatur [Block 1]";
        diagrams[7] = "Batterietemperatur [Block 2]";
        diagrams[8] = "Batterietemperatur [Block 3]";
        diagrams[9] = "Batterietemperatur [Block 4]";
        diagrams[10] = "Ladestromstärke [Block 1]";
        diagrams[11] = "Ladestromstärke [Block 2]";
        diagrams[12] = "Ladestromstärke [Block 3]";
        diagrams[13] = "Ladestromstärke [Block 4]";
        diagrams[14] = "Aktueller Verbrauch";
        diagrams[15] = "Zurückgelegte Kilometer";

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Diagramm wählen")
                .setItems(diagrams, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                        chooseDiagram.setText(diagrams[which]);
                    }
                });
        builder.create().show();
    }






    // Charting
    private void initChart() {
        drawAmperageOverTimeChart();
    }

    private void drawAmperageOverTimeChart() {
        Object[] data = StatisticsStorage.getInstance().getDrivingAmperageChartData();
        LineDataSet d1 = new LineDataSet((ArrayList<Entry>) data[0], null);
        LineData lineData = new LineData(d1);
        chart.setData(lineData);
        //chart.getXAxis().setValueFormatter(new IndexAxisValueFormatter((ArrayList<String>)data[1]));
    }


    private void createChooseDiagramListener() {
        chooseDiagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public void notifyCarUpdate(StatsElement e) {

    }
}
