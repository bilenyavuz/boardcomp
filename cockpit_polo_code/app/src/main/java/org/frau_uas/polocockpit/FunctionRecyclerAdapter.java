package org.frau_uas.polocockpit;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import org.frau_uas.polocockpit.MainRecyclerAdapter;
import org.frau_uas.polocockpit.R;
import org.frau_uas.polocockpit.functions.CockpitFunction;
import org.frau_uas.polocockpit.functions.Fpm;
import org.frau_uas.polocockpit.functions.MusicPlayer;
import org.frau_uas.polocockpit.functions.Navi;
import org.frau_uas.polocockpit.functions.Radio;

import java.util.ArrayList;
import java.util.List;

public class FunctionRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<CockpitFunction> functions;
    private CockpitFunction curr;
    MainRecyclerAdapter mainAdapter;

    public FunctionRecyclerAdapter(List<CockpitFunction> functions, CockpitFunction curr
            , MainRecyclerAdapter mainAdapter) {
        this.functions = functions;
        this.curr = curr;
        this.functions.remove(curr);
        this.mainAdapter = mainAdapter;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        int layout = R.layout.card_view_menu_option;
        View v = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(layout, viewGroup, false);
        return new ViewHolderMenuOption(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ((ViewHolderMenuOption) viewHolder).fill(functions.get(i));
    }

    @Override
    public int getItemCount() {
        return functions.size();
    }

    void changeCurr(CockpitFunction newCurr) {
        mainAdapter.setCockpitFunction(newCurr);
        int newCurrIndex = functions.indexOf(newCurr);
        functions.set(newCurrIndex, curr);
        curr = newCurr;
        functions.remove(newCurr);
        notifyDataSetChanged();
        mainAdapter.notifyDataSetChanged();
    }

    public class ViewHolderMenuOption extends RecyclerView.ViewHolder {

        private View v;
        private ImageView img;

        ViewHolderMenuOption(@NonNull View itemView) {
            super(itemView);
            this.img = itemView.findViewById(R.id.imageView_function_icon);
            this.v = itemView;
        }

        void fill(final CockpitFunction cf) {
            if (cf instanceof Fpm) {
                img.setImageDrawable(v.getContext().getDrawable(R.drawable.fpm_icon));
            } else if (cf instanceof MusicPlayer) {
                img.setImageDrawable(v.getContext().getDrawable(R.drawable.music_icon));
            } else if (cf instanceof Navi) {
                img.setImageDrawable(v.getContext().getDrawable(R.drawable.navi_icon));
            } else if(cf instanceof Radio)
                img.setImageDrawable(v.getContext().getDrawable(R.drawable.radio_icon));

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changeCurr(cf);
                }
            });
            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changeCurr(cf);
                }
            });
        }


    }
}
