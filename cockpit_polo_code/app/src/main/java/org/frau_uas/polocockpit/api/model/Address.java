package org.frau_uas.polocockpit.api.model;

import io.realm.RealmObject;

public class Address extends RealmObject {

    private String country = null;
    private String city = null;
    private String zip = null;
    private String street = null;
    private Boolean isStored = false;

    public Address() {

    }

    public Address(String country, String city, String zip, String street) {
        this.country = country;
        this.city = city;
        this.zip = zip;
        this.street = street;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Boolean getStored() {
        return isStored;
    }

    public void setStored(Boolean stored) {
        isStored = stored;
    }
}
