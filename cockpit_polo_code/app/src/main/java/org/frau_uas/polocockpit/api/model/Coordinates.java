package org.frau_uas.polocockpit.api.model;

import io.realm.RealmObject;

public class Coordinates extends RealmObject {

    private String latitude = null;
    private String longitude = null;
    private Boolean isStored = false;

    public Coordinates() {

    }

    public Coordinates(String latitude, String longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Boolean getStored() {
        return isStored;
    }

    public void setStored(Boolean stored) {
        isStored = stored;
    }
}
