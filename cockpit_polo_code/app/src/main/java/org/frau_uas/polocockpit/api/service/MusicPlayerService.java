package org.frau_uas.polocockpit.api.service;

import android.media.AudioManager;
import android.media.MediaPlayer;


import org.frau_uas.polocockpit.api.model.music.PlayList;
import org.frau_uas.polocockpit.api.model.music.Track;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class MusicPlayerService implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {

    MediaPlayer mediaPlayer;
    private List<Track> trackList;
    private int currentTrack = -1;
    private boolean play = false;

    private MusicPlayerService(){
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnCompletionListener(this);
        trackList = new ArrayList<>();


    }



    void loadCurrentTrack(){
        try {

            mediaPlayer.reset();
            mediaPlayer.setDataSource(getCurrentTrack().getFilePath());
            mediaPlayer.prepareAsync();
        }
        catch (Exception exc){

        }
    }
    public void togglePlay(){
        if(play)
        {
            play = false;
            if(mediaPlayer.isPlaying())
                mediaPlayer.pause();
        }
        else {
            play = true;
            if(currentTrack == -1)
            {
                currentTrack = 0;
                loadCurrentTrack();
            }
            else if(!mediaPlayer.isPlaying())
                mediaPlayer.start();
        }

    }

    public  List<Track> getTrackList(){
        return this.trackList;
    }

    public  Track getCurrentTrack(){
        if(currentTrack >= 0 && currentTrack < trackList.size())
            return trackList.get(currentTrack);
        else
            return null;
    }



    public  boolean isPlaying(){
        return play;
    }

    static  MusicPlayerService _current;
    public  static  MusicPlayerService getCurrent(){
        if(_current == null)
            _current = new MusicPlayerService();

        return _current;
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
            getCurrentTrack().setDuration(mediaPlayer.getDuration());
           if(play)
               mediaPlayer.start();




    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        if(play) {
            currentTrack++;
            if (currentTrack < 0 || currentTrack >= trackList.size())
                currentTrack = 0;

            loadCurrentTrack();
        }
    }

    public void EndPlayer(){
        if(mediaPlayer != null){
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        if(play) {
            currentTrack++;
            if (currentTrack < 0 && currentTrack >= trackList.size()-1)
                currentTrack = 0;

            loadCurrentTrack();


        }
        return false;
    }

    public void load(PlayList playList) {
        trackList = playList.getTacks();
        currentTrack = -1;

    }

    public  void play(int i){
        currentTrack = i;
        mediaPlayer.stop();
        play = true;

        loadCurrentTrack();
    }

    public void clear() {
        play = false;
        trackList = new ArrayList<>();
        if(mediaPlayer.isPlaying())
            mediaPlayer.stop();
    }

    public void shuffle() {
        List<Track> shuffleList = new ArrayList<>(trackList);
        trackList.clear();
        while (shuffleList.size()>1){
            int i = new Random().nextInt(shuffleList.size());
            trackList.add(shuffleList.get(i));
            shuffleList.remove(i);
        }

        trackList.add(shuffleList.get(0));
    }

    public void skip(){
        if(currentTrack < trackList.size()-1)
            play(currentTrack + 1);
        else
            play(0);
    }
}
