package org.frau_uas.polocockpit.api.service;

import org.frau_uas.polocockpit.api.model.Drives;
import org.frau_uas.polocockpit.api.model.User;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface UserClient {

    @Headers("Content-Type: application/json")
    @POST("login")
    Call<User> loginAdmin(@Body User user);

    @Headers("Content-Type: application/json")
    @POST("drives")
    Call<ArrayList<Drives>> postDrives(@Header("Authorization") String authToken, @Body Drives drives);

    @Headers("Content-Type: application/json")
    @GET("users")
    Call<ArrayList<User>> getAllUsers(@Header("Authorization") String authToken);
}
