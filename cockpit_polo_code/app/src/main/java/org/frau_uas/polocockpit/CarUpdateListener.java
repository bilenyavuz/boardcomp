package org.frau_uas.polocockpit;

import org.frau_uas.polocockpit.statistics.StatsElement;

public interface CarUpdateListener {

    public void notifyCarUpdate(StatsElement e);
}
