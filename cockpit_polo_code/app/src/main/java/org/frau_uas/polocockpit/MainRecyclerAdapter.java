package org.frau_uas.polocockpit;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.frau_uas.polocockpit.functions.CockpitFunction;
import org.frau_uas.polocockpit.functions.Fpm;
import org.frau_uas.polocockpit.functions.MusicPlayer;
import org.frau_uas.polocockpit.functions.Navi;
import org.frau_uas.polocockpit.functions.Statistics;

public class MainRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private CockpitFunction curr;

    MainRecyclerAdapter(CockpitFunction curr) {
        this.curr = curr;
        //curr.startCockpitFunction();
    }

    void setCockpitFunction(CockpitFunction next) {
        curr.stopCockpitFunction();
        this.curr = next;
    }

    CockpitFunction getCockpitFunction() {
        return curr;
    }

    @NonNull
    @Override
    // called when creating a ViewHolder
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        int layout = curr.getLayout();
        View v = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(layout, viewGroup, false);

        curr.setCardView((CardView) v);

        switch (viewType) {
            case 1:
                return new ViewHolderFpm(v);
            case 2:
                return new ViewHolderMusic(v);
            case 3:
                return new ViewHolderNavi(v);
            case 4:
                return new ViewHolderStats(v);
            default:
                return new ViewHolderNavi(v);
        }
    }

    @Override
    // after the ViewHolder is created this gets called to fill it with data
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        curr.startCockpitFunction();
    }

    @Override
    public int getItemViewType(int position) {
        if (curr instanceof Fpm) {

            return 1;

        } else if (curr instanceof MusicPlayer) {

            return 2;

        }  else if (curr instanceof Navi) {

            return 3;

        } else if (curr instanceof Statistics) {

            return 4;
        }
        return 0;
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public static class ViewHolderMusic extends RecyclerView.ViewHolder {

        ViewHolderMusic(View itemView) {
            super(itemView);
        }
    }

    public static class ViewHolderNavi extends RecyclerView.ViewHolder {

        ViewHolderNavi(View itemView) {
            super(itemView);
        }
    }

    public static class ViewHolderFpm extends RecyclerView.ViewHolder {

        ViewHolderFpm(View itemView) {
            super(itemView);
        }
    }

    public static class ViewHolderStats extends RecyclerView.ViewHolder {

        ViewHolderStats(View itemView) {
            super(itemView);
        }
    }
}
