package org.frau_uas.polocockpit.api.model.music;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class PlayList {
    String artist;

    String name;
    List<Track> tracks;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public List<Track> getTacks() {
        return tracks;
    }

    public  PlayList(){
        tracks = new ArrayList<>();
    }

    public  PlayList(String artist,String name){
        this.artist = artist;
        this.name = name;
        tracks = new ArrayList<>();
    }

    public void add(Track track) {
        tracks.add(track);
        tracks.sort((track1, t1) -> track1.getTitle().compareTo(t1.getTitle()));
    }
}
