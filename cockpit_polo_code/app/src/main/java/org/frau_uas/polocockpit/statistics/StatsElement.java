package org.frau_uas.polocockpit.statistics;

import android.graphics.drawable.Drawable;

import org.frau_uas.polocockpit.R;

import java.util.Date;

import io.realm.RealmObject;

public class StatsElement {

    // Time these stats where true
    private Date timestamp;

    // Battery stats
    private int[] batteryVoltage = new int[60];
    private int[] batteryTemp = new int[60];
    private int[] loadingAmperage = new int[4];
    private int[] chargerTemp = new int[4];

    // Driving stats
    private int currDrivingAmperage;



    StatsElement(Date timestamp, int[] batteryVoltage, int[] batteryTemp
            , int[] loadingAmperage, int[] chargerTemp, int currDrivingAmperage) {
        this.timestamp = timestamp;
        this.batteryVoltage = batteryVoltage;
        this.batteryTemp = batteryTemp;
        this.loadingAmperage = loadingAmperage;
        this.chargerTemp = chargerTemp;
        this.currDrivingAmperage = currDrivingAmperage;
    }


    private int getDrawableForPercentage(int percentage) {
        if (!isLoading()) {
            if (percentage > 95) return R.drawable.battery_100;
            if (percentage > 85) return R.drawable.battery_90;
            if (percentage > 55) return R.drawable.battery_60;
            if (percentage > 45) return R.drawable.battery_50;
            if (percentage > 25) return R.drawable.battery_30;
            if (percentage > 15) return R.drawable.battery_20;
            return R.drawable.battery_alert;
        } else {
            if (percentage > 95) return R.drawable.battery_charging_100;
            if (percentage > 85) return R.drawable.battery_charging_90;
            if (percentage > 55) return R.drawable.battery_charging_60;
            if (percentage > 45) return R.drawable.battery_charging_50;
            if (percentage > 25) return R.drawable.battery_charging_30;
            return R.drawable.battery_charging_20;
        }
    }


    public int getPercentage() {
        return (int) (Math.random() * (100 - 0)) + 0;
    }


    public int getPercentageDrawable() {
        int percentage = getPercentage();
        return getDrawableForPercentage(percentage);
    }


    public int getPercentage(int block) {
        return getPercentage();
    }


    public int getPercentageDrawable(int block) {
        int percentage = getPercentage(block);
        return getDrawableForPercentage(percentage);
    }


    public boolean isLoading() {
        return (!(loadingAmperage[0] == 0
                && loadingAmperage[1] == 0
                && loadingAmperage[2] == 0
                && loadingAmperage[3] == 0));
    }


    public int getMinVoltagePerBlock(int block) {
        int[] range = getRangeForBlock(block);
        int start = range[0];
        int end = range[1];
        int min = Integer.MAX_VALUE;
        for (int i = start; i < end; i++) {
            if (min > batteryVoltage[i]) min = batteryVoltage[i];
        }
        return min;
    }


    public int getMaxVoltagePerBlock(int block) {
        int[] range = getRangeForBlock(block);
        int start = range[0];
        int end = range[1];
        int max = 0;
        for (int i = start; i < end; i++) {
            if (max < batteryVoltage[i]) max = batteryVoltage[i];
        }
        return max;
    }


    public int getSumVoltagePerBlock(int block) {
        int[] range = getRangeForBlock(block);
        int start = range[0];
        int end = range[1];
        int sum = 0;
        for (int i = start; i < end; i++) {
            sum = sum + batteryVoltage[i];
        }
        return sum;
    }


    private int[] getRangeForBlock(int block) {
        int start = -1;
        int end = -1;
        if (block == 1) {
            start = 0; end = 15;
        } else if (block == 2) {
            start = 15; end = 30;
        } else if (block == 3) {
            start = 30; end = 45;
        } else if (block == 4) {
            start = 45; end = 60;
        }
        return new int[]{start, end};
    }


    public Date getTimestamp() {
        return timestamp;
    }

    public int[] getBatteryVoltage() {
        return batteryVoltage;
    }

    public int[] getBatteryTemp() {
        return batteryTemp;
    }

    public int[] getLoadingAmperage() {
        return loadingAmperage;
    }

    public int[] getChargerTemp() {
        return chargerTemp;
    }

    public int getCurrDrivingAmperage() {
        return currDrivingAmperage;
    }
}
