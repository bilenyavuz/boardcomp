package org.frau_uas.polocockpit.statistics;

import android.app.Activity;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineDataSet;

import org.frau_uas.polocockpit.functions.Statistics;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.realm.Realm;

public class StatisticsStorage {

    // hier werden 3600(!) Objekte pro Stunde drin erstellt. Bessere Lösung überlegen!
    private List<StatsElement> statsList = new ArrayList<>();
    private static StatisticsStorage instance;
    private DateFormat df = new SimpleDateFormat("dd.MM HH:mm");

    private StatisticsStorage() {
        readSavedData();
    }

    public static StatisticsStorage getInstance() {
        if (StatisticsStorage.instance == null) {
            instance = new StatisticsStorage();
        }
        return instance;
    }

    protected void addElementToStorage(StatsElement statsElement, Activity activity) {
        /*if (statsElement != null) {

            Realm.init(activity);
            Realm realm = Realm.getDefaultInstance();

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    StatsElement e = new StatsElement();
                    e.setBatteryTemp(statsElement.getBatteryTemp());
                    e.setBatteryVoltage(statsElement.getBatteryVoltage());
                    e.setChargerTemp(statsElement.getChargerTemp());
                    e.setCurrDrivingAmperage(statsElement.getCurrDrivingAmperage());
                    e.setLoadingAmperage(statsElement.getLoadingAmperage());
                    e.setTimestamp(statsElement.getTimestamp());

                    realm.insertOrUpdate(e);
                }
            });


            statsList.add(statsElement);
        }*/
    }






    //TODO OBSOLETE BECAUSE DATA NOW IS SAVED IN REALM
    private void readSavedData() {
        //TODO READ SAVED DATA
        // THIS IS TESTDATA
        statsList.add(new StatsElement(
                new Date(Calendar.getInstance().getTime().getTime() - 50000)
                , new int[60]
                , new int[60]
                , new int[4]
                , new int[4]
                , 8
        ));
        statsList.add(new StatsElement(
                new Date(Calendar.getInstance().getTime().getTime() - 40000)
                , new int[60]
                , new int[60]
                , new int[4]
                , new int[4]
                , 7
        ));
        statsList.add(new StatsElement(
                new Date(Calendar.getInstance().getTime().getTime() - 30000)
                , new int[60]
                , new int[60]
                , new int[4]
                , new int[4]
                , 5
        ));
        statsList.add(new StatsElement(
                new Date(Calendar.getInstance().getTime().getTime() - 20000)
                , new int[60]
                , new int[60]
                , new int[4]
                , new int[4]
                , 11
        ));
        statsList.add(new StatsElement(
                new Date(Calendar.getInstance().getTime().getTime() - 10000)
                , new int[60]
                , new int[60]
                , new int[4]
                , new int[4]
                , 9
        ));
        statsList.add(new StatsElement(
                new Date(Calendar.getInstance().getTime().getTime())
                , new int[60]
                , new int[60]
                , new int[4]
                , new int[4]
                , 4
        ));
    }



    // Methods for getting charting data
    public Object[] getDrivingAmperageChartData() {
        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<String> xAxis = new ArrayList<>();
        Object[] res = new Object[2];

        for (int i = 0; i < statsList.size(); i++) {
            StatsElement tmp = statsList.get(i);
            xAxis.add(df.format(tmp.getTimestamp()));
            entries.add(new Entry(i, tmp.getCurrDrivingAmperage()));
        }
        res[0] = entries;
        res[1] = xAxis;

        return res;
    }
}
