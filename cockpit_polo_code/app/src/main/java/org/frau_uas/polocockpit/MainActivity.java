package org.frau_uas.polocockpit;

import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import org.frau_uas.polocockpit.functions.BatteryInfo;
import org.frau_uas.polocockpit.functions.CockpitFunction;
import org.frau_uas.polocockpit.functions.Fpm;
import org.frau_uas.polocockpit.functions.MusicPlayer;
import org.frau_uas.polocockpit.functions.Navi;
import org.frau_uas.polocockpit.functions.Radio;
import org.frau_uas.polocockpit.functions.Statistics;
import org.frau_uas.polocockpit.statistics.StatisticsReceiver;
import org.frau_uas.polocockpit.statistics.StatsElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private MainRecyclerAdapter adapter;
    private ImageButton batteryButton;
    private ImageView imageViewStatsLink;
    private boolean charging = false;

    final Statistics stats = new Statistics(this);
    final BatteryInfo batteryInfo = new BatteryInfo(this);
    CockpitFunction tmp;

    private Navi navi;

    // for navi
    private Bundle savedInstanceState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideSystemUI();
        setContentView(R.layout.activity_main);

        this.savedInstanceState = savedInstanceState;


        initRecyclers();
        animateBattery();
        initQuickStats();
        initCarListener();
    }

    private void initCarListener() {
        StatisticsReceiver receiver = StatisticsReceiver.init(this);

    }

    private void initRecyclers() {
        final MusicPlayer musicplayer = new MusicPlayer(this);
        final Fpm fpm = new Fpm(this);
        navi = new Navi(this);

        List<CockpitFunction> functionList = new ArrayList<>();
        functionList.add(musicplayer);
        functionList.add(new Radio(this));
        functionList.add(fpm);
        functionList.add(navi);


        RecyclerView mainRecycler = findViewById(R.id.mainRecycler);
        mainRecycler.setLayoutManager(
                new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));

        mainRecycler.setHasFixedSize(true);
        adapter = new MainRecyclerAdapter(navi);
        mainRecycler.setAdapter(adapter);

        RecyclerView functionRecycler = findViewById(R.id.functionRecycler);
        functionRecycler.setLayoutManager(
                new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));

        functionRecycler.setHasFixedSize(true);
        functionRecycler.setAdapter(new FunctionRecyclerAdapter(functionList, navi, adapter));
    }

    private void animateBattery() {

        int delay = 5000;
        AnimationDrawable batteryAnim = new AnimationDrawable();
        batteryAnim.addFrame(getDrawable(R.drawable.battery_100), delay);
        batteryAnim.addFrame(getDrawable(R.drawable.battery_90), delay);
        batteryAnim.addFrame(getDrawable(R.drawable.battery_80), delay);
        batteryAnim.addFrame(getDrawable(R.drawable.battery_60), delay);
        batteryAnim.addFrame(getDrawable(R.drawable.battery_50), delay);
        batteryAnim.addFrame(getDrawable(R.drawable.battery_30), delay);
        batteryAnim.addFrame(getDrawable(R.drawable.battery_20), delay);
        batteryAnim.addFrame(getDrawable(R.drawable.battery_alert), delay);
        batteryAnim.addFrame(getDrawable(R.drawable.battery_charging_20), delay);
        batteryAnim.addFrame(getDrawable(R.drawable.battery_charging_30), delay);
        batteryAnim.addFrame(getDrawable(R.drawable.battery_charging_50), delay);
        batteryAnim.addFrame(getDrawable(R.drawable.battery_charging_60), delay);
        batteryAnim.addFrame(getDrawable(R.drawable.battery_charging_80), delay);
        batteryAnim.addFrame(getDrawable(R.drawable.battery_charging_90), delay);
        batteryAnim.addFrame(getDrawable(R.drawable.battery_charging_100), delay);
        batteryButton = findViewById(R.id.imageButton_battery);
        batteryButton.setImageDrawable(batteryAnim);
        batteryAnim.start();

        TimerTask timerTask = new TimerTask() {

            @Override
            public void run() {
                disableCharing();
                try {
                    Thread.sleep(34000);
                    enableCharging();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        new Timer().scheduleAtFixedRate(timerTask, 0, (batteryAnim.getNumberOfFrames()*5000));

        }

    private void initQuickStats() {
        final TextView consumption = findViewById(R.id.textView_quick_stats_power_val);
        final TextView range = findViewById(R.id.textView_quick_stats_range_val);
        final TextView driven = findViewById(R.id.textView_quick_stats_driven_val);

        final int i = 30;

        final Random random = new Random();
        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {

            @Override
            public void run() {
                if (charging) {
                    setText(range, "---");
                    setText(driven, "---");
                    return;
                }
                String consumptionText = consumption.getText().toString();
                StringBuilder consumptionBuilder = new StringBuilder(consumptionText);
                consumptionBuilder
                        .setCharAt(1, String.valueOf(random.nextInt(4)).charAt(0));
                setText(consumption, consumptionBuilder.toString());

                int rand = random.nextInt(1)+1;

                String rangeString = range.getText().toString();
                if (rangeString.equals("---") || rangeString.equals("")) rangeString = "150";
                int rangeVal = Integer.parseInt(rangeString.replaceAll("[\\D]", ""));

                if (rangeVal <= 2) enableCharging();

                rangeVal = rangeVal - rand;
                setText(range, String.valueOf(rangeVal));

                String drivenString = driven.getText().toString();
                if (drivenString.equals("---") || drivenString.equals("")) drivenString = "0";
                int drivenVal = Integer.parseInt(drivenString.replaceAll("[\\D]", ""));
                drivenVal = drivenVal + rand;
                setText(driven, String.valueOf(drivenVal));
            }
        };
        timer.scheduleAtFixedRate(timerTask, 0, 250);

        batteryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (adapter.getCockpitFunction() == batteryInfo) {
                    adapter.setCockpitFunction(tmp);
                    tmp = null;
                } else if (adapter.getCockpitFunction() == stats) {
                    adapter.setCockpitFunction(batteryInfo);
                } else {
                    tmp = adapter.getCockpitFunction();
                    adapter.setCockpitFunction(batteryInfo);
                }
                adapter.notifyItemChanged(0);
            }
        });

        imageViewStatsLink = findViewById(R.id.imageView_button_normal_stats);
        imageViewStatsLink.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (adapter.getCockpitFunction() == stats) {
                            adapter.setCockpitFunction(tmp);
                            tmp = null;
                        } else if (adapter.getCockpitFunction() == batteryInfo) {
                            adapter.setCockpitFunction(stats);
                        } else {
                            tmp = adapter.getCockpitFunction();
                            adapter.setCockpitFunction(stats);
                        }
                        adapter.notifyItemChanged(0);
                    }
                }
        );
    }


    public void notifyCarUpdate(StatsElement e) {
        // TODO update right site
        CockpitFunction curr = adapter.getCockpitFunction();
        if (curr instanceof CarUpdateListener) {
            ((CarUpdateListener) curr).notifyCarUpdate(e);
        }
    }


    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);

        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
    }

    // because tasks cant write on Views
    private void setText(final TextView text,final String value){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                text.setText(value);
            }
        });
    }
    private void disableCharing() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                charging = false;
            }
        });
    }
    private void enableCharging() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                charging = true;
            }
        });
    }



    // for navi
    public Bundle getSavedInstanceState() {
        return savedInstanceState;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 300) {

        } else {
            navi.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        navi.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        navi.onLowMemory();
    }
}
