package org.frau_uas.polocockpit.functions;

import android.app.Activity;
import android.os.RemoteException;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.hzbhd.midwareproxy.radio.RadioManager;
import com.hzbhd.midwareproxy.radio.aidl.RDSInfoChangeListener;
import com.hzbhd.midwareproxy.radio.aidl.RadioInfoChangeListener;

import org.frau_uas.polocockpit.R;

public class Radio extends CockpitFunction implements View.OnClickListener, View.OnLongClickListener {

    public TextView lblCurrStation_NORDS;
    public TextView lblRTInfo;
    public RadioManager radioManager;
    private RDSInfoChangeListener mRDSInfoChangeListener;
    private Button freqUp;
    private Button freqDown;
    private Button tuneUp;
    private Button tuneDown;
    private Button prebtn0;
    private Button prebtn1;
    private Button prebtn2;
    private Button prebtn3;
    private Button prebtn4;
    private Button prebtn5;

    private String[] presets;

    private int mFreq, moldFreq=0;

    public  Radio(Activity activity){
        super(activity);
    }
    @Override
    public void startCockpitFunction() {

        try {
            this.radioManager = RadioManager.getRadioManager();
        } catch (Exception e) {
        }

        this.lblCurrStation_NORDS = (TextView) cardView.findViewById(R.id.frequency);
        this.lblRTInfo = (TextView) cardView.findViewById(R.id.RTInfo);


        try {
            if(radioManager != null) {
                String tmpstr = radioManager.getCurrentFreq();
                this.lblCurrStation_NORDS.setText(tmpstr);
                mFreq = (int) Float.valueOf(tmpstr.trim()).floatValue() * 100;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        try {
            if(radioManager != null)
                this.lblRTInfo.setText(radioManager.getCurRTInfo());
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        try {
            if(radioManager != null)
                presets=radioManager.getPresetFreqList(1);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        freqUp = (Button) cardView.findViewById(R.id.freqUp);
        freqUp.setOnClickListener(this);
        freqDown = (Button) cardView.findViewById(R.id.freqDown);
        freqDown.setOnClickListener(this);
        tuneUp = (Button) cardView.findViewById(R.id.tuneUp);
        tuneUp.setOnClickListener(this);
        tuneDown = (Button) cardView.findViewById(R.id.tuneDown);
        tuneDown.setOnClickListener(this);


        prebtn0=cardView.findViewById(R.id.pre1);
        prebtn0.setOnClickListener(this);
        prebtn0.setOnLongClickListener(this);
        prebtn1=cardView.findViewById(R.id.pre2);
        prebtn1.setOnClickListener(this);
        prebtn1.setOnLongClickListener(this);
        prebtn2=cardView.findViewById(R.id.pre3);
        prebtn2.setOnClickListener(this);
        prebtn2.setOnLongClickListener(this);
        prebtn3=cardView.findViewById(R.id.pre4);
        prebtn3.setOnClickListener(this);
        prebtn3.setOnLongClickListener(this);
        prebtn4=cardView.findViewById(R.id.pre5);
        prebtn4.setOnClickListener(this);
        prebtn4.setOnLongClickListener(this);
        prebtn5=cardView.findViewById(R.id.pre6);
        prebtn5.setOnClickListener(this);
        prebtn5.setOnLongClickListener(this);
        setPresets();

        if(presets != null && presets.length >= 6) {
            for (int i = 0; i < 6; i++) {
                Log.e("Preset(" + i + ") = ", presets[i].toString());
            }
        }


        try {
//            this.radioManager.setServiceConnectionStateListener(this.mServiceConnectionState);
            if(radioManager != null)
                this.radioManager.setRadioInfoChangeListener(this.mRadioInfoChangeListener);
//            this.radioManager.setRDSInfoChangeListener(this.mRDSInfoChangeListener);
        } catch (Exception e2) {
        }

        if(radioManager != null) {
            try {
                Log.e("CurPSInfo", radioManager.getCurPSInfo());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            try {
                Log.e("CurPTYInfo", radioManager.getCurPTYInfo());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            try {
                Log.e("CurPSInfo", radioManager.getCurPSInfo());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            try {
                Log.e("RDSType", radioManager.getRDSType());
            } catch (RemoteException e) {
                e.printStackTrace();
            }

            Log.e("PresetPSName(1)", radioManager.getPresetPSName(1));
        }

    }

    @Override
    public void stopCockpitFunction() {

    }

    @Override
    public CardView getCardView() {
        return cardView;
    }



    @Override
    public int getLayout() {
        return R.layout.card_view_radio;
    }


    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.freqUp:
                try {
                    moldFreq=0;
                    radioManager.seekUp();
                    Log.e("Radio", "seekUp");
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;

            case R.id.freqDown:
                try {
                    moldFreq=0;
                    radioManager.seekDown();
                    Log.e("Radio", "seekDown");
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.tuneUp:
                try {
                    moldFreq=0;
                    radioManager.tuneUp();
                    Log.e("Radio", "tuneUp");
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.tuneDown:
                try {
                    moldFreq=0;
                    radioManager.tuneDown();
                    Log.e("Radio", "tuneDown");
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.pre1:
                try {
                    radioManager.playPresetFreq(0);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.pre2:
                try {
                    radioManager.playPresetFreq(1);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.pre3:
                try {
                    radioManager.playPresetFreq(2);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.pre4:
                try {
                    radioManager.playPresetFreq(3);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.pre5:
                try {
                    radioManager.playPresetFreq(4);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.pre6:
                try {
                    radioManager.playPresetFreq(5);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
            default:
                Log.e("Radio", "default");
                break;
        }
    }
    private RadioInfoChangeListener mRadioInfoChangeListener = new RadioInfoChangeListener() {
        private TextView lblAudio;
        private TextView lblLocal;

        public void radioChannelTypeChanged(String channelTypeValue) {
        }

        public void radioSignalStrengthChanged(int signal) {
            Log.e("Radio", "signal==" + signal);
        }

        public void radioFreqChanged(String freq) {
            lblCurrStation_NORDS.setText(freq);
            mFreq=(int)Float.valueOf(freq.trim()).floatValue()*100;
        }

        public void radioBandTypeChanged(String bandType) {
        }

        public void radioAutoSearchStatusChanged(boolean flag) {
        }

        public void radioAutoSearchWithPreviewStatusChanged(boolean flag) {
        }

        public void radioStepUpStatusChanged(boolean flag) {
        }

        public void radioStepDownStatusChanged(boolean flag) {
        }

        public void radioPresetFreqListChanged(String[] list) {
            if (list != null) {
                presets = list;
                setPresets();
            }
        }

        public void radioPresetFreqPSListChanged(String[] list) {
        }

        public void radioPresetFreqPSNameChanged(int presetPsIdx, String presetPSName) {
        }

        public void radioStereoChanged(int mStereo) {
        }

        public void radioLocalChanged(int mLocal) {
        }
    };
    private void setPresets() {
        if(presets != null && presets.length >= 6) {
            prebtn0.setText(String.format("%.2f", Float.valueOf(presets[0].toString()) / 100.0));
            prebtn1.setText(String.format("%.2f", Float.valueOf(presets[1].toString()) / 100.0));
            prebtn2.setText(String.format("%.2f", Float.valueOf(presets[2].toString()) / 100.0));
            prebtn3.setText(String.format("%.2f", Float.valueOf(presets[3].toString()) / 100.0));
            prebtn4.setText(String.format("%.2f", Float.valueOf(presets[4].toString()) / 100.0));
            prebtn5.setText(String.format("%.2f", Float.valueOf(presets[5].toString()) / 100.0));
        }
    }


    public boolean onLongClick(View v) {
        switch (v.getId()) {

            case R.id.pre1:
                try {
                    radioManager.playPresetFreq(21);
                    radioManager.getPresetFreqList(0);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.pre2:
                try {
                    radioManager.playPresetFreq(22);
                    radioManager.getPresetFreqList(0);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.pre3:
                try {
                    radioManager.playPresetFreq(23);
                    radioManager.getPresetFreqList(0);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.pre4:
                try {
                    radioManager.playPresetFreq(24);
                    radioManager.getPresetFreqList(0);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.pre5:
                try {
                    radioManager.playPresetFreq(25);
                    radioManager.getPresetFreqList(0);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.pre6:
                try {
                    radioManager.playPresetFreq(26);
                    radioManager.getPresetFreqList(0);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
            default:
                Log.e("Radio", "Long Click default");
                break;
        }
        return false;
    }
}
