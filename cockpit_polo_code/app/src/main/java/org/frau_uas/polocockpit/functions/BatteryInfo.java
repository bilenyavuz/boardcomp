package org.frau_uas.polocockpit.functions;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.widget.ImageView;
import android.widget.TextView;

import org.frau_uas.polocockpit.CarUpdateListener;
import org.frau_uas.polocockpit.R;
import org.frau_uas.polocockpit.statistics.StatsElement;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Timer;
import java.util.TimerTask;

public class BatteryInfo extends CockpitFunction implements CarUpdateListener {

    private boolean initialized = false;

    // Battery Info
    private TextView[] cellVoltages = new TextView[60];
    private TextView[] cellTemperatures = new TextView[60];
    private TextView[] voltageSums = new TextView[4];
    private TextView[] voltageMaxes = new TextView[4];
    private TextView[] voltageMins = new TextView[4];
    private ImageView[] percentageImgs = new ImageView[4];

    // Charging Info
    private TextView[] chargeAmperes = new TextView[4];
    private TextView[] chargeTemps = new TextView[4];
    private ImageView[] chargingImg = new ImageView[4];

    public BatteryInfo(Activity activity) {
        super(activity);
    }

    @Override
    public void startCockpitFunction() {
        if (!initialized) initUI();
    }

    @Override
    public void stopCockpitFunction() {

    }

    @Override
    public CardView getCardView() {
        return cardView;
    }

    @Override
    public int getLayout() {
        return R.layout.card_view_battery_info;
    }


    @Override
    public void notifyCarUpdate(StatsElement e) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!initialized) return;

                for (int i = 0; i < 60; i++) {
                    cellVoltages[i].setText(Integer.toString(e.getBatteryVoltage()[i]));
                    cellTemperatures[i].setText(Integer.toString(e.getBatteryTemp()[i]));
                }
                for (int i = 0; i < 4; i++) {
                    voltageSums[i].setText(Integer.toString(e.getSumVoltagePerBlock(i)));
                    voltageMaxes[i].setText(Integer.toString(e.getMaxVoltagePerBlock(i)));
                    voltageMins[i].setText(Integer.toString(e.getMinVoltagePerBlock(i)));
                    percentageImgs[i].setImageDrawable(activity.getDrawable(e.getPercentageDrawable(i)));
                    if (e.isLoading()) {
                        chargingImg[i].setImageDrawable(
                                activity.getDrawable(R.drawable.ic_power_black_24dp));
                        chargeAmperes[i].setText(Integer.toString(e.getLoadingAmperage()[i]));
                        chargeTemps[i].setText(Integer.toString(e.getChargerTemp()[i]));
                    } else {
                        chargingImg[i].setImageDrawable(
                                activity.getDrawable(R.drawable.ic_baseline_power_off_24px));
                        chargeAmperes[i].setText("---");
                        chargeTemps[i].setText("---");
                    }
                }

            }
        });
    }


    private void initUI() {

        for (int i = 0; i < 60; i++) {
            cellVoltages[i] = getCardView().findViewById(
                    getResId("vc"+Integer.toString(i+1), R.id.class));

            cellTemperatures[i] = getCardView().findViewById(
                    getResId("tc"+Integer.toString(i+1), R.id.class));
        }

        for (int i = 0; i < 4; i++) {
            voltageSums[i] = getCardView().findViewById(
                    getResId("vsumb"+Integer.toString(i+1), R.id.class));

            voltageMaxes[i] = getCardView().findViewById(
                    getResId("vmaxb"+Integer.toString(i+1), R.id.class));

            voltageMins[i] = getCardView().findViewById(
                    getResId("vminb"+Integer.toString(i+1), R.id.class));

            percentageImgs[i] = getCardView().findViewById(
                    getResId("img_pb"+Integer.toString(i+1), R.id.class));

            chargingImg[i] = getCardView().findViewById(
                    getResId("img_chb"+Integer.toString(i+1), R.id.class));

            chargeAmperes[i] = getCardView().findViewById(
                    getResId("cab"+Integer.toString(i+1), R.id.class));

            chargeTemps[i] = getCardView().findViewById(
                    getResId("ctb"+Integer.toString(i+1), R.id.class));
        }
        initialized = true;
    }


    private int getResId(String resName, Class<?> c) {

        try {
            Field idField = c.getDeclaredField(resName);
            return idField.getInt(idField);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
}
