package org.frau_uas.polocockpit.functions;

import android.graphics.Color;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.frau_uas.polocockpit.FunctionRecyclerAdapter;
import org.frau_uas.polocockpit.R;
import org.frau_uas.polocockpit.api.model.music.PlayList;
import org.frau_uas.polocockpit.api.model.music.Track;
import org.frau_uas.polocockpit.api.service.MusicLibrary;
import org.frau_uas.polocockpit.api.service.MusicPlayerService;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class TrackListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private MusicPlayerService playerService;
    private RecyclerView recyclerView;

    public TrackListAdapter(RecyclerView recyclerView) {
        playerService = MusicPlayerService.getCurrent();
        this.recyclerView = recyclerView;

    }



    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, final int i) {
        int layout = R.layout.card_view_track;
        View v = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(layout, viewGroup, false);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               playerService.play(recyclerView.getChildAdapterPosition(view));
            }
        });
        return new ViewHolderTrack(v,i);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ((ViewHolderTrack) viewHolder).fill(playerService.getTrackList().get(i));
    }

    @Override
    public int getItemCount() {
        return playerService.getTrackList().size();
    }


    public class ViewHolderTrack extends RecyclerView.ViewHolder {

        private TextView textViewInterpret;
        private TextView textViewSong;
        private TextView textViewDuration;
        public int index;

        public ViewHolderTrack(@NonNull View itemView, int i) {
            super(itemView);
            textViewInterpret = itemView.findViewById(R.id.textView_interpret);
            textViewSong = itemView.findViewById(R.id.textView_song);
            textViewDuration = itemView.findViewById(R.id.textView_duration);
            index = i;
        }

        void fill(Track track) {
            textViewInterpret.setText(track.getArtist());
            textViewSong.setText(track.getTitle());
            textViewDuration.setText(track.getDuration());
        }

    }


}
