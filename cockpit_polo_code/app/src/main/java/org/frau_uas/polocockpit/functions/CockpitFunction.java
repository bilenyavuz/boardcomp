package org.frau_uas.polocockpit.functions;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.text.Layout;

public abstract class CockpitFunction {

    CardView cardView;
    Activity activity;

    public CockpitFunction(Activity activity) {
        this.activity = activity;
    }

    public void setCardView(CardView cardView) {
        this.cardView = cardView;
    }

    public abstract void startCockpitFunction();

    public abstract void stopCockpitFunction();

    public abstract CardView getCardView();

    public abstract int getLayout();
}
