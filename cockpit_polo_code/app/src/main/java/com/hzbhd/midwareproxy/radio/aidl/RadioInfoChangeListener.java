package com.hzbhd.midwareproxy.radio.aidl;

public interface RadioInfoChangeListener {
    void radioAutoSearchStatusChanged(boolean z);

    void radioAutoSearchWithPreviewStatusChanged(boolean z);

    void radioBandTypeChanged(String str);

    void radioChannelTypeChanged(String str);

    void radioFreqChanged(String str);

    void radioLocalChanged(int i);

    void radioPresetFreqListChanged(String[] strArr);

    void radioPresetFreqPSListChanged(String[] strArr);

    void radioPresetFreqPSNameChanged(int i, String str);

    void radioSignalStrengthChanged(int i);

    void radioStepDownStatusChanged(boolean z);

    void radioStepUpStatusChanged(boolean z);

    void radioStereoChanged(int i);
}
