package com.hzbhd.midwareproxy.radio.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IRadioManager extends IInterface {

    public static abstract class Stub extends Binder implements IRadioManager {
        private static final String DESCRIPTOR = "com.hzbhd.midwareproxy.radio.aidl.IRadioManager";
        static final int TRANSACTION_autoSearch = 19;
        static final int TRANSACTION_autoSearchWithPreview = 20;
        static final int TRANSACTION_bandChange = 40;
        static final int TRANSACTION_enableAFSwitch = 59;
        static final int TRANSACTION_enableCTSwitch = 63;
        static final int TRANSACTION_enableEONSwitch = 61;
        static final int TRANSACTION_enablePSSwitch = 69;
        static final int TRANSACTION_enablePTYSwitch = 67;
        static final int TRANSACTION_enableRDSSwitch = 55;
        static final int TRANSACTION_enableRegionSwitch = 65;
        static final int TRANSACTION_enableTASwitch = 57;
        static final int TRANSACTION_getAFSeekStatus = 12;
        static final int TRANSACTION_getAFSwitchStatus = 60;
        static final int TRANSACTION_getAutoSearchStatus = 8;
        static final int TRANSACTION_getAutoSearchWithPreviewStatus = 9;
        static final int TRANSACTION_getCTSwitchStatus = 64;
        static final int TRANSACTION_getCurPSInfo = 5;
        static final int TRANSACTION_getCurPTYInfo = 6;
        static final int TRANSACTION_getCurRTInfo = 7;
        static final int TRANSACTION_getCurrPresetFreq = 52;
        static final int TRANSACTION_getCurrentArea = 32;
        static final int TRANSACTION_getCurrentBand = 24;
        static final int TRANSACTION_getCurrentFreq = 29;
        static final int TRANSACTION_getDefaultArea = 34;
        static final int TRANSACTION_getEONStatus = 18;
        static final int TRANSACTION_getEONSwitchStatus = 62;
        static final int TRANSACTION_getEWSStatus = 17;
        static final int TRANSACTION_getPSSwitchStatus = 70;
        static final int TRANSACTION_getPTYSeekStatus = 14;
        static final int TRANSACTION_getPTYSwitchStatus = 68;
        static final int TRANSACTION_getPresetFreqList = 47;
        static final int TRANSACTION_getPresetFreqListCount = 46;
        static final int TRANSACTION_getPresetFreqPSList = 49;
        static final int TRANSACTION_getPresetListPSInfo = 51;
        static final int TRANSACTION_getPresetPSName = 50;
        static final int TRANSACTION_getRDSSwitchStatus = 56;
        static final int TRANSACTION_getRDSType = 80;
        static final int TRANSACTION_getRFInputMode = 27;
        static final int TRANSACTION_getRegionSwitchStatus = 66;
        static final int TRANSACTION_getSignalStrength = 2;
        static final int TRANSACTION_getStepDownStatus = 11;
        static final int TRANSACTION_getStepUpStatus = 10;
        static final int TRANSACTION_getTASeekStatus = 15;
        static final int TRANSACTION_getTAStatus = 16;
        static final int TRANSACTION_getTASwitchStatus = 58;
        static final int TRANSACTION_getTAVolume = 73;
        static final int TRANSACTION_getTAVolume_After = 77;
        static final int TRANSACTION_getTAVolume_Before = 75;
        static final int TRANSACTION_getTPSeekStatus = 13;
        static final int TRANSACTION_getTunerModel = 43;
        static final int TRANSACTION_getValidFreqList = 45;
        static final int TRANSACTION_getValidFreqListCount = 44;
        static final int TRANSACTION_getVoiceChannelType = 1;
        static final int TRANSACTION_initRadioData = 85;
        static final int TRANSACTION_isRDSStation = 4;
        static final int TRANSACTION_isTP = 3;
        static final int TRANSACTION_isValidFreq = 30;
        static final int TRANSACTION_playPresetFreq = 39;
        static final int TRANSACTION_removeRDSInfoChangeListener = 84;
        static final int TRANSACTION_removeRadioInfoChangeListener = 82;
        static final int TRANSACTION_restoreFactorySettings = 54;
        static final int TRANSACTION_saveAppdata = 86;
        static final int TRANSACTION_searchSpecialRDSProgram = 79;
        static final int TRANSACTION_searchStepDown = 23;
        static final int TRANSACTION_searchStepUp = 22;
        static final int TRANSACTION_seekDown = 38;
        static final int TRANSACTION_seekUp = 37;
        static final int TRANSACTION_sendAppdata = 87;
        static final int TRANSACTION_setCurrPresetFreq = 53;
        static final int TRANSACTION_setCurrentArea = 31;
        static final int TRANSACTION_setCurrentBand = 25;
        static final int TRANSACTION_setCurrentFreq = 28;
        static final int TRANSACTION_setDefaultArea = 33;
        static final int TRANSACTION_setPresetFreqList = 48;
        static final int TRANSACTION_setRDSAFLevel = 78;
        static final int TRANSACTION_setRDSInfoChangeListener = 83;
        static final int TRANSACTION_setRDSType = 71;
        static final int TRANSACTION_setRFInputMode = 26;
        static final int TRANSACTION_setRadioInfoChangeListener = 81;
        static final int TRANSACTION_setTAVolume = 72;
        static final int TRANSACTION_setTAVolume_After = 76;
        static final int TRANSACTION_setTAVolume_Before = 74;
        static final int TRANSACTION_setTunerReg = 41;
        static final int TRANSACTION_setTunerRegWithModel = 42;
        static final int TRANSACTION_stopSearch = 21;
        static final int TRANSACTION_tuneDown = 36;
        static final int TRANSACTION_tuneUp = 35;

        private static class Proxy implements IRadioManager {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            public String getVoiceChannelType() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getSignalStrength() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isTP() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(3, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isRDSStation() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(4, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getCurPSInfo() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(5, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getCurPTYInfo() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(6, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getCurRTInfo() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(7, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean getAutoSearchStatus() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(8, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean getAutoSearchWithPreviewStatus() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(9, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean getStepUpStatus() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(10, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean getStepDownStatus() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(11, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean getAFSeekStatus() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(12, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean getTPSeekStatus() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(13, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean getPTYSeekStatus() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(14, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean getTASeekStatus() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(15, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean getTAStatus() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(16, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean getEWSStatus() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(17, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean getEONStatus() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(18, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean autoSearch(int searchType) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(searchType);
                    this.mRemote.transact(19, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean autoSearchWithPreview(int searchType) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(searchType);
                    this.mRemote.transact(20, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean stopSearch() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(21, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean searchStepUp(int stepType) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(stepType);
                    this.mRemote.transact(22, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean searchStepDown(int stepType) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(stepType);
                    this.mRemote.transact(23, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getCurrentBand() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(24, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setCurrentBand(int bandType) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(bandType);
                    this.mRemote.transact(25, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setRFInputMode(int rfMode) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(rfMode);
                    this.mRemote.transact(26, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getRFInputMode() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(27, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setCurrentFreq(String freq) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(freq);
                    this.mRemote.transact(28, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getCurrentFreq() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(29, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isValidFreq() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(30, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setCurrentArea(int area) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(area);
                    this.mRemote.transact(31, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getCurrentArea() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(32, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setDefaultArea() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(33, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getDefaultArea() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(34, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean tuneUp() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(35, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean tuneDown() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(36, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean seekUp() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(37, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean seekDown() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(38, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void playPresetFreq(int index) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(index);
                    this.mRemote.transact(39, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void bandChange() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(40, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setTunerReg(byte regIndex, byte regValue) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeByte(regIndex);
                    _data.writeByte(regValue);
                    this.mRemote.transact(41, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setTunerRegWithModel(byte tunerModel) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeByte(tunerModel);
                    this.mRemote.transact(42, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public byte getTunerModel() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(43, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readByte();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getValidFreqListCount(int bandType) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(bandType);
                    this.mRemote.transact(44, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String[] getValidFreqList(int bandType) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(bandType);
                    this.mRemote.transact(45, _data, _reply, 0);
                    _reply.readException();
                    return _reply.createStringArray();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getPresetFreqListCount(int bandType) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(bandType);
                    this.mRemote.transact(46, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String[] getPresetFreqList(int bandType) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(bandType);
                    this.mRemote.transact(47, _data, _reply, 0);
                    _reply.readException();
                    return _reply.createStringArray();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setPresetFreqList(int bandType, int index, String currentFreq) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(bandType);
                    _data.writeInt(index);
                    _data.writeString(currentFreq);
                    this.mRemote.transact(48, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String[] getPresetFreqPSList(int bandType) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(bandType);
                    this.mRemote.transact(49, _data, _reply, 0);
                    _reply.readException();
                    return _reply.createStringArray();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getPresetPSName(int presetIdx) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(presetIdx);
                    this.mRemote.transact(50, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void getPresetListPSInfo() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(51, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getCurrPresetFreq(int bandType) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(bandType);
                    this.mRemote.transact(52, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setCurrPresetFreq(int bandType) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(bandType);
                    this.mRemote.transact(53, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean restoreFactorySettings() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(54, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean enableRDSSwitch(boolean enable) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (enable) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(55, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean getRDSSwitchStatus() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(56, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean enableTASwitch(boolean enable) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (enable) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(57, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean getTASwitchStatus() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(58, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean enableAFSwitch(boolean enable) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (enable) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(59, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean getAFSwitchStatus() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(60, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean enableEONSwitch(boolean enable) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (enable) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(61, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean getEONSwitchStatus() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(62, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean enableCTSwitch(boolean enable) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (enable) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(63, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean getCTSwitchStatus() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(64, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean enableRegionSwitch(boolean enable) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (enable) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(65, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean getRegionSwitchStatus() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(66, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean enablePTYSwitch(boolean enable) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (enable) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(67, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean getPTYSwitchStatus() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(68, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean enablePSSwitch(boolean enable) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (enable) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(69, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean getPSSwitchStatus() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(70, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setRDSType(byte rdsType) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeByte(rdsType);
                    this.mRemote.transact(71, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setTAVolume(int vol_before, int vol, int vol_after) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(vol_before);
                    _data.writeInt(vol);
                    _data.writeInt(vol_after);
                    this.mRemote.transact(72, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getTAVolume() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(73, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setTAVolume_Before(int vol) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(vol);
                    this.mRemote.transact(74, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getTAVolume_Before() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(75, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setTAVolume_After(int vol) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(vol);
                    this.mRemote.transact(76, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getTAVolume_After() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getTAVolume_After, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setRDSAFLevel(byte level) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeByte(level);
                    this.mRemote.transact(Stub.TRANSACTION_setRDSAFLevel, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean searchSpecialRDSProgram(int ptyType) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(ptyType);
                    this.mRemote.transact(Stub.TRANSACTION_searchSpecialRDSProgram, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getRDSType() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(80, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setRadioInfoChangeListener(IRadioInfoChangeListener listener) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(listener != null ? listener.asBinder() : null);
                    this.mRemote.transact(81, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void removeRadioInfoChangeListener(IRadioInfoChangeListener listener) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(listener != null ? listener.asBinder() : null);
                    this.mRemote.transact(Stub.TRANSACTION_removeRadioInfoChangeListener, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setRDSInfoChangeListener(IRDSInfoChangeListener listener) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(listener != null ? listener.asBinder() : null);
                    this.mRemote.transact(Stub.TRANSACTION_setRDSInfoChangeListener, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void removeRDSInfoChangeListener(IRDSInfoChangeListener listener) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(listener != null ? listener.asBinder() : null);
                    this.mRemote.transact(Stub.TRANSACTION_removeRDSInfoChangeListener, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void initRadioData() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_initRadioData, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void saveAppdata(String filePath) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(filePath);
                    this.mRemote.transact(Stub.TRANSACTION_saveAppdata, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void sendAppdata(String filePath) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(filePath);
                    this.mRemote.transact(Stub.TRANSACTION_sendAppdata, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static IRadioManager asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
            if (iin == null || !(iin instanceof IRadioManager)) {
                return new Proxy(obj);
            }
            return (IRadioManager) iin;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            boolean _arg0;
            boolean _arg02;
            boolean _arg03;
            boolean _arg04;
            boolean _arg05;
            boolean _arg06;
            boolean _arg07;
            boolean _arg08;
            int i = 0;
            switch (code) {
                case 1:
                    data.enforceInterface(DESCRIPTOR);
                    String _result = getVoiceChannelType();
                    reply.writeNoException();
                    reply.writeString(_result);
                    return true;
                case 2:
                    data.enforceInterface(DESCRIPTOR);
                    int _result2 = getSignalStrength();
                    reply.writeNoException();
                    reply.writeInt(_result2);
                    return true;
                case 3:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result3 = isTP();
                    reply.writeNoException();
                    if (_result3) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 4:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result4 = isRDSStation();
                    reply.writeNoException();
                    if (_result4) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 5:
                    data.enforceInterface(DESCRIPTOR);
                    String _result5 = getCurPSInfo();
                    reply.writeNoException();
                    reply.writeString(_result5);
                    return true;
                case 6:
                    data.enforceInterface(DESCRIPTOR);
                    String _result6 = getCurPTYInfo();
                    reply.writeNoException();
                    reply.writeString(_result6);
                    return true;
                case 7:
                    data.enforceInterface(DESCRIPTOR);
                    String _result7 = getCurRTInfo();
                    reply.writeNoException();
                    reply.writeString(_result7);
                    return true;
                case 8:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result8 = getAutoSearchStatus();
                    reply.writeNoException();
                    if (_result8) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 9:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result9 = getAutoSearchWithPreviewStatus();
                    reply.writeNoException();
                    if (_result9) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 10:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result10 = getStepUpStatus();
                    reply.writeNoException();
                    if (_result10) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 11:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result11 = getStepDownStatus();
                    reply.writeNoException();
                    if (_result11) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 12:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result12 = getAFSeekStatus();
                    reply.writeNoException();
                    if (_result12) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 13:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result13 = getTPSeekStatus();
                    reply.writeNoException();
                    if (_result13) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 14:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result14 = getPTYSeekStatus();
                    reply.writeNoException();
                    if (_result14) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 15:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result15 = getTASeekStatus();
                    reply.writeNoException();
                    if (_result15) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 16:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result16 = getTAStatus();
                    reply.writeNoException();
                    if (_result16) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 17:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result17 = getEWSStatus();
                    reply.writeNoException();
                    if (_result17) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 18:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result18 = getEONStatus();
                    reply.writeNoException();
                    if (_result18) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 19:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result19 = autoSearch(data.readInt());
                    reply.writeNoException();
                    if (_result19) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 20:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result20 = autoSearchWithPreview(data.readInt());
                    reply.writeNoException();
                    if (_result20) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 21:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result21 = stopSearch();
                    reply.writeNoException();
                    if (_result21) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 22:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result22 = searchStepUp(data.readInt());
                    reply.writeNoException();
                    if (_result22) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 23:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result23 = searchStepDown(data.readInt());
                    reply.writeNoException();
                    if (_result23) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 24:
                    data.enforceInterface(DESCRIPTOR);
                    String _result24 = getCurrentBand();
                    reply.writeNoException();
                    reply.writeString(_result24);
                    return true;
                case 25:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result25 = setCurrentBand(data.readInt());
                    reply.writeNoException();
                    if (_result25) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 26:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result26 = setRFInputMode(data.readInt());
                    reply.writeNoException();
                    if (_result26) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 27:
                    data.enforceInterface(DESCRIPTOR);
                    String _result27 = getRFInputMode();
                    reply.writeNoException();
                    reply.writeString(_result27);
                    return true;
                case 28:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result28 = setCurrentFreq(data.readString());
                    reply.writeNoException();
                    if (_result28) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 29:
                    data.enforceInterface(DESCRIPTOR);
                    String _result29 = getCurrentFreq();
                    reply.writeNoException();
                    reply.writeString(_result29);
                    return true;
                case 30:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result30 = isValidFreq();
                    reply.writeNoException();
                    if (_result30) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 31:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result31 = setCurrentArea(data.readInt());
                    reply.writeNoException();
                    if (_result31) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 32:
                    data.enforceInterface(DESCRIPTOR);
                    String _result32 = getCurrentArea();
                    reply.writeNoException();
                    reply.writeString(_result32);
                    return true;
                case 33:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result33 = setDefaultArea();
                    reply.writeNoException();
                    if (_result33) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 34:
                    data.enforceInterface(DESCRIPTOR);
                    String _result34 = getDefaultArea();
                    reply.writeNoException();
                    reply.writeString(_result34);
                    return true;
                case 35:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result35 = tuneUp();
                    reply.writeNoException();
                    if (_result35) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 36:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result36 = tuneDown();
                    reply.writeNoException();
                    if (_result36) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 37:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result37 = seekUp();
                    reply.writeNoException();
                    if (_result37) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 38:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result38 = seekDown();
                    reply.writeNoException();
                    if (_result38) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 39:
                    data.enforceInterface(DESCRIPTOR);
                    playPresetFreq(data.readInt());
                    reply.writeNoException();
                    return true;
                case 40:
                    data.enforceInterface(DESCRIPTOR);
                    bandChange();
                    reply.writeNoException();
                    return true;
                case 41:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result39 = setTunerReg(data.readByte(), data.readByte());
                    reply.writeNoException();
                    if (_result39) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 42:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result40 = setTunerRegWithModel(data.readByte());
                    reply.writeNoException();
                    if (_result40) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 43:
                    data.enforceInterface(DESCRIPTOR);
                    byte _result41 = getTunerModel();
                    reply.writeNoException();
                    reply.writeByte(_result41);
                    return true;
                case 44:
                    data.enforceInterface(DESCRIPTOR);
                    int _result42 = getValidFreqListCount(data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result42);
                    return true;
                case 45:
                    data.enforceInterface(DESCRIPTOR);
                    String[] _result43 = getValidFreqList(data.readInt());
                    reply.writeNoException();
                    reply.writeStringArray(_result43);
                    return true;
                case 46:
                    data.enforceInterface(DESCRIPTOR);
                    int _result44 = getPresetFreqListCount(data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result44);
                    return true;
                case 47:
                    data.enforceInterface(DESCRIPTOR);
                    String[] _result45 = getPresetFreqList(data.readInt());
                    reply.writeNoException();
                    reply.writeStringArray(_result45);
                    return true;
                case 48:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result46 = setPresetFreqList(data.readInt(), data.readInt(), data.readString());
                    reply.writeNoException();
                    if (_result46) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 49:
                    data.enforceInterface(DESCRIPTOR);
                    String[] _result47 = getPresetFreqPSList(data.readInt());
                    reply.writeNoException();
                    reply.writeStringArray(_result47);
                    return true;
                case 50:
                    data.enforceInterface(DESCRIPTOR);
                    String _result48 = getPresetPSName(data.readInt());
                    reply.writeNoException();
                    reply.writeString(_result48);
                    return true;
                case 51:
                    data.enforceInterface(DESCRIPTOR);
                    getPresetListPSInfo();
                    reply.writeNoException();
                    return true;
                case 52:
                    data.enforceInterface(DESCRIPTOR);
                    String _result49 = getCurrPresetFreq(data.readInt());
                    reply.writeNoException();
                    reply.writeString(_result49);
                    return true;
                case 53:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result50 = setCurrPresetFreq(data.readInt());
                    reply.writeNoException();
                    if (_result50) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 54:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result51 = restoreFactorySettings();
                    reply.writeNoException();
                    if (_result51) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 55:
                    data.enforceInterface(DESCRIPTOR);
                    if (data.readInt() != 0) {
                        _arg08 = true;
                    } else {
                        _arg08 = false;
                    }
                    boolean _result52 = enableRDSSwitch(_arg08);
                    reply.writeNoException();
                    if (_result52) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 56:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result53 = getRDSSwitchStatus();
                    reply.writeNoException();
                    if (_result53) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 57:
                    data.enforceInterface(DESCRIPTOR);
                    if (data.readInt() != 0) {
                        _arg07 = true;
                    } else {
                        _arg07 = false;
                    }
                    boolean _result54 = enableTASwitch(_arg07);
                    reply.writeNoException();
                    if (_result54) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 58:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result55 = getTASwitchStatus();
                    reply.writeNoException();
                    if (_result55) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 59:
                    data.enforceInterface(DESCRIPTOR);
                    if (data.readInt() != 0) {
                        _arg06 = true;
                    } else {
                        _arg06 = false;
                    }
                    boolean _result56 = enableAFSwitch(_arg06);
                    reply.writeNoException();
                    if (_result56) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 60:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result57 = getAFSwitchStatus();
                    reply.writeNoException();
                    if (_result57) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 61:
                    data.enforceInterface(DESCRIPTOR);
                    if (data.readInt() != 0) {
                        _arg05 = true;
                    } else {
                        _arg05 = false;
                    }
                    boolean _result58 = enableEONSwitch(_arg05);
                    reply.writeNoException();
                    if (_result58) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 62:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result59 = getEONSwitchStatus();
                    reply.writeNoException();
                    if (_result59) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 63:
                    data.enforceInterface(DESCRIPTOR);
                    if (data.readInt() != 0) {
                        _arg04 = true;
                    } else {
                        _arg04 = false;
                    }
                    boolean _result60 = enableCTSwitch(_arg04);
                    reply.writeNoException();
                    if (_result60) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 64:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result61 = getCTSwitchStatus();
                    reply.writeNoException();
                    if (_result61) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 65:
                    data.enforceInterface(DESCRIPTOR);
                    if (data.readInt() != 0) {
                        _arg03 = true;
                    } else {
                        _arg03 = false;
                    }
                    boolean _result62 = enableRegionSwitch(_arg03);
                    reply.writeNoException();
                    if (_result62) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 66:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result63 = getRegionSwitchStatus();
                    reply.writeNoException();
                    if (_result63) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 67:
                    data.enforceInterface(DESCRIPTOR);
                    if (data.readInt() != 0) {
                        _arg02 = true;
                    } else {
                        _arg02 = false;
                    }
                    boolean _result64 = enablePTYSwitch(_arg02);
                    reply.writeNoException();
                    if (_result64) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 68:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result65 = getPTYSwitchStatus();
                    reply.writeNoException();
                    if (_result65) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 69:
                    data.enforceInterface(DESCRIPTOR);
                    if (data.readInt() != 0) {
                        _arg0 = true;
                    } else {
                        _arg0 = false;
                    }
                    boolean _result66 = enablePSSwitch(_arg0);
                    reply.writeNoException();
                    if (_result66) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 70:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result67 = getPSSwitchStatus();
                    reply.writeNoException();
                    if (_result67) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 71:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result68 = setRDSType(data.readByte());
                    reply.writeNoException();
                    if (_result68) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 72:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result69 = setTAVolume(data.readInt(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    if (_result69) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 73:
                    data.enforceInterface(DESCRIPTOR);
                    int _result70 = getTAVolume();
                    reply.writeNoException();
                    reply.writeInt(_result70);
                    return true;
                case 74:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result71 = setTAVolume_Before(data.readInt());
                    reply.writeNoException();
                    if (_result71) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 75:
                    data.enforceInterface(DESCRIPTOR);
                    int _result72 = getTAVolume_Before();
                    reply.writeNoException();
                    reply.writeInt(_result72);
                    return true;
                case 76:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result73 = setTAVolume_After(data.readInt());
                    reply.writeNoException();
                    if (_result73) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case TRANSACTION_getTAVolume_After /*77*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result74 = getTAVolume_After();
                    reply.writeNoException();
                    reply.writeInt(_result74);
                    return true;
                case TRANSACTION_setRDSAFLevel /*78*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result75 = setRDSAFLevel(data.readByte());
                    reply.writeNoException();
                    if (_result75) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case TRANSACTION_searchSpecialRDSProgram /*79*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result76 = searchSpecialRDSProgram(data.readInt());
                    reply.writeNoException();
                    if (_result76) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 80:
                    data.enforceInterface(DESCRIPTOR);
                    String _result77 = getRDSType();
                    reply.writeNoException();
                    reply.writeString(_result77);
                    return true;
                case 81:
                    data.enforceInterface(DESCRIPTOR);
                    setRadioInfoChangeListener(com.hzbhd.midwareproxy.radio.aidl.IRadioInfoChangeListener.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case TRANSACTION_removeRadioInfoChangeListener /*82*/:
                    data.enforceInterface(DESCRIPTOR);
                    removeRadioInfoChangeListener(com.hzbhd.midwareproxy.radio.aidl.IRadioInfoChangeListener.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case TRANSACTION_setRDSInfoChangeListener /*83*/:
                    data.enforceInterface(DESCRIPTOR);
                    setRDSInfoChangeListener(com.hzbhd.midwareproxy.radio.aidl.IRDSInfoChangeListener.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case TRANSACTION_removeRDSInfoChangeListener /*84*/:
                    data.enforceInterface(DESCRIPTOR);
                    removeRDSInfoChangeListener(com.hzbhd.midwareproxy.radio.aidl.IRDSInfoChangeListener.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case TRANSACTION_initRadioData /*85*/:
                    data.enforceInterface(DESCRIPTOR);
                    initRadioData();
                    reply.writeNoException();
                    return true;
                case TRANSACTION_saveAppdata /*86*/:
                    data.enforceInterface(DESCRIPTOR);
                    saveAppdata(data.readString());
                    reply.writeNoException();
                    return true;
                case TRANSACTION_sendAppdata /*87*/:
                    data.enforceInterface(DESCRIPTOR);
                    sendAppdata(data.readString());
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    boolean autoSearch(int i) throws RemoteException;

    boolean autoSearchWithPreview(int i) throws RemoteException;

    void bandChange() throws RemoteException;

    boolean enableAFSwitch(boolean z) throws RemoteException;

    boolean enableCTSwitch(boolean z) throws RemoteException;

    boolean enableEONSwitch(boolean z) throws RemoteException;

    boolean enablePSSwitch(boolean z) throws RemoteException;

    boolean enablePTYSwitch(boolean z) throws RemoteException;

    boolean enableRDSSwitch(boolean z) throws RemoteException;

    boolean enableRegionSwitch(boolean z) throws RemoteException;

    boolean enableTASwitch(boolean z) throws RemoteException;

    boolean getAFSeekStatus() throws RemoteException;

    boolean getAFSwitchStatus() throws RemoteException;

    boolean getAutoSearchStatus() throws RemoteException;

    boolean getAutoSearchWithPreviewStatus() throws RemoteException;

    boolean getCTSwitchStatus() throws RemoteException;

    String getCurPSInfo() throws RemoteException;

    String getCurPTYInfo() throws RemoteException;

    String getCurRTInfo() throws RemoteException;

    String getCurrPresetFreq(int i) throws RemoteException;

    String getCurrentArea() throws RemoteException;

    String getCurrentBand() throws RemoteException;

    String getCurrentFreq() throws RemoteException;

    String getDefaultArea() throws RemoteException;

    boolean getEONStatus() throws RemoteException;

    boolean getEONSwitchStatus() throws RemoteException;

    boolean getEWSStatus() throws RemoteException;

    boolean getPSSwitchStatus() throws RemoteException;

    boolean getPTYSeekStatus() throws RemoteException;

    boolean getPTYSwitchStatus() throws RemoteException;

    String[] getPresetFreqList(int i) throws RemoteException;

    int getPresetFreqListCount(int i) throws RemoteException;

    String[] getPresetFreqPSList(int i) throws RemoteException;

    void getPresetListPSInfo() throws RemoteException;

    String getPresetPSName(int i) throws RemoteException;

    boolean getRDSSwitchStatus() throws RemoteException;

    String getRDSType() throws RemoteException;

    String getRFInputMode() throws RemoteException;

    boolean getRegionSwitchStatus() throws RemoteException;

    int getSignalStrength() throws RemoteException;

    boolean getStepDownStatus() throws RemoteException;

    boolean getStepUpStatus() throws RemoteException;

    boolean getTASeekStatus() throws RemoteException;

    boolean getTAStatus() throws RemoteException;

    boolean getTASwitchStatus() throws RemoteException;

    int getTAVolume() throws RemoteException;

    int getTAVolume_After() throws RemoteException;

    int getTAVolume_Before() throws RemoteException;

    boolean getTPSeekStatus() throws RemoteException;

    byte getTunerModel() throws RemoteException;

    String[] getValidFreqList(int i) throws RemoteException;

    int getValidFreqListCount(int i) throws RemoteException;

    String getVoiceChannelType() throws RemoteException;

    void initRadioData() throws RemoteException;

    boolean isRDSStation() throws RemoteException;

    boolean isTP() throws RemoteException;

    boolean isValidFreq() throws RemoteException;

    void playPresetFreq(int i) throws RemoteException;

    void removeRDSInfoChangeListener(IRDSInfoChangeListener iRDSInfoChangeListener) throws RemoteException;

    void removeRadioInfoChangeListener(IRadioInfoChangeListener iRadioInfoChangeListener) throws RemoteException;

    boolean restoreFactorySettings() throws RemoteException;

    void saveAppdata(String str) throws RemoteException;

    boolean searchSpecialRDSProgram(int i) throws RemoteException;

    boolean searchStepDown(int i) throws RemoteException;

    boolean searchStepUp(int i) throws RemoteException;

    boolean seekDown() throws RemoteException;

    boolean seekUp() throws RemoteException;

    void sendAppdata(String str) throws RemoteException;

    boolean setCurrPresetFreq(int i) throws RemoteException;

    boolean setCurrentArea(int i) throws RemoteException;

    boolean setCurrentBand(int i) throws RemoteException;

    boolean setCurrentFreq(String str) throws RemoteException;

    boolean setDefaultArea() throws RemoteException;

    boolean setPresetFreqList(int i, int i2, String str) throws RemoteException;

    boolean setRDSAFLevel(byte b) throws RemoteException;

    void setRDSInfoChangeListener(IRDSInfoChangeListener iRDSInfoChangeListener) throws RemoteException;

    boolean setRDSType(byte b) throws RemoteException;

    boolean setRFInputMode(int i) throws RemoteException;

    void setRadioInfoChangeListener(IRadioInfoChangeListener iRadioInfoChangeListener) throws RemoteException;

    boolean setTAVolume(int i, int i2, int i3) throws RemoteException;

    boolean setTAVolume_After(int i) throws RemoteException;

    boolean setTAVolume_Before(int i) throws RemoteException;

    boolean setTunerReg(byte b, byte b2) throws RemoteException;

    boolean setTunerRegWithModel(byte b) throws RemoteException;

    boolean stopSearch() throws RemoteException;

    boolean tuneDown() throws RemoteException;

    boolean tuneUp() throws RemoteException;
}
