package com.hzbhd.midware.constant;

public interface NaviConstantsDef {
    public static final String ACTION_AUDIO_REQUEST = "com.hzbhd.navi.audioRequest";
    public static final String ACTION_NAV_EXIT = "com.hzbhd.navi.exit";
    public static final String ACTION_NAV_REQUEST_PTT = "com.hzbhd.navi.requestPTT";
    public static final String ACTION_NAV_TURN_INFO = "com.hzbhd.navi.turnInfoUpdae";
    public static final String ACTION_SET_RADIO_FREQ = "com.hzbhd.navi.FMFrequencySet";
    public static final String EXTRA_DEST_DISTANCE = "destDistance";
    public static final String EXTRA_RADIO_FREQ = "frequency";
    public static final String EXTRA_REQUEST_FLAG = "requestFlag";
    public static final String EXTRA_TRRN_DISTANCE = "turnDistance";
    public static final String EXTRA_TRRN_FLAG = "trunFlag";
    public static final int FLAG_1ST_DEST = 11;
    public static final int FLAG_2ND_DEST = 12;
    public static final int FLAG_3RD_DEST = 13;
    public static final int FLAG_4TH_DEST = 14;
    public static final int FLAG_AHEAD = 4;
    public static final int FLAG_CHANNEL = 15;
    public static final int FLAG_CITY_ENTRY_1 = 24;
    public static final int FLAG_CITY_ENTRY_2 = 25;
    public static final int FLAG_CITY_EXIT = 23;
    public static final int FLAG_CYCLE = 3;
    public static final int FLAG_FINAL_DEST = 22;
    public static final int FLAG_GO_AHEAD = 29;
    public static final int FLAG_GO_ASSIT_ROAD = 18;
    public static final int FLAG_GO_DOWN_OVERHEAD = 26;
    public static final int FLAG_GO_MAIN_ROAD = 17;
    public static final int FLAG_GO_UP_OVERHEAD = 27;
    public static final int FLAG_HIGH_WAY_EXIT = 28;
    public static final int FLAG_LANE = 19;
    public static final int FLAG_LEFT_BACK = 5;
    public static final int FLAG_LEFT_FRONT = 9;
    public static final int FLAG_LEFT_TURN_AROUND = 7;
    public static final int FLAG_NONE = 0;
    public static final int FLAG_RIGHT_BACK = 6;
    public static final int FLAG_RIGHT_FRONT = 10;
    public static final int FLAG_RIGHT_TURN_AROUND = 8;
    public static final int FLAG_SERVICE_AREA = 21;
    public static final int FLAG_SHIP = 20;
    public static final int FLAG_TOLL_GATE = 16;
    public static final int FLAG_TURN_LEFT = 2;
    public static final int FLAG_TURN_RIGHT = 1;

    public enum HOST_CMD_CODE {
        ILLUM_STATE_CHANGED,
        SHOW_HIDE_NAV,
        ONEKEY_NAVI_GUIDE,
        RUN_NAVI
    }

    public enum ILLUM_NIGHT_STATE {
        ON,
        OFF
    }

    public enum SHOW_HIDE_STATE {
        SHOW,
        HIDE
    }
}
