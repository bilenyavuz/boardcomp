package com.hzbhd.midware.constant;

public class channelManagerDef {
    public static final String ACTION_VIDEO_STATE_CHANGED = "com.hzbhd.intent.action.vidio.statechanged";
    public static final String EXTRA_ACTION_VIDEO_STATE_CHANGED = "isConnected";
    public static final String HzbhdCOMMONAction = "com.hzbhd.intent.action.common";
    public static final String hzbhdAPPAction = "com.hzbhd.intent.action.app";
    public static final String hzbhdATVAction = "com.hzbhd.intent.action.atv";
    public static final String hzbhdAUX1Action = "com.hzbhd.intent.action.aux1";
    public static final String hzbhdAUX2Action = "com.hzbhd.intent.action.aux2";
    public static final String hzbhdAVOFFAction = "com.hzbhd.intent.action.avoff";
    public static final String hzbhdAndroidPlayerAction = "com.hzbhd.intent.action.androidplayer";
    public static final String hzbhdAudioPreviewAction = "com.hzbhd.intent.action.audiopreview";
    public static final String hzbhdBTAUDIOAction = "com.hzbhd.intent.action.btaudio";
    public static final String hzbhdBTPHONEAction = "com.hzbhd.intent.action.btphone";
    public static final String hzbhdBTUIAUDIOAction = "com.hzbhd.intent.action.btuiaudio";
    public static final String hzbhdBTUIPHONEAction = "com.hzbhd.intent.action.btuiphone";
    public static final String hzbhdBlackOutAction = "com.hzbhd.intent.action.blackout";
    public static final String hzbhdCDCAction = "com.hzbhd.intent.action.cdc";
    public static final String hzbhdCMMBAction = "com.hzbhd.intent.action.cmmb";
    public static final String hzbhdCameraAction = "com.hzbhd.intent.action.camera";
    public static final String hzbhdCloseSourceAction = "com.hzbhd.intent.action.closesource";
    public static final String hzbhdDABAction = "com.hzbhd.intent.action.dab";
    public static final String hzbhdDABTAAction = "com.hzbhd.intent.action.dabta";
    public static final String hzbhdDTVAction = "com.hzbhd.intent.action.dtv";
    public static final String hzbhdDVBTAction = "com.hzbhd.intent.action.dvbt";
    public static final String hzbhdDVRAction = "com.hzbhd.intent.action.dvr";
    public static final String hzbhdEQAction = "com.hzbhd.intent.action.eq";
    public static final String hzbhdFMAction = "com.hzbhd.intent.action.fm";
    public static final String hzbhdG3PHONEAction = "com.hzbhd.intent.action.g3phone";
    public static final String hzbhdHDMILINKAction = "com.hzbhd.intent.action.hdmilink";
    public static final String hzbhdHVACAction = "com.hzbhd.intent.action.hvac";
    public static final String hzbhdIMODEAction = "com.hzbhd.intent.action.imode";
    public static final String hzbhdIPODAction = "com.hzbhd.intent.action.ipod";
    public static final String hzbhdISDBAction = "com.hzbhd.intent.action.isdb";
    public static final String hzbhdMPEGAction = "com.hzbhd.intent.action.mpeg";
    public static final String hzbhdMPEGTESTAction = "com.hzbhd.intent.action.mpegtest";
    public static final String hzbhdMediaInsertAction = "com.hzbhd.intent.action.mediainsert";
    public static final String hzbhdMusicAction = "com.hzbhd.intent.action.music";
    public static final String hzbhdNAVIAction = "com.hzbhd.intent.action.navigation";
    public static final String hzbhdNormalAction = "com.hzbhd.intent.action.normal";
    public static final String hzbhdPhotoAction = "com.hzbhd.intent.action.photo";
    public static final String hzbhdRDSAction = "com.hzbhd.intent.action.rds";
    public static final String hzbhdREAR = "com.hzbhd.intent.action.REAR";
    public static final String hzbhdREARUSBAction = "com.hzbhd.intent.action.rearusb";
    public static final String hzbhdRearAction = "com.hzbhd.intent.action.rear";
    public static final String hzbhdRearviewAction = "com.hzbhd.intent.action.rearview";
    public static final String hzbhdSDAction = "com.hzbhd.intent.action.sd";
    public static final String hzbhdSWCAction = "com.hzbhd.intent.action.swc";
    public static final String hzbhdSXMAction = "com.hzbhd.intent.action.sxm";
    public static final String hzbhdSettingChangedAction = "com.hzbhd.intent.action.settingchanged";
    public static final String hzbhdSettingsAction = "com.hzbhd.intent.action.settings";
    public static final String hzbhdSourceChangeAction = "com.hzbhd.intent.action.sourcechange";
    public static final String hzbhdSourceDeactiveAction = "com.hzbhd.intent.action.sourcedeactive";
    public static final String hzbhdTPMSAction = "com.hzbhd.intent.action.TPMS";
    public static final String hzbhdUSB1Action = "com.hzbhd.intent.action.usb1";
    public static final String hzbhdUSB2Action = "com.hzbhd.intent.action.usb2";
    public static final String hzbhdUSB3Action = "com.hzbhd.intent.action.usb3";
    public static final String hzbhdUSBAction = "com.hzbhd.intent.action.usb";
    public static final String hzbhdUpgradeAction = "com.hzbhd.intent.action.upgrade";
    public static final String hzbhdVideoAction = "com.hzbhd.intent.action.video";
    public static final String hzbhduTTSAction = "com.hzbhd.intent.action.tts";
    public static final String openRecentSource = "com.hzbhd.intent.action.openRecentSource";

    public enum MEDIA_SOURCE_ID {
        NULL,
        SD,
        USB,
        USB1,
        USB2,
        USB3,
        USB4,
        MPEG,
        CDC,
        IPOD,
        AUX1,
        AUX2,
        HDMILINK,
        NAVIGATION,
        NAVIAUDIO,
        APP,
        TTS,
        FM,
        TPMS,
        BTPHONE,
        BTAUDIO,
        BTUIPHONE,
        BTUIAUDIO,
        BTCONNECTION,
        IMODE,
        CMMB,
        G3PHONE,
        CAMERA,
        DVR,
        DVBT,
        ISDB,
        ATV,
        EQ,
        SETTINGS,
        UPGRADE,
        DAB,
        DABTA,
        SWC,
        REAR,
        MUSIC,
        VIDEO,
        PHOTO,
        HVAC,
        SXM,
        CLOSESOURCE,
        BLACKOUT,
        AUDIOPREVIEW,
        RDS,
        AVOFF,
        REARUSB,
        ANDROIDPLAYER,
        SLEEP,
        REARVIEW,
        MPEGTEST,
        DTV,
        NORMAL_SOURCE
    }

    public enum SEAT_TYPE {
        front_seat,
        back_seat
    }

    public enum SWITCH_RESULT_TYPE {
        success,
        fail,
        notsupport,
        nodevice
    }
}
