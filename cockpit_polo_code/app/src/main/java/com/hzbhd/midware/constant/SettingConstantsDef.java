package com.hzbhd.midware.constant;

import android.content.Context;
import android.provider.Settings.System;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.util.SparseIntArray;
//import com.autochips.bluetooth.hf.BluetoothHfAtPhonebook;
//import com.hzbhd.midwareproxy.utils.SystemConstant;
import nfore.android.bt.res.NfDef;

public final class SettingConstantsDef {
    public static final String ACTION_BRIGHTNESS_DIALOG_CLOSE = "com.hzbhd.android.settings.Action_brightness_dialog_close";
    public static final String ACTION_BTPHONE_END_ACTION = "Bluetooth.call.end.action";
    public static final String ACTION_BTPHONE_START_ACTION = "Bluetooth.call.start.action";
    public static final String ACTION_FACTORY_RESET = "com.hzbhd.android.settings.ACTION_FACTORY_RESET";
    public static final String ACTION_PRINT_LOG = "com.hzbhd.android.settings.ACTION_PRINT_LOG";
    public static final String ACTION_START_EQ = "com.hzbhd.android.settings.ACTION_START_EQ";
    public static final String ACTION_VOLUME_CHANGE = "com.hzbhd.android.settings.ACTION_VOLUME_CHANGE";
    public static final String ACTION_VOLUME_DIALOG_CLOSE = "com.hzbhd.android.settings.Action_volume_dialog_close";
    public static final String ADAYO_INPUTMETHODS_DEFAULT = "com.android.inputmethod.latin/.LatinIME";
    public static final String ADAYO_INPUTMETHODS_ENABLED = "com.android.inputmethod.pinyin/.PinyinIME:com.android.inputmethod.latin/.LatinIME:jp.co.omronsoft.openwnn/.OpenWnnJAJP";
    public static final String ADAYO_INPUTMETHODS_PINYINIME = "com.android.inputmethod.pinyin/.PinyinIME";
    public static final String ADAYO_SETTING_INIT = "com.hzbhd.android.settings.ADAYO_SETTING_INIT";
    public static final String AUTOTEST_MUSIC_FILE_FROM = "music_file_from";
    public static final String AVSTATE = "AVSTATE";
    public static final int AVSTATE_OFF = 0;
    public static final int AVSTATE_ON = 1;
    public static final String BACKCARPROGRESS_KEY = "hzbhd_backcarprogress_key";
    public static final SparseIntArray BALANCE = new SparseIntArray(25);
    public static final int BALANCE_FR_DEFAULT = 0;
    public static final String BALANCE_FR_KEY = "hzbhd_balance_fr";
    public static final int BALANCE_LR_DEFAULT = 0;
    public static final String BALANCE_LR_KEY = "hzbhd_balance_lr";
    public static final int BALANCE_SPACE_MAX_VALUE = (Array.Balance_au4TrimValue.length - 1);
    public static final SparseIntArray BLACKOUT = new SparseIntArray(3);
    public static final String BOOT_SECURITY_CODE = "BOOT_SECURITY_CODE";
    public static final int BRIGHTNESS_DEFAULT = 32;
    public static final String BTPROGRESS_KEY = "hzbhd_btprogress_key";
    public static final SparseIntArray CAMERAMODE = new SparseIntArray(2);
    public static final SparseIntArray CHILDRENLOCKLEVEL = new SparseIntArray(8);
    public static final SparseIntArray CHILDRENLOCKLEVELSTATE = new SparseIntArray(2);
    public static final String COLOR_SCHEME = "COLOR_SCHEME";
    public static final SparseIntArray CURRENTAREA = new SparseIntArray(6);
    public static final SparseIntArray EQ = new SparseIntArray(29);
    public static final SparseIntArray EQTYPE = new SparseIntArray(8);
    public static final SparseIntArray EQVALUE = new SparseIntArray(4);
    public static final String EQ_ACTIVITY = "com.hzbhd.eq.MainActivity";
    public static final String EQ_GAIN_SPLIT_KEY = "_hzbhd_eq_gain_split_key_";
    public static final String EQ_KEY = "hzbhd_eq_key";
    public static final String EQ_PACKAGE = "com.hzbhd.eq";
    public static final EQ_TYPE EQ_TYPE_DEFAULT = EQ_TYPE.user;
    public static final String EXTRA_BUTTON_FROM = "button_from";
    public static final String EXTRA_HASTHRIDAPP_PLAY = "EXTRA_HASTHRIDAPP_PLAY";
    public static final String EXTRA_KEY_STATE = "key_state";
    public static final String EXTRA_KEY_VALUE = "key_value";
    public static final String EXTRA_KEY_VOLUNE_MUTE = "key_volume_mute";
    public static final String EXTRA_KEY_VOLUNE_TYPE = "key_volume_type";
    public static final String EXTRA_KEY_VOLUNE_VALUE = "key_volume_value";
    public static final String EXTRA_SOURCE_ID = "SOURCE_ID";
    public static final SparseIntArray FACTORYFMLOCAL = new SparseIntArray(16);
    public static final SparseIntArray FACTORYRADIOMODEL = new SparseIntArray(3);
    public static final SparseIntArray FACTORYRDSTYPE = new SparseIntArray(3);
    public static final SparseIntArray FACTORYSYSTEMREGIONCODE = new SparseIntArray(7);
    public static final SparseIntArray FACTORYSYSTEMTVAREA = new SparseIntArray(9);
    public static final String FORYOU_AUTO_DIMMER = "FORYOU_AUTO_DIMMER";
    public static final String FORYOU_CAMERA_AUDIO = "FORYOU_CAMERA_AUDIO";
    public static final String FORYOU_GESTURE_THREE_FINGER_SHRINK_KEY = "hzbhd_gesture_three_finger_shrink";
    public static final String FORYOU_GESTURE_TWO_FINGER_DOWN_KEY = "hzbhd_gesture_two_finger_down";
    public static final String FORYOU_GESTURE_TWO_FINGER_LEFT_KEY = "hzbhd_gesture_two_finger_left";
    public static final String FORYOU_GESTURE_TWO_FINGER_RIGHT_KEY = "hzbhd_gesture_two_finger_right";
    public static final String FORYOU_GESTURE_TWO_FINGER_UP_KEY = "hzbhd_gesture_two_finger_up";
    public static final String FORYOU_LANGUAGE_FLAG = "FORYOU_LANGUAGE_FLAG";
    public static final String FORYOU_NAVI_INTENT_KEY = "hzbhd_navi_intent";
    public static final String FORYOU_OFF_TIMER = "FORYOU_OFF_TIMER";
    public static final String FORYOU_REMOTE_CONTROL = "FORYOU_REMOTE_CONTROL";
    public static final String FORYOU_SCREEN_BRIGHTNESS = "hzbhd_screen_brightness_key";
    public static final String FORYOU_TURN_ON_VOLUME = "FORYOU_TURN_ON_VOLUME";
    public static final String FORYOU_VOLUME = "FORYOU_VOLUME";
    public static final String FRONTPROGRESS_KEY = "hzbhd_frontprogress_key";
    public static final String FRONTVOLUME_KEY = "hzbhd_frontvolume_key";
    public static final String GISPROGRESS_KEY = "hzbhd_gisprogress_key";
    public static final SparseIntArray INPUTGAIN = new SparseIntArray(12);
    public static final SparseIntArray KEYCOLOR = new SparseIntArray(2);
    public static final int LOUDNESS_DEFAULT = 0;
    public static final String LOUDNESS_KEY = "hzbhd_loudness";
    public static final String MCU_KEY_IND_ACTION = "com.hzbhd.android.MCU_KEY_IND_ACTION";
    public static final String NAVIClassName = "NAVIClassName";
    public static final String NAVIPackageName = "NAVIPackageName";
    public static final SparseIntArray QVALUE = new SparseIntArray(4);
    public static final String REARVOLUME_KEY = "hzbhd_rearvolume_key";
    public static final String SCAN_TIME = "SCAN_TIME";
    public static final String SETTINGS_KEY_BACKCAR_VOLUME = "SETTINGS_KEY_BACKCAR_VOLUME";
    public static final String SETTINGS_KEY_BOOTLOAD = "SETTINGS_KEY_BOOTLOAD";
    public static final int STREAM_ALARM = 4;
    public static final int STREAM_BACKCAR = 12;
    public static final int STREAM_BLUETOOTH_SCO = 6;
    public static final int STREAM_DTMF = 8;
    public static final int STREAM_GIS = 10;
    public static final int STREAM_IPOD = 11;
    public static final int STREAM_MUSIC = 3;
    public static final int STREAM_NOTIFICATION = 5;
    public static final int STREAM_RING = 2;
    public static final int STREAM_SYSTEM = 1;
    public static final int STREAM_SYSTEM_ENFORCED = 7;
    public static final int STREAM_TTS = 9;
    public static final int STREAM_VOICE_CALL = 0;
    public static final SparseIntArray SUBWFILTER = new SparseIntArray(3);
    public static final SparseIntArray SUBWGAIN = new SparseIntArray(25);
    public static final String THEFT_PROTECTION_SWITCH = "THEFT_PROTECTION_SWITCH";
    public static final SparseIntArray VIDEOINPUT = new SparseIntArray(4);
    public static final String VOLUME_AUXIN1_KEY = "hzbhd_volume_auxin1";
    public static final String VOLUME_AUXIN2_KEY = "hzbhd_volume_auxin2";
    public static final String VOLUME_DAB_KEY = "hzbhd_volume_dab";
    public static final String VOLUME_DISC_KEY = "hzbhd_volume_disc";
    public static final String VOLUME_IPOD_KEY = "hzbhd_volume_ipod";
    public static final String VOLUME_MEDIA_KEY = "hzbhd_volume_media";
    public static final String VOLUME_RADIO_KEY = "hzbhd_volume_radio";
    public static final int VOLUME_REAR_DEFAULT = 20;
    public static final String VOLUME_TV_KEY = "hzbhd_volume_tv";
    public static final String VOLUME_XM_KEY = "hzbhd_volume_xm";

    public static final class Array {
        public static int[] Balance_au4TrimValue = {0, 7104, 9249, 12321, 15265, 17312, 22016, 24609, 29601, 36863, 41449, 46506, 52181, 58548, 65692, 73707, 82701, 92792, 101298, 115282, 131072, 138839, 147065, 155779, 165010, 174787, 185144, 196115, 207735, 220044, 233083, 246894, 261523, 277019, 293434, 310821, 329238, 348747, 369411, 391300, 414486};
        public static int[][] LoudNess_gLoudNessGain = {new int[]{0, 0, 0, 0, 0, 0}, new int[]{AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, 14726105, 1003040, 2053686, 15771655, 5938296}, new int[]{AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, 14728833, 1000374, 2053686, 15771655, 5938296}, new int[]{AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, 14731723, 997553, 2053686, 15771655, 5938296}, new int[]{AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, 14734784, 994570, 2053686, 15771655, 5938296}, new int[]{AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, 14738026, 991414, 2053686, 15771655, 5938296}, new int[]{AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, 14741461, 988078, 2053686, 15771655, 5938296}, new int[]{AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, 14745099, 984549, 2053686, 15771655, 5938296}, new int[]{AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, 14748953, 980819, 2053686, 15771655, 5938296}, new int[]{AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, 14748953, 980819, 2053686, 15771655, 5938296}, new int[]{AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, 14748953, 980819, 2053686, 15771655, 5938296}, new int[]{AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, 14748953, 980819, 2053686, 15771655, 5938296}, new int[]{AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, 14748953, 980819, 2053686, 15771655, 5938296}, new int[]{AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, 14748953, 980819, 2053686, 15771655, 5938296}, new int[]{AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, 14748953, 980819, 2053686, 15771655, 5938296}, new int[]{AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, 14748953, 980819, 2053686, 15771655, 5938296}, new int[]{AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, 14748953, 980819, 2053686, 15771655, 5938296}, new int[]{AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, 14748953, 980819, 2053686, 15771655, 5938296}, new int[]{AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, 14748953, 980819, 2053686, 15771655, 5938296}, new int[]{AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, 14748953, 980819, 2053686, 15771655, 5938296}};
        public static String MTKSettings = "com.android.settings_preferences";
        public static int[] ReverbCoef_arena = {AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, 8388607, 79, 73, 71, 67, 79, 73, 71, 67, 79, 73, 71, 67};
        public static int[] ReverbCoef_bathroom = {4194304, 8388607, 19, 17, 11, 7, 19, 17, 11, 7, 19, 17, 11, 7};
        public static int[] ReverbCoef_cave = {6291456, 8388607, 59, 47, 37, 23, 59, 47, 37, 23, 59, 47, 37, 23};
        public static int[] ReverbCoef_concert = {AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_END, 8388607, 79, 67, 59, 47, 79, 67, 59, 47, 79, 67, 59, 47};
        public static int[] ReverbCoef_hall = {3145728, 8388607, 79, 59, 37, 19, 79, 59, 37, 19, 79, 59, 37, 19};
        public static int[] ReverbCoef_live = {1572864, 8388607, 41, 31, 23, 13, 41, 31, 23, 13, 41, 31, 23, 13};
        public static int[] ReverbCoef_off = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        public static int[] gEQTypeGain_classic = {65536, 0, 0, 0, 0, 0, 0, -7126, -13478, -24185, -28682};
        public static int[] gEQTypeGain_dance = {65536, 141707, 119169, 81180, 38331, 0, -19140, -28682, -19140, -7126, 0};
        public static int[] gEQTypeGain_live = {65536, 262922, 195367, 99082, 16968, -13478, 0, 65225, 141707, 99082, 38331};
        public static int[] gEQTypeGain_off = {65536, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        public static int[] gEQTypeGain_pop = {65536, -13478, 0, 38331, 99082, 141707, 99082, 51005, 7996, -13478, -24185};
        public static int[] gEQTypeGain_rock = {65536, 141707, 99082, 51005, 0, -13478, 0, 16968, 38331, 99082, 141707};
        public static int[] gEQTypeGain_soft = {65536, 51005, 38331, 27036, 16968, 7996, -7126, -19140, -28682, -36262, -39445};
        public static int[][] gEQTypePos = {new int[]{14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14}, new int[]{14, 24, 22, 19, 14, 12, 14, 16, 18, 22, 24}, new int[]{14, 12, 14, 18, 22, 24, 22, 19, 15, 12, 10}, new int[]{14, 28, 26, 22, 16, 12, 14, 20, 24, 22, 18}, new int[]{14, 24, 23, 21, 18, 14, 11, 9, 11, 13, 14}, new int[]{14, 14, 14, 14, 14, 14, 14, 13, 12, 10, 9}, new int[]{14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14}, new int[]{14, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10}};
        public static int[] g_dryValues = {13076, 14671, 16461, 18470, 20724, 23253, 26090, 29273, 32845, 36853, 41350, 46395, 52057, 58409, 65536, 73532, 82504, 92572, 103867, 116541, 130761, 146716, 164618, 184705, 207243, 232530, 260903, 292738, 328458};
        public static int[] g_ganValues = {-52459, -50864, -49074, -47065, -44811, -42282, -39445, -36262, -32690, -28682, -24185, -19140, -13478, -7126, 0, 7996, 16968, 27036, 38331, 51005, 65225, 81180, 99082, 119169, 141707, 166994, 195367, 227202, 262922};
        public static int[] g_volume_values = {0, 5, 30, 60, 90, 120, NfDef.STATE_OUTGOING_CALL, 200, NfDef.STATE_3WAY_M_HOLD, 300, 400, 500, 600, 800, 1000, 1200, 1400, 1600, 1800, 2000, 2200, 2400, 2800, 3200, 3600, 4000, 4500, 5000, 6000, 7500, 9000, 9999, 13000, 15000, 17000, 20000, 23000, 28000, 60000, 110000, 131072};
        public static int[] rUpmixGain_0 = {0, 0, 0, 0, 0, 0, 0, 0};
        public static int[] rUpmixGain_1 = {8388607, 0, 0, 8388607, 0, 0, 8388607, 8388607};

        private Array() {
        }
    }

    public enum AutoTestBTOprate {
        music_prev,
        music_next,
        music_play,
        music_pause,
        phone_answer,
        phone_hangup
    }

    public enum AutoTestDVDOprate {
        prev,
        next,
        pause,
        play,
        stop
    }

    public enum AutoTestSource {
        NULL(0),
        FM(1),
        MPEG(2),
        G3PHONE(3),
        AUX1(4),
        SXM(5),
        USB1(6),
        BTUIPHONE(7),
        HDRADIO(8),
        AUX2(9),
        CMMB(10),
        VirDisk(11),
        SD(12),
        NAVIGATION(13),
        MPU(14),
        TTS(15),
        CAMMER(16),
        SD2(17),
        IPOD(18),
        USB2(19),
        DAB(20),
        ISDB(21),
        DVBT(22),
        IPOD2(23),
        USB3(24),
        REARUSB(25);
        
        private final int index;

        private AutoTestSource(int aIndex) {
            this.index = aIndex;
        }

        public static AutoTestSource getByIndex(int aIndex) {
            if (aIndex >= 0 && aIndex < values().length) {
                for (AutoTestSource source : values()) {
                    if (aIndex == source.index) {
                        return source;
                    }
                }
            }
            return null;
        }
    }

    public enum BeepType {
        OK,
        ERROR,
        FAIL,
        UPGRADE
    }

    public enum EQ_TYPE {
        user(0),
        rock(1),
        pop(2),
        techno(3),
        jazz(4),
        classic(5),
        flat(6),
        optimal(7);
        
        public final int index;

        private EQ_TYPE(int aIndex) {
            this.index = aIndex;
        }

        public static EQ_TYPE getByIndex(int aIndex) {
            int i = 0;
            while (aIndex >= 0 && aIndex < values().length && i < values().length) {
                EQ_TYPE source = values()[i];
                if (aIndex == source.index) {
                    return source;
                }
                i++;
            }
            return null;
        }
    }

    public enum KeyState {
        NONE,
        PRESS_DOWN,
        PRESS_UP,
        LONG_EVENT,
        LONG_UP
    }

    public enum Source_Gain {
        FM(0),
        DISC(2),
        IPOD(11),
        BACKCAR(12),
        AUX1(4),
        AUX2(5),
        TV(7),
        XM(8),
        DAB(9),
        MUSIC(3),
        BLUETOOTH(6),
        NAVI(10),
        OTHER(1);
        
        public final int stream_index;

        private Source_Gain(int aIndex) {
            this.stream_index = aIndex;
        }
    }

    private SettingConstantsDef() {
    }

    static {
        QVALUE.put(0, 0);
        QVALUE.put(1, 1);
        QVALUE.put(2, 2);
        QVALUE.put(3, 3);
        VIDEOINPUT.put(0, 0);
        VIDEOINPUT.put(1, 1);
        VIDEOINPUT.put(2, 2);
        VIDEOINPUT.put(3, 3);
        CAMERAMODE.put(0, 0);
        CAMERAMODE.put(1, 1);
        CHILDRENLOCKLEVELSTATE.put(0, 0);
        CHILDRENLOCKLEVELSTATE.put(1, 1);
        KEYCOLOR.put(0, 0);
        KEYCOLOR.put(1, 1);
        BLACKOUT.put(0, 0);
        BLACKOUT.put(1, 1);
        BLACKOUT.put(2, 2);
        SUBWFILTER.put(0, 0);
        SUBWFILTER.put(1, 1);
        SUBWFILTER.put(2, 2);
        SUBWFILTER.put(3, 3);
        FACTORYRADIOMODEL.put(0, 0);
        FACTORYRADIOMODEL.put(1, 1);
        FACTORYRADIOMODEL.put(2, 2);
        FACTORYRDSTYPE.put(0, 0);
        FACTORYRDSTYPE.put(1, 1);
        FACTORYRDSTYPE.put(2, 2);
        CURRENTAREA.put(0, 0);
        CURRENTAREA.put(1, 1);
        CURRENTAREA.put(2, 2);
        CURRENTAREA.put(3, 3);
        CURRENTAREA.put(4, 4);
        CURRENTAREA.put(5, 5);
        CURRENTAREA.put(6, 6);
        CURRENTAREA.put(7, 7);
        CURRENTAREA.put(8, 8);
        CURRENTAREA.put(9, 9);
        CHILDRENLOCKLEVEL.put(0, 0);
        CHILDRENLOCKLEVEL.put(1, 1);
        CHILDRENLOCKLEVEL.put(2, 2);
        CHILDRENLOCKLEVEL.put(3, 3);
        CHILDRENLOCKLEVEL.put(4, 4);
        CHILDRENLOCKLEVEL.put(5, 5);
        CHILDRENLOCKLEVEL.put(6, 6);
        CHILDRENLOCKLEVEL.put(7, 7);
        EQTYPE.put(0, 0);
        EQTYPE.put(1, 1);
        EQTYPE.put(2, 2);
        EQTYPE.put(3, 3);
        EQTYPE.put(4, 4);
        EQTYPE.put(5, 5);
        EQTYPE.put(6, 6);
        EQTYPE.put(7, 7);
        FACTORYSYSTEMREGIONCODE.put(0, 0);
        FACTORYSYSTEMREGIONCODE.put(1, 1);
        FACTORYSYSTEMREGIONCODE.put(2, 2);
        FACTORYSYSTEMREGIONCODE.put(3, 3);
        FACTORYSYSTEMREGIONCODE.put(4, 4);
        FACTORYSYSTEMREGIONCODE.put(5, 5);
        FACTORYSYSTEMREGIONCODE.put(6, 6);
        FACTORYSYSTEMTVAREA.put(0, 0);
        FACTORYSYSTEMTVAREA.put(1, 1);
        FACTORYSYSTEMTVAREA.put(2, 2);
        FACTORYSYSTEMTVAREA.put(3, 3);
        FACTORYSYSTEMTVAREA.put(4, 4);
        FACTORYSYSTEMTVAREA.put(5, 5);
        FACTORYSYSTEMTVAREA.put(6, 6);
        FACTORYSYSTEMTVAREA.put(7, 7);
        FACTORYSYSTEMTVAREA.put(8, 8);
        INPUTGAIN.put(1, 1);
        INPUTGAIN.put(2, 2);
        INPUTGAIN.put(3, 3);
        INPUTGAIN.put(4, 4);
        INPUTGAIN.put(5, 5);
        INPUTGAIN.put(6, 6);
        INPUTGAIN.put(7, 7);
        INPUTGAIN.put(8, 8);
        INPUTGAIN.put(9, 9);
        INPUTGAIN.put(10, 10);
        INPUTGAIN.put(11, 11);
        INPUTGAIN.put(12, 12);
        FACTORYFMLOCAL.put(0, 0);
        FACTORYFMLOCAL.put(1, 1);
        FACTORYFMLOCAL.put(2, 2);
        FACTORYFMLOCAL.put(3, 3);
        FACTORYFMLOCAL.put(4, 4);
        FACTORYFMLOCAL.put(5, 5);
        FACTORYFMLOCAL.put(6, 6);
        FACTORYFMLOCAL.put(7, 7);
        FACTORYFMLOCAL.put(8, 8);
        FACTORYFMLOCAL.put(9, 9);
        FACTORYFMLOCAL.put(10, 10);
        FACTORYFMLOCAL.put(11, 11);
        FACTORYFMLOCAL.put(12, 12);
        FACTORYFMLOCAL.put(13, 13);
        FACTORYFMLOCAL.put(14, 14);
        FACTORYFMLOCAL.put(15, 15);
        EQVALUE.put(0, 0);
        EQVALUE.put(1, 1);
        EQVALUE.put(2, 2);
        EQVALUE.put(3, 3);
        BALANCE.put(0, 0);
        BALANCE.put(1, 1);
        BALANCE.put(2, 2);
        BALANCE.put(3, 3);
        BALANCE.put(4, 4);
        BALANCE.put(5, 5);
        BALANCE.put(6, 6);
        BALANCE.put(7, 7);
        BALANCE.put(8, 8);
        BALANCE.put(9, 9);
        BALANCE.put(10, 10);
        BALANCE.put(11, 11);
        BALANCE.put(12, 12);
        BALANCE.put(13, 13);
        BALANCE.put(14, 14);
        BALANCE.put(15, 15);
        BALANCE.put(16, 16);
        BALANCE.put(17, 17);
        BALANCE.put(18, 18);
        BALANCE.put(19, 19);
        BALANCE.put(20, 20);
        BALANCE.put(21, 21);
        BALANCE.put(22, 22);
        BALANCE.put(23, 23);
        BALANCE.put(24, 24);
        SUBWGAIN.put(0, 0);
        SUBWGAIN.put(1, 1);
        SUBWGAIN.put(2, 2);
        SUBWGAIN.put(3, 3);
        SUBWGAIN.put(4, 4);
        SUBWGAIN.put(5, 5);
        SUBWGAIN.put(6, 6);
        SUBWGAIN.put(7, 7);
        SUBWGAIN.put(8, 8);
        SUBWGAIN.put(9, 9);
        SUBWGAIN.put(10, 10);
        SUBWGAIN.put(11, 11);
        SUBWGAIN.put(12, 12);
        SUBWGAIN.put(13, 13);
        SUBWGAIN.put(14, 14);
        SUBWGAIN.put(15, 15);
        SUBWGAIN.put(16, 16);
        SUBWGAIN.put(17, 17);
        SUBWGAIN.put(18, 18);
        SUBWGAIN.put(19, 19);
        SUBWGAIN.put(20, 20);
        SUBWGAIN.put(21, 21);
        SUBWGAIN.put(22, 22);
        SUBWGAIN.put(23, 23);
        SUBWGAIN.put(24, 24);
        EQ.put(0, 0);
        EQ.put(1, 1);
        EQ.put(2, 2);
        EQ.put(3, 3);
        EQ.put(4, 4);
        EQ.put(5, 5);
        EQ.put(6, 6);
        EQ.put(7, 7);
        EQ.put(8, 8);
        EQ.put(9, 9);
        EQ.put(10, 10);
        EQ.put(11, 11);
        EQ.put(12, 12);
        EQ.put(13, 13);
        EQ.put(14, 14);
        EQ.put(15, 15);
        EQ.put(16, 16);
        EQ.put(17, 17);
        EQ.put(18, 18);
        EQ.put(19, 19);
        EQ.put(20, 20);
        EQ.put(21, 21);
        EQ.put(22, 22);
        EQ.put(23, 23);
        EQ.put(24, 24);
        EQ.put(25, 25);
        EQ.put(26, 26);
        EQ.put(27, 27);
        EQ.put(28, 28);
    }

    public static int getKeyFromMapWithValue(SparseIntArray map, int value) {
        if (map == null) {
            return -1;
        }
        int index = map.indexOfValue(value);
        if (index != -1) {
            return map.keyAt(index);
        }
        return -1;
    }

    public static byte[] xy2FadeBalance(int x, int y) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5 = BALANCE_SPACE_MAX_VALUE;
        if (x > 0) {
            i = x;
        } else {
            i = 0;
        }
        int iFadBanFL = (i5 - i) - (y < 0 ? -y : 0);
        int i6 = BALANCE_SPACE_MAX_VALUE;
        if (x < 0) {
            i2 = -x;
        } else {
            i2 = 0;
        }
        int iFadBanFR = (i6 - i2) - (y < 0 ? -y : 0);
        int i7 = BALANCE_SPACE_MAX_VALUE;
        if (x > 0) {
            i3 = x;
        } else {
            i3 = 0;
        }
        int iFadBanRL = (i7 - i3) - (y > 0 ? y : 0);
        int i8 = BALANCE_SPACE_MAX_VALUE;
        if (x < 0) {
            i4 = -x;
        } else {
            i4 = 0;
        }
        int i9 = i8 - i4;
        if (y <= 0) {
            y = 0;
        }
        int iFadBanRR = i9 - y;
        if (iFadBanFL < 0) {
            iFadBanFL = 0;
        }
        if (iFadBanFR < 0) {
            iFadBanFR = 0;
        }
        if (iFadBanRL < 0) {
            iFadBanRL = 0;
        }
        if (iFadBanRR < 0) {
            iFadBanRR = 0;
        }
        return new byte[]{(byte) iFadBanFL, (byte) iFadBanFR, (byte) iFadBanRL, (byte) iFadBanRR};
    }

    public static EQ_TYPE getCurrentEqType(Context aContext) {
        if (aContext == null) {
            throw new NullPointerException();
        }
        int mCurrentEQ = System.getInt(aContext.getContentResolver(), EQ_KEY, EQ_TYPE_DEFAULT.index);
        if (mCurrentEQ < 0 || mCurrentEQ >= Array.gEQTypePos.length) {
            return EQ_TYPE_DEFAULT;
        }
        return EQ_TYPE.getByIndex(mCurrentEQ);
    }

    public static void setCurrentEqType(Context aContext, EQ_TYPE aType) {
        if (aContext == null || aType == null) {
            throw new NullPointerException();
        }
        System.putInt(aContext.getContentResolver(), EQ_KEY, aType.index);
    }
}
