package com.hzbhd.midware.constant;

public class MediaConstant {
    public static final String ALBUM_ID = "album_id";
    public static final String ALBUM_NAME = "album_name";
    public static final String ARTIST_ID = "artist_id";
    public static final String ARTIST_NAME = "artist_name";
    public static final String Albums = "Albums";
    public static final String Artists = "Artists";
    public static final String CATEGORY = "category";
    public static final String COMPOSER_ID = "composer_id";
    public static final String COMPOSER_NAME = "composer_name";
    public static final String Composers = "composers";
    public static final String DURATION = "duration";
    public static final String FILE_ID = "file_id";
    public static final String FILE_SIZE_B = "B";
    public static final String FILE_SIZE_GB = "GB";
    public static final String FILE_SIZE_KB = "KB";
    public static final String FILE_SIZE_MB = "MB";
    public static final String FILE_SIZE_NA = "N/A";
    public static final String FILE_SIZE_TB = "TB";
    public static final String GENRE_ID = "genre_id";
    public static final String GENRE_NAME = "genre_name";
    public static final String Genres = "Genres";
    public static final String ID3CATEGORYTYPE = "id3categorytype";
    public static final String PIC_NAME = "pic_name";
    public static final String PLAYLIST_NAME = "playlist_name";
    public static final String Playlists = "Playlists";
    public static final String Songs = "Songs";
    public static final String TITLE = "title";
    public static final String TOTAL = "total";
    public static final String TRACK_NUMBER = "track_number";
    public static final String _ID = "_id";
}
