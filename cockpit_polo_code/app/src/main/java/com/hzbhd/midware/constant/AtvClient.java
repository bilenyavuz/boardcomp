package com.hzbhd.midware.constant;

public interface AtvClient {
    void reportChannelFrequncy(float f);

    void reportChannelList(float f);

    void reportChannelStatus(int i, int i2, int i3, int i4);

    void reportParkingState(boolean z);

    void reportVideoStatus(int i, int i2, int i3);
}
