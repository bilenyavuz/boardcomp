package com.hzbhd.midware.constant;

public interface AuxVideoConstantsDef {

    public interface ParkingCallBackListenr {
        void onParingStateChanged(int i);

        void reportVideoStatus(VIDEO_TYPE video_type, VIDEO_STATE video_state, VIDEO_FORMAT video_format);
    }

    public enum VIDEO_FORMAT {
        NTSC,
        PAL,
        SECAM
    }

    public enum VIDEO_STATE {
        OFF,
        AUDIO_ONLY_ON,
        VIDEO_ON
    }

    public enum VIDEO_TYPE {
        MPEG,
        AUX2,
        TV,
        CAMERA,
        DVDC,
        AUX1,
        IPOD
    }
}
