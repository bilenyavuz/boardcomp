package com.hzbhd.midware.constant;

public class NavProtocol {
    public static final int FMHzsetting_Req_0x30 = 48;
    public static final int TTSdelay_Ind_0x28 = 40;
    public static final int TTSdelay_confirm_0x28 = 40;
    public static final int brightnessAdjust_Req = 9;
    public static final int btdial_Req_0x2f = 47;
    public static final int btstatus_Ind_0x2e = 46;
    public static final int carRate_Req = 10;
    public static final int clock_Answer0x07 = 7;
    public static final int clock_Req_0x23 = 35;
    public static final int exitNav_Ind_0x02 = 2;
    public static final int exitNav_Req_0x32 = 50;
    public static final int initbootstatus_Ind_0x64 = 100;
    public static final int initbootstatus_Req_0x43 = 67;
    public static final int lightstatus_Ind = 8;
    public static final int media_Req_0x03 = 3;
    public static final int mediaplaymsg_Ind_0x27 = 39;
    public static final int protocol_Req_0x34 = 52;
    public static final int protocol_answer_0x35 = 53;
    public static final int showNav_Ind_0x33 = 51;
    public static final int sourcecontrol_Req_0x26 = 38;
    public static final int sourcemenu_Req = 11;
    public static final int sourceswitch_Req_0x24 = 36;
    public static final int turnmsg_Ind_0x2c = 44;
    public static final int volumecontrol_Req_0x04 = 4;
}
