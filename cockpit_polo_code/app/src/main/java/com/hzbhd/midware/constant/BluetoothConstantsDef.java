package com.hzbhd.midware.constant;

import android.annotation.SuppressLint;
import com.hzbhd.midware.constant.channelManagerDef.MEDIA_SOURCE_ID;

public interface BluetoothConstantsDef {
    public static final String BLUETOOTH_CALL_ACTION = "com.hzbhd.bluetooth.call.action";
    public static final String BLUETOOTH_IMPORT_VCARD_ACTION = "com.hzbhd.bluetooth.import.vcard.action";
    public static final int CONTACT_CONTACT_STATUS = 2;
    public static final int CONTACT_DISPLAY_NAME = 1;
    public static final int CONTACT_DISPLAY_NAME_ALTERNATIVE = 5;
    public static final int CONTACT_ID = 0;
    public static final int CONTACT_LOOKUP_KEY = 3;
    public static final int CONTACT_STARRED = 4;
    public static final int DATA_SET = 0;
    @SuppressLint({"InlinedApi"})
    public static final String[] FILTER_PROJECTION_ACCOUNT = {"data_set"};
    public static final String[] FILTER_PROJECTION_PRIMARY = {"_id", "display_name", "contact_status", "lookup", "starred", "display_name_alt"};
    @SuppressLint({"InlinedApi"})
    public static final String[] FILTER_PROJECTION_PRIMARY_PHONE = {"_id", "display_name"};
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_WRITE = 3;
    public static final int PHONE_CONTACT_DISPLAY_NAME = 1;
    public static final int PHONE_RAW_CONTACT_ID = 0;
    public static final String SNIPPET_ARGS = "\u0001,\u0001,…,5";
    public static final String SNIPPET_ELLIPSIS = "…";
    public static final char SNIPPET_END_MATCH = '\u0001';
    public static final int SNIPPET_MAX_TOKENS = 5;
    public static final char SNIPPET_START_MATCH = '\u0001';

    public enum A2DP_ACTION {
        NOT_INIT,
        READY,
        CONNECTING,
        CONNECTED,
        STREAMING,
        DISCONNECTED
    }

    public enum A2DP_PLAY_STATE {
        PLAY,
        PAUSE
    }

    public enum AUDIO_TRUANSFER_STATE {
        PHONE,
        HAND_FREE
    }

    public enum BLUETOOTH_CALL_METHOD {
        SETNAME,
        SETPIN,
        CONNECT_DEVICE,
        CONNECT_HFP,
        CONNECT_A2DP,
        DISCONNECT_HFP,
        DISCONNECT_A2DP,
        HANGUP,
        REJECT,
        TERMINATE,
        CALLOUT
    }

    public enum BLUETOOTH_CALL_RESULT {
        SUCCESS,
        FAIL,
        TIMEOUT
    }

    public enum BLUETOOTH_SERVICE_MSG {
        INIT_BLUETOOTH,
        POWER_OFF_BLUETOOTH,
        POWER_ON_BLUETOOTH,
        INIT_BLUETOOTH_NAME,
        INIT_BLUETOOTH_PIN,
        INIT_BLUETOOTH_PAIR_MODE,
        INIT_BLUETOOTH_SSP,
        INIT_BLUETOOTH_AUTO_CONNECT,
        INIT_BLUETOOTH_AUTO_ANWSER,
        INIT_BLUETOOTH_DISCOVERABLE,
        BLUETOOTH_STATE_OFF,
        START_BLUETOOTH_UPDATE,
        INIT_BLUETOOTH_UPDATE,
        GET_PAIRED_DEVICES_LIST,
        AUTO_CONNECT_LAST_DEVICE_MSG,
        BLUETOOTH_AUTO_ANSWER,
        BLUETOOTH_NOT_CONNECT_MSG,
        UPDATE_TIME,
        UPDATE_TIME_THREE_WAY_CALL,
        SYNC_CALL_LOG_MSG,
        QUERY_CALL_LOG_MSG,
        UPDATE_STATUS_BAR_MSG,
        REQ_AVRCP_PLAY_STATUS_MSG,
        REQ_CONNECT_HFP_MSG,
        REQ_CONNECT_A2DP_MSG,
        RECHECK_A2DP_STATE_MSG,
        FOCUSCHANGE,
        REQUEST_SPP_CONNECT,
        REQUEST_SPP_DISCONNECT,
        SEND_START_UPDATE_COMMAND,
        SEND_UART_SPEED_COMMAND,
        SEND_UART_SPEED_PLF_COMMAND,
        BLUETOOTH_UPDATE_FAIL,
        CHECK_A2DP_STATUS,
        CHECK_HFP_STATUS
    }

    public interface BluetoothUpdateListener {
        void A2DPActionChanged(A2DP_ACTION a2dp_action);

        void A2dpPlayStateChanged(A2DP_PLAY_STATE a2dp_play_state);

        void BTAudioChannelRequest(boolean z, MEDIA_SOURCE_ID media_source_id);

        void HFPActionChanged(HFP_ACTION hfp_action);

        void HFPMicStateChanged(MIC_MUTE_STATE mic_mute_state);

        void HFPSwitchStateChanged(AUDIO_TRUANSFER_STATE audio_truansfer_state);

        void InquiryStateChanged(boolean z);

        void MethodResultReturned(BLUETOOTH_CALL_METHOD bluetooth_call_method, BLUETOOTH_CALL_RESULT bluetooth_call_result);

        void newConnection(int i, String str);

        void newDevicePaired(String str, String str2);

        void newDeviceSearch(String str, String str2);

        void phoneBookSync(PHONE_BOOK_RECORDS_TYPE phone_book_records_type, String str, String str2, PHONEBOOK_SYNC_ACTION phonebook_sync_action);
    }

    public enum CONNECT_PROFILE {
        HFP,
        A2DP
    }

    public enum HFP_ACTION {
        NOT_INIT,
        READY,
        CONNECTING,
        CONNECTED,
        DISCONNECTED,
        OUTGOING_ESTABLISH,
        INCOMING_ESTABLISH,
        ACTIVE,
        CALLERNUM_OK,
        WAITING_CALL,
        MULTI_CALL,
        CALL_ON_HOLD
    }

    public enum MIC_MUTE_STATE {
        MUTE,
        UNMUTE
    }

    public enum PHONEBOOK_SYNC_ACTION {
        START,
        PROGRESSING,
        END,
        TIMEOUT,
        ERROR
    }

    public enum PHONE_BOOK_RECORDS_TYPE {
        CONTACT_PHONE,
        CONTACT_SIMCard,
        MISSED_CALL,
        RECEIVED_CALL,
        DIALED_CALL
    }

    public enum PHONE_BOOK_SYNC_RECORDS {
        PHONE_MEMORY,
        SIM_CARD,
        PHONE_AND_SIMCard,
        CALL_HISTORY,
        ALL_RECORDS
    }

    public enum THREE_WAY_CALL_ACTION {
        REJECT_WAITING_OR_RELEASE_HOLD,
        RELEASE_ACTIVE_AND_ACCEPT_OTHER,
        ACCEPT_WAITING_AND_HOLD_ACTIVE
    }
}
