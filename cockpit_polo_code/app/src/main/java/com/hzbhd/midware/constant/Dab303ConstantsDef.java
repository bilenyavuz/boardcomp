package com.hzbhd.midware.constant;

public interface Dab303ConstantsDef {
    public static final byte DAB_ALTS_AVAIL = 1;
    public static final byte DAB_AUDIO_ATTENUATION = 30;
    public static final byte DAB_BAND_BAND3 = 2;
    public static final byte DAB_BAND_BAND3_LBAND = 5;
    public static final byte DAB_BAND_CANADA = 3;
    public static final byte DAB_BAND_KOREA = 4;
    public static final byte DAB_BAND_LBAND = 1;
    public static final byte DAB_BAND_UK = 0;
    public static final byte DAB_CLEAR_SERVICES = 2;
    public static final int DAB_FOLLOW_ALL = 1;
    public static final int DAB_FOLLOW_DAB_DAB = 2;
    public static final int DAB_FOLLOW_DAB_RDS = 3;
    public static final int DAB_FOLLOW_NONE = 0;
    public static final byte DAB_FS_FAIL = -123;
    public static final byte DAB_FS_OK = 0;
    public static final byte DAB_IDLE = 0;
    public static final byte DAB_INACTIVE = 0;
    public static final byte DAB_LIST_END = 1;
    public static final byte DAB_NONE = 0;
    public static final byte DAB_NOTIFY_OFF = 0;
    public static final byte DAB_NOTIFY_ON = 1;
    public static final byte DAB_NO_ALTS_AVAIL = 0;
    public static final byte DAB_PACKET_BAD = Byte.MIN_VALUE;
    public static final byte DAB_RESET = 1;
    public static final byte DAB_SCAN = 1;
    public static final byte DAB_SCANNING = 5;
    public static final byte DAB_SELECTED = 4;
    public static final byte DAB_SELECTING = 3;
    public static final byte DAB_TA_OFF = 0;
    public static final byte DAB_TA_ON = 2;
    public static final byte DAB_TUNED = 2;
    public static final byte DAB_TUNING = 1;
    public static final byte FS_ITEM_DOES_NOT_EXIST = -126;
    public static final byte FS_NODE_BLOCKED = -125;
    public static final byte FS_NODE_DOES_NOT_EXIST = -127;
    public static final byte FS_REPLY_TOO_BIG = -124;
    public static final byte FS_REQUEST_INVALID = -122;

    public enum DAB_MESSAGE_IDS {
        ITEM_GET,
        ITEM_GET_RESPONSE,
        LIST_GET,
        LIST_GET_NEXT,
        LIST_GET_PREV,
        LIST_GET_RESPONSE,
        ITEM_SET,
        ITEM_SET_RESPONSE,
        LIST_SET,
        LIST_SET_RESPONSE,
        ITEM_SETNOTIFY,
        ITEM_SETNOTIFY_RESPONSE,
        ITEM_NOTIFICATION,
        RESPONSE,
        LIST_COUNT,
        LIST_COUNT_RESPONSE
    }
}
