package com.hzbhd.midware.constant;

public class SourceSwitchConstant {

    public enum SourceActionType {
        STOP,
        RUN,
        UPDATE,
        DISCONNECT,
        SourceChange,
        StartActivity,
        PAUSE,
        UIFinish
    }

    public enum SourceSwitchStatus {
        Start,
        BT_disconnect,
        PreSource_stop,
        NewSource_RUN,
        StartActivity,
        PreSource_UIFinish,
        End
    }
}
