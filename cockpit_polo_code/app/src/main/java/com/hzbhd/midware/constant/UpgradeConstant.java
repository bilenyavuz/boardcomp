package com.hzbhd.midware.constant;

public interface UpgradeConstant {
    public static final boolean DEBUG = false;
    public static final int ITEM_MUSIC_INDEX = 0;
    public static final int ITEM_PHOTO_INDEX = 2;
    public static final int ITEM_VIDEO_INDEX = 1;
    public static final int MCU_FileReadFail = 0;
    public static final String MCU_FileReadFailTag = "mcuUPDFile";
    public static final int MCU_UPGRADE_CHECKSUM_CFM = 150;
    public static final int MCU_UPGRADE_CHECKSUM_REQ = 22;
    public static final int MCU_UPGRADE_DATA_CFM = 149;
    public static final int MCU_UPGRADE_DATA_REQ = 21;
    public static final int MCU_UPGRADE_FAIL = 9;
    public static final int MCU_UPGRADE_START_CFM = 148;
    public static final int MCU_UPGRADE_START_REQ = 20;
    public static final int MCU_UPGRADE_SUCCESS = 8;
    public static final String MCU_UpdateEnd = "END";
    public static final String MCU_UpgradeProgress = "progress";
    public static final int MCU_UpgradeResult = 2;
    public static final String MCU_UpgradeResultTag = "result";
    public static final int REQUEST_CODE_PROGRESS = 2;
    public static final int REQUEST_CODE_WARN = 1;
    public static final String TAG = "UPGRADE";
    public static final String UpgradeAction = "com.hzbhd.intent.action.upgrade";
    public static final String UpgradeServiceAction = "com.hzbhd.intent.action.upgrade.service";

    public enum UpgradeFileType {
        NO_FILE,
        MCU,
        HDMI,
        MPEG,
        BLUETOOTH
    }
}
