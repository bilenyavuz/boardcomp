package com.hzbhd.midware.constant;

public interface MediaControlConstantsDef {

    public enum EQ_MODE {
        Base,
        Voice,
        Power,
        Nature,
        HeavyBass,
        Character
    }

    public static class FadeBalanceVal {
        public int fadeX;
        public int fadeY;
    }

    public enum POSITION_CHANNEL {
        FRONT_LEFT,
        FRONT_RIGHT,
        BACK_LEFT,
        BACK_RIGHT
    }
}
