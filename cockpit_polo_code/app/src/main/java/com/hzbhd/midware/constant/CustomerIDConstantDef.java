package com.hzbhd.midware.constant;

//import android.app.DefaultSharedUtil;
import android.content.Context;
import android.util.Log;

import com.hzbhd.midware.constant.channelManagerDef.MEDIA_SOURCE_ID;
import org.frau_uas.polocockpit.R;

public class CustomerIDConstantDef {
    private static Object lockObject = new Object();
    private static CustomerIDConstantDef mCustomerIDConstantDef = new CustomerIDConstantDef();
    private static CUSTOMER_ID mCustomerId = CUSTOMER_ID.INVALID;
    private static LedName mLedName;

    /* renamed from: com.hzbhd.midware.constant.CustomerIDConstantDef$1 reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID = new int[CUSTOMER_ID.values().length];
    }

    public enum ATV_AREA {
        USA,
        EUROPE,
        CHINA,
        ITALY,
        OIRT,
        NEW_ZEALAND,
        SOUTH_AFRICA,
        FRENCH
    }

    public enum CUSTOMER_ID {
        BHD_AWA8001,
        INVALID
    }

    public enum DAB_TYPE {
        DAB302,
        DAB303
    }

    public enum LED_NAME {
        DEFAULT,
        OPTION
    }

    public class LedName {
        public String mDefault = "Default";
        public String mOption = "Option";

        public LedName() {
        }
    }

    public enum MPEG_TYPE {
        ZORAN,
        SPEH
    }

    public enum PROCDUCTION_EXAMPLE {
        EP,
        PP
    }

    public enum PRODUCTION_CODE {
        AWA8001,
        AN6QH7,
        AVT
    }

    static {
        CustomerIDConstantDef customerIDConstantDef = mCustomerIDConstantDef;
        customerIDConstantDef.getClass();
        LedName mLedName;
    }

    public static boolean isTProject() {
        try {
            if (getCustomerID().name().startsWith("T_")) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isAProject() {
        try {
            if (getCustomerID().name().startsWith("T_")) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public static String getCustomerIDForMixString() {
        CUSTOMER_ID customerID = getCustomerID();
        return "";
    }

    public static CUSTOMER_ID getCustomerID() {
        CUSTOMER_ID customer_id;
        synchronized (lockObject) {
//            try {
                if (mCustomerId == CUSTOMER_ID.INVALID) {
                    mCustomerId = getCustomerIDFromProperty();
                }
                customer_id = mCustomerId;
//            }
        }
        return customer_id;
    }

    public static void setCustomerId(CUSTOMER_ID id) {
        synchronized (lockObject) {
            mCustomerId = id;
        }
    }

    public static boolean canSupportScanner() {
        return true;
    }

    public static boolean canSupportSourceSwitch() {
        return true;
    }

    public static boolean canSupportVolumeSetting() {
        return true;
    }

    public static boolean canSupportAutoTestEQ() {
        return true;
    }

    public static boolean canSupportAux() {
        return true;
    }

    public static boolean canSupportCanbox() {
        return false;
    }

    public static boolean canSupportDTV() {
        return true;
    }

    public static boolean canSupportIPOD() {
        return false;
    }

    /*
    public static boolean canSupportNav() {
        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return true;
    }

    public static boolean canSupportCDC() {
        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return false;
    }

    public static boolean canSupportMPEG() {
        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return true;
    }
*/
    public static boolean canSupportUARTMPEG() {
        return false;
    }

    public static boolean canSupportDAB() {
        return false;
    }

    public static boolean canSupportUARTTPMSORO() {
        return false;
//        return DefaultSharedUtil.getBool(SHARED_KEY.TPMSORO.getKey());
    }

    public static boolean canSupportSXM() {
        return false;
    }

    public static boolean canSupportSetting() {
        return true;
    }

    public static boolean canSupportBackCar() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return false;
//        return true;
    }

    public static boolean canSupportUpgrade() {
        return true;
    }

    public static boolean canSupportUpgradeUI() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
//        return true;
        return false;
    }

    public static boolean canSupportMisc() {
        return true;
    }

    public static boolean canSupportRadio() {
        return true;
    }

    public static boolean canMusicSupportAAC() {
        return true;
    }

    public static boolean canSupportAppInstallLimit() {
        return false;
    }

    public static DAB_TYPE getDABType() {
//        CUSTOMER_ID customerID = SettingManager.getInstance().getCustomerID();
        return DAB_TYPE.DAB303;
    }

    public static boolean canShowVolumeSettingAuxIn2() {
//        CUSTOMER_ID customerID = SettingManager.getInstance().getCustomerID();
        return false;
    }

    public static boolean canShowVolumeSettingDTV() {
//        CUSTOMER_ID customerID = SettingManager.getInstance().getCustomerID();
//        return true;
        return false;
    }

    public static boolean canShowVolumeSettingDAB() {
//        CUSTOMER_ID customerID = SettingManager.getInstance().getCustomerID();
        return false;
    }

    public static boolean canShowVolumeSettingSXM() {
//        CUSTOMER_ID customerID = SettingManager.getInstance().getCustomerID();
        return false;
    }

    public static boolean canShowRadioRDS() {
//        CUSTOMER_ID customerID = SettingManager.getInstance().getCustomerID();
        return true;
    }

    public static MEDIA_SOURCE_ID getDefaultStartSource() {
        MEDIA_SOURCE_ID media_source_id = MEDIA_SOURCE_ID.FM;
//        CUSTOMER_ID customerID = SettingManager.getInstance().getCustomerID();
        return MEDIA_SOURCE_ID.FM;
    }

    public static boolean canShowBTiMode() {
//        CUSTOMER_ID customerID = SettingManager.getInstance().getCustomerID();
//        return true;
        return false;
    }

    public static boolean canShowPhoneMIC() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return false;
    }

    public static boolean canShowTVArea() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return false;
    }

    public static boolean canShowSettingParking() {
        return true;
    }

    public static boolean canShowSettingLighting() {
        return true;
    }

    public static boolean canShowSettingSoundMix() {
        return true;
    }

    public static int getUartPortDAB() {
        return 4;
    }

    public static int getUartPortSXM() {
        PROCDUCTION_EXAMPLE productionExample = getProductionExample();
        return 4;
    }

    public static int getVideoOutMode() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return 0;
    }

    public static boolean canShowRadarBackCarCamera() {
        return true;
    }

    public static int isSupportRearUSBForiPod() {
        return 1;
    }

    private static PROCDUCTION_EXAMPLE getProductionExample() {
        String id = getSystemProperties("production.example");
        if (id == null) {
            Log.e("getProductionExample", "getProductionExample() == null");
            return PROCDUCTION_EXAMPLE.PP;
        }
        for (PROCDUCTION_EXAMPLE temID : PROCDUCTION_EXAMPLE.values()) {
            if (id.equals(temID.toString())) {
                return temID;
            }
        }
        Log.e("getProductionExample", "no PROCDUCTION_EXAMPLE for " + id);
        return PROCDUCTION_EXAMPLE.PP;
    }

    private static CUSTOMER_ID getCustomerIDFromProperty() {
        String id = getSystemProperties("customer.id");
        if (id == null) {
            Log.e("CustomerIDConstantDef", "getSystemProperties(customer.id) == null");
            return CUSTOMER_ID.BHD_AWA8001;
        }
        for (CUSTOMER_ID temID : CUSTOMER_ID.values()) {
            if (id.equals(temID.toString())) {
                return temID;
            }
        }
        Log.e("CustomerIDConstantDef", "no CUSTOMER_ID for " + id);
        return CUSTOMER_ID.BHD_AWA8001;
    }

    private static String getSystemProperties(String key) {
        try {
            return (String) invokeStaticMethod("android.os.SystemProperties", "get", new Object[]{key});
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static Object invokeStaticMethod(String className, String methodName, Object[] args) throws Exception {
        Class ownerClass = Class.forName(className);
        Class[] argsClass = new Class[args.length];
        int j = args.length;
        for (int i = 0; i < j; i++) {
            argsClass[i] = args[i].getClass();
        }
        return ownerClass.getMethod(methodName, argsClass).invoke(null, args);
    }

    public static int getDefaultInputMethod() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return 0;
    }

    public static boolean canShowNewVolumePanel() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return false;
    }

    public static String getIPodAuthProductID() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return "AWA8001";
    }

    public static String getIPodAccessoryName() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return "Car DVD Player";
    }

    public static String getIPodAccessoryManufaturerName() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return "Bohuida";
    }

    public static boolean canShowRadioPresetName() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return true;
    }

    public static boolean canShowRadioFavouriteList() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return true;
    }

    public static String getAutoTestModelName() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[getCustomerID().ordinal()];
        return "AWA8001";
    }

    public static boolean showCarBackCameraByAPP() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return true;
    }

    public static boolean canMuteRadioWhenMixAudio() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return false;
    }

    public static PRODUCTION_CODE canSetScalerBright() {
//        CUSTOMER_ID customId = SettingManager.getInstance().getCustomerID();
        PRODUCTION_CODE production_code = PRODUCTION_CODE.AWA8001;
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[customId.ordinal()];
        return PRODUCTION_CODE.AWA8001;
    }

    public static MPEG_TYPE getMPEGType() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return MPEG_TYPE.ZORAN;
    }

    public static boolean startCustomerDefaultApp() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return false;
    }

    public static boolean canHandleScreenBrigtnessWithCarLight() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return false;
    }

    public static boolean canShowEQFilterPass() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return false;
    }

    public static String getEQBaseFreqName() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return "Frequency";
    }

    public static String getEQFABName() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return "FAB";
    }

    public static boolean canShowVolumeSettingNavigagion() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return false;
    }

    public static LedName getLedLightingName() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return mLedName;
    }

    public static String getRadioRegionName() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return "South America";
    }

    public static boolean canShowSettingVirtualKeys() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return false;
    }

    public static boolean canShowDivxRegcode() {
//        int i = AnonymousClass1.$SwitchMap$com$hzbhd$midware$constant$CustomerIDConstantDef$CUSTOMER_ID[SettingManager.getInstance().getCustomerID().ordinal()];
        return false;
    }

    public static String getAppVersion(Context context) {
        return context.getString(R.string.midware_version);
    }
}
