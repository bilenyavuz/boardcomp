This page documents used frameworks and project dependencies.

NodeJS Server
* [Node JS](https://nodejs.org/en/) - NodeJS Server
* [Express](http://expressjs.com/de/) - Web Framework
* [passportJS](http://passportjs.org) - Authentication Framework
* [jsonWebToken JWT](https://jwt.io) - Authentication Token
* [Morgan](https://www.npmjs.com/package/morgan) - Logger 
* [Mongoose](http://mongoosejs.com) - mongoDB communication Framework

Database
* [MongoDB](https://www.mongodb.com) - Document based Database