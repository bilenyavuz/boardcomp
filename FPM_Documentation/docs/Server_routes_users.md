# …/users

## GET

consumes: -/-

produces: application/json

### Request

Needed role: Administrator

**Header**
`
Authentication: Bearer <Token>
`
### Response

**Success**
 
HTTP status **200**

**Body**

JSON Array with [User](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_user) objects 

**Error**

HTTP status **401**
`Unauthorized`

HTTP status **403**
`Forbidden`

HTTP status **500**
```json
{"message": "<ErrorMessage>"}
```

## POST

consumes: application/json

produces: -/-

### Request

Needed role: Administrator

**Header**
`
Authentication: Bearer <Token>

Content-Type: application/json
`
**Body**

JSON Object [User](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_user) 

without the values: `_id`, `__v`, `hash`, `salt`.

additional value: `password`

```json
  {
    "username": "<UserName>",
    "password": "<Password>",
    "firstname": "<FirstName>",
    "lastname": "<LastName>",
    "email": "<eMail>",
    "phone": "<Phone>",
    "role": "administrator|driver|…|disabled"
  }
```

### Response

**Success**
 
HTTP status **201**

##

**Error**

HTTP status **401**
`Unauthorized`

HTTP status **403**
`Forbidden`

HTTP status **500**
```json
{"message": "<ErrorMessage>"}
```

# …/users/:id

## GET

consumes: -/-

produces: application/json

### Request

Needed role: Administrator

**Header**
`
Authentication: Bearer <Token>
`
### Response

**Success**

HTTP status **200**

**Body**

JSON Object [User](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_user) witch's _id value matches the :id parameter 

**Error**

HTTP status **401**
`Unauthorized`

HTTP status **403**
`Forbidden`

HTTP status **500**
```json
{"message": "<ErrorMessage>"}
```

## PUT

consumes: application/json

produces: -/-

### Request

Needed role: Administrator

With a `PUT` request it is possible to change every value of the key-value pairs or just a single one. (full or partial update) 

**Header**

`
Authentication: Bearer <Token>

Content-Type: application/json
`

**Body**

JSON Object [User](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_user) 

```json
  {
    "username": "<UserName>",
    "password": "<Password>",
    "firstname": "<FirstName>",
    "lastname": "<LastName>",
    "email": "<eMail>",
    "phone": "<Phone>",
    "role": "administrator|driver|…|disabled",
    "password": "<password>"
  }
```

### Response

**Success**
 
HTTP status **200**

**Error**

HTTP status **401**
`Unauthorized`

HTTP status **403**
`Forbidden`

HTTP status **500**
```json
{"message": "<ErrorMessage>"}
```


## DELETE

Removes the [User](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_user) with the given :id from the database

### Request

**Header**

`
Authentication: Bearer <Token>
`

### Response

**Success**
 
HTTP status **200**

**Error**

HTTP status **401**
`Unauthorized`

HTTP status **403**
`Forbidden`

HTTP status **500**
```json
{"message": "<ErrorMessage>"}
```
