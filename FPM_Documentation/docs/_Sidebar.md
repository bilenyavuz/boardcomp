[**Home**](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki)

**General**

* [Responsibilities](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/General_responsibilities)

* [Project / coding conventions](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/General_conventions)

* [Privacy](https://github.com/fabianlocker/FuhrparkManagement_Documentation/wiki/General_Privacy_Questions)

* [Meeting agenda](https://github.com/fabianlocker/FuhrparkManagement_Documentation/wiki/General_Agenda)

**Android**

* Database
    * SQLLite
* Logic
    * [Route data](https://github.com/fabianlocker/FuhrparkManagement_Documentation/wiki/Route-data)
    * [List of cars](https://github.com/fabianlocker/FuhrparkManagement_Documentation/wiki/List-of-cars)
    * [Get data with AsyncTask] (https://github.com/fabianlocker/FuhrparkManagement_Documentation/wiki/Get-data-with-AsyncTask)    
* UI
* Frameworks
    * Realm.IO vs SQLite

**BackEnd**

* [Routes](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_routes_home)
    * […/login](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_routes_login)
    * […/register](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_routes_register)
    * […/users](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_routes_users)
    * […/cars](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_routes_cars)
    * […/drives](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_routes_drives)
    * […/reservations](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_routes_reservations)
* [Database](https://github.com/fabianlocker/FuhrparkManagement_Documentation/wiki/Server_database_home)
    * [Models](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_home)
        * [User](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_user)
        * [Car](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_car)
        * [Drive](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_drive)
        * [Reservation](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_reservation)

* [Frameworks](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_frameworks_home)

[**WebApp**](https://github.com/fabianlocker/FuhrparkManagement_Documentation/wiki/WebApplication_home)
* [UI](https://github.com/fabianlocker/FuhrparkManagement_Documentation/wiki/WebApplication_ui_home)
    * [MockUps](https://github.com/fabianlocker/FuhrparkManagement_Documentation/wiki/WebApplication_ui_mockups)
    * [Components](https://github.com/fabianlocker/FuhrparkManagement_Documentation/wiki/WebApplication_ui_components)
* [Logic](https://github.com/fabianlocker/FuhrparkManagement_Documentation/wiki/WebApplication_logic)
* [Frameworks](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/WebApplication_frameworks_home)