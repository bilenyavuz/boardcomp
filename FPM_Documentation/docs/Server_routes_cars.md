# …/cars

## GET

consumes: -/-

produces: application/json

### Request

**Header**

optional:
`
Authentication: Bearer <Token>
`

### Response

**Success**
 
HTTP status **200**

**Body**

JSON Array with [Car](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_car) objects 

**Error**

HTTP status **500**
```json
{"message": "<ErrorMessage>"}
```

## POST

consumes: application/json

produces: -/-

### Request

Needed role: Administrator

**Header**

`
Authentication: Bearer <Token>

Content-Type: application/json
`

**Body**

JSON Object [Car](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_car) 

without the values: `_id`, `__v`.

```json
{
  "vendor": "<Vendor>",
  "model": "<Model>",
  "mileage": 0,
  "available": false,
  "battery": 0,
  "charging": false,
  "chargedkWh": 0,
  "licenseNumber": "XX - XX 0000"
}
```

### Response

**Success**
 
HTTP status **201**

##

**Error**

HTTP status **401**
`Unauthorized`

HTTP status **403**
`Forbidden`

HTTP status **500**
```json
{"message": "<ErrorMessage>"}
```

# …/cars/:id

## GET

consumes: -/-

produces: application/json

### Request

Needed role: Administrator

**Header**

`
Authentication: Bearer <Token>
`

### Response

**Success**

HTTP status **200**

**Body**

JSON Object [car](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_car) witch's _id value matches the :id parameter 

**Error**

HTTP status **500**
```json
{"message": "<ErrorMessage>"}
```

## PUT

consumes: application/json

produces: -/-

### Request

Needed role: Administrator

With a `PUT` request it is possible to change every value of the key-value pairs or just a single one. (full or partial update) 

**Header**

`
Authentication: Bearer \<Token\>

Content-Type: application/json
`

**Body**

JSON Object [car](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_car) 

```json
{
  "vendor": "<Vendor>",
  "model": "<Model>",
  "mileage": 0,
  "available": false,
  "battery": 0,
  "charging": false,
  "chargedkWh": 0,
  "licenseNumber": "XX - XX 0000"
}
```

### Response

**Success**
 
HTTP status **200**

**Error**

HTTP status **401**
`Unauthorized`

HTTP status **403**
`Forbidden`

HTTP status **500**
```json
{"message": "<ErrorMessage>"}
```


## DELETE

Removes the [car](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_car) with the given :id from the database

### Request

**Header**

`
Authentication: Bearer <Token>
`

### Response

**Success**
 
HTTP status **200**

**Error**

HTTP status **401**
`Unauthorized`

HTTP status **403**
`Forbidden`

HTTP status **500**
```json
{"message": "<ErrorMessage>"}
```