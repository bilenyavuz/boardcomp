# WebFrontend General

## Access to WebFrontend
The newest Version of the WebFrontend is directly accessible [HERE](http://10.18.2.151:8100).

## Framework
Decission Making for React:
- Learning curve is better than other Frameworks
- React only View Layer
- Flexible with application logic, other frameworks can be used
- OpenSource
- State of the art framework


<img src="https://static.thinkster.io/uovYKNr.png" width="200" height="200" />