# Logic

##Refactoring components

In order to get rid of clunky code and improve code readability, you can cut out the classes into different jsx-modules.
Each module requires you to do two things:<br> <br>
1. Define the react component(at the top), by making a new variable:
`var React = require('react');`<br>
2. Export the component(at the bottom):
`module.exports = GreeterMessage;` //similar to the "return"-statement in a method <br>

The whole jsx file looks like the following:

GreeterMessage.jsx

`var React = require('react');`

`var GreeterMessage = React.createClass({`
    `render: function () {`
      `var name = this.props.name;`
      `var message = this.props.message;`

      `return (`
        `<div>`
          `<h1>Hello {name}!</h1>`
          `<p>{message}</p>`
        `</div>`
      `);`
    `}`
`});`

`module.exports = GreeterMessage;`
<br><br>
whenever you require a component in another file,whatever gets set to module.exports is what you gets returned.
<br><br>
If you want to access a component from another file, you define a variable at the top of your code and pass the filepath:<br>
`var GreeterMessage = require('./components/GreeterMessage.jsx');`

 