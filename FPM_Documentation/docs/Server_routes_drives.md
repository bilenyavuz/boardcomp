# …/drives

## GET

consumes: -/-

produces: application/json

### Request

Needed role: administrator

**Header**

`
Authentication: Bearer <Token>
`

### Response

**Success**
 
HTTP status **200**

**Body**

JSON Array with [Drive](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_drive) objects 

**Error**

HTTP status **401**
`Unauthorized`

HTTP status **403**
`Forbidden`

HTTP status **500**
```json
{"message": "<ErrorMessage>"}
```

## POST

consumes: application/json

produces: -/-

### Request

Needed role: driver

**Header**

`
Authentication: Bearer <Token>

Content-Type: application/json
`

**Body**

JSON Object [Drive](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_drive) 

without the values: `_id`, `__v`.

```json
{
  "user": "<ObjectId>",
  "car": "<ObjectId>",
  "startDate": "<date>",
  "endDate": "<date>",
  "startCoord": "<coords>",
  "endCoord": "<coords>",
  "startAddress": {
      "country": "<country>",
      "city": "<city>",
      "zip": "<zip>",
      "street": "<street>"
    },
  "endAddress": {
      "country": "<country>",
      "city": "<city>",
      "zip": "<zip>",
      "street": "<street>"
    },
  "startPOI": "<POI>",
  "endPOI": "<POI>",
  "startMileage": 0,
  "endMileAge": 0,
  "usedkWh": 0
}
```

### Response

**Success**
 
HTTP status **201**

##

**Error**

HTTP status **401**
`Unauthorized`

HTTP status **403**
`Forbidden`

HTTP status **500**
```json
{"message": "<ErrorMessage>"}
```

# …/drives/:id

## GET

consumes: -/-

produces: application/json

### Request

Needed role: administrator

**Header**

`
Authentication: Bearer <Token>
`

### Response

**Success**

HTTP status **200**

**Body**

JSON Object [Drive](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_drive) witch's _id value matches the :id parameter 

**Error**

HTTP status **401**
`Unauthorized`

HTTP status **403**
`Forbidden`

HTTP status **500**
```json
{"message": "<ErrorMessage>"}
```

## PUT

consumes: application/json

produces: -/-

### Request

Needed role: administrator

With a `PUT` request it is possible to change every value of the key-value pairs or just a single one. (full or partial update) 

**Header**

`
Authentication: Bearer <Token>

Content-Type: application/json
`

**Body**

JSON Object [Drive](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_drive) 

```json
{
  "user": "<ID>",
  "car": "<ID>",
  "startDate": "<date>",
  "endDate": "<date>",
  "startCoord": "<coords>",
  "endCoord": "<coords>",
  "startAddress": {
      "country": "<country>",
      "city": "<city>",
      "zip": "<zip>",
      "street": "<street>"
    },
  "endAddress": {
      "country": "<country>",
      "city": "<city>",
      "zip": "<zip>",
      "street": "<street>"
    },
  "startPOI": "<POI>",
  "endPOI": "<POI>",
  "startMileage": 0,
  "endMileAge": 0,
  "usedkWh": 0
}
```

### Response

**Success**
 
HTTP status **200**

**Error**

HTTP status **401**
`Unauthorized`

HTTP status **403**
`Forbidden`

HTTP status **500**
```json
{"message": "<ErrorMessage>"}
```


## DELETE

Removes the [Drive](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_drive) with the given :id from the database

### Request

**Header**

`
Authentication: Bearer <Token>
`

### Response

**Success**
 
HTTP status **200**

**Error**

HTTP status **401**
`Unauthorized`

HTTP status **403**
`Forbidden`

HTTP status **500**
```json
{"message": "<ErrorMessage>"}
```
