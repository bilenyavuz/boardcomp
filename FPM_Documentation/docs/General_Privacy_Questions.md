Riechert

1. Which data of the model can be accessible for administrators?

2. How we store data in the backend?

3. How we transmit data over Internet(Encryption)?

4. User Data Safety, data is persitent on device?

5. Organizational Process logbook, is the data model correct?

6. How we export data to tax report?

7. The design of a logbook entry? More Points of Interests in one drive?

8. Periods for the deletion of the data?


Deegener

1. Third party user access to admin frontend?

2. Process of User registration? Automatical accept user or review process?



##Meeting 2016-11-23

Anwesende: Fabian Locker, Kai Kroth, Herr Deegener, Frau Riechert

Hauptansprechpartner zur Organisatorischen Fragestellungen:
Frau Ruppert Datenschutz, Herr Becker Justiziariat ,Herr  Schreck Personalrat

Das Gesprächt mit Frau Riechert hat folgende Ergebnisse geliefert:

1. Die Speicherung der Adresse sollte nicht in der Lösung abgebildet werden, sonder über die Personalabteilung abegebildet werden. Dies verhindert Datenschutzrechtliche  Problematiken. 

2. Verschlüssung: Generell gilt: Eingesetzte Techniken zur Sicherung der Daten muss den aktuellen Stand der Technik entsprechen! 
Für das speichern sensibler Daten(wie Bewegenungprofile)sind die Anforderungen mit Frau Ruppert zu klären.

3. Um Chancen bei den entsprechenden Entscheidungsinstanzen zu haben sind bereits jetzt alle Daten zu verschlüsseln. Dies Betrifft die persistente Speicherung auf den Android Geräten als auch im BackEnd (Server) - alle Daten(Banken) sind zu verschlüsseln. Weiter ist jegliche Kommunikation zu verschlüsseln. 

| Source        | Destination   | Method  |
| ------------- |:-------------:| -------:|
| Android       | BackEnd       | HTTPS   |
| BackEnd       | Android       | HTTPS   |
| BackEnd       | Database      | SSL/TLS |
| Database      | BackEnd       | SSL/TLS |


Offene Fragen die sich aus dem Gespräch ergeben haben, die von uns zu klären sind:
Sind Richtlinien für die Aufbewahung der Fahrtenbücher vorhanden,wenn ja gibt es ein Prozess dazu? Wie sieht dieser aus?
Wer hat Zugriffrechte auf die Daten? Wie is der Ist-Zustand der Datenerhebung für Fahrzugbenutzer?
Personalrat Herr Schreck Export der Datenerhebeung, ist ein vier Augenprinzip heir Notwendig?
Informationen Fahrtbuch welche Einträge sind für die Steuer und Revision wichtig?
Ist eine Datenaufbewahrung erforderlich, wie sind die Anforderungen dieser?



