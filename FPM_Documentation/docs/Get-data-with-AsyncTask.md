### To get data from an URL, we have to realize it with Goolge's own class called AsyncTask

```java
public class JSONTask extends AsyncTask<String , String, List<Model>> {

        @Override
        protected List<Model> doInBackground(String... params) {

            // create and open a HttpURLConnection
            HttpURLConnection connection = null;
            BufferedReader bufferedReader = null;

            try {
                URL url = new URL("your url");
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                // now try to read the data and put it to a string variable
                InputStream inputStream = connection.getInputStream();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuffer stringBuffer = new StringBuffer();
                String line = "";
                while ((line = bufferedReader.readLine()) != null)
                {
                    stringBuffer.append(line);
                }

                // translate the string to JSON and put it to a list
                String finalJSONText = stringBuffer.toString();
                JSONArray parentArray = new JSONArray(finalJSONText);

                List<YourModel> myList = new ArrayList<>();
                for(int i=0; i<parentArray.length(); i++)
                {
                    JSONObject finalObject = parentArray.getJSONObject(i);

                    Model myModel = new Model();
                    myModel.setVendor(finalObject.getString("vendor"));
                    myModel.setModel(finalObject.getString("model"));
                    ........
                    ........

                    myList.add(myModel);
                }

                return myList;

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if(connection != null)
                    connection.disconnect();
                try {
                    if(bufferedReader != null)
                        bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }


        // get the result from "doInBackground" and show it on the UI
        @Override
        protected void onPostExecute(List<Model> result) {
            super.onPostExecute(result);

            for (int i = 0; i < result.size(); i++) {
                string_vendor = result.get(i).getVendor();
                textViewvCar.setText(strVendor);
            }
        }```