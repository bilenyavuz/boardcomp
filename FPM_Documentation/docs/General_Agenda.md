## 07.12.2016

1. Open discussion
2. Presentation: weekly reports
3. Logo discussion
4. etc

## 30.11.2016

1. Open discussion: Project problems
    1. Partizipation
2. Presentation: weekly reports
3. Shifted Topics
    1. Slack
    2. GitHub
        1. Partizipation
        2. .gitignore


## 23.11.2016

Meeting with Prof. Dr. Anne Riechert - Data protection / privacy

[Protocol](https://github.com/fabianlocker/FuhrparkManagement_Documentation/wiki/General_Privacy_Questions)

## 15.11.2016

1. Presentation: [Documentation Wiki](https://github.com/fabianlocker/FuhrparkManagement_Documentation/wiki)
2. Presentation: weekly reports
3. allocation of [responsibilities](https://github.com/fabianlocker/FuhrparkManagement_Documentation/wiki/General_responsibilities)
    1. Android
    2. WebApplication
    3. BackEnd
4. Determine all DataModels 
5. Project Language
    1. Documentation: english
    2. UI: german
    3. GitHub Commits: english
6. Slack (shifted)
7. GitHub (shifted)