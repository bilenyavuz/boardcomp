
## MockUps

This list shows the created MockUps and the gathered requierements during the planning phase:
####Login

<img src="https://github.com/fabianlocker/FuhrparkManagement_Documentation/blob/master/images/WebFrontend_mockups/login.png"  height="300" width="400">

User Story: User and Administrators should be able to login to the WebApp.

####Register

<img src="https://github.com/fabianlocker/FuhrparkManagement_Documentation/blob/master/images/WebFrontend_mockups/register.png"  height="300" width="400">

User Story: A user or admin can register for an account of the WebApp

###Admin interface
####Car Management

<img src="https://github.com/fabianlocker/FuhrparkManagement_Documentation/blob/master/images/WebFrontend_mockups/admin_carmgmt.png"  height="250" width="900">

User Story: An administator can create update and delte cars 

####User Management

<img src="https://github.com/fabianlocker/FuhrparkManagement_Documentation/blob/master/images/WebFrontend_mockups/admin_usermgmt.png"  height="250" width="900">

User Story: An administator can create update and delte users

####Reservation Management

<img src="https://github.com/fabianlocker/FuhrparkManagement_Documentation/blob/master/images/WebFrontend_mockups/admin_reservation_mgmt.png"  height="250" width="900">

User Story: An administator can create update and delte reservations

####Drive Export

<img src="https://github.com/fabianlocker/FuhrparkManagement_Documentation/blob/master/images/WebFrontend_mockups/admin_export.png"  height="300" width="600">

User Story: An administator can create update and delte cars 

###User interface

####Car reservation


<img src="https://github.com/fabianlocker/FuhrparkManagement_Documentation/blob/master/images/WebFrontend_mockups/user_booking1.png"  height="300" width="600">
<img src="https://github.com/fabianlocker/FuhrparkManagement_Documentation/blob/master/images/WebFrontend_mockups/user_booking2.png"  height="300" width="600">
<img src="https://github.com/fabianlocker/FuhrparkManagement_Documentation/blob/master/images/WebFrontend_mockups/user_booking3.png"  height="300" width="600">

User Story: A user can make a reservation by a car over a specific time period

####My reservations

<img src="https://github.com/fabianlocker/FuhrparkManagement_Documentation/blob/master/images/WebFrontend_mockups/user_reservation.png"  height="300" width="600">

User Story: A user can delete his own reservations to a car

####Free/Busy Lookup

<img src=""  height="300" width="600">

User Story: A user can lookup if a car is occupied on a specific date

####My Profile

<img src="https://github.com/fabianlocker/FuhrparkManagement_Documentation/blob/master/images/WebFrontend_mockups/user_editdetails.png"  height="300" width="600">

User Story: A user can modify his own profile information



