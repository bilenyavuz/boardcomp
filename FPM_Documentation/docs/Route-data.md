### We have to get some information about the route and the Android device, which is in the car, to send data over the network

By clicking the start and stop button, we have to set the following information about our route:

* User: show name of the driver in the start screen
* Car: to set the information we have to know for which car it is
* Start date: on which day and time the route starts
* End date: on which day and time the route stops
* Start coordinate: from which location does the user start
* End coordinate: on which location does the user stops
* Start address: to get the exact position, we have to use the GPS sensor of the device
* End address: see "Start address"
* Start mileage: to knwo the distance we have to record mileage of the car
* End mileage: see "Start mileage"
* Battery used kWh: how much battery is consumed