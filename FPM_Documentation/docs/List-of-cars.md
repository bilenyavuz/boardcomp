### This section describes the cars, which can be managed only by the web application. Here we use a list to show the cars and these can be selected in the corresponding vehicle to start the logbook.

For every car we get the [routes] (https://github.com/fabianlocker/FuhrparkManagement_Documentation/wiki/Server_routes_car) from the backend and use it in our Activity.

The following data are relevant for us, to visually represent the cars:
* vendor
* model
* mileage
* battery life
* picture (optional)