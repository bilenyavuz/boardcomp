# Naming Conventions

## Wiki Pages

Name format of new pages: 
* **REPOSITORYNAME_CATEGORY_SITENAME**

# Coding Conventions

## Date format

**ISO 8601**: 

`YYYY-MM-DDTHH:mm:ss.sssZ`

`2016-11-23T09:23:58.368Z`

## Registration number format

`X[XX]-A[B] 0[000]`

`F - US 341`


# Other Project Decisions

## Language
Project Documentation:

* English

UI:
* German