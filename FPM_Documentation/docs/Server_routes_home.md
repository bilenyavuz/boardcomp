This pages describes which routes are available. Further it gives you information about the consumed and produced data and details are presented in the specific subpage.


host: 10.18.2.151

port: 443

protocol: https

API endpoint: …/api

## […/login](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_routes_login)

Is the API endpoint for authentication. Returned [JWT](http://jwt.io) is used to determine the user role and the session. 

## […/register](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_routes_register)

Gives new users the possibility to create their own account.

## […/user](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_routes_user)

With this route one can interact with the [user model](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_user). It is possible to create / read / update / delete (CRUD) users. 

## […/car](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_routes_car)

The Endpoint car has to be user for CRUD interactions with the model [car](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_car)

## […/drive](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_routes_drive)

…/drive is the core of this application. This route implements CRUD operations for the [electronic logbook](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_drive).

## […/reservation](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_routes_reservation)

… is used to realize the nice-to-have feature to reserve a car. -> [Reservation model](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_reservation)