# WebApp User Interface

## Project Structure

In the following diagram you can see the project structure of the UI part of the Web Applicaiton project.

<img src="https://github.com/fabianlocker/FuhrparkManagement_Documentation/blob/master/images/WebFrontend_project_structure/project_structure.png"  height="250" width="400">

The WebFrontend is a single page application. The main entry point for client is the index.html, which included a java script resource which includes all logic and UI elemtents.

For the UI part of the project the app.js is responsible for routing and holds react components which are displayed on every page e.x. Navigation Header or Footer. For each route on the Web Application there is one main components which holds more sub-components. The modular approach supports a reusability of the sub-components in other main components. The structe of components can be found [HERE](https://github.com/fabianlocker/FuhrparkManagement_Documentation/wiki/WebApplication_ui_components).

