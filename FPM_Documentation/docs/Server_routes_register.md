# …/register

**POST**

consumes: application/json

produces: application/json

##Request
**Header**

`
Content-Type: application/json
`

**Body**

JSON Object [User](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_user) 

without the values: `_id`, `__v`, `role`, `hash`, `salt`.

additional value: `password`
```json
  {
    "username": "<UserName>",
    "password": "<Password>",
    "firstname": "<FirstName>",
    "lastname": "<LastName>",
    "email": "<eMail>",
    "phone": "<Phone>"
  }
```
##Response

###Success 
HTTP status **201**

**Body**
```json
{"token": "<Token>"}
```

##

###Error
HTTP status **400**

**Body**
```json
{"message": "Please fill out all fields"}
```
