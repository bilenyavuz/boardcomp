This section provides a rough overview about the entity models, details are shown in the subpages.

* The User model stores personal data like username, name, emailAddress or phone number. It represents a person/user and stores required data like its role - Driver / Administrator / etc. 

* To manage the vehicle fleet the car model stores properties such as vendor, model, mileage, ...
It is used to map drives to a car and prove which car was used for the journey.

* Core of the project is to replace the paper logbook with an electronic logbook, therefor the model drive is used. 
A drive contains a user, start and endpoint of the journey, and further the date/time and implicit duration of the journey. 

* The nice to have feature of a reservation function is realized in the reservation model. A User can reserve a car for a specific duration. This is not 'legally' binding. A reserved car can be taken from another person. - But remember, it can be figured out who took the car without reservation. ;) 
