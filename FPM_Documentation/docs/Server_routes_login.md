# …/login

**POST**

consumes: application/json

produces: application/json

##Request
**Header**

`
Content-Type: application/json
`

**Body**
```json
{"username":"<Username>", "password":"password"}
```

##Response

###Success 
HTTP status **200**

**Body**
```json
{"token": "<Token>"}
```

##

###Error
HTTP status **400**

**Body**
```json
{"message": "Please fill out all fields"}
```

##

HTTP status **401**

Case: unknown username

**Body**
```json
{
  "message": "Incorrect username."
}
```

Case: wrong password

**Body**
```json
{
  "message": "Incorrect password."
}
```