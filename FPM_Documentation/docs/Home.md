# **Welcome to the SoftwareProject WS2016/17 Documentation wiki!**

Please write all necessary documentation in this Repository/Wiki. 
Each Group has to document their **work** and **progress**.

Part of the documentation is also a listing and decision document of all used Frameworks.

**Rule to Add Wiki Entries:**
Name format of new pages: REPOSITORYNAME_CATEGORY_SITENAME

Example: WebApplication_framework_home