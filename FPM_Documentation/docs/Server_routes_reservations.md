# …/reservations

## GET

consumes: -/-

produces: application/json

### Request

Needed role: driver

**Header**

`
Authentication: Bearer <Token>
`

### Response

**Success**
 
HTTP status **200**

**Body**

JSON Array with [Reservation](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_reservation) objects 

**Error**

HTTP status **401**
`Unauthorized`

HTTP status **403**
`Forbidden`

HTTP status **500**
```json
{"message": "<ErrorMessage>"}
```

## POST

consumes: application/json

produces: -/-

### Request

Needed role: driver

**Header**

`
Authentication: Bearer <Token>

Content-Type: application/json
`

**Body**

JSON Object [reservation](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_reservation) 

without the values: `_id`, `__v`.

```json
{
  "user": "<ID>",
  "car": "<ID>",
  "startDate": "<Date>",
  "endDate": "<Date>"
}
```

### Response

**Success**
 
HTTP status **201**

##

**Error**

HTTP status **401**
`Unauthorized`

HTTP status **403**
`Forbidden`

HTTP status **500**
```json
{"message": "<ErrorMessage>"}
```

# …/reservations/:id

## GET

consumes: -/-

produces: application/json

### Request

Needed role: driver

**Header**

`
Authentication: Bearer <Token>
`

### Response

**Success**

HTTP status **200**

**Body**

JSON Object [Reservation](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_reservation) witch's _id value matches the :id parameter 

**Error**

HTTP status **401**
`Unauthorized`

HTTP status **403**
`Forbidden`

HTTP status **500**
```json
{"message": "<ErrorMessage>"}
```

## PUT

consumes: application/json

produces: -/-

### Request

Needed role: driver

With a `PUT` request it is possible to change every value of the key-value pairs or just a single one. (full or partial update) 

**Header**

`
Authentication: Bearer <Token>

Content-Type: application/json
`

**Body**

JSON Object [Reservation](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_reservation) 

```json
 {
  "user": "<ID>",
  "car": "<ID>",
  "startDate": "<Date>",
  "endDate": "<Date>"
}
```

### Response

**Success**
 
HTTP status **200**

**Error**

HTTP status **401**
`Unauthorized`

HTTP status **403**
`Forbidden`

HTTP status **500**
```json
{"message": "<ErrorMessage>"}
```


## DELETE

Removes the [reservation](https://github.com/callmeFabii/FuhrparkManagement_Documentation/wiki/Server_database_models_reservation) with the given :id from the database

### Request

**Header**

`
Authorization: Bearer <Token>
`

### Response

**Success**
 
HTTP status **200**

**Error**

HTTP status **401**
`Unauthorized`

HTTP status **403**
`Forbidden`

HTTP status **500**
```json
{"message": "<ErrorMessage>"}
```