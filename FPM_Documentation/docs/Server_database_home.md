# [MongoDB](https://docs.mongodb.com/manual/)

## CLI

### [MongoDB Server](https://docs.mongodb.com/manual/reference/program/mongod/)

Starting mongoDB without authentication and SSL

`mongod`

Starting mongoDB with authentication and SSL mode

`mongod --auth --sslMode requireSSL --sslPEMKeyFile <Path to .pem>`

### [MongoShell](https://docs.mongodb.com/manual/reference/program/mongo/)

Without authentication and SSL

`mongo`

With Authentication and SSL

`mongo -u "<User>" -p "<PWD>" --authenticationDatabase "<AuthDB>" --ssl --sslAllowInvalidCertificates`

## [DB Authentication](https://docs.mongodb.com/manual/core/authentication/) 

MongoDB Roles: https://docs.mongodb.com/v3.2/reference/built-in-roles

AuthenticationDB: `admin`

All user passwords are set to: `1234567890`

### User

#### `Administrator`

Roles: \<Role\>, \<Database Scope\>

* `root`, `admin`
* `dbOwner`, `admin`
* `userAdminAnyDatabase`, `admin`
* `dbAdminAnyDatabase`, `admin`

#### `FuhrparkManagementAdmin`

Roles:

* `dbOwner`, `FuhrparkManagement`

#### `FuhrparkManagementUser`

Roles:

* `readWrite`, `FuhrparkManagement`
