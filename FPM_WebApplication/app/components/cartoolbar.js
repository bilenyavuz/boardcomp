import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';

export default class ToolbarExamplesSimple extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            available: false,
            openAdd: false,
            openEdit: false,
            charging: false,
        };
    }


    handleOpenAdd = () => {
        this.setState({openAdd: true});
    };

    handleCloseAdd = () => {
        this.setState({openAdd: false});
    };
    handleOpenEdit = () => {
        this.setState({openEdit: true});
    };
    handleCloseEdit = () => {
        this.setState({openEdit: false});
    };

    render() {

                const actionsAdd = [
            <FlatButton
                label="Speichern"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this.handleCloseAdd}
            />,
            <FlatButton
                label="Abbrechen"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this.handleCloseAdd}
            />,
        ];
        const actionsEdit = [
            <FlatButton
                label="Speichern"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this.handleCloseEdit}
            />,
            <FlatButton
                label="Abbrechen"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this.handleCloseEdit}
            />,
        ];
        return (
            <div>
            <Toolbar>
                <ToolbarGroup>
                    <RaisedButton label="Hinzufügen" primary={true} onTouchTap={this.handleOpenAdd}/>
                    <RaisedButton label="Bearbeiten" primary={true} onTouchTap={this.handleOpenEdit}/>
                </ToolbarGroup>
                <ToolbarGroup lastChild="true">
                    <RaisedButton label="Löschen" backgroundColor="#EF5350" />
                </ToolbarGroup>
            </Toolbar>
                <Dialog
                    title="Auto hinzufügen"
                    actions={actionsAdd}
                    modal={false}
                    open={this.state.openAdd}
                    onRequestClose={this.handleCloseAdd}
                >
                    <TextField
                        floatingLabelText="Hersteller"
                    />
                    <TextField
                        floatingLabelText="Modell"
                    />
                    <TextField
                        floatingLabelText="Kennzeichen"
                    />
                    <TextField
                        floatingLabelText="Kilometerstand"
                    />
                    <TextField
                        floatingLabelText="Geladene kWh"
                    />
                    <div>
                    <DropDownMenu value={this.state.available}>
                        <MenuItem value={true} primaryText="Verfügbar" />
                        <MenuItem value={false} primaryText="Nicht verfügbar" />
                    </DropDownMenu>
                    <DropDownMenu value={this.state.charging}>
                        <MenuItem value={true} primaryText="Ladend" />
                        <MenuItem value={false} primaryText="Nicht Ladend" />
                    </DropDownMenu>
                    </div>

                </Dialog>
                <Dialog
                    title="Auto bearbeiten"
                    actions={actionsEdit}
                    modal={false}
                    open={this.state.openEdit}
                    onRequestClose={this.handleCloseEdit}
                >
                    <TextField
                        floatingLabelText="Hersteller"
                    />
                    <TextField
                        floatingLabelText="Modell"
                    />
                    <TextField
                        floatingLabelText="Kennzeichen"
                    />
                    <TextField
                        floatingLabelText="Kilometerstand"
                    />
                    {/*True False für verfügbar*/}
                    {/*True False für Ladend*/}
                    <TextField
                        floatingLabelText="Geladene kWh"
                    />
                    <div>
                        <DropDownMenu value={this.state.available}>
                            <MenuItem value={true} primaryText="Verfügbar" />
                            <MenuItem value={false} primaryText="Nicht verfügbar" />
                        </DropDownMenu>
                        <DropDownMenu value={this.state.charging}>
                            <MenuItem value={true} primaryText="Ladend" />
                            <MenuItem value={false} primaryText="Nicht Ladend" />
                        </DropDownMenu>
                    </div>
                </Dialog>
            </div>
        );
    }
}