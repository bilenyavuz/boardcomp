import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';

export default class UserToolbar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            openAdd: false,
            openEdit: false,
            userrole: 0,
        };
    }

    handleOpenAdd = () => {
        this.setState({openAdd: true});
    };

    handleCloseAdd = () => {
        this.setState({openAdd: false});
    };
    handleOpenEdit = () => {
        this.setState({openEdit: true});
    };
    handleCloseEdit = () => {
        this.setState({openEdit: false});
    };


    render() {

        const actionsAdd = [
            <FlatButton
                label="Speichern"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this.handleCloseAdd}
            />,
            <FlatButton
                label="Abbrechen"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this.handleCloseAdd}
            />,
        ];
        const actionsEdit = [
            <FlatButton
                label="Speichern"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this.handleCloseEdit}
            />,
            <FlatButton
                label="Abbrechen"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this.handleCloseEdit}
            />,
        ];

        return (
            <div>
            <Toolbar>
                <ToolbarGroup>
                    <RaisedButton label="Hinzufügen" primary={true} onTouchTap={this.handleOpenAdd} />
                    <RaisedButton label="Bearbeiten" primary={true} onTouchTap={this.handleOpenEdit}/>
                </ToolbarGroup>
                <ToolbarGroup lastChild="true">
                    <RaisedButton label="Löschen" backgroundColor="#EF5350" />
                </ToolbarGroup>
            </Toolbar>
                <Dialog
                    title="Benutzer hinzufügen"
                    actions={actionsAdd}
                    modal={false}
                    open={this.state.openAdd}
                    onRequestClose={this.handleCloseAdd}
                >
                    <TextField
                        floatingLabelText="Benutzername"
                    />
                    <TextField
                        floatingLabelText="Vorname"
                    />
                    <TextField
                        floatingLabelText="Nachname"
                    />
                    <TextField
                        floatingLabelText="E-Mail"
                    />
                    <TextField
                        floatingLabelText="Telefon"
                    />
                    <div>
                        <div>
                        <a>Rollenverwaltung</a>
                        </div>
                    <DropDownMenu value={this.state.userrole}>
                        <MenuItem value={0} primaryText="Admin" />
                        <MenuItem value={1} primaryText="User" />
                    </DropDownMenu>
                    </div>
                </Dialog>
                <Dialog
                    title="Benutzer bearbeiten"
                    actions={actionsEdit}
                    modal={false}
                    open={this.state.openEdit}
                    onRequestClose={this.handleCloseEdit}
                >
                    <TextField
                        floatingLabelText="Benutzername"
                    />
                    <TextField
                        floatingLabelText="Vorname"
                    />
                    <TextField
                        floatingLabelText="Nachname"
                    />
                    <TextField
                        floatingLabelText="E-Mail"
                    />
                    <TextField
                        floatingLabelText="Telefon"
                    />
                    <div>
                        <div>
                        <a>Rollenverwaltung</a>
                        </div>
                    <DropDownMenu value={this.state.userrole}>
                        <MenuItem value={0} primaryText="Admin" />
                        <MenuItem value={1} primaryText="User" />
                    </DropDownMenu>
                    </div>
                </Dialog>
            </div>
        );
    }
}