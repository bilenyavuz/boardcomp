/**
 * Created by Kai on 22.11.2016.
 */
import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Center from 'react-center';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import RaisedButton from 'material-ui/RaisedButton';


const styles = {
    space: {
        padding: 50,
    },
    header: {
        paddingTop: 50,
    },
    textField: {
        padding: 50,
        float: "left",
    },
    textFieldButtom:{
        clear: "both",
        padding: 50,
    },
    button:{
        padding: 50,
        float: "left",
    },

};

class UserDetails extends React.Component {

    constructor(props) {
        super(props);
    }

    handleChange = (event, index, value) => this.setState({value});

    render() {

        return (
            <MuiThemeProvider>
                <div>
                <div>
                    <div style={styles.textField}>
                    <div>
                        <a>Benutzerdetails</a>
                    </div>
                    <div>
                        <TextField
                            hintText="Ihr Benutzername"
                            floatingLabelText="Benutzername"
                        />
                    </div>
                    <div>
                        <TextField
                            hintText="Ihr Vorname"
                            floatingLabelText="Vorname"
                        />
                    </div>
                    <div>
                        <TextField
                            hintText="Ihr Nachname"
                            floatingLabelText="Nachname"
                        />
                    </div>
                    </div>
                    <div style={styles.textField}>
                        <div>
                            <div>
                                <a>Kontaktdaten</a>
                            </div>
                            <div>
                                <TextField
                                    hintText="E-Mail Adresse"
                                    floatingLabelText="E-Mail Adresse"
                                />
                            </div>
                            <div>
                                <TextField
                                    hintText="Telefon"
                                    floatingLabelText="E-Mail Adresse"
                                />
                            </div>
                        </div>
                    </div>
                </div>
                    <div style={styles.textFieldButtom}>
                    <Center>
                    <div style={styles.button}>
                    <RaisedButton label="Bearbeiten" primary={true} />
                        </div>
                    <div style={styles.button}>
                    <RaisedButton label="Speichern" primary={true} />
                    </div>
                    </Center>
                    </div>
                </div>
            </MuiThemeProvider>
    );
    }
    }

    export default UserDetails;
