import React from 'react';
import {Table, TableBody, TableFooter, TableHeader, TableHeaderColumn, TableRow, TableRowColumn}
    from 'material-ui/Table';
import AddCar from './cartoolbar';
import IconCar from 'material-ui/svg-icons/maps/directions-car';


const styles = {
    propContainer: {
        width: 200,
        overflow: 'hidden',
        margin: '20px auto 0',
    },
    propToggleHeader: {
        margin: '20px auto 10px',
    },
};

export default class TableExampleComplex extends React.Component {

    constructor(props) {
        super(props);
        
        var self = this;
        
        this.state = {
            fixedHeader: true,
            fixedFooter: true,
            stripedRows: false,
            showRowHover: false,
            selectable: true,
            multiSelectable: false,
            enableSelectAll: false,
            deselectOnClickaway: true,
            showCheckboxes: true,
            height: '500px',
        };
        
                // daten vom server holen
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "https://10.18.2.151/api/cars");
        xhr.setRequestHeader('Authentication', 'Bearer '+localStorage.getItem('jwtToken'));
        xhr.setRequestHeader('Authorization', 'Bearer '+localStorage.getItem('jwtToken'));
        xhr.onreadystatechange = function() {
            if(xhr.readyState==4)
            {
                self.setState({carData: JSON.parse(xhr.response)});
            }
        };
        xhr.send(null);
    }

    handleToggle = (event, toggled) => {
        this.setState({
            [event.target.car]: toggled,
        });
    };

    handleChange = (event) => {
        this.setState({height: event.target.value});
    };

    render() {
           
        var carData = this.state.carData || [];
    
        return (
            <div>
                <AddCar/>
                <Table
                    height={this.state.height}
                    fixedHeader={this.state.fixedHeader}
                    fixedFooter={this.state.fixedFooter}
                    selectable={this.state.selectable}
                    multiSelectable={this.state.multiSelectable}
                >
                    <TableHeader
                        displaySelectAll={this.state.showCheckboxes}
                        adjustForCheckbox={this.state.showCheckboxes}
                        enableSelectAll={this.state.enableSelectAll}
                    >
                        <TableRow>
                            <TableHeaderColumn colSpan="9" style={{textAlign: 'center'}}>
                                <IconCar/>
                                <div>Auto Verwaltung</div>
                            </TableHeaderColumn>
                        </TableRow>
                        <TableRow>
                            <TableHeaderColumn tooltip="Fahrzeug ID">ID</TableHeaderColumn>
                            <TableHeaderColumn tooltip="Hersteller">Hersteller</TableHeaderColumn>
                            <TableHeaderColumn tooltip="Modell">Modell</TableHeaderColumn>
                            <TableHeaderColumn tooltip="Aktueller Kilometerstand">Kilometerstand</TableHeaderColumn>
                            <TableHeaderColumn tooltip="Kennzeichen">Kennzeichen</TableHeaderColumn>
                            <TableHeaderColumn tooltip="Zeigt Verfügbarkeit des Fahrzeugs an">Verfügbar</TableHeaderColumn>
                            <TableHeaderColumn tooltip="Batterie">Batterie</TableHeaderColumn>
                            <TableHeaderColumn tooltip="Ladezustand">Ladezustand</TableHeaderColumn>
                            <TableHeaderColumn tooltip="Geladene kWh">Geladene kWh</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody
                        displayRowCheckbox={this.state.showCheckboxes}
                        deselectOnClickaway={this.state.deselectOnClickaway}
                        showRowHover={this.state.showRowHover}
                        stripedRows={this.state.stripedRows}
                    >
                        {carData.map( (row, index) => (
                            <TableRow key={index} selected={row.selected}>
                                <TableRowColumn>{index}</TableRowColumn>
                                <TableRowColumn>{row.vendor}</TableRowColumn>
                                <TableRowColumn>{row.model}</TableRowColumn>
                                <TableRowColumn>{row.mileage}</TableRowColumn>
                                <TableRowColumn>{row.licenseNumber}</TableRowColumn>
                                <TableRowColumn>{row.available}</TableRowColumn>
                                <TableRowColumn>{row.battery}</TableRowColumn>
                                <TableRowColumn>{row.charging}</TableRowColumn>
                                <TableRowColumn>{row.chargedkWh}</TableRowColumn>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>

            </div>
        );
    }
}