import React from 'react';
import {Table, TableBody, TableFooter, TableHeader, TableHeaderColumn, TableRow, TableRowColumn}
    from 'material-ui/Table';
import UserToolbar from './usertoolbar';
import IconUser from 'material-ui/svg-icons/action/account-circle';


const styles = {
    propContainer: {
        width: 200,
        overflow: 'hidden',
        margin: '20px auto 0',
    },
    propToggleHeader: {
        margin: '20px auto 10px',
    },
};

export default class UserTable extends React.Component {

    constructor(props) {
        super(props);

        var myself = this;

        this.state = {
            fixedHeader: true,
            fixedFooter: true,
            stripedRows: false,
            showRowHover: false,
            selectable: true,
            multiSelectable: false,
            enableSelectAll: false,
            deselectOnClickaway: true,
            showCheckboxes: true,
            height: '500px',
        };

        // daten vom server holen
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "https://10.18.2.151/api/users");
        xhr.setRequestHeader('Authentication', 'Bearer '+localStorage.getItem('jwtToken'));
        xhr.setRequestHeader('Authorization', 'Bearer '+localStorage.getItem('jwtToken'));
        xhr.onreadystatechange = function() {
            if(xhr.readyState==4)
            {
                myself.setState({userData: JSON.parse(xhr.response)});
            }
        };
        xhr.send(null);

    }

    handleToggle = (event, toggled) => {
        this.setState({
            [event.target.car]: toggled,
        });
    };

    handleChange = (event) => {
        this.setState({height: event.target.value});
    };

    render() {
        var userData = this.state.userData || [];
        return (
            <div>
                <UserToolbar/>
                <Table
                    height={this.state.height}
                    fixedHeader={this.state.fixedHeader}
                    fixedFooter={this.state.fixedFooter}
                    selectable={this.state.selectable}
                    multiSelectable={this.state.multiSelectable}
                >
                    <TableHeader
                        displaySelectAll={this.state.showCheckboxes}
                        adjustForCheckbox={this.state.showCheckboxes}
                        enableSelectAll={this.state.enableSelectAll}
                    >
                        <TableRow>
                            <TableHeaderColumn colSpan="6" style={{textAlign: 'center'}}>
                                <IconUser/>
                                <div>Benutzer Verwaltung</div>
                            </TableHeaderColumn>
                        </TableRow>
                        <TableRow>
                            <TableHeaderColumn tooltip="Benutzer ID">ID</TableHeaderColumn>
                            <TableHeaderColumn tooltip="Zeigt Benutzernamen an">Benutzername</TableHeaderColumn>
                            <TableHeaderColumn tooltip="Vorname">Vorname</TableHeaderColumn>
                            <TableHeaderColumn tooltip="Nachname">Nachname</TableHeaderColumn>
                            <TableHeaderColumn tooltip="Zeigt die Rolle des Benutzers an">Rolle</TableHeaderColumn>
                            <TableHeaderColumn tooltip="Zeigt die E-Mail-Adresse des Benutzers an">E-Mail</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody
                        displayRowCheckbox={this.state.showCheckboxes}
                        deselectOnClickaway={this.state.deselectOnClickaway}
                        showRowHover={this.state.showRowHover}
                        stripedRows={this.state.stripedRows}
                    >
                        {userData.map( (row, index) => (
                            <TableRow key={index} selected={row.selected}>
                                <TableRowColumn>{index}</TableRowColumn>
                                <TableRowColumn>{row.username}</TableRowColumn>
                                <TableRowColumn>{row.firstname}</TableRowColumn>
                                <TableRowColumn>{row.lastname}</TableRowColumn>
                                <TableRowColumn>{row.role}</TableRowColumn>
                                <TableRowColumn>{row.email}</TableRowColumn>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </div>
        );
    }
}