import React from 'react';
import {
    Table, TableBody, TableFooter, TableHeader, TableHeaderColumn, TableRow, TableRowColumn
}
    from 'material-ui/Table';

const tableData = [
    {
        car: 'Car1',
        status: 'Free',
    },
    {
        name: 'Car2',
        status: 'Reserved',
    },
    {
        name: 'Car3',
        status: 'Free',
    },
    {
        name: 'Car4',
        status: 'Reserved',
    },
    {
        name: 'Car5',
        status: 'Free',
    },
    {
        name: 'Car6',
        status: 'Free',
    },
];

export default class TableExampleComplex extends React.Component {

        render() {
        return (
            <div>
                <div>
                    <a>Wählen Sie ein Fahrzeug aus der Liste, welches Sie buchen möchten.</a>
                </div>
                <Table>
                    <TableHeader                    >
                        <TableRow>
                            <TableHeaderColumn colSpan="3" tooltip="Super Header" style={{textAlign: 'center'}}>
                                Freie Fahzeuge
                            </TableHeaderColumn>
                        </TableRow>
                        <TableRow>
                            <TableHeaderColumn tooltip="The ID">ID</TableHeaderColumn>
                            <TableHeaderColumn tooltip="The Name">Name</TableHeaderColumn>
                            <TableHeaderColumn tooltip="The Status">Status</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody
                        displayRowCheckbox={true}
                        deselectOnClickaway={true}
                    >
                        {tableData.map( (row, index) => (
                            <TableRow key={index} selected={row.selected}>
                                <TableRowColumn>{index}</TableRowColumn>
                                <TableRowColumn>{row.name}</TableRowColumn>
                                <TableRowColumn>{row.status}</TableRowColumn>
                            </TableRow>
                        ))}
                    </TableBody>
                    <TableFooter
                        adjustForCheckbox={true}
                    >
                        <TableRow>
                            <TableRowColumn>ID</TableRowColumn>
                            <TableRowColumn>Name</TableRowColumn>
                            <TableRowColumn>Status</TableRowColumn>
                        </TableRow>
                        <TableRow>
                        </TableRow>
                    </TableFooter>
                </Table>
            </div>
        );
    }
}