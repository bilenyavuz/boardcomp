/**
 * Created by Kai on 09.11.2016.
 */

import React from 'react';


class NotFound extends React.Component {
    render() {
        return (
            <h2>404... Diese Seite wird nicht angefahren</h2>
        );
    }
}

export default NotFound;
