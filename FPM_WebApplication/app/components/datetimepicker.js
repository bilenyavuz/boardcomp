/**
 * Created by Kai on 20.11.2016.
 */

import React from 'react';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';
import Center from 'react-center';

const styles = {
    input: {
        padding: 50,
    }

};

class DateTimePicker extends React.Component {

    handleToggle = () => this.setState({openAdd: !this.state.openAdd});

    render() {
        return (<div>
                <a>Wählen Sie Ihr Abfahrts- und Rückkehr-Datum aus um ein Fahrzueg zu reservieren</a>
                <Center>
                    <div style={styles.input}>
                        <a>Abfahrt</a>
                        <DatePicker hintText="Von Datum" mode="landscape"/>
                        <TimePicker
                            format="24hr"
                            hintText="Von Uhrzeit"
                        />
                    </div>
                    <div style={styles.input}>
                        <a>Rückkehr</a>
                        <DatePicker hintText="Bis Datum" mode="landscape"/>
                        <TimePicker
                            format="24hr"
                            hintText="Bis Uhrzeit"
                        />
                    </div>
                </Center>
            </div>
        );
    }


}

export default DateTimePicker;