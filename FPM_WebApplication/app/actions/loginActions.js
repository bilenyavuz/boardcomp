/**
 * Created by kai on 17.01.2017.
 */

import axios from 'axios';
import setAuthToken from './setAuthToken';
import jwt from 'jsonwebtoken';
import {SET_CURRENT_USER} from './types'

export function setCurrentUser(user) {
    return{
        type: SET_CURRENT_USER,
        user
    };

}

export function login(userData) {
    return dispatch =>{
        return axios({
            method: 'post',
            url: 'https://10.18.2.151/api/login',
            headers: {'Content-Type': 'application/json'},
            data: userData
        }).then(function (response) {
            localStorage.setItem('jwtToken', response.data.token);
            setAuthToken(response.data.token);
            console.log(jwt.decode(response.data.token));
            dispatch(setCurrentUser(jwt.decode(response.data.token)));
        });
    };
}