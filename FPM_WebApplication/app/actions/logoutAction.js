/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * Created by Christian 20.05.2017
 * 
 */

import axios from 'axios';
import setAuthToken from './setAuthToken';
import jwt from 'jsonwebtoken';
import {SET_CURRENT_USER} from './types';

export function setCurrentUser(user) {
    return{
        type: SET_CURRENT_USER,
        user
    }

}

export function logout(userData){
    return dispatch=>{
        return axios({
            method: 'post',
            url: 'https://10.18.2.151/api/logout',
            headers: {'Content-Type': 'application/json'},
            data: userData,        
        }).then(function (response) {
            localStorage.setItem('jwtToken', response.data.token);
            setAuthToken(response.data.token);
            console.log(jwt.decode(response.data.token));
            dispatch(setCurrentUser(jwt.decode(response.data.token)));
        });
    }    
}