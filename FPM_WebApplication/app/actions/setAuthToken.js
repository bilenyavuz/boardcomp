/**
 * Created by kai on 17.01.2017.
 */
import axios from 'axios';

export default function setAuthToken(token){
    if(token)
    {
        var bearer = "Bearer " + token;
        axios.defaults.headers.common['Authorization'] = bearer;
    }else {
        delete axios.defaults.headers.common['Authorization']
    }
}
