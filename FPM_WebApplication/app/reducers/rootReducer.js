/**
 * Created by kai on 17.01.2017.
 */

import {combineReducers} from 'redux';
import authReducer from './authReducer';

export default combineReducers({
    authReducer
});
