/**
 * Created by kai on 11.01.2017.
 */
import React from 'react';
import Login from './../components/login';
import {connect} from 'react-redux';

class LoginContainer extends React.Component {
    render() {
        return (
            <Login/>
        );
    }
}

export default connect((state) => {return{} },)(LoginContainer);
