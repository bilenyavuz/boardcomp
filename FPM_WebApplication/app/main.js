/**
 * Created by Kai on 09.11.2016.
 */
import React from 'react'
import ReactDOM from 'react-dom'
import App from './app'
import {Provider} from 'react-redux'
import thunk from 'redux-thunk'
import {createStore,applyMiddleware} from 'redux'
import setAuthToken from './actions/setAuthToken'

const store = createStore(
    (state = {})=> state,
applyMiddleware(thunk)
);

setAuthToken(localStorage.jwtToken);

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'))