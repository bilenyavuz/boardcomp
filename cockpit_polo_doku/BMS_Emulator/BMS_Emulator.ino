
int16_t amp, volt;
bool increase;
int16_t mytimer;

int8_t cells[60]={0,3, 0, -2, 1, 4, -3,0,-2,-1,0,2,-5,-3,-4,
                   1,-3,2,4,-3,-5,2,4,0,0,3,-2,4,-2, -5,
                   -2,0,0,2,1,-1,0,3,0,-1,0,-1, 2, -1, -2,
                   1,2,3,-2,-3,4,-1,0,0,-2,-2,0,2,0,-2};

                   

void setup() {
  Serial.begin(115200);
  increase = true;
  mytimer = 0;
  volt = 20000;
  amp = 0;
}

void loop() {
  int i;
  if (increase) {
    amp ++;
    volt-=10;

    if (amp > 250) {
      amp = 250;
      increase = false;
    }
  }
  else {
    amp --;
    volt +=10;

    if (amp < -120) {
      amp = -120;
      increase = true;
    }    
  }

  Serial.print("CellVoltage:");
  uint16_t cellv = volt/60;
  for (i=0; i < 60; i++) {
    Serial.print(((int)cellv+cells[i]), DEC);
    Serial.print(";");
  }
  Serial.println();

  Serial.println("CellTemp:40;21;43;35;32;40;21;43;35;32;40;21;43;35;32;40;21;43;35;32");
  Serial.print("DrivingAmperage:");
  Serial.println(amp);

  Serial.print("CharingAmperage:");
  Serial.println("0;44;42;45");
  Serial.println("ChargerTemp:23;33;34;36");

  delay(990);
  
}
