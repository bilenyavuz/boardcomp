#define LTC6804_CS PA4



#include <Arduino.h>      // nicht benötigt

#include <SPI.h>          // nicht benötigt
#include "ltc6804.h"

#define OVERVOLTAGE 34000


const uint8_t TOTAL_IC = 2;//!<number of ICs in the daisy chain

/*
void print_rxconfig();
void init_cfg();
void print_menu();
void run_command(uint32_t cmd);
void print_cells();*/


/******************************************************
 *** Global Battery Variables received from 6804 commands
 These variables store the results from the LTC6804
 register reads and the array lengths must be based
 on the number of ICs on the stack
 ******************************************************/
uint16_t cell_codes[TOTAL_IC][12];
/*!<
  The cell codes will be stored in the cell_codes[][12] array in the following format:

  |  cell_codes[0][0]| cell_codes[0][1] |  cell_codes[0][2]|    .....     |  cell_codes[0][11]|  cell_codes[1][0] | cell_codes[1][1]|  .....   |
  |------------------|------------------|------------------|--------------|-------------------|-------------------|-----------------|----------|
  |IC1 Cell 1        |IC1 Cell 2        |IC1 Cell 3        |    .....     |  IC1 Cell 12      |IC2 Cell 1         |IC2 Cell 2       | .....    |
****/

uint16_t aux_codes[TOTAL_IC][6];
/*!<
 The GPIO codes will be stored in the aux_codes[][6] array in the following format:

 |  aux_codes[0][0]| aux_codes[0][1] |  aux_codes[0][2]|  aux_codes[0][3]|  aux_codes[0][4]|  aux_codes[0][5]| aux_codes[1][0] |aux_codes[1][1]|  .....    |
 |-----------------|-----------------|-----------------|-----------------|-----------------|-----------------|-----------------|---------------|-----------|
 |IC1 GPIO1        |IC1 GPIO2        |IC1 GPIO3        |IC1 GPIO4        |IC1 GPIO5        |IC1 Vref2        |IC2 GPIO1        |IC2 GPIO2      |  .....    |
*/

uint8_t tx_cfg[TOTAL_IC][6];
/*!<
  The tx_cfg[][6] stores the LTC6804 configuration data that is going to be written
  to the LTC6804 ICs on the daisy chain. The LTC6804 configuration data that will be
  written should be stored in blocks of 6 bytes. The array should have the following format:

 |  tx_cfg[0][0]| tx_cfg[0][1] |  tx_cfg[0][2]|  tx_cfg[0][3]|  tx_cfg[0][4]|  tx_cfg[0][5]| tx_cfg[1][0] |  tx_cfg[1][1]|  tx_cfg[1][2]|  .....    |
 |--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|-----------|
 |IC1 CFGR0     |IC1 CFGR1     |IC1 CFGR2     |IC1 CFGR3     |IC1 CFGR4     |IC1 CFGR5     |IC2 CFGR0     |IC2 CFGR1     | IC2 CFGR2    |  .....    |
*/

uint8_t rx_cfg[TOTAL_IC][8];
/*!<
  the rx_cfg[][8] array stores the data that is read back from a LTC6804-1 daisy chain.
  The configuration data for each IC  is stored in blocks of 8 bytes. Below is an table illustrating the array organization:

|rx_config[0][0]|rx_config[0][1]|rx_config[0][2]|rx_config[0][3]|rx_config[0][4]|rx_config[0][5]|rx_config[0][6]  |rx_config[0][7] |rx_config[1][0]|rx_config[1][1]|  .....    |
|---------------|---------------|---------------|---------------|---------------|---------------|-----------------|----------------|---------------|---------------|-----------|
|IC1 CFGR0      |IC1 CFGR1      |IC1 CFGR2      |IC1 CFGR3      |IC1 CFGR4      |IC1 CFGR5      |IC1 PEC High     |IC1 PEC Low     |IC2 CFGR0      |IC2 CFGR1      |  .....    |
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


typedef struct u_list {
  uint16_t voltage;
  struct u_list* next;
  uint8_t num;
  } u_list_t;


typedef enum states {READONLY, READ, DISCHARGE} states_t;


states_t States = READONLY;

uint16_t voltages[TOTAL_IC][12];


/********************************************************************************
 * own functions
 */

void discharge(void)
{
  int num;
  u_list_t *top, *act, *nn, *prev;
  top = NULL;
  act = top;
  uint16_t dis;
  

  if ((States == READONLY) || (States == READ) )
  {
    States=DISCHARGE;
    Serial.print("Discharge : ");

    for (int j = 0; j < TOTAL_IC; j++)
      {
      num = 0;
      act = top;
      for (int i = 0; i < 12; i++)
        {
        if (voltages[j][i] > OVERVOLTAGE)   // Ãœberspannung !!
          {
          num++;
 /*         Serial.print("Overvoltage : Cell(");
          Serial.print(i);
          Serial.print(") = ");
          Serial.println(voltages[j][i]);*/
          
          if (top == NULL)        // erstes Element
            {
//    Serial.print("Insert first : ");
              act=new(u_list_t);
              act->voltage = voltages[j][i]; 
              act->next = NULL;
              act->num = i;
//    Serial.print(act->num);
              top = act;
            }
          else
            {
            prev = top;
            act = top;
            for (int k=0; k < num -1; k++)
              {
//    Serial.print("Insert ");
//    Serial.println(k);
              if (act->voltage <  voltages[j][i])   // oben einfÃ¼gen
                {
//    Serial.print("On top ");
                nn=new(u_list_t);
                nn->voltage = voltages[j][i]; 
                nn->next = act;
                nn->num = i;
                if (act == top){
                  top = nn;
//                  prev = nn;
                }
                else
                  prev->next = nn;
//                prev = nn;
//    Serial.print(nn->num);
                break;
                }
              else                  //unten anhÃ¤ngen
                {
//    Serial.println("Pos. ");
                  if (act->next == NULL) 
                  {
//    Serial.print("Pos. insert ");
                    nn=new(u_list_t);
                    nn->voltage = voltages[j][i]; 
                    nn->next = act->next;
                    nn->num = i;
                    act->next = nn;
//    Serial.print(nn->num);
                    break;
                  }
                  prev = act;
                  act = act->next;
                }
              }
            }
          }
//          Serial.print("num = ");
//          Serial.println(num);
          act = top;
          for (int ii=0; ii < num; ii++)
            {
              int number = act->num;
//              Serial.print("number = ");
//              Serial.println(number);
              act = act->next;
            }

        }

          
        if (num > 0)                      // Wenn wir mindestens eine Ãœberspannung haben !!!
          {
//    Serial.print("num = ");
//    Serial.println(num);

          act = top;
          dis = 0;
          for (int i=0; i < num; i++)
            {
              int number = act->num;
//     Serial.print("number = ");
//    Serial.println(number);
             if (number > 0) 
              {
                if ( ((dis & (1 << (number-1)) ) == 0) && ((dis & (1 <<(number+1)) ) == 0) )  // ÃœberprÃ¼fen ob rechts oder links schon ein Balancer aktiv ist.
                {
//     Serial.print("bedin1 = ");
//    Serial.println(dis,BIN);
         
                  dis |= 1 << number;
                }
//                else Serial.print("not bedin1 ");
              }
              else 
                if ( ((dis & (1 <<(number+1)) ) == 0) )  // ÃœberprÃ¼fen ob rechts schon ein Balancer aktiv ist.
                {
                  dis |= 1 << number;
//     Serial.print("bedin2 = ");
//    Serial.println(dis,BIN);
                }
//               else Serial.print("not bedin2 ");
              act = act->next;
            }
            Serial.print("Balance :");
            Serial.println(dis,BIN);
            tx_cfg[j][4] = (dis & 0xff);
            tx_cfg[j][5] = (dis & 0x0f00) >> 8;
            LTC6804_wrcfg(j+1,tx_cfg);
            rx_cfg[j][4] = 0xff;
            rx_cfg[j][5] = 0xff;
            while ((rx_cfg[j][4] != tx_cfg[j][4] ) || (rx_cfg[j][5] != rx_cfg[j][5]) )
            {
              delay (50);
              int error = LTC6804_rdcfg(TOTAL_IC,rx_cfg);
              if (error == -1)
                {
                  Serial.println("A PEC error was detected in the received data");
                  break;
                }
              print_rxconfig();
              LTC6804_wrcfg(j+1,tx_cfg);
            }
//            Serial.println("finished");
              print_rxconfig();
          }
        else
          {
            States=READONLY;
          }
// Next j (< TOTAL_IC)
// Löschen des erstellten Baums.
        act = top;
        for (int ii=0; ii < num; ii++)
          {
            if (act) {
              nn = act;
              act = act->next;
              if (nn) delete nn;
            }
          }

        top = NULL;
        act = top;          
      }
  }   
  else 
    {
      States=READ;
      for (int j = 0; j < TOTAL_IC; j++)
      {
          tx_cfg[j][4] = 0;
          tx_cfg[j][5] = 0;
          LTC6804_wrcfg(j+1,tx_cfg);
          Serial.println("Ballancing off");
      }
    }
}          




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////






// Cell 1 = 0x01
// Cell 2 = 0x02
// Cell 3 = 0x04
// Cell 4 = 0x08
// Cell 5 = 0x10
// Cell 6 = 0x20
// Cell 7 = 0x40
// Cell 8 = 0x80
// Cell 9 = 0x01
// Cell 10 = 0x02
// Cell 11 = 0x04
// Cell 12 = 0x08


/*!**********************************************************************
 \brief  Inititializes hardware and variables
 ***********************************************************************/
void setup()
{
  Serial.begin(57600);
  pinMode(13,OUTPUT);
  digitalWrite(13,1);
  LTC6804_initialize();  //Initialize LTC6804 hardware
  init_cfg();        //initialize the 6804 configuration array to be written
  //Serial.print("CSpin =");
  //Serial.println(LTC6804_CS);
  pinMode(14,INPUT);
  print_menu();
}

/*!*********************************************************************
  \brief main loop
***********************************************************************/
void loop()
{
static uint8_t user_command = 0;

  if (Serial.available())           // Check for user input
  {
    user_command = read_int();      // Read the user command

    // different
    Serial.println(user_command);
  }

  //voltageToApp();
  
  run_command(user_command);
  if (user_command != 7) user_command = 0;
}

void voltageToApp(){      
    wakeup_sleep();
      LTC6804_wrcfg(TOTAL_IC,tx_cfg);

        wakeup_idle();
        LTC6804_adcv();
        delay(10);
        wakeup_idle();
        int error = LTC6804_rdcv(0, TOTAL_IC,cell_codes);
        if (error == -1)
        {
          Serial.println("A PEC error was detected in the received data");
        }
        print_cells();
}


void run_command(uint32_t cmd)
{
  int8_t error = 0;

  char input = 0;
  uint16_t ddelay = 500;

  switch (cmd)
  {

    case 1:
      wakeup_sleep();
      LTC6804_wrcfg(TOTAL_IC,tx_cfg);
      print_config();
      print_menu();
      break;

    case 2:
      wakeup_sleep();
      error = LTC6804_rdcfg(TOTAL_IC,rx_cfg);
      if (error == -1)
      {
        Serial.println("A PEC error was detected in the received data");
      }
      print_rxconfig();
      print_menu();
      break;

    case 3:
      wakeup_sleep();
      LTC6804_adcv();
      delay(3);
      Serial.println("cell conversion completed");
      Serial.println();
      print_menu();
      break;

    case 4:
      wakeup_sleep();
      error = LTC6804_rdcv(0, TOTAL_IC,cell_codes); // Set to read back all cell voltage registers
      if (error == -1)
      {
        Serial.println("A PEC error was detected in the received data");
      }
      print_cells();
      print_menu();
      break;

    case 5:
      wakeup_sleep();
      LTC6804_adax();
      delay(3);
      Serial.println("aux conversion completed");
      Serial.println();
      print_menu();
      break;

    case 6:
      wakeup_sleep();
      error = LTC6804_rdaux(0,TOTAL_IC,aux_codes); // Set to read back all aux registers
      if (error == -1)
      {
        Serial.println("A PEC error was detected in the received data");
      }
      print_aux();
      print_menu();
      break;

    case 7:
      Serial.println("transmit 'm' to quit");
      wakeup_sleep();
      LTC6804_wrcfg(TOTAL_IC,tx_cfg);
      while (input != 'm')
      {
        wakeup_idle();
        LTC6804_adcv();
        delay(10);
        wakeup_idle();
        error = LTC6804_rdcv(0, TOTAL_IC,cell_codes);
        if (error == -1)
        {
          Serial.println("A PEC error was detected in the received data");
        }
        print_cells();
        
        switch (States) 
        {
    
          case READONLY:
            ddelay = 500;
            for (int j = 0; j < TOTAL_IC; j++)
              for (int i = 0; i < 12; i++)
                voltages[j][i] = cell_codes[j][i]; 
            input = 'm';
            break;
          case READ:
            for (int j = 0; j < TOTAL_IC; j++)
              for (int i = 0; i < 12; i++)
                voltages[j][i] = cell_codes[j][i];
            ddelay = 100;
//            input = 'm';
            break;
          case DISCHARGE:
            ddelay = 400;
            input = 'm';
            break;
          default:
            break;
        }

        discharge();
        delay(ddelay);
      }
//      print_menu();
      break;

    default:
//      Serial.println("Incorrect Option");
      break;
  }
}

/*!***********************************
 \brief Initializes the configuration array
 **************************************/
void init_cfg()
{
  for (int i = 0; i<TOTAL_IC; i++)
  {
    tx_cfg[i][0] = 0xFE;
    tx_cfg[i][1] = 0x00 ;
    tx_cfg[i][2] = 0x00 ;
    tx_cfg[i][3] = 0x00 ;
    tx_cfg[i][4] = 0x00 ;
    tx_cfg[i][5] = 0x00 ;
  }

}

/*!*********************************
  \brief Prints the main menu
***********************************/
void print_menu()
{
  //Serial.print("CSpin =");
  //Serial.println(LTC6804_CS);
  Serial.println("Please enter LTC6804 Command");
  Serial.println("Write Configuration: 1");
  Serial.println("Read Configuration: 2");
  Serial.println("Start Cell Voltage Conversion: 3");
  Serial.println("Read Cell Voltages: 4");
  Serial.println("Start Aux Voltage Conversion: 5");
  Serial.println("Read Aux Voltages: 6");
  Serial.println("loop cell voltages: 7");
  Serial.println("Please enter command: ");
  Serial.println(77);
}

/*!************************************************************
  \brief Prints cell coltage codes to the serial port
 *************************************************************/
void print_cells()
{
  for (int current_ic = 0 ; current_ic < TOTAL_IC; current_ic++)
  {
    Serial.print(" IC ");
    Serial.print(current_ic+1,DEC);
    for (int i=0; i<12; i++)
    {
      Serial.print(" C");
      Serial.print(i+1,DEC);
      Serial.print(": ");
      Serial.print(cell_codes[current_ic][i]*0.0001,4);
      Serial.print(",");
    }
    Serial.println();
  }
  Serial.println();
}

/*!****************************************************************************
  \brief Prints GPIO voltage codes and Vref2 voltage code onto the serial port
 *****************************************************************************/
void print_aux()
{
  for (int current_ic =0 ; current_ic < TOTAL_IC; current_ic++)
  {
    Serial.print(" IC ");
    Serial.print(current_ic+1,DEC);
    for (int i=0; i < 5; i++)
    {
      Serial.print(" GPIO-");
      Serial.print(i+1,DEC);
      Serial.print(":");
      Serial.print(aux_codes[current_ic][i]*0.0001,4);
      Serial.print(",");
    }
    Serial.print(" Vref2");
    Serial.print(":");
    Serial.print(aux_codes[current_ic][5]*0.0001,4);
    Serial.println();
  }
  Serial.println();
}
/*!******************************************************************************
 \brief Prints the configuration data that is going to be written to the LTC6804
 to the serial port.
 ********************************************************************************/
void print_config()
{
  int cfg_pec;

  Serial.println("Written Configuration: ");
  for (int current_ic = 0; current_ic<TOTAL_IC; current_ic++)
  {
    Serial.print(" IC ");
    Serial.print(current_ic+1,DEC);
    Serial.print(": ");
    Serial.print("0x");
    serial_print_hex(tx_cfg[current_ic][0]);
    Serial.print(", 0x");
    serial_print_hex(tx_cfg[current_ic][1]);
    Serial.print(", 0x");
    serial_print_hex(tx_cfg[current_ic][2]);
    Serial.print(", 0x");
    serial_print_hex(tx_cfg[current_ic][3]);
    Serial.print(", 0x");
    serial_print_hex(tx_cfg[current_ic][4]);
    Serial.print(", 0x");
    serial_print_hex(tx_cfg[current_ic][5]);
    Serial.print(", Calculated PEC: 0x");
    cfg_pec = pec15_calc(6,&tx_cfg[current_ic][0]);
    serial_print_hex((uint8_t)(cfg_pec>>8));
    Serial.print(", 0x");
    serial_print_hex((uint8_t)(cfg_pec));
    Serial.println();
  }
  Serial.println();
}

/*!*****************************************************************
 \brief Prints the configuration data that was read back from the
 LTC6804 to the serial port.
 *******************************************************************/
void print_rxconfig()
{
  Serial.println("Received Configuration ");
  for (int current_ic=0; current_ic<TOTAL_IC; current_ic++)
  {
    Serial.print(" IC ");
    Serial.print(current_ic+1,DEC);
    Serial.print(": 0x");
    serial_print_hex(rx_cfg[current_ic][0]);
    Serial.print(", 0x");
    serial_print_hex(rx_cfg[current_ic][1]);
    Serial.print(", 0x");
    serial_print_hex(rx_cfg[current_ic][2]);
    Serial.print(", 0x");
    serial_print_hex(rx_cfg[current_ic][3]);
    Serial.print(", 0x");
    serial_print_hex(rx_cfg[current_ic][4]);
    Serial.print(", 0x");
    serial_print_hex(rx_cfg[current_ic][5]);
    Serial.print(", Received PEC: 0x");
    serial_print_hex(rx_cfg[current_ic][6]);
    Serial.print(", 0x");
    serial_print_hex(rx_cfg[current_ic][7]);
    Serial.println(33);
  }
  Serial.println();
}

void serial_print_hex(uint8_t data)
{
  if (data< 16)
  {
    Serial.print("0");
    Serial.print((byte)data,HEX);
  }
  else
    Serial.print((byte)data,HEX);
}
