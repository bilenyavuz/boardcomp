package GUI.views;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Scanner;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import com.fazecast.jSerialComm.SerialPort;
import javax.swing.border.EtchedBorder;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 * The BMS JAVA program is developed to read in the data from the serial monitor
 * of Arduino software through any of the available ports and write the data
 * appropriately in the desired logic and format required for the purpose of
 * this thesis.
 *
 * @author Prajwal Kumar
 * @version 1.0
 * @since 2018-05-10
 */
@SuppressWarnings("serial")
public class BMS extends JFrame {

	private JPanel contentPane;
	public static SerialPort chosenPort;
	private JTable table;
	private double bc[] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
	static int counter = 0;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BMS frame = new BMS();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BMS() {
		setForeground(Color.BLACK);
		setTitle("BATTERY MANAGEMENT SYSTEM");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 486, 429);
		contentPane = new JPanel();
		contentPane.setForeground(new Color(0, 0, 0));
		contentPane.setBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0)));
		contentPane.setBackground(Color.LIGHT_GRAY);
		setContentPane(contentPane);

		Object rowData[][] = { { "C1", bc[1], "C2", bc[2], "C3", bc[3] }, { "C4", bc[4], "C5", bc[5], "C6", bc[6] },
				{ "C7", bc[7], "C8", bc[8], "C9", bc[9] }, { "C10", bc[10], "C11", bc[11], "C12", bc[12] },
				{ "C13", bc[13], "C14", bc[14], "C15", bc[15] } };
		String columnNames[] = { "Battery", "s", "s", "s", "s", "s" };
		JTextPane txtpnCellBalancing = new JTextPane();
		txtpnCellBalancing.setBackground(SystemColor.inactiveCaption);
		txtpnCellBalancing.setText("Cell Balancing");

		JLabel lblSelectComPort = new JLabel("Select COM Port");

		JComboBox<String> portSelect = new JComboBox<String>();

		// populate the drop-down box
		SerialPort[] portNames = SerialPort.getCommPorts();
		for (int i = 0; i < portNames.length; i++)
			portSelect.addItem(portNames[i].getSystemPortName());

		JButton btnConnect = new JButton("CONNECT");

		btnConnect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (btnConnect.getText().equals("CONNECT")) {

					// attempt to connect to the serial port
					chosenPort = SerialPort.getCommPort(portSelect.getSelectedItem().toString());
					chosenPort.setComPortTimeouts(SerialPort.TIMEOUT_SCANNER, 0, 0);
					chosenPort.setBaudRate(57600);
					if (chosenPort.openPort()) {
						btnConnect.setText("Disconnect");
						portSelect.setEnabled(false);
					}
					// Sending the command to start balancing
					OutputStream out = chosenPort.getOutputStream();
					try {
						out.write("start\n".getBytes());
					} catch (IOException e1) {
						e1.printStackTrace();
					}

					Thread thread = new Thread() {
						@Override
						public void run() {
							// Initializing the input stream to read the cell balancing data
							Scanner scanner = new Scanner(chosenPort.getInputStream());
							while (scanner.hasNext()) {
								try {
									String line = scanner.nextLine();
									String[] parts = line.split(":");
									Double d = Double.parseDouble(parts[1]);

									if (counter <= 15) {
										bc[counter] = d;
										System.out.println(bc[counter]);
										if (counter != 0)
											rowData[(counter - 1) / 3][((2 * counter) - 1) % 6] = bc[counter];
										counter++;
										textField.setText("" + bc[0]);
										table.repaint();
										table.validate();
									}

									if (counter > 15)
										counter = 0;

								} catch (Exception e1) {
									e1.printStackTrace();
								}
							}
							scanner.close();
						}
					};
					thread.start();

				} else {
					// disconnect from the serial port
					chosenPort.closePort();
					portSelect.setEnabled(true);
					btnConnect.setText("CONNECT");
				}
			}
		});

		// UI related blocks
		JTextPane txtpnBatteryVoltages = new JTextPane();
		txtpnBatteryVoltages.setBackground(SystemColor.inactiveCaption);
		txtpnBatteryVoltages.setText("Battery Voltages");

		JButton btnBattery1 = new JButton("Battery 1");

		JButton btnBattery2 = new JButton("Battery 2");

		JButton btnBattery3 = new JButton("Battery 3");

		JButton btnBattery4 = new JButton("Battery 4");

		// Creating a single instance of the Button handler to handle the user actions

		BatteryPackButtonHandler battPackHandler = new BatteryPackButtonHandler();
		btnBattery1.addActionListener(battPackHandler);
		btnBattery2.addActionListener(battPackHandler);
		btnBattery3.addActionListener(battPackHandler);
		btnBattery4.addActionListener(battPackHandler);
		Image image = new ImageIcon(this.getClass().getResource("/88448-200.png")).getImage();

		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(image));
		label.setBounds(10, 10, 200, 50);

		table = new JTable(rowData, columnNames);

		textField = new JTextField();
		textField.setForeground(Color.BLACK);
		textField.setColumns(10);

		// UI related Blocks
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(gl_contentPane
				.createSequentialGroup()
				.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(gl_contentPane
						.createSequentialGroup()
						.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_contentPane.createSequentialGroup().addComponent(btnBattery2).addGap(3))
								.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addComponent(txtpnBatteryVoltages, GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addGroup(gl_contentPane.createSequentialGroup()
												.addComponent(txtpnCellBalancing, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addGap(38).addComponent(lblSelectComPort))))
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
										.addPreferredGap(ComponentPlacement.UNRELATED).addComponent(portSelect,
												GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_contentPane.createSequentialGroup().addGap(28).addComponent(btnBattery3)))
						.addPreferredGap(ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_contentPane.createSequentialGroup().addComponent(btnConnect)
										.addPreferredGap(ComponentPlacement.RELATED))
								.addGroup(gl_contentPane.createSequentialGroup().addComponent(btnBattery4).addGap(8))))
						.addGroup(gl_contentPane.createSequentialGroup().addGap(37).addComponent(btnBattery1).addGap(75)
								.addComponent(label)))
				.addContainerGap(30, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup().addContainerGap()
						.addComponent(table, GroupLayout.PREFERRED_SIZE, 439, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(21, Short.MAX_VALUE))
				.addGroup(gl_contentPane
						.createSequentialGroup().addContainerGap().addComponent(textField, GroupLayout.PREFERRED_SIZE,
								GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(338, Short.MAX_VALUE)));
		gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(gl_contentPane
				.createSequentialGroup().addContainerGap()
				.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(txtpnCellBalancing, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE).addComponent(lblSelectComPort)
								.addComponent(portSelect, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(btnConnect, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)))
				.addGap(31)
				.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(txtpnBatteryVoltages, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(label, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE))
				.addGap(18)
				.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnBattery4, GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)
						.addComponent(btnBattery3, GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)
						.addComponent(btnBattery2, GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)
						.addComponent(btnBattery1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
				.addPreferredGap(ComponentPlacement.UNRELATED)
				.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
						GroupLayout.PREFERRED_SIZE)
				.addGap(17).addComponent(table, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
				.addGap(18)));
		contentPane.setLayout(gl_contentPane);

	}
}
