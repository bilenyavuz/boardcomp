//package GUI.EventHandlers;
package GUI.views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.OutputStream;
import javax.swing.JButton;

/**
 * The Battery Pack button handler JAVA program is developed to describe the
 * action event handlers when the buttons are clicked.
 *
 * @author Prajwal Kumar
 * @version 1.0
 * @since 2018-05-10
 */
public class BatteryPackButtonHandler implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		// Getting the source of the click event for the battery pack
		final int ic1Indices[] = { 1, 2, 3, 4, 7, 8, 9, 10 };
		final int ic2Indices[] = { 1, 2, 3, 4, 7, 8, 9 };
		JButton source = (JButton) e.getSource();
		if (BMS.chosenPort.isOpen()) {
			String userInput = "";
			if (source.getText().equals("Battery 1")) {
				userInput = "B1";

			} else if (source.getText().equals("Battery 2")) {
				userInput = "B2";
			} else if (source.getText().equals("Battery 3")) {
				userInput = "B3";
			} else if (source.getText().equals("Battery 4")) {
				userInput = "B4";
			}

			try {
				if (!BMS.chosenPort.isOpen())
					BMS.chosenPort.openPort();
				OutputStream out = BMS.chosenPort.getOutputStream();
				out.write((userInput + "\n").getBytes());
				for (int i = 1; i <= 15; i++) {
					if (i <= 8)
						out.write((userInput + "I" + 1 + "C" + ic1Indices[i - 1] + "\n").getBytes());
					else
						out.write((userInput + "I" + 2 + "C" + ic2Indices[i - 8 - 1] + "\n").getBytes());

				}
				out.write("start\n".getBytes());
			} catch (IOException e1) {
				e1.printStackTrace();
			}

		}
	}

}
