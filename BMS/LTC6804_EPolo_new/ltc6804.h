
#define LTC6804_CS 13


void spi_enable(uint8_t spi_clock_divider);   //!< Configures SCK frequency. Use constant defined in header file.
void spi_write(int8_t data); //!< Byte to be written to SPI port
int8_t spi_read(int8_t data); //!< The data byte to be written


// Macros
//! Set "pin" low
//! @param pin pin to be driven LOW
#define output_low(pin)   digitalWrite(pin, LOW)
//! Set "pin" high
//! @param pin pin to be driven HIGH
#define output_high(pin)  digitalWrite(pin, HIGH)
//! Return the state of pin "pin"
//! @param pin pin to be read (HIGH or LOW).
//! @return the state of pin "pin"
#define input(pin)        digitalRead(pin)

//! @name ENDIAN DEPENDENT BYTE INDEXES
//! @{
//! Arduino/Linduino is a Little Endian Device, where the least significant byte is stored in the first byte of larger data types.
#ifdef BIG_ENDIAN
#define LSB 1 //!< Location of Least Signficant Byte when Word is accessed as Byte Array
#define MSB 0 //!< Location of Most Signficant Byte when Word is accessed as Byte Array
#define LSW 1 //!< Location of Least Signficant Word when Long Word is accessed as Byte Array
#define MSW 0 //!< Location of most Signficant Word when Long Word is accessed as Byte Array
#else
#define LSB 0 //!< Location of Least Signficant Byte when Word is accessed as Byte Array
#define MSB 1 //!< Location of Most Signficant Byte when Word is accessed as Byte Array
#define LSW 0 //!< Location of Least Signficant Word when Long Word is accessed as Byte Array
#define MSW 1 //!< Location of most Signficant Word when Long Word is accessed as Byte Array
#endif
//! @}

#define UI_BUFFER_SIZE 64
#define SERIAL_TERMINATOR '\n'



/////////////////////////////////////
/**************** LTC68041
 *  
 */

 

void LTC6804_initialize();

void set_adc(uint8_t MD, uint8_t DCP, uint8_t CH, uint8_t CHG);

void LTC6804_adcv();

void LTC6804_adax();

uint8_t LTC6804_rdcv(uint8_t reg, uint8_t total_ic, uint16_t cell_codes[][12]);

void LTC6804_rdcv_reg(uint8_t reg, uint8_t nIC, uint8_t *data);

int8_t LTC6804_rdaux(uint8_t reg, uint8_t nIC, uint16_t aux_codes[][6]);

void LTC6804_rdaux_reg(uint8_t reg, uint8_t nIC,uint8_t *data);

void LTC6804_clrcell();

void LTC6804_clraux();

void LTC6804_wrcfg(uint8_t nIC,uint8_t config[][6]);

int8_t LTC6804_rdcfg(uint8_t nIC, uint8_t r_config[][8]);

void wakeup_idle();

void wakeup_sleep();


uint16_t pec15_calc(uint8_t len, uint8_t *data);
void spi_write_array( uint8_t length, uint8_t *data);

void spi_write_read(uint8_t *TxData, uint8_t TXlen, uint8_t *rx_data, uint8_t RXlen);


#include <stdint.h>

#define UI_BUFFER_SIZE 64
#define SERIAL_TERMINATOR '\n'

// io buffer
extern char ui_buffer[UI_BUFFER_SIZE];

// Read data from the serial interface into the ui_buffer buffer
uint8_t read_data();

// Read a float value from the serial interface
float read_float();

// Read an integer from the serial interface.
// The routine can recognize Hex, Decimal, Octal, or Binary
// Example:
// Hex:     0x11 (0x prefix)
// Decimal: 17
// Octal:   O21 (leading letter O prefix)
// Binary:  B10001 (leading letter B prefix)
int32_t read_int();

// Read a string from the serial interface.  Returns a pointer to the ui_buffer.
char *read_string();

// Read a character from the serial interface
int8_t read_char();




