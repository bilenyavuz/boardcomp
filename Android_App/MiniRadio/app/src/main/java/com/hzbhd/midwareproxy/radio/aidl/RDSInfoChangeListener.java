package com.hzbhd.midwareproxy.radio.aidl;

public interface RDSInfoChangeListener {
    void AFLevelChanged(String str);

    void AFSeekChanged(boolean z);

    void AFStatusChanged(boolean z);

    void CurrentStationStatusChanged(boolean z);

    void EONStatusChanged(boolean z);

    void EWSArrivedChanged(boolean z);

    void EWSStatusChanged(boolean z);

    void PSInfoChanged(String str);

    void PSStatusChanged(boolean z);

    void PTYInfoChanged(String str);

    void PTYSeekChanged(boolean z);

    void PTYTypeInfoChanged(String str);

    void RTInfoChanged(String str);

    void RTStatusChanged(boolean z);

    void TASeekChanged(boolean z);

    void TAStatusChanged(boolean z);

    void TPInfoChanged(boolean z);

    void TPSeekChanged(boolean z);
}
