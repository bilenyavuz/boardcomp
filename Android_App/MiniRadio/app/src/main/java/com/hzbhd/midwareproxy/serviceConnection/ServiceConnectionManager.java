package com.hzbhd.midwareproxy.serviceConnection;

//import com.hzbhd.midwareproxy.utils.Log;
import android.util.Log;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import java.util.concurrent.locks.ReentrantLock;

public class ServiceConnectionManager {
    private static final String TAG = "ServiceConnectionManage";
    static ServiceConnectionManager mServiceManager;
    private ReentrantLock mLock = new ReentrantLock();
    /* access modifiers changed from: private */
    public Vector<ServiceConnection> mServiceNeedConnected = new Vector<>();
    private HashMap<String, ServiceConnection> mServices = new HashMap<>();
    private Thread mWorkThread = new Thread(null, new MonitorRunnable(), "ServiceConnectionManager thread");

    private class MonitorRunnable implements Runnable {
        private MonitorRunnable() {
        }

        public void run() {
            while (true) {
                try {
                    if (ServiceConnectionManager.this.mServiceNeedConnected.isEmpty()) {
                        Thread.sleep(3000);
                    } else {
                        ServiceConnectionManager.this.connectService();
                        Thread.sleep(3000);
                        ServiceConnectionManager.this.checkService();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static ServiceConnectionManager getServiceConnectionManager() {
        if (mServiceManager == null) {
            mServiceManager = new ServiceConnectionManager();
        }
        return mServiceManager;
    }

    private ServiceConnectionManager() {
        this.mWorkThread.start();
    }

    public void registerConnectionListener(String name, ServiceConnection service) {
        this.mLock.lock();
        if (!this.mServices.containsKey(name)) {
            this.mServices.put(name, service);
        }
        this.mLock.unlock();
    }

    public void onServiceDisconnected(String name) {
        this.mLock.lock();
        ServiceConnection disconnectedService = this.mServices.get(name);
        if (disconnectedService != null && !this.mServiceNeedConnected.contains(disconnectedService)) {
            this.mServiceNeedConnected.add(disconnectedService);
        }
        this.mLock.unlock();
    }

    public boolean connectService() {
        this.mLock.lock();
        Iterator<ServiceConnection> it = this.mServiceNeedConnected.iterator();
        while (it.hasNext()) {
            ServiceConnection serviceDisconnected = it.next();
            if (!serviceDisconnected.getServiceConnection()) {
                Log.w(TAG, "doBindService in back thread fail: " + serviceDisconnected.getServiceName());
            } else {
                Log.i(TAG, "doBindService in back thread success: " + serviceDisconnected.getServiceName());
                serviceDisconnected.serviceReConnected();
            }
        }
        this.mLock.unlock();
        return false;
    }

    public void checkService() {
        this.mLock.lock();
        Vector<ServiceConnection> vectService = new Vector<>();
        Iterator<ServiceConnection> it = this.mServiceNeedConnected.iterator();
        while (it.hasNext()) {
            ServiceConnection serviceDisconnected = it.next();
            if (!serviceDisconnected.isServiceConnected()) {
                vectService.add(serviceDisconnected);
                Log.i(TAG, "checkService in back thread add: " + serviceDisconnected.getServiceName());
            } else {
                Log.i(TAG, "checkService in back thread del: " + serviceDisconnected.getServiceName());
            }
        }
        this.mServiceNeedConnected = vectService;
        this.mLock.unlock();
    }
}
