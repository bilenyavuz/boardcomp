package com.hzbhd.midwareproxy.radio;

import android.annotation.SuppressLint;
import android.hardware.input.InputManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
//import com.autochips.ipod.IPodSDK.IPodException;
import com.hzbhd.midware.constant.RadioConstantsDef.StepType;
import com.hzbhd.midware.constant.ServiceConstants;
//import com.hzbhd.midwareproxy.iPod.IPodServiceManager.ServiceConnectionState;
import com.hzbhd.midwareproxy.radio.aidl.IRDSInfoChangeListener;
import com.hzbhd.midwareproxy.radio.aidl.IRDSInfoChangeListener.Stub;
import com.hzbhd.midwareproxy.radio.aidl.IRadioInfoChangeListener;
import com.hzbhd.midwareproxy.radio.aidl.IRadioManager;
import com.hzbhd.midwareproxy.radio.aidl.RDSInfoChangeListener;
import com.hzbhd.midwareproxy.radio.aidl.RadioInfoChangeListener;
import com.hzbhd.midwareproxy.serviceConnection.ServiceConnection;
//import com.hzbhd.midwareproxy.utils.Log;
import java.util.HashMap;

public class RadioManager extends ServiceConnection implements Handler.Callback {
    private static final String TAG = "RadioManager";
    private static RadioManager mRadioManager = null;
    private static IRadioManager mRadioManagerService = null;
    private HashMap<RadioInfoChangeListener, RadioInfoChangeListenerTransport> mListeners = new HashMap<>();
    private HashMap<RDSInfoChangeListener, RDSInfoChangeListenerTransport> mRDSListeners = new HashMap<>();

    @Override
    public boolean handleMessage(Message msg) {
        return false;
    }
//    private ServiceConnectionState mServiceConnectionState;

    private class RDSInfoChangeListenerTransport extends Stub {
        private static final int MESSAGE_AFLevel_CHANGED = 1;
        private static final int MESSAGE_AFSeek_CHANGED = 2;
        private static final int MESSAGE_AF_STATUS_CHANGED = 12;
        private static final int MESSAGE_CURRENT_STATION_CHANGED = 15;
        private static final int MESSAGE_EONStatus_CHANGED = 3;
        private static final int MESSAGE_EWSStatus_CHANGED = 4;
        private static final int MESSAGE_EWS_ARRIVED_CHANGED = 17;
        private static final int MESSAGE_PSInfo_CHANGED = 5;
        private static final int MESSAGE_PS_STATUS_CHANGED = 16;
        private static final int MESSAGE_PTYInfo_CHANGED = 6;
        private static final int MESSAGE_PTYSeek_CHANGED = 7;
        private static final int MESSAGE_PTY_TYPE_INFO_CHANGED = 14;
        private static final int MESSAGE_RTInfo_CHANGED = 8;
        private static final int MESSAGE_RT_STATUS_CHANGED = 18;
        private static final int MESSAGE_TASeek_CHANGED = 9;
        private static final int MESSAGE_TAStatus_CHANGED = 10;
        private static final int MESSAGE_TPINFO_CHANGED = 13;
        private static final int MESSAGE_TPSeek_CHANGED = 11;
        private RDSInfoChangeListener mListener;
        private final Handler mListenerHandler;

        @SuppressLint({"HandlerLeak"})
        RDSInfoChangeListenerTransport(RDSInfoChangeListener listener, Looper looper) {
            this.mListener = listener;
            if (looper == null) {
                this.mListenerHandler = new Handler(com.hzbhd.midwareproxy.radio.RadioManager.this) {
                    public void handleMessage(Message msg) {
                        RDSInfoChangeListenerTransport.this._handleMessage(msg);
                    }
                };
            } else {
                this.mListenerHandler = new Handler(looper, com.hzbhd.midwareproxy.radio.RadioManager.this) {
                    public void handleMessage(Message msg) {
                        RDSInfoChangeListenerTransport.this._handleMessage(msg);
                    }
                };
            }
        }

        /* access modifiers changed from: private */
        public void _handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    this.mListener.AFLevelChanged((String) msg.obj);
                    return;
                case 2:
                    this.mListener.AFSeekChanged(((Boolean) msg.obj).booleanValue());
                    return;
                case 3:
                    this.mListener.EONStatusChanged(((Boolean) msg.obj).booleanValue());
                    return;
                case 4:
                    this.mListener.EWSStatusChanged(((Boolean) msg.obj).booleanValue());
                    return;
                case 5:
                    this.mListener.PSInfoChanged((String) msg.obj);
                    return;
                case 6:
                    this.mListener.PTYInfoChanged((String) msg.obj);
                    return;
                case 7:
                    this.mListener.PTYSeekChanged(((Boolean) msg.obj).booleanValue());
                    return;
                case 8:
                    this.mListener.RTInfoChanged((String) msg.obj);
                    return;
                case 9:
                    this.mListener.TASeekChanged(((Boolean) msg.obj).booleanValue());
                    return;
                case 10:
                    this.mListener.TAStatusChanged(((Boolean) msg.obj).booleanValue());
                    return;
                case 11:
                    this.mListener.TPSeekChanged(((Boolean) msg.obj).booleanValue());
                    return;
                case 12:
                    this.mListener.AFStatusChanged(((Boolean) msg.obj).booleanValue());
                    return;
                case 13:
                    this.mListener.TPInfoChanged(((Boolean) msg.obj).booleanValue());
                    return;
                case 14:
                    this.mListener.PTYTypeInfoChanged((String) msg.obj);
                    return;
                case 15:
                    this.mListener.CurrentStationStatusChanged(((Boolean) msg.obj).booleanValue());
                    return;
                case 16:
                    this.mListener.PSStatusChanged(((Boolean) msg.obj).booleanValue());
                    return;
                case 17:
                    this.mListener.EWSArrivedChanged(((Boolean) msg.obj).booleanValue());
                    return;
                case 18:
                    this.mListener.RTStatusChanged(((Boolean) msg.obj).booleanValue());
                    return;
                default:
                    return;
            }
        }

        public void AFLevelChanged(String level) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 1;
            msg.obj = level;
            this.mListenerHandler.sendMessage(msg);
        }

        public void AFSeekChanged(boolean flag) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 2;
            msg.obj = Boolean.valueOf(flag);
            this.mListenerHandler.sendMessage(msg);
        }

        public void EONStatusChanged(boolean flag) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 3;
            msg.obj = Boolean.valueOf(flag);
            this.mListenerHandler.sendMessage(msg);
        }

        public void EWSStatusChanged(boolean flag) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 4;
            msg.obj = Boolean.valueOf(flag);
            this.mListenerHandler.sendMessage(msg);
        }

        public void PSInfoChanged(String psInfo) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 5;
            msg.obj = psInfo;
            this.mListenerHandler.sendMessage(msg);
        }

        public void PTYTypeInfoChanged(String ptyTypeInfo) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 14;
            msg.obj = ptyTypeInfo;
            this.mListenerHandler.sendMessage(msg);
        }

        public void PTYInfoChanged(String ptyInfo) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 6;
            msg.obj = ptyInfo;
            this.mListenerHandler.sendMessage(msg);
        }

        public void PTYSeekChanged(boolean flag) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 7;
            msg.obj = Boolean.valueOf(flag);
            this.mListenerHandler.sendMessage(msg);
        }

        public void RTInfoChanged(String rtInfo) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 8;
            msg.obj = rtInfo;
            this.mListenerHandler.sendMessage(msg);
        }

        public void TASeekChanged(boolean flag) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 9;
            msg.obj = Boolean.valueOf(flag);
            this.mListenerHandler.sendMessage(msg);
        }

        public void TAStatusChanged(boolean flag) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 10;
            msg.obj = Boolean.valueOf(flag);
            this.mListenerHandler.sendMessage(msg);
        }

        public void TPSeekChanged(boolean flag) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 11;
            msg.obj = Boolean.valueOf(flag);
            this.mListenerHandler.sendMessage(msg);
        }

        public void AFStatusChanged(boolean onoff) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 12;
            msg.obj = Boolean.valueOf(onoff);
            this.mListenerHandler.sendMessage(msg);
        }

        public void TPInfoChanged(boolean flag) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 13;
            msg.obj = Boolean.valueOf(flag);
            this.mListenerHandler.sendMessage(msg);
        }

        public void CurrentStationStatusChanged(boolean CurrentStation) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 15;
            msg.obj = Boolean.valueOf(CurrentStation);
            this.mListenerHandler.sendMessage(msg);
        }

        public void PSStatusChanged(boolean psStatus) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 16;
            msg.obj = Boolean.valueOf(psStatus);
            this.mListenerHandler.sendMessage(msg);
        }

        public void EWSArrivedChanged(boolean EWSArrived) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 17;
            msg.obj = Boolean.valueOf(EWSArrived);
            this.mListenerHandler.sendMessage(msg);
        }

        public void RTStatusChanged(boolean rtStatus) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 18;
            msg.obj = Boolean.valueOf(rtStatus);
            this.mListenerHandler.sendMessage(msg);
        }
    }

    private class RadioInfoChangeListenerTransport extends IRadioInfoChangeListener.Stub {
        private static final int MESSAGE_AutoSearchStatus_CHANGED = 4;
        private static final int MESSAGE_AutoSearchWithPreviewStatus_CHANGED = 5;
        private static final int MESSAGE_BandType_CHANGED = 3;
        private static final int MESSAGE_ChannelType_CHANGED = 0;
        private static final int MESSAGE_Freq_CHANGED = 2;
        private static final int MESSAGE_LOCAL_CHANGED = 12;
        private static final int MESSAGE_PRESET_PS_NAME_CHANGED = 13;
        private static final int MESSAGE_PresetList_CHANGED = 8;
        private static final int MESSAGE_PresetPSList_CHANGED = 9;
        private static final int MESSAGE_STEREO_CHANGED = 11;
        private static final int MESSAGE_SignalStrength_CHANGED = 1;
        private static final int MESSAGE_StepDownStatus_CHANGED = 7;
        private static final int MESSAGE_StepUpStatus_CHANGED = 6;
        private RadioInfoChangeListener mListener;
        private final Handler mListenerHandler;

        RadioInfoChangeListenerTransport(RadioInfoChangeListener listener, Looper looper) {
            this.mListener = listener;
            if (looper == null) {
                this.mListenerHandler = new Handler(com.hzbhd.midwareproxy.radio.RadioManager.this) {
                    public void handleMessage(Message msg) {
                        RadioInfoChangeListenerTransport.this._handleMessage(msg);
                    }
                };
            } else {
                this.mListenerHandler = new Handler(looper, com.hzbhd.midwareproxy.radio.RadioManager.this) {
                    public void handleMessage(Message msg) {
                        RadioInfoChangeListenerTransport.this._handleMessage(msg);
                    }
                };
            }
        }

        /* access modifiers changed from: private */
        public void _handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    this.mListener.radioChannelTypeChanged((String) msg.obj);
                    return;
                case 1:
                    this.mListener.radioSignalStrengthChanged(((Integer) msg.obj).intValue());
                    return;
                case 2:
                    this.mListener.radioFreqChanged((String) msg.obj);
                    return;
                case 3:
                    this.mListener.radioBandTypeChanged((String) msg.obj);
                    return;
                case 4:
                    this.mListener.radioAutoSearchStatusChanged(((Boolean) msg.obj).booleanValue());
                    return;
                case 5:
                    this.mListener.radioAutoSearchWithPreviewStatusChanged(((Boolean) msg.obj).booleanValue());
                    return;
                case 6:
                    this.mListener.radioStepUpStatusChanged(((Boolean) msg.obj).booleanValue());
                    return;
                case 7:
                    this.mListener.radioStepDownStatusChanged(((Boolean) msg.obj).booleanValue());
                    return;
                case 8:
                    this.mListener.radioPresetFreqListChanged((String[]) msg.obj);
                    return;
                case 9:
                    this.mListener.radioPresetFreqPSListChanged((String[]) msg.obj);
                    return;
                case 11:
                    this.mListener.radioStereoChanged(((Integer) msg.obj).intValue());
                    return;
                case 12:
                    this.mListener.radioLocalChanged(((Integer) msg.obj).intValue());
                    return;
                case 13:
                    this.mListener.radioPresetFreqPSNameChanged(msg.arg1, (String) msg.obj);
                    return;
                default:
                    return;
            }
        }

        public void radioAutoSearchStatusChanged(boolean flag) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 4;
            msg.obj = Boolean.valueOf(flag);
            this.mListenerHandler.sendMessage(msg);
        }

        public void radioAutoSearchWithPreviewStatusChanged(boolean flag) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 5;
            msg.obj = Boolean.valueOf(flag);
            this.mListenerHandler.sendMessage(msg);
        }

        public void radioBandTypeChanged(String bandType) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 3;
            msg.obj = bandType;
            this.mListenerHandler.sendMessage(msg);
        }

        public void radioChannelTypeChanged(String channelTypeValue) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 0;
            msg.obj = channelTypeValue;
            this.mListenerHandler.sendMessage(msg);
        }

        public void radioFreqChanged(String freq) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 2;
            msg.obj = freq;
            this.mListenerHandler.sendMessage(msg);
        }

        public void radioSignalStrengthChanged(int signal) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 1;
            msg.obj = Integer.valueOf(signal);
            this.mListenerHandler.sendMessage(msg);
        }

        public void radioStepDownStatusChanged(boolean flag) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 7;
            msg.obj = Boolean.valueOf(flag);
            this.mListenerHandler.sendMessage(msg);
        }

        public void radioStepUpStatusChanged(boolean flag) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 6;
            msg.obj = Boolean.valueOf(flag);
            this.mListenerHandler.sendMessage(msg);
        }

        public void radioPresetFreqListChanged(String[] list) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 8;
            msg.obj = list;
            this.mListenerHandler.sendMessage(msg);
        }

        public void radioPresetFreqPSListChanged(String[] list) throws RemoteException {
            Message msg = Message.obtain();
            msg.what = 9;
            msg.obj = list;
            this.mListenerHandler.sendMessage(msg);
        }

        public void radioStereoChanged(int mStereo) throws RemoteException {
            this.mListenerHandler.obtainMessage(11, Integer.valueOf(mStereo)).sendToTarget();
        }

        public void radioLocalChanged(int mLocal) throws RemoteException {
            this.mListenerHandler.obtainMessage(12, Integer.valueOf(mLocal)).sendToTarget();
        }

        public void radioPresetFreqPSNameChanged(int presetIdx, String presetPSName) throws RemoteException {
            this.mListenerHandler.obtainMessage(13, presetIdx, 0, presetPSName).sendToTarget();
        }
    }

    public static RadioManager getRadioManager() {
        if (mRadioManager == null) {
            mRadioManager = new RadioManager();
        }
        if (mRadioManagerService == null) {
            return null;
        }
        return mRadioManager;
    }

    private RadioManager() {
        getServiceConnection();
    }

    public String getServiceName() {
        return ServiceConstants.SERVICE_NAME_RADIO;
    }

    public boolean getServiceConnection() {
        IBinder getServiceObj = connectService();
        if (getServiceObj != null) {
            mRadioManagerService = IRadioManager.Stub.asInterface(getServiceObj);
            Log.e(TAG, "mRadioManagerService connected1:");
            return true;
        }
        mRadioManagerService = null;
        Log.e(TAG, "mRadioManagerService not connected:");
        return false;
    }

/*
    public void serviceDied() {
        mRadioManager = null;
        if (this.mServiceConnectionState != null) {
            this.mServiceConnectionState.serviceDead();
        }
    }

    public void setServiceConnectionStateListener(ServiceConnectionState serviceConnectionState) {
        this.mServiceConnectionState = serviceConnectionState;
    }
*/

    public void setRadioInfoChangeListener(RadioInfoChangeListener listener) {
        try {
            synchronized (this.mListeners) {
                RadioInfoChangeListenerTransport transport = this.mListeners.get(listener);
                if (transport == null) {
                    transport = new RadioInfoChangeListenerTransport(listener, null);
                }
                this.mListeners.put(listener, transport);
                mRadioManagerService.setRadioInfoChangeListener(transport);
            }
        } catch (RemoteException ex) {
            Log.e(TAG, "setRadioInfoChangeListener: DeadObjectException", ex);
        }
    }

    public void removeRadioInfoChangeListener(RadioInfoChangeListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException("listener==null");
        }
        Log.d(TAG, "removeRadioInfoChangeListener: listener = " + listener);
        try {
            RadioInfoChangeListenerTransport transport = this.mListeners.remove(listener);
            if (transport != null) {
                mRadioManagerService.removeRadioInfoChangeListener(transport);
            }
        } catch (RemoteException ex) {
            Log.e(TAG, "removeRadioInfoChangeListener: DeadObjectException", ex);
        }
    }

    public void setRDSInfoChangeListener(RDSInfoChangeListener listener) {
        try {
            synchronized (this.mRDSListeners) {
                RDSInfoChangeListenerTransport transport = this.mRDSListeners.get(listener);
                if (transport == null) {
                    transport = new RDSInfoChangeListenerTransport(listener, null);
                }
                this.mRDSListeners.put(listener, transport);
                mRadioManagerService.setRDSInfoChangeListener(transport);
            }
        } catch (RemoteException ex) {
            Log.e(TAG, "setRDSInfoChangeListener: DeadObjectException", ex);
        }
    }

    public void removeRDSInfoChangeListener(RDSInfoChangeListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException("listener==null");
        }
        Log.d(TAG, "removeRDSInfoChangeListener: listener = " + listener);
        try {
            RDSInfoChangeListenerTransport transport = this.mRDSListeners.remove(listener);
            if (transport != null) {
                mRadioManagerService.removeRDSInfoChangeListener(transport);
            }
        } catch (RemoteException ex) {
            Log.e(TAG, "removeRDSInfoChangeListener: DeadObjectException", ex);
        }
    }

    public boolean autoSearch(int searchType) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.autoSearch(searchType);
        } catch (RemoteException ex) {
            Log.e(TAG, "autoSearch: RemoteException", ex);
            return flag;
        }
    }

    public boolean autoSearchWithPreview(int searchType) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.autoSearchWithPreview(searchType);
        } catch (RemoteException ex) {
            Log.e(TAG, "autoSearchWithPreview: RemoteException", ex);
            return flag;
        }
    }

    public boolean enableAFSwitch(boolean enable) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.enableAFSwitch(enable);
        } catch (RemoteException ex) {
            Log.e(TAG, "enableAFSwitch: RemoteException", ex);
            return flag;
        }
    }

    public boolean enableCTSwitch(boolean enable) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.enableCTSwitch(enable);
        } catch (RemoteException ex) {
            Log.e(TAG, "enableCTSwitch: RemoteException", ex);
            return flag;
        }
    }

    public boolean enableEONSwitch(boolean enable) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.enableEONSwitch(enable);
        } catch (RemoteException ex) {
            Log.e(TAG, "enableEONSwitch: RemoteException", ex);
            return flag;
        }
    }

    public boolean enablePSSwitch(boolean enable) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.enablePSSwitch(enable);
        } catch (RemoteException ex) {
            Log.e(TAG, "enablePSSwitch: RemoteException", ex);
            return flag;
        }
    }

    public boolean enablePTYSwitch(boolean enable) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.enablePTYSwitch(enable);
        } catch (RemoteException ex) {
            Log.e(TAG, "enablePTYSwitch: RemoteException", ex);
            return flag;
        }
    }

    public boolean enableRDSSwitch(boolean enable) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.enableRDSSwitch(enable);
        } catch (RemoteException ex) {
            Log.e(TAG, "enableRDSSwitch: RemoteException", ex);
            return flag;
        }
    }

    public boolean enableRegionSwitch(boolean enable) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.enableRegionSwitch(enable);
        } catch (RemoteException ex) {
            Log.e(TAG, "enableRegionSwitch: RemoteException", ex);
            return flag;
        }
    }

    public boolean enableTASwitch(boolean enable) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.enableTASwitch(enable);
        } catch (RemoteException ex) {
            Log.e(TAG, "enableTASwitch: RemoteException", ex);
            return flag;
        }
    }

    public boolean getAFSeekStatus() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.getAFSeekStatus();
        } catch (RemoteException ex) {
            Log.e(TAG, "getAFSeekStatus: RemoteException", ex);
            return flag;
        }
    }

    public boolean getAFSwitchStatus() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.getAFSwitchStatus();
        } catch (RemoteException ex) {
            Log.e(TAG, "getAFSwitchStatus: RemoteException", ex);
            return flag;
        }
    }

    public boolean getAutoSearchStatus() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.getAutoSearchStatus();
        } catch (RemoteException ex) {
            Log.e(TAG, "getAutoSearchStatus: RemoteException", ex);
            return flag;
        }
    }

    public boolean getAutoSearchWithPreviewStatus() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.getAutoSearchWithPreviewStatus();
        } catch (RemoteException ex) {
            Log.e(TAG, "getAutoSearchWithPreviewStatus: RemoteException", ex);
            return flag;
        }
    }

    public boolean getCTSwitchStatus() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.getCTSwitchStatus();
        } catch (RemoteException ex) {
            Log.e(TAG, "getCTSwitchStatus: RemoteException", ex);
            return flag;
        }
    }

    public String getCurPSInfo() throws RemoteException {
        String str = "";
        try {
            return mRadioManagerService.getCurPSInfo();
        } catch (RemoteException ex) {
            Log.e(TAG, "getCurPSInfo: RemoteException", ex);
            return str;
        }
    }

    public String getCurPTYInfo() throws RemoteException {
        String str = "";
        try {
            return mRadioManagerService.getCurPTYInfo();
        } catch (RemoteException ex) {
            Log.e(TAG, "getCurPTYInfo: RemoteException", ex);
            return str;
        }
    }

    public String getCurRTInfo() throws RemoteException {
        String str = "";
        try {
            return mRadioManagerService.getCurRTInfo();
        } catch (RemoteException ex) {
            Log.e(TAG, "getCurRTInfo: RemoteException", ex);
            return str;
        }
    }

    public String getCurrPresetFreq(int bandType) throws RemoteException {
        String str = "";
        try {
            return mRadioManagerService.getCurrPresetFreq(bandType);
        } catch (RemoteException ex) {
            Log.e(TAG, "getCurrPresetFreq: RemoteException", ex);
            return str;
        }
    }

    public String getCurrentArea() throws RemoteException {
        String str = "";
        try {
            return mRadioManagerService.getCurrentArea();
        } catch (RemoteException ex) {
            Log.e(TAG, "getCurrentArea: RemoteException", ex);
            return str;
        }
    }

    public String getCurrentBand() throws RemoteException {
        String str = "";
        try {
            return mRadioManagerService.getCurrentBand();
        } catch (RemoteException ex) {
            Log.e(TAG, "getCurrentBand: RemoteException", ex);
            return str;
        }
    }

    public String getCurrentFreq() throws RemoteException {
        String str = "";
        try {
            return mRadioManagerService.getCurrentFreq();
        } catch (RemoteException ex) {
            Log.e(TAG, "getCurrentFreq: RemoteException", ex);
            return str;
        }
    }

    public String getDefaultArea() throws RemoteException {
        return mRadioManagerService.getDefaultArea();
    }

    public boolean getEONStatus() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.getEONStatus();
        } catch (RemoteException ex) {
            Log.e(TAG, "getEONStatus: RemoteException", ex);
            return flag;
        }
    }

    public boolean getEONSwitchStatus() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.getEONSwitchStatus();
        } catch (RemoteException ex) {
            Log.e(TAG, "getEONSwitchStatus: RemoteException", ex);
            return flag;
        }
    }

    public boolean getEWSStatus() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.getEWSStatus();
        } catch (RemoteException ex) {
            Log.e(TAG, "getEWSStatus: RemoteException", ex);
            return flag;
        }
    }

    public boolean getPSSwitchStatus() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.getPSSwitchStatus();
        } catch (RemoteException ex) {
            Log.e(TAG, "getPSSwitchStatus: RemoteException", ex);
            return flag;
        }
    }

    public boolean getPTYSeekStatus() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.getPTYSeekStatus();
        } catch (RemoteException ex) {
            Log.e(TAG, "getPTYSeekStatus: RemoteException", ex);
            return flag;
        }
    }

    public boolean getPTYSwitchStatus() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.getPTYSwitchStatus();
        } catch (RemoteException ex) {
            Log.e(TAG, "getPTYSwitchStatus: RemoteException", ex);
            return flag;
        }
    }

    public String[] getPresetFreqList(int bandType) throws RemoteException {
        String[] list = null;
        try {
            return mRadioManagerService.getPresetFreqList(bandType);
        } catch (RemoteException ex) {
            Log.e(TAG, "getPresetFreqList: RemoteException", ex);
            return list;
        }
    }

    public String getPresetPSName(int presetIdx) {
        try {
            return mRadioManagerService.getPresetPSName(presetIdx);
        } catch (RemoteException e) {
            Log.e(TAG, "getPresetPSName: RemoteException", e);
            return "";
        }
    }

    public void getPresetListPSInfo() {
        try {
            mRadioManagerService.getPresetListPSInfo();
        } catch (RemoteException e) {
            Log.e(TAG, "getPresetListPSInfo: RemoteException", e);
        }
    }

    public int getPresetFreqListCount(int bandType) throws RemoteException {
        return mRadioManagerService.getPresetFreqListCount(bandType);
    }

    public String[] getPresetFreqPSList(int bandType) throws RemoteException {
        String[] list = null;
        try {
            return mRadioManagerService.getPresetFreqPSList(bandType);
        } catch (RemoteException ex) {
            Log.e(TAG, "getPresetFreqPSList: RemoteException", ex);
            return list;
        }
    }

    public boolean getRDSSwitchStatus() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.getRDSSwitchStatus();
        } catch (RemoteException ex) {
            Log.e(TAG, "getRDSSwitchStatus: RemoteException", ex);
            return flag;
        }
    }

    public String getRDSType() throws RemoteException {
        String str = "";
        try {
            return mRadioManagerService.getRDSType();
        } catch (RemoteException ex) {
            Log.e(TAG, "getRDSType: RemoteException", ex);
            return str;
        }
    }

    public String getRFInputMode() throws RemoteException {
        String str = "";
        try {
            return mRadioManagerService.getRFInputMode();
        } catch (RemoteException ex) {
            Log.e(TAG, "getRFInputMode: RemoteException", ex);
            return str;
        }
    }

    public boolean getRegionSwitchStatus() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.getRegionSwitchStatus();
        } catch (RemoteException ex) {
            Log.e(TAG, "getRegionSwitchStatus: RemoteException", ex);
            return flag;
        }
    }

    public int getSignalStrength() throws RemoteException {
        return mRadioManagerService.getSignalStrength();
    }

    public boolean getStepDownStatus() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.getStepDownStatus();
        } catch (RemoteException ex) {
            Log.e(TAG, "getStepDownStatus: RemoteException", ex);
            return flag;
        }
    }

    public boolean getStepUpStatus() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.getStepUpStatus();
        } catch (RemoteException ex) {
            Log.e(TAG, "getStepUpStatus: RemoteException", ex);
            return flag;
        }
    }

    public boolean getTASeekStatus() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.getTASeekStatus();
        } catch (RemoteException ex) {
            Log.e(TAG, "getTASeekStatus: RemoteException", ex);
            return flag;
        }
    }

    public boolean getTAStatus() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.getTAStatus();
        } catch (RemoteException ex) {
            Log.e(TAG, "getTAStatus: RemoteException", ex);
            return flag;
        }
    }

    public boolean getTASwitchStatus() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.getTASwitchStatus();
        } catch (RemoteException ex) {
            Log.e(TAG, "getTASwitchStatus: RemoteException", ex);
            return flag;
        }
    }

    public int getTAVolume() throws RemoteException {
        return mRadioManagerService.getTAVolume();
    }

    public int getTAVolume_Before() throws RemoteException {
        return mRadioManagerService.getTAVolume_Before();
    }

    public int getTAVolume_After() throws RemoteException {
        return mRadioManagerService.getTAVolume_After();
    }

    public boolean getTPSeekStatus() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.getTPSeekStatus();
        } catch (RemoteException ex) {
            Log.e(TAG, "getTPSeekStatus: RemoteException", ex);
            return flag;
        }
    }

    public byte getTunerModel() throws RemoteException {
        return mRadioManagerService.getTunerModel();
    }

    public String[] getValidFreqList(int bandType) throws RemoteException {
        String[] list = null;
        try {
            return mRadioManagerService.getValidFreqList(bandType);
        } catch (RemoteException ex) {
            Log.e(TAG, "getValidFreqList: RemoteException", ex);
            return list;
        }
    }

    public int getValidFreqListCount(int bandType) throws RemoteException {
        return mRadioManagerService.getValidFreqListCount(bandType);
    }

    public String getVoiceChannelType() throws RemoteException {
        String str = "";
        try {
            return mRadioManagerService.getVoiceChannelType();
        } catch (RemoteException ex) {
            Log.e(TAG, "getVoiceChannelType: RemoteException", ex);
            return str;
        }
    }

    public boolean isRDSStation() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.isRDSStation();
        } catch (RemoteException ex) {
            Log.e(TAG, "isRDSStation: RemoteException", ex);
            return flag;
        }
    }

    public boolean isTP() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.isTP();
        } catch (RemoteException ex) {
            Log.e(TAG, "isTP: RemoteException", ex);
            return flag;
        }
    }

    public boolean isValidFreq() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.isValidFreq();
        } catch (RemoteException ex) {
            Log.e(TAG, "isValidFreq: RemoteException", ex);
            return flag;
        }
    }

    public boolean restoreFactorySettings() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.restoreFactorySettings();
        } catch (RemoteException ex) {
            Log.e(TAG, "restoreFactorySettings: RemoteException", ex);
            return flag;
        }
    }

    public boolean searchSpecialRDSProgram(int ptyType) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.searchSpecialRDSProgram(ptyType);
        } catch (RemoteException ex) {
            Log.e(TAG, "searchSpecialRDSProgram: RemoteException", ex);
            return flag;
        }
    }

    public boolean searchStepDown(int stepType) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.searchStepDown(stepType);
        } catch (RemoteException ex) {
            Log.e(TAG, "searchStepDown: RemoteException", ex);
            return flag;
        }
    }

    public boolean searchStepDown() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.searchStepDown(StepType.STEP_DEFAULT.ordinal());
        } catch (RemoteException ex) {
            Log.e(TAG, "searchStepDown: RemoteException", ex);
            return flag;
        }
    }

    public boolean searchStepUp(int stepType) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.searchStepUp(stepType);
        } catch (RemoteException ex) {
            Log.e(TAG, "searchStepUp: RemoteException", ex);
            return flag;
        }
    }

    public boolean searchStepUp() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.searchStepUp(StepType.STEP_DEFAULT.ordinal());
        } catch (RemoteException ex) {
            Log.e(TAG, "searchStepUp: RemoteException", ex);
            return flag;
        }
    }

    public boolean setCurrPresetFreq(int bandType) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.setCurrPresetFreq(bandType);
        } catch (RemoteException ex) {
            Log.e(TAG, "setCurrPresetFreq: RemoteException", ex);
            return flag;
        }
    }

    public boolean setCurrentArea(int area) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.setCurrentArea(area);
        } catch (RemoteException ex) {
            Log.e(TAG, "setCurrentArea: RemoteException", ex);
            return flag;
        }
    }

    public boolean setCurrentBand(int bandType) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.setCurrentBand(bandType);
        } catch (RemoteException ex) {
            Log.e(TAG, "setCurrentBand: RemoteException", ex);
            return flag;
        }
    }

    public boolean setCurrentFreq(String freq) throws RemoteException {
        try {
            mRadioManagerService.setCurrentFreq(freq);
        } catch (RemoteException ex) {
            Log.e(TAG, "setCurrentFreq: RemoteException", ex);
        }
        return false;
    }

    public boolean setDefaultArea() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.setDefaultArea();
        } catch (RemoteException ex) {
            Log.e(TAG, "setDefaultArea: RemoteException", ex);
            return flag;
        }
    }

    public boolean setPresetFreqList(int bandType, int index, String currentFreq) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.setPresetFreqList(bandType, index, currentFreq);
        } catch (RemoteException ex) {
            Log.e(TAG, "setPresetFreqList: RemoteException", ex);
            return flag;
        }
    }

    public boolean setRDSAFLevel(byte level) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.setRDSAFLevel(level);
        } catch (RemoteException ex) {
            Log.e(TAG, "setRDSAFLevel: RemoteException", ex);
            return flag;
        }
    }

    public boolean setRDSType(byte rdsType) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.setRDSType(rdsType);
        } catch (RemoteException ex) {
            Log.e(TAG, "setRDSType: RemoteException", ex);
            return flag;
        }
    }

    public boolean setRFInputMode(int rfMode) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.setRFInputMode(rfMode);
        } catch (RemoteException ex) {
            Log.e(TAG, "setRFInputMode: RemoteException", ex);
            return flag;
        }
    }

    public boolean setTAVolume(int vol_before, int vol, int vol_after) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.setTAVolume(vol_before, vol, vol_after);
        } catch (RemoteException ex) {
            Log.e(TAG, "setTAVolume: RemoteException", ex);
            return flag;
        }
    }

    public boolean setTAVolume_Before(int vol) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.setTAVolume_Before(vol);
        } catch (RemoteException ex) {
            Log.e(TAG, "setTAVolume: RemoteException", ex);
            return flag;
        }
    }

    public boolean setTAVolume_After(int vol) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.setTAVolume_After(vol);
        } catch (RemoteException ex) {
            Log.e(TAG, "setTAVolume: RemoteException", ex);
            return flag;
        }
    }

    public boolean setTunerReg(byte regIndex, byte regValue) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.setTunerReg(regIndex, regValue);
        } catch (RemoteException ex) {
            Log.e(TAG, "setTunerReg: RemoteException", ex);
            return flag;
        }
    }

    public boolean setTunerRegWithModel(byte tunerModel) throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.setTunerRegWithModel(tunerModel);
        } catch (RemoteException ex) {
            Log.e(TAG, "setTunerRegWithModel: RemoteException", ex);
            return flag;
        }
    }

    public boolean stopSearch() throws RemoteException {
        boolean flag = false;
        try {
            return mRadioManagerService.stopSearch();
        } catch (RemoteException ex) {
            Log.e(TAG, "stopSearch: RemoteException", ex);
            return flag;
        }
    }

    public IBinder asBinder() {
        return null;
    }

    public boolean tuneUp() throws RemoteException {
        return mRadioManagerService.tuneUp();
    }

    public boolean tuneDown() throws RemoteException {
        return mRadioManagerService.tuneDown();
    }

    public boolean seekUp() throws RemoteException {
        return mRadioManagerService.seekUp();
    }

    public void playPresetFreq(int index) throws RemoteException {
        mRadioManagerService.playPresetFreq(index);
    }

    public boolean seekDown() throws RemoteException {
        return mRadioManagerService.seekDown();
    }

    public void bandChange() throws RemoteException {
        mRadioManagerService.bandChange();
    }

    public void setRadioInfoChangeListener(IRadioInfoChangeListener listener) throws RemoteException {
    }

    public void removeRadioInfoChangeListener(IRadioInfoChangeListener listener) throws RemoteException {
    }

    public void setRDSInfoChangeListener(IRDSInfoChangeListener listener) throws RemoteException {
    }

    public void removeRDSInfoChangeListener(IRDSInfoChangeListener listener) throws RemoteException {
    }

    public void initRadioData() throws RemoteException {
        mRadioManagerService.initRadioData();
    }

/*    public void system_btn_down(int code, long downTime, int action) {
        InputManager.getInstance().injectInputEvent(new KeyEvent(downTime, SystemClock.uptimeMillis(), action, code, 0, 0, -1, 0, 72, IPodException.LOAD_ER), 0);
    }
*/
    public void saveAppdata(String filePath) throws Exception {
        mRadioManagerService.saveAppdata(filePath);
    }

    public void sendAppdata(String filePath) throws Exception {
        mRadioManagerService.sendAppdata(filePath);
    }
}
