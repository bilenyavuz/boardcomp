package com.hzbhd.midware.constant;

import android.content.ComponentName;

public class HzbhdComponentName {
    public static final ComponentName AppIconPicker = new ComponentName("com.android.settings", "com.android.settings.hzbhd.AppIconPicker");
    public static final ComponentName AtvMainActivity = new ComponentName("com.hzbhd.misc", "com.hzbhd.misc.atv.MainActivity");
    public static final ComponentName BubbleCall = new ComponentName("com.hzbhd.bluetooth", "com.hzbhd.bluetooth.call.BubbleCall");
    public static final ComponentName CanBusAirInfoCanbusAirService = new ComponentName("com.hzbhd.canbusairinfo", "com.hzbhd.canbusairinfo.CanbusAirService");
    public static final ComponentName CanbusCarInfoMainActivity = new ComponentName("com.hzbhd.canbuscarinfo", "com.hzbhd.canbuscarinfo.MainActivity");
    public static final ComponentName CanbusCarInfoService = new ComponentName("com.hzbhd.canbuscarinfo", "com.hzbhd.canbuscarinfo.CanbusCarInfoService");
    public static final ComponentName CanbusRadarService = new ComponentName("com.hzbhd.misc", "com.hzbhd.misc.CanbusRadarService");
    public static final ComponentName ColorMainActivity = new ComponentName("com.hzbhd.ColorLed", "com.hzbhd.ColorLed.MainActivity");
    public static final ComponentName DTVMainActivity = new ComponentName("com.hzbhd.misc", "com.hzbhd.misc.dtv.DTVMainActivity");
    public static final ComponentName DabMainActivity = new ComponentName("com.ex.dabplayer.pad", "com.ex.dabplayer.pad.activity.MainActivity");
    public static final ComponentName Disc = new ComponentName("com.hzbhd.disc", "com.hzbhd.disc.Disc");
    public static final ComponentName DvrCameraActivity = new ComponentName("com.hzbhd.dvr", "com.hzbhd.dvr.CameraActivity");
    public static final ComponentName DvrMainActivity = new ComponentName("com.hzbhd.misc", "com.hzbhd.misc.dvr.MainActivity");
    public static final ComponentName EqMainActivity = new ComponentName(SettingConstantsDef.EQ_PACKAGE, SettingConstantsDef.EQ_ACTIVITY);
    public static final ComponentName FordEdgeTouchActivity = new ComponentName("com.hzbhd.canbusairinfo", "com.hzbhd.canbusairinfo.FordEdgeTouchActivity");
    public static final ComponentName FordSyncActivity = new ComponentName("com.hzbhd.canbuscarinfo", "com.hzbhd.fordcarinfo.FordSyncActivity");
    public static final ComponentName GmOnStarActivity = new ComponentName("com.hzbhd.canbuscarinfo", "com.hzbhd.gmcarinfo.GmOnStarActivity");
    public static final ComponentName GmTouchActivity = new ComponentName("com.hzbhd.canbusairinfo", "com.hzbhd.canbusairinfo.GmTouchActivity");
    public static final ComponentName GmcOnstarAudioOnlyActivity = new ComponentName("com.hzbhd.canbuscarinfo", "com.hzbhd.gmcarinfo.GmcOnstarAudioOnlyActivity");
    public static final ComponentName GocReceiver = new ComponentName("com.goodocom.gocsdk", "com.goodocom.gocsdk.receiver.GocReceiver");
    public static final ComponentName HondaCarDeviceActivity = new ComponentName("com.hzbhd.canbuscarinfo", "com.hzbhd.hondacarinfo.HondaCarDeviceActivity");
    public static final ComponentName HondaCarRadioActivity = new ComponentName("com.hzbhd.canbuscarinfo", "com.hzbhd.hondacarinfo.HondaCarRadioActivity");
    public static final ComponentName JeepCanbusCDInfoActivity = new ComponentName("com.hzbhd.canbuscarinfo", "com.hzbhd.jeepcarinfo.JeepCanbusCDInfoActivity");
    public static final ComponentName JeepTouchActivity = new ComponentName("com.hzbhd.canbusairinfo", "com.hzbhd.canbusairinfo.JeepTouchActivity");
    public static final ComponentName MediaImage = new ComponentName("com.hzbhd.media", "com.hzbhd.media.Image");
    public static final ComponentName MzdCanbusCDInfoActivity = new ComponentName("com.hzbhd.canbuscarinfo", "com.hzbhd.mzd.mazda3_mazda6_2017.MzdCanbusCDInfoActivity");
    public static final ComponentName PreCameraMainActivity = new ComponentName("com.hzbhd.misc", "com.hzbhd.misc.precamera.MainActivity");
    public static final ComponentName PsaTouchActivity = new ComponentName("com.hzbhd.canbusairinfo", "com.hzbhd.canbusairinfo.PsaTouchActivity");
    public static final ComponentName ScannerRunService = new ComponentName("com.hzbhd.scanner", "com.hzbhd.mediaScanner.service.RunService");
    public static final ComponentName Settings$BodaFactoryTouchLearningStep1Activity = new ComponentName("com.android.settings", "com.android.settings.Settings$BodaFactoryTouchLearningStep1Activity");
    public static final ComponentName SettingsAppWidgetProvider = new ComponentName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
    public static final ComponentName SoundRecorder = new ComponentName("com.android.soundrecorder", "com.android.soundrecorder.SoundRecorder");
    public static final ComponentName SystemuiCanbusAirService = new ComponentName("com.android.systemui", "com.android.systemui.hzbhd.CanbusAirService");
    public static final ComponentName TakeScreenshotService = new ComponentName("com.android.systemui", "com.android.systemui.screenshot.TakeScreenshotService");
    public static final ComponentName Torque = new ComponentName("org.prowl.torque", "org.prowl.torque.landing.FrontPage");
    public static final ComponentName TpmsoroBootCompleteReceiver = new ComponentName("com.hzbhd.tpmsoro", "com.hzbhd.tpmsoro.BootCompleteReceiver");
    public static final ComponentName TpmsoroMainActivity = new ComponentName("com.hzbhd.tpmsoro", "com.hzbhd.tpmsoro.MainActivity");
    public static final ComponentName TpmsoroTpmsService = new ComponentName("com.hzbhd.tpmsoro", "com.hzbhd.tpmsoro.TpmsService");
    public static final ComponentName UPDProgressActivity = new ComponentName("com.hzbhd.upgrade", "com.hzbhd.upgrade.UPDProgressActivity");
    public static final ComponentName VolumeService = new ComponentName("com.hzbhd.misc", "com.hzbhd.misc.volume.VolumeService");
}
