package com.hzbhd.midware.constant;

public interface CalibrationConstantsDef {
    public static final String ACTION_CALIBRATE_REAULT = "com.hzbhd.calibrator.result";
    public static final String ACTION_DRAW_POINTER = "com.hzbhd.calibrator.drawPointer";
    public static final String EXTRA_RESULT = "result";
    public static final String EXTRA_X = "x";
    public static final String EXTRA_Y = "y";
}
