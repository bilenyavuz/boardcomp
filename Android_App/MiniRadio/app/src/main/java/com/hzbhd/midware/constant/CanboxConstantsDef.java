package com.hzbhd.midware.constant;

//import com.autochips.dvp.MultiMediaConstant;
import nfore.android.bt.hsp.HspErrorInfo;
import nfore.android.bt.res.NfDef;

public interface CanboxConstantsDef {
    public static final int[] CANBOX_COMID = {18, 19, 20, 21, 71, HotKeyConstant.K_SLEEP, 104, 22, 30, HspErrorInfo.HSP_ERROR_CONNECTION_ALREADY_EXIST, 133, 99, 100, 69, 70, 193, 31, NfDef.STATE_3WAY_M_HOLD};
    public static final int CANBOX_FILE_TYPE_AUDIO = 1;
    public static final int CANBOX_FILE_TYPE_PHOTO = 3;
    public static final int CANBOX_FILE_TYPE_UNKOWN = 0;
    public static final int CANBOX_FILE_TYPE_VIDEO = 2;
    public static final int CANBOX_MEDIA_FB = 5;
    public static final int CANBOX_MEDIA_FF = 4;
    public static final int CANBOX_MEDIA_PAUSE = 2;
    public static final int CANBOX_MEDIA_PLAYING = 1;
    public static final int CANBOX_MEDIA_READING = 0;
    public static final int CANBOX_MEDIA_STOP = 3;

    public enum CANBOX_PARSER_TYPE {
        mACParser,
        mCarBodyDetailsParser,
        mConsumptionParser,
        mDriverAssistanceSystemsParser,
        mDrivingModeSelectionParser,
        mLamplightParser,
        mLoadConsumptionParser,
        mMaintenanceParser,
        mMirrorAndWiperParser,
        mMotionModeParser,
        mMutiDisplayParser,
        mOpenAndCloseParser,
        mParkAndShuntParser,
        mTyreParser,
        mUnitParser,
        mVehicleIdentificationNumberParser,
        mVersionParser,
        mDateAndTimeParser
    }

    public interface CanboxCallBackListenr {
        void refreshSettingsInfo(CANBOX_PARSER_TYPE canbox_parser_type);

        void retAcInfo(int i, int[] iArr, boolean z);

        void retRadarInfo(int i, int[] iArr);
    }
}
