package com.hzbhd.midwareproxy.serviceConnection;

import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.util.Log;

//import com.hzbhd.midwareproxy.utils.Log;

public class ServiceConnection {
    private static final String TAG = "ServiceOperation";
    /* access modifiers changed from: private */
    public ServiceConnectionManager mConnectionManager;
    protected Boolean mIsConnected;
    protected ServiceConnection mServiceConnection;

    public class BinderDeathRecipient implements DeathRecipient {
        public BinderDeathRecipient() {
        }

        public void binderDied() {
            Log.e(ServiceConnection.TAG, "ServiceConnection BinderDeathRecipient: binderDied");
            ServiceConnection.this.mIsConnected = Boolean.valueOf(false);
            ServiceConnection.this.serviceDied();
            ServiceConnection.this.mConnectionManager.onServiceDisconnected(ServiceConnection.this.getServiceName());
        }
    }

    public ServiceConnection() {
        this.mServiceConnection = null;
        this.mIsConnected = Boolean.valueOf(true);
        this.mConnectionManager = null;
        this.mConnectionManager = ServiceConnectionManager.getServiceConnectionManager();
        this.mConnectionManager.registerConnectionListener(getServiceName(), this);
    }

    public String getServiceName() {
        return null;
    }

    public IBinder connectService() {
        Object object = new Object();
        IBinder serviceObj = null;
        try {
            serviceObj = (IBinder) Class.forName("android.os.ServiceManager").getMethod("getService", new Class[]{String.class}).invoke(object, new Object[]{getServiceName()});
            if (serviceObj != null) {
                this.mIsConnected = Boolean.valueOf(true);
                serviceObj.linkToDeath(new BinderDeathRecipient(), 0);
            } else {
                this.mIsConnected = Boolean.valueOf(false);
                this.mConnectionManager.onServiceDisconnected(getServiceName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (serviceObj != null) {
            this.mIsConnected = Boolean.valueOf(true);
        } else {
            this.mIsConnected = Boolean.valueOf(false);
        }
        return serviceObj;
    }

    public boolean getServiceConnection() {
        return false;
    }

    public boolean isServiceConnected() {
        return this.mIsConnected.booleanValue();
    }

    public void serviceReConnected() {
    }

    public void serviceDied() {
    }
}
