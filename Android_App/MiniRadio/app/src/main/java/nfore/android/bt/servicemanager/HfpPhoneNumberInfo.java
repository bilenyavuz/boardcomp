package nfore.android.bt.servicemanager;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class HfpPhoneNumberInfo implements Parcelable {
    public static final Creator<HfpPhoneNumberInfo> CREATOR = new Creator<HfpPhoneNumberInfo>() {
        public HfpPhoneNumberInfo createFromParcel(Parcel source) {
            return new HfpPhoneNumberInfo(source);
        }

        public HfpPhoneNumberInfo[] newArray(int size) {
            return new HfpPhoneNumberInfo[size];
        }
    };
    private int dir;
    private int idx;
    private int mode;
    private int mpty;
    private String number;
    private int status;
    private int type;

    public HfpPhoneNumberInfo() {
    }

    public HfpPhoneNumberInfo(Parcel parcel) {
        readFromParcel(parcel);
    }

    public void readFromParcel(Parcel parcel) {
        this.idx = parcel.readInt();
        this.dir = parcel.readInt();
        this.status = parcel.readInt();
        this.mode = parcel.readInt();
        this.mpty = parcel.readInt();
        this.type = parcel.readInt();
        this.number = parcel.readString();
    }

    public int getIdx() {
        return this.idx;
    }

    public void setIdx(int idx2) {
        this.idx = idx2;
    }

    public int getDir() {
        return this.dir;
    }

    public void setDir(int dir2) {
        this.dir = dir2;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status2) {
        this.status = status2;
    }

    public int getMode() {
        return this.mode;
    }

    public void setMode(int mode2) {
        this.mode = mode2;
    }

    public int getMpty() {
        return this.mpty;
    }

    public void setMpty(int mpty2) {
        this.mpty = mpty2;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    public String getNumber() {
        return this.number;
    }

    public void setNumber(String number2) {
        this.number = number2;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int arg1) {
        parcel.writeInt(this.idx);
        parcel.writeInt(this.dir);
        parcel.writeInt(this.status);
        parcel.writeInt(this.mode);
        parcel.writeInt(this.mpty);
        parcel.writeInt(this.type);
        parcel.writeString(this.number);
    }

    public HfpPhoneNumberInfo clone() {
        HfpPhoneNumberInfo hpni = new HfpPhoneNumberInfo();
        hpni.setIdx(this.idx);
        hpni.setDir(this.dir);
        hpni.setStatus(this.status);
        hpni.setMode(this.mode);
        hpni.setMpty(this.mpty);
        hpni.setType(this.type);
        hpni.setNumber(this.number);
        return hpni;
    }
}
