package nfore.android.bt.servicemanager;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface INfServiceCommand extends IInterface {

    public static abstract class Stub extends Binder implements INfServiceCommand {
        private static final String DESCRIPTOR = "nfore.android.bt.servicemanager.INfServiceCommand";
        static final int TRANSACTION_getA2dpConnectedDeviceAddress = 104;
        static final int TRANSACTION_getA2dpCurrentState = 105;
        static final int TRANSACTION_getAvrcpConnectedDeviceAddress = 109;
        static final int TRANSACTION_getAvrcpCurrentState = 106;
        static final int TRANSACTION_getBluetoothCurrentState = 17;
        static final int TRANSACTION_getBluetoothLocalAdapterAddress = 14;
        static final int TRANSACTION_getBluetoothLocalAdapterName = 12;
        static final int TRANSACTION_getBluetoothRemoteDeviceName = 13;
        static final int TRANSACTION_getHfpConnectedDeviceAddress = 30;
        static final int TRANSACTION_getHfpCurrentScoState = 32;
        static final int TRANSACTION_getHfpCurrentState = 31;
        static final int TRANSACTION_getHfpRemoteDeviceBatteryStatus = 51;
        static final int TRANSACTION_getHfpRemoteDeviceName = 45;
        static final int TRANSACTION_getHfpRemoteDeviceSignalStatus = 52;
        static final int TRANSACTION_getHidCurrentState = 165;
        static final int TRANSACTION_getHspConnectedDeviceAddress = 62;
        static final int TRANSACTION_getHspCurrentScoState = 64;
        static final int TRANSACTION_getHspCurrentState = 63;
        static final int TRANSACTION_getMapCurrentState = 98;
        static final int TRANSACTION_getMapRegisterState = 99;
        static final int TRANSACTION_getPanConnectedDeviceAddress = 157;
        static final int TRANSACTION_getPanCurrentState = 158;
        static final int TRANSACTION_getPbapCurrentState = 86;
        static final int TRANSACTION_isA2dpNativeRunning = 152;
        static final int TRANSACTION_isAvrcp13Supported = 110;
        static final int TRANSACTION_isAvrcp14Supported = 111;
        static final int TRANSACTION_isAvrcpBrowsingChannelEstablished = 148;
        static final int TRANSACTION_isBluetoothAutoReconnectEnable = 22;
        static final int TRANSACTION_isBluetoothDiscoverable = 19;
        static final int TRANSACTION_isBluetoothEnable = 16;
        static final int TRANSACTION_isBluetoothScanning = 18;
        static final int TRANSACTION_isHfpRemoteDeviceOnRoaming = 50;
        static final int TRANSACTION_isHfpRemoteDeviceTelecomServiceOn = 49;
        static final int TRANSACTION_isHfpVoiceControlOn = 33;
        static final int TRANSACTION_isMapNotificationRegistered = 93;
        static final int TRANSACTION_isSppConnected = 76;
        static final int TRANSACTION_registerA2dpAvrcpCallback = 100;
        static final int TRANSACTION_registerHfpCallback = 26;
        static final int TRANSACTION_registerHidCallback = 159;
        static final int TRANSACTION_registerHspCallback = 58;
        static final int TRANSACTION_registerMapCallback = 87;
        static final int TRANSACTION_registerNforeServiceCallback = 1;
        static final int TRANSACTION_registerPanCallback = 153;
        static final int TRANSACTION_registerPbapCallback = 78;
        static final int TRANSACTION_registerSppCallback = 71;
        static final int TRANSACTION_reqA2dpConnect = 102;
        static final int TRANSACTION_reqA2dpDisconnect = 103;
        static final int TRANSACTION_reqAvrcpAbortMetadataReceiving = 132;
        static final int TRANSACTION_reqAvrcpAddToNowPlaying = 147;
        static final int TRANSACTION_reqAvrcpBackward = 116;
        static final int TRANSACTION_reqAvrcpChangePath = 143;
        static final int TRANSACTION_reqAvrcpForward = 115;
        static final int TRANSACTION_reqAvrcpGetCapabilitiesSupportEvent = 123;
        static final int TRANSACTION_reqAvrcpGetElementAttributesPlaying = 128;
        static final int TRANSACTION_reqAvrcpGetFolderItems = 142;
        static final int TRANSACTION_reqAvrcpGetItemAttributes = 144;
        static final int TRANSACTION_reqAvrcpGetPlayStatus = 129;
        static final int TRANSACTION_reqAvrcpGetPlayerSettingAttributeText = 136;
        static final int TRANSACTION_reqAvrcpGetPlayerSettingAttributesList = 124;
        static final int TRANSACTION_reqAvrcpGetPlayerSettingCurrentValues = 126;
        static final int TRANSACTION_reqAvrcpGetPlayerSettingValueText = 137;
        static final int TRANSACTION_reqAvrcpGetPlayerSettingValuesList = 125;
        static final int TRANSACTION_reqAvrcpInformBatteryStatusOfCt = 139;
        static final int TRANSACTION_reqAvrcpInformDisplayableCharset = 138;
        static final int TRANSACTION_reqAvrcpNextGroup = 133;
        static final int TRANSACTION_reqAvrcpPause = 114;
        static final int TRANSACTION_reqAvrcpPlay = 112;
        static final int TRANSACTION_reqAvrcpPlaySelectedItem = 145;
        static final int TRANSACTION_reqAvrcpPreviousGroup = 134;
        static final int TRANSACTION_reqAvrcpRegisterEventWatcher = 130;
        static final int TRANSACTION_reqAvrcpSearch = 146;
        static final int TRANSACTION_reqAvrcpSetAbsoluteVolume = 149;
        static final int TRANSACTION_reqAvrcpSetAddressedPlayer = 140;
        static final int TRANSACTION_reqAvrcpSetBrowsedPlayer = 141;
        static final int TRANSACTION_reqAvrcpSetMetadataCompanyId = 135;
        static final int TRANSACTION_reqAvrcpSetPlayerSettingValue = 127;
        static final int TRANSACTION_reqAvrcpStartFastForward = 119;
        static final int TRANSACTION_reqAvrcpStartRewind = 121;
        static final int TRANSACTION_reqAvrcpStop = 113;
        static final int TRANSACTION_reqAvrcpStopFastForward = 120;
        static final int TRANSACTION_reqAvrcpStopRewind = 122;
        static final int TRANSACTION_reqAvrcpUnregisterEventWatcher = 131;
        static final int TRANSACTION_reqAvrcpVolumeDown = 118;
        static final int TRANSACTION_reqAvrcpVolumeUp = 117;
        static final int TRANSACTION_reqBluetoothConnectAll = 23;
        static final int TRANSACTION_reqBluetoothDisconnectAll = 24;
        static final int TRANSACTION_reqBluetoothPair = 7;
        static final int TRANSACTION_reqBluetoothPairedDevices = 11;
        static final int TRANSACTION_reqBluetoothRemoteUuids = 25;
        static final int TRANSACTION_reqBluetoothScanning = 6;
        static final int TRANSACTION_reqBluetoothUnpair = 8;
        static final int TRANSACTION_reqBluetoothUnpairAll = 9;
        static final int TRANSACTION_reqBluetoothUnpairAllNextTime = 10;
        static final int TRANSACTION_reqHfpAnswerCall = 37;
        static final int TRANSACTION_reqHfpAtDownloadPhonebook = 54;
        static final int TRANSACTION_reqHfpAtDownloadSMS = 55;
        static final int TRANSACTION_reqHfpAudioTransfer = 42;
        static final int TRANSACTION_reqHfpConnect = 28;
        static final int TRANSACTION_reqHfpDialCall = 34;
        static final int TRANSACTION_reqHfpDisconnect = 29;
        static final int TRANSACTION_reqHfpMemoryDial = 36;
        static final int TRANSACTION_reqHfpMultiCallControl = 40;
        static final int TRANSACTION_reqHfpReDial = 35;
        static final int TRANSACTION_reqHfpRejectIncomingCall = 38;
        static final int TRANSACTION_reqHfpRemoteDeviceInfo = 44;
        static final int TRANSACTION_reqHfpRemoteDeviceNetworkOperator = 47;
        static final int TRANSACTION_reqHfpRemoteDevicePhoneNumber = 46;
        static final int TRANSACTION_reqHfpRemoteDeviceSubscriberNumber = 48;
        static final int TRANSACTION_reqHfpRemoteDeviceVolumeSync = 56;
        static final int TRANSACTION_reqHfpSendDtmf = 41;
        static final int TRANSACTION_reqHfpTerminateOngoingCall = 39;
        static final int TRANSACTION_reqHfpVoiceDial = 57;
        static final int TRANSACTION_reqHidConnect = 161;
        static final int TRANSACTION_reqHidDisconnect = 162;
        static final int TRANSACTION_reqHspAnswerCall = 66;
        static final int TRANSACTION_reqHspAudioTransfer = 69;
        static final int TRANSACTION_reqHspConnect = 60;
        static final int TRANSACTION_reqHspDisconnect = 61;
        static final int TRANSACTION_reqHspReDial = 65;
        static final int TRANSACTION_reqHspRejectIncomingCall = 67;
        static final int TRANSACTION_reqHspRemoteDeviceVolumeSync = 70;
        static final int TRANSACTION_reqHspTerminateOngoingCall = 68;
        static final int TRANSACTION_reqMapCleanDatabase = 97;
        static final int TRANSACTION_reqMapDatabaseAvailable = 95;
        static final int TRANSACTION_reqMapDeleteDatabaseByAddress = 96;
        static final int TRANSACTION_reqMapDownloadAllMessages = 89;
        static final int TRANSACTION_reqMapDownloadInterrupt = 94;
        static final int TRANSACTION_reqMapDownloadSingleMessage = 90;
        static final int TRANSACTION_reqMapRegisterNotification = 91;
        static final int TRANSACTION_reqMapUnregisterNotification = 92;
        static final int TRANSACTION_reqPanConnect = 155;
        static final int TRANSACTION_reqPanDisconnect = 156;
        static final int TRANSACTION_reqPbapCleanDatabase = 84;
        static final int TRANSACTION_reqPbapDatabaseAvailable = 81;
        static final int TRANSACTION_reqPbapDeleteDatabaseByAddress = 83;
        static final int TRANSACTION_reqPbapDownloadAllVcards = 80;
        static final int TRANSACTION_reqPbapDownloadInterrupt = 85;
        static final int TRANSACTION_reqPbapQueryName = 82;
        static final int TRANSACTION_reqSppConnect = 73;
        static final int TRANSACTION_reqSppConnectedDeviceAddressList = 75;
        static final int TRANSACTION_reqSppDisconnect = 74;
        static final int TRANSACTION_reqSppSendData = 77;
        static final int TRANSACTION_sendHidMouseCommand = 163;
        static final int TRANSACTION_sendHidVirtualKeyCommand = 164;
        static final int TRANSACTION_setA2dpLocalMute = 108;
        static final int TRANSACTION_setA2dpLocalVolume = 107;
        static final int TRANSACTION_setA2dpNativeStart = 150;
        static final int TRANSACTION_setA2dpNativeStop = 151;
        static final int TRANSACTION_setBluetoothAutoReconnect = 20;
        static final int TRANSACTION_setBluetoothAutoReconnectForceOor = 21;
        static final int TRANSACTION_setBluetoothDiscoverableDuration = 4;
        static final int TRANSACTION_setBluetoothDiscoverablePermanent = 5;
        static final int TRANSACTION_setBluetoothEnable = 3;
        static final int TRANSACTION_setBluetoothLocalAdapterName = 15;
        static final int TRANSACTION_setHfpAtNewSmsNotify = 53;
        static final int TRANSACTION_setHfpLocalRingEnable = 43;
        static final int TRANSACTION_unregisterA2dpAvrcpCallback = 101;
        static final int TRANSACTION_unregisterHfpCallback = 27;
        static final int TRANSACTION_unregisterHidCallback = 160;
        static final int TRANSACTION_unregisterHspCallback = 59;
        static final int TRANSACTION_unregisterMapCallback = 88;
        static final int TRANSACTION_unregisterNforeServiceCallback = 2;
        static final int TRANSACTION_unregisterPanCallback = 154;
        static final int TRANSACTION_unregisterPbapCallback = 79;
        static final int TRANSACTION_unregisterSppCallback = 72;

        private static class Proxy implements INfServiceCommand {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            public boolean registerNforeServiceCallback(IServiceCallback cb) throws RemoteException {
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean unregisterNforeServiceCallback(IServiceCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setBluetoothEnable(boolean isEnable) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (isEnable) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(3, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setBluetoothDiscoverableDuration(int duration) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(duration);
                    this.mRemote.transact(4, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setBluetoothDiscoverablePermanent(boolean isEnable) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (isEnable) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(5, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqBluetoothScanning(boolean isEnable) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (isEnable) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(6, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqBluetoothPair(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(7, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqBluetoothUnpair(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(8, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqBluetoothUnpairAll() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(9, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqBluetoothUnpairAllNextTime() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(10, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqBluetoothPairedDevices() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(11, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getBluetoothLocalAdapterName() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(12, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getBluetoothRemoteDeviceName(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(13, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getBluetoothLocalAdapterAddress() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(14, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setBluetoothLocalAdapterName(String name) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(name);
                    this.mRemote.transact(15, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isBluetoothEnable() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(16, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getBluetoothCurrentState() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(17, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isBluetoothScanning() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(18, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isBluetoothDiscoverable() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(19, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setBluetoothAutoReconnect(boolean isEnable) throws RemoteException {
                int i = 0;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (isEnable) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(20, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setBluetoothAutoReconnectForceOor(boolean isEnable) throws RemoteException {
                int i = 0;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (isEnable) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(21, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isBluetoothAutoReconnectEnable() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(22, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int reqBluetoothConnectAll(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(23, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int reqBluetoothDisconnectAll() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(24, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int reqBluetoothRemoteUuids(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(25, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean registerHfpCallback(IHfpCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(26, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean unregisterHfpCallback(IHfpCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(27, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpConnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(28, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpDisconnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(29, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getHfpConnectedDeviceAddress() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(30, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getHfpCurrentState(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(31, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getHfpCurrentScoState(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(32, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isHfpVoiceControlOn(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(33, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpDialCall(String address, String callNumber) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(callNumber);
                    this.mRemote.transact(34, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpReDial(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(35, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpMemoryDial(String address, String memoryLocation) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(memoryLocation);
                    this.mRemote.transact(36, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpAnswerCall(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(37, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpRejectIncomingCall(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(38, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpTerminateOngoingCall(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(39, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpMultiCallControl(String address, byte opCode) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(opCode);
                    this.mRemote.transact(40, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpSendDtmf(String address, String dtmfNumber) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(dtmfNumber);
                    this.mRemote.transact(41, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpAudioTransfer(String address, byte opCode) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(opCode);
                    this.mRemote.transact(42, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setHfpLocalRingEnable(boolean isEnable) throws RemoteException {
                int i = 0;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (isEnable) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(43, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void reqHfpRemoteDeviceInfo(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(44, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getHfpRemoteDeviceName(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(45, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpRemoteDevicePhoneNumber(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(46, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpRemoteDeviceNetworkOperator(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(47, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpRemoteDeviceSubscriberNumber(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(48, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isHfpRemoteDeviceTelecomServiceOn(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(49, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isHfpRemoteDeviceOnRoaming(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(50, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getHfpRemoteDeviceBatteryStatus(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(51, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getHfpRemoteDeviceSignalStatus(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(52, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setHfpAtNewSmsNotify(String address, boolean isEnable) throws RemoteException {
                int i = 0;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    if (isEnable) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(53, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpAtDownloadPhonebook(String address, int storage) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(storage);
                    this.mRemote.transact(54, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpAtDownloadSMS(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(55, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpRemoteDeviceVolumeSync(String address, byte attributeId, int volume) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(attributeId);
                    _data.writeInt(volume);
                    this.mRemote.transact(56, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHfpVoiceDial(String address, boolean isEnable) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    if (isEnable) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(57, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean registerHspCallback(IHspCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(58, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean unregisterHspCallback(IHspCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(59, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHspConnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(60, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHspDisconnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(61, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getHspConnectedDeviceAddress() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(62, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getHspCurrentState(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(63, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getHspCurrentScoState(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(64, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHspReDial(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(65, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHspAnswerCall(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(66, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHspRejectIncomingCall(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(67, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHspTerminateOngoingCall(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(68, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHspAudioTransfer(String address, byte opCode) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(opCode);
                    this.mRemote.transact(69, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHspRemoteDeviceVolumeSync(String address, byte attributeId, int volume) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(attributeId);
                    _data.writeInt(volume);
                    this.mRemote.transact(70, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean registerSppCallback(ISppCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(71, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean unregisterSppCallback(ISppCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(72, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqSppConnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(73, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqSppDisconnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(74, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void reqSppConnectedDeviceAddressList() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(75, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isSppConnected(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(76, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void reqSppSendData(String address, byte[] sppData) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByteArray(sppData);
                    this.mRemote.transact(Stub.TRANSACTION_reqSppSendData, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean registerPbapCallback(IPbapCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(Stub.TRANSACTION_registerPbapCallback, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean unregisterPbapCallback(IPbapCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(Stub.TRANSACTION_unregisterPbapCallback, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqPbapDownloadAllVcards(String address, int storage, int notifyFreq, boolean isUseContactsProvider) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(storage);
                    _data.writeInt(notifyFreq);
                    if (isUseContactsProvider) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(80, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void reqPbapDatabaseAvailable(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(81, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void reqPbapQueryName(String address, String phoneNumber) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(phoneNumber);
                    this.mRemote.transact(Stub.TRANSACTION_reqPbapQueryName, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void reqPbapDeleteDatabaseByAddress(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(Stub.TRANSACTION_reqPbapDeleteDatabaseByAddress, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void reqPbapCleanDatabase() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_reqPbapCleanDatabase, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqPbapDownloadInterrupt(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(Stub.TRANSACTION_reqPbapDownloadInterrupt, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getPbapCurrentState(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(Stub.TRANSACTION_getPbapCurrentState, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean registerMapCallback(IMapCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(Stub.TRANSACTION_registerMapCallback, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean unregisterMapCallback(IMapCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(Stub.TRANSACTION_unregisterMapCallback, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqMapDownloadAllMessages(String address, int folder, int notifyFreq, boolean isContentDownload) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(folder);
                    _data.writeInt(notifyFreq);
                    if (isContentDownload) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(Stub.TRANSACTION_reqMapDownloadAllMessages, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqMapDownloadSingleMessage(String address, int folder, String handle) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(folder);
                    _data.writeString(handle);
                    this.mRemote.transact(90, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqMapRegisterNotification(String address, boolean downloadNewMessage) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    if (downloadNewMessage) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(91, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void reqMapUnregisterNotification(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(92, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isMapNotificationRegistered(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(Stub.TRANSACTION_isMapNotificationRegistered, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqMapDownloadInterrupt(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(Stub.TRANSACTION_reqMapDownloadInterrupt, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void reqMapDatabaseAvailable(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(95, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void reqMapDeleteDatabaseByAddress(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(96, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void reqMapCleanDatabase() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(97, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getMapCurrentState(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(98, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getMapRegisterState(String address) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(99, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean registerA2dpAvrcpCallback(IA2dpCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(100, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean unregisterA2dpAvrcpCallback(IA2dpCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(101, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqA2dpConnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(102, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqA2dpDisconnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(103, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getA2dpConnectedDeviceAddress() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(104, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getA2dpCurrentState() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(105, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getAvrcpCurrentState() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(106, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setA2dpLocalVolume(int vol) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(vol);
                    this.mRemote.transact(107, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean setA2dpLocalMute(boolean isMute) throws RemoteException {
                int i;
                boolean _result = true;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (isMute) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(108, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() == 0) {
                        _result = false;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getAvrcpConnectedDeviceAddress() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(109, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isAvrcp13Supported(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(110, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isAvrcp14Supported(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(111, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpPlay(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(112, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpStop(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(113, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpPause(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(114, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpForward(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(115, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpBackward(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(116, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpVolumeUp(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(117, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpVolumeDown(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(118, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpStartFastForward(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(119, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpStopFastForward(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(120, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpStartRewind(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(121, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpStopRewind(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(122, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpGetCapabilitiesSupportEvent(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(123, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpGetPlayerSettingAttributesList(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(124, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpGetPlayerSettingValuesList(String address, byte attributeId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(attributeId);
                    this.mRemote.transact(125, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpGetPlayerSettingCurrentValues(String address, byte attributeId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(attributeId);
                    this.mRemote.transact(126, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpSetPlayerSettingValue(String address, byte attributeId, byte valueId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(attributeId);
                    _data.writeByte(valueId);
                    this.mRemote.transact(127, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpGetElementAttributesPlaying(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(128, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpGetPlayStatus(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(129, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpRegisterEventWatcher(String address, byte eventId, long interval) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(eventId);
                    _data.writeLong(interval);
                    this.mRemote.transact(130, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpUnregisterEventWatcher(String address, byte eventId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(eventId);
                    this.mRemote.transact(131, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpAbortMetadataReceiving(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(132, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpNextGroup(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(133, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpPreviousGroup(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(134, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpSetMetadataCompanyId(String address, int companyId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(companyId);
                    this.mRemote.transact(135, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpGetPlayerSettingAttributeText(String address, byte attributeId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(attributeId);
                    this.mRemote.transact(136, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpGetPlayerSettingValueText(String address, byte attributeId, byte valueId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(attributeId);
                    _data.writeByte(valueId);
                    this.mRemote.transact(137, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpInformDisplayableCharset(String address, int[] charsetId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeIntArray(charsetId);
                    this.mRemote.transact(138, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpInformBatteryStatusOfCt(String address, byte batteryStatusId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(batteryStatusId);
                    this.mRemote.transact(139, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpSetAddressedPlayer(String address, int playerId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(playerId);
                    this.mRemote.transact(140, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpSetBrowsedPlayer(String address, int playerId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(playerId);
                    this.mRemote.transact(141, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpGetFolderItems(String address, byte scopeId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(scopeId);
                    this.mRemote.transact(142, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpChangePath(String address, int uidCounter, long uid, byte direction) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeInt(uidCounter);
                    _data.writeLong(uid);
                    _data.writeByte(direction);
                    this.mRemote.transact(143, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpGetItemAttributes(String address, byte scopeId, int uidCounter, long uid) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(scopeId);
                    _data.writeInt(uidCounter);
                    _data.writeLong(uid);
                    this.mRemote.transact(144, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpPlaySelectedItem(String address, byte scopeId, int uidCounter, long uid) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(scopeId);
                    _data.writeInt(uidCounter);
                    _data.writeLong(uid);
                    this.mRemote.transact(145, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpSearch(String address, String text) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeString(text);
                    this.mRemote.transact(146, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpAddToNowPlaying(String address, byte scopeId, int uidCounter, long uid) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(scopeId);
                    _data.writeInt(uidCounter);
                    _data.writeLong(uid);
                    this.mRemote.transact(147, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isAvrcpBrowsingChannelEstablished(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(148, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqAvrcpSetAbsoluteVolume(String address, byte volume) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    _data.writeByte(volume);
                    this.mRemote.transact(149, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setA2dpNativeStart() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(150, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setA2dpNativeStop() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(151, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isA2dpNativeRunning() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_isA2dpNativeRunning, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean registerPanCallback(IPanCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(153, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean unregisterPanCallback(IPanCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(154, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqPanConnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(155, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqPanDisconnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(Stub.TRANSACTION_reqPanDisconnect, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public String getPanConnectedDeviceAddress() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(157, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getPanCurrentState() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(158, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean registerHidCallback(IHidCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(159, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean unregisterHidCallback(IHidCallback cb) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(cb != null ? cb.asBinder() : null);
                    this.mRemote.transact(160, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHidConnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(161, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean reqHidDisconnect(String address) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(162, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int sendHidMouseCommand(int button, int offset_x, int offset_y, int wheel) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(button);
                    _data.writeInt(offset_x);
                    _data.writeInt(offset_y);
                    _data.writeInt(wheel);
                    this.mRemote.transact(163, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int sendHidVirtualKeyCommand(int key_1, int key_2) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(key_1);
                    _data.writeInt(key_2);
                    this.mRemote.transact(164, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getHidCurrentState() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(165, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static INfServiceCommand asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
            if (iin == null || !(iin instanceof INfServiceCommand)) {
                return new Proxy(obj);
            }
            return (INfServiceCommand) iin;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result = registerNforeServiceCallback(nfore.android.bt.servicemanager.IServiceCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result ? 1 : 0);
                    return true;
                case 2:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result2 = unregisterNforeServiceCallback(nfore.android.bt.servicemanager.IServiceCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result2 ? 1 : 0);
                    return true;
                case 3:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result3 = setBluetoothEnable(data.readInt() != 0);
                    reply.writeNoException();
                    reply.writeInt(_result3 ? 1 : 0);
                    return true;
                case 4:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result4 = setBluetoothDiscoverableDuration(data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result4 ? 1 : 0);
                    return true;
                case 5:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result5 = setBluetoothDiscoverablePermanent(data.readInt() != 0);
                    reply.writeNoException();
                    reply.writeInt(_result5 ? 1 : 0);
                    return true;
                case 6:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result6 = reqBluetoothScanning(data.readInt() != 0);
                    reply.writeNoException();
                    reply.writeInt(_result6 ? 1 : 0);
                    return true;
                case 7:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result7 = reqBluetoothPair(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result7 ? 1 : 0);
                    return true;
                case 8:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result8 = reqBluetoothUnpair(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result8 ? 1 : 0);
                    return true;
                case 9:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result9 = reqBluetoothUnpairAll();
                    reply.writeNoException();
                    reply.writeInt(_result9 ? 1 : 0);
                    return true;
                case 10:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result10 = reqBluetoothUnpairAllNextTime();
                    reply.writeNoException();
                    reply.writeInt(_result10 ? 1 : 0);
                    return true;
                case 11:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result11 = reqBluetoothPairedDevices();
                    reply.writeNoException();
                    reply.writeInt(_result11 ? 1 : 0);
                    return true;
                case 12:
                    data.enforceInterface(DESCRIPTOR);
                    String _result12 = getBluetoothLocalAdapterName();
                    reply.writeNoException();
                    reply.writeString(_result12);
                    return true;
                case 13:
                    data.enforceInterface(DESCRIPTOR);
                    String _result13 = getBluetoothRemoteDeviceName(data.readString());
                    reply.writeNoException();
                    reply.writeString(_result13);
                    return true;
                case 14:
                    data.enforceInterface(DESCRIPTOR);
                    String _result14 = getBluetoothLocalAdapterAddress();
                    reply.writeNoException();
                    reply.writeString(_result14);
                    return true;
                case 15:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result15 = setBluetoothLocalAdapterName(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result15 ? 1 : 0);
                    return true;
                case 16:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result16 = isBluetoothEnable();
                    reply.writeNoException();
                    reply.writeInt(_result16 ? 1 : 0);
                    return true;
                case 17:
                    data.enforceInterface(DESCRIPTOR);
                    int _result17 = getBluetoothCurrentState();
                    reply.writeNoException();
                    reply.writeInt(_result17);
                    return true;
                case 18:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result18 = isBluetoothScanning();
                    reply.writeNoException();
                    reply.writeInt(_result18 ? 1 : 0);
                    return true;
                case 19:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result19 = isBluetoothDiscoverable();
                    reply.writeNoException();
                    reply.writeInt(_result19 ? 1 : 0);
                    return true;
                case 20:
                    data.enforceInterface(DESCRIPTOR);
                    setBluetoothAutoReconnect(data.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case 21:
                    data.enforceInterface(DESCRIPTOR);
                    setBluetoothAutoReconnectForceOor(data.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case 22:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result20 = isBluetoothAutoReconnectEnable();
                    reply.writeNoException();
                    reply.writeInt(_result20 ? 1 : 0);
                    return true;
                case 23:
                    data.enforceInterface(DESCRIPTOR);
                    int _result21 = reqBluetoothConnectAll(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result21);
                    return true;
                case 24:
                    data.enforceInterface(DESCRIPTOR);
                    int _result22 = reqBluetoothDisconnectAll();
                    reply.writeNoException();
                    reply.writeInt(_result22);
                    return true;
                case 25:
                    data.enforceInterface(DESCRIPTOR);
                    int _result23 = reqBluetoothRemoteUuids(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result23);
                    return true;
                case 26:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result24 = registerHfpCallback(nfore.android.bt.servicemanager.IHfpCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result24 ? 1 : 0);
                    return true;
                case 27:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result25 = unregisterHfpCallback(nfore.android.bt.servicemanager.IHfpCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result25 ? 1 : 0);
                    return true;
                case 28:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result26 = reqHfpConnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result26 ? 1 : 0);
                    return true;
                case 29:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result27 = reqHfpDisconnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result27 ? 1 : 0);
                    return true;
                case 30:
                    data.enforceInterface(DESCRIPTOR);
                    String _result28 = getHfpConnectedDeviceAddress();
                    reply.writeNoException();
                    reply.writeString(_result28);
                    return true;
                case 31:
                    data.enforceInterface(DESCRIPTOR);
                    int _result29 = getHfpCurrentState(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result29);
                    return true;
                case 32:
                    data.enforceInterface(DESCRIPTOR);
                    int _result30 = getHfpCurrentScoState(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result30);
                    return true;
                case 33:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result31 = isHfpVoiceControlOn(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result31 ? 1 : 0);
                    return true;
                case 34:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result32 = reqHfpDialCall(data.readString(), data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result32 ? 1 : 0);
                    return true;
                case 35:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result33 = reqHfpReDial(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result33 ? 1 : 0);
                    return true;
                case 36:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result34 = reqHfpMemoryDial(data.readString(), data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result34 ? 1 : 0);
                    return true;
                case 37:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result35 = reqHfpAnswerCall(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result35 ? 1 : 0);
                    return true;
                case 38:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result36 = reqHfpRejectIncomingCall(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result36 ? 1 : 0);
                    return true;
                case 39:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result37 = reqHfpTerminateOngoingCall(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result37 ? 1 : 0);
                    return true;
                case 40:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result38 = reqHfpMultiCallControl(data.readString(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result38 ? 1 : 0);
                    return true;
                case 41:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result39 = reqHfpSendDtmf(data.readString(), data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result39 ? 1 : 0);
                    return true;
                case 42:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result40 = reqHfpAudioTransfer(data.readString(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result40 ? 1 : 0);
                    return true;
                case 43:
                    data.enforceInterface(DESCRIPTOR);
                    setHfpLocalRingEnable(data.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case 44:
                    data.enforceInterface(DESCRIPTOR);
                    reqHfpRemoteDeviceInfo(data.readString());
                    reply.writeNoException();
                    return true;
                case 45:
                    data.enforceInterface(DESCRIPTOR);
                    String _result41 = getHfpRemoteDeviceName(data.readString());
                    reply.writeNoException();
                    reply.writeString(_result41);
                    return true;
                case 46:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result42 = reqHfpRemoteDevicePhoneNumber(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result42 ? 1 : 0);
                    return true;
                case 47:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result43 = reqHfpRemoteDeviceNetworkOperator(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result43 ? 1 : 0);
                    return true;
                case 48:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result44 = reqHfpRemoteDeviceSubscriberNumber(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result44 ? 1 : 0);
                    return true;
                case 49:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result45 = isHfpRemoteDeviceTelecomServiceOn(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result45 ? 1 : 0);
                    return true;
                case 50:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result46 = isHfpRemoteDeviceOnRoaming(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result46 ? 1 : 0);
                    return true;
                case 51:
                    data.enforceInterface(DESCRIPTOR);
                    int _result47 = getHfpRemoteDeviceBatteryStatus(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result47);
                    return true;
                case 52:
                    data.enforceInterface(DESCRIPTOR);
                    int _result48 = getHfpRemoteDeviceSignalStatus(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result48);
                    return true;
                case 53:
                    data.enforceInterface(DESCRIPTOR);
                    setHfpAtNewSmsNotify(data.readString(), data.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case 54:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result49 = reqHfpAtDownloadPhonebook(data.readString(), data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result49 ? 1 : 0);
                    return true;
                case 55:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result50 = reqHfpAtDownloadSMS(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result50 ? 1 : 0);
                    return true;
                case 56:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result51 = reqHfpRemoteDeviceVolumeSync(data.readString(), data.readByte(), data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result51 ? 1 : 0);
                    return true;
                case 57:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result52 = reqHfpVoiceDial(data.readString(), data.readInt() != 0);
                    reply.writeNoException();
                    reply.writeInt(_result52 ? 1 : 0);
                    return true;
                case 58:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result53 = registerHspCallback(nfore.android.bt.servicemanager.IHspCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result53 ? 1 : 0);
                    return true;
                case 59:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result54 = unregisterHspCallback(nfore.android.bt.servicemanager.IHspCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result54 ? 1 : 0);
                    return true;
                case 60:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result55 = reqHspConnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result55 ? 1 : 0);
                    return true;
                case 61:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result56 = reqHspDisconnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result56 ? 1 : 0);
                    return true;
                case 62:
                    data.enforceInterface(DESCRIPTOR);
                    String _result57 = getHspConnectedDeviceAddress();
                    reply.writeNoException();
                    reply.writeString(_result57);
                    return true;
                case 63:
                    data.enforceInterface(DESCRIPTOR);
                    int _result58 = getHspCurrentState(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result58);
                    return true;
                case 64:
                    data.enforceInterface(DESCRIPTOR);
                    int _result59 = getHspCurrentScoState(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result59);
                    return true;
                case 65:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result60 = reqHspReDial(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result60 ? 1 : 0);
                    return true;
                case 66:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result61 = reqHspAnswerCall(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result61 ? 1 : 0);
                    return true;
                case 67:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result62 = reqHspRejectIncomingCall(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result62 ? 1 : 0);
                    return true;
                case 68:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result63 = reqHspTerminateOngoingCall(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result63 ? 1 : 0);
                    return true;
                case 69:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result64 = reqHspAudioTransfer(data.readString(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result64 ? 1 : 0);
                    return true;
                case 70:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result65 = reqHspRemoteDeviceVolumeSync(data.readString(), data.readByte(), data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result65 ? 1 : 0);
                    return true;
                case 71:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result66 = registerSppCallback(nfore.android.bt.servicemanager.ISppCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result66 ? 1 : 0);
                    return true;
                case 72:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result67 = unregisterSppCallback(nfore.android.bt.servicemanager.ISppCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result67 ? 1 : 0);
                    return true;
                case 73:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result68 = reqSppConnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result68 ? 1 : 0);
                    return true;
                case 74:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result69 = reqSppDisconnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result69 ? 1 : 0);
                    return true;
                case 75:
                    data.enforceInterface(DESCRIPTOR);
                    reqSppConnectedDeviceAddressList();
                    reply.writeNoException();
                    return true;
                case 76:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result70 = isSppConnected(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result70 ? 1 : 0);
                    return true;
                case TRANSACTION_reqSppSendData /*77*/:
                    data.enforceInterface(DESCRIPTOR);
                    reqSppSendData(data.readString(), data.createByteArray());
                    reply.writeNoException();
                    return true;
                case TRANSACTION_registerPbapCallback /*78*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result71 = registerPbapCallback(nfore.android.bt.servicemanager.IPbapCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result71 ? 1 : 0);
                    return true;
                case TRANSACTION_unregisterPbapCallback /*79*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result72 = unregisterPbapCallback(nfore.android.bt.servicemanager.IPbapCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result72 ? 1 : 0);
                    return true;
                case 80:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result73 = reqPbapDownloadAllVcards(data.readString(), data.readInt(), data.readInt(), data.readInt() != 0);
                    reply.writeNoException();
                    reply.writeInt(_result73 ? 1 : 0);
                    return true;
                case 81:
                    data.enforceInterface(DESCRIPTOR);
                    reqPbapDatabaseAvailable(data.readString());
                    reply.writeNoException();
                    return true;
                case TRANSACTION_reqPbapQueryName /*82*/:
                    data.enforceInterface(DESCRIPTOR);
                    reqPbapQueryName(data.readString(), data.readString());
                    reply.writeNoException();
                    return true;
                case TRANSACTION_reqPbapDeleteDatabaseByAddress /*83*/:
                    data.enforceInterface(DESCRIPTOR);
                    reqPbapDeleteDatabaseByAddress(data.readString());
                    reply.writeNoException();
                    return true;
                case TRANSACTION_reqPbapCleanDatabase /*84*/:
                    data.enforceInterface(DESCRIPTOR);
                    reqPbapCleanDatabase();
                    reply.writeNoException();
                    return true;
                case TRANSACTION_reqPbapDownloadInterrupt /*85*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result74 = reqPbapDownloadInterrupt(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result74 ? 1 : 0);
                    return true;
                case TRANSACTION_getPbapCurrentState /*86*/:
                    data.enforceInterface(DESCRIPTOR);
                    int _result75 = getPbapCurrentState(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result75);
                    return true;
                case TRANSACTION_registerMapCallback /*87*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result76 = registerMapCallback(nfore.android.bt.servicemanager.IMapCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result76 ? 1 : 0);
                    return true;
                case TRANSACTION_unregisterMapCallback /*88*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result77 = unregisterMapCallback(nfore.android.bt.servicemanager.IMapCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result77 ? 1 : 0);
                    return true;
                case TRANSACTION_reqMapDownloadAllMessages /*89*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result78 = reqMapDownloadAllMessages(data.readString(), data.readInt(), data.readInt(), data.readInt() != 0);
                    reply.writeNoException();
                    reply.writeInt(_result78 ? 1 : 0);
                    return true;
                case 90:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result79 = reqMapDownloadSingleMessage(data.readString(), data.readInt(), data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result79 ? 1 : 0);
                    return true;
                case 91:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result80 = reqMapRegisterNotification(data.readString(), data.readInt() != 0);
                    reply.writeNoException();
                    reply.writeInt(_result80 ? 1 : 0);
                    return true;
                case 92:
                    data.enforceInterface(DESCRIPTOR);
                    reqMapUnregisterNotification(data.readString());
                    reply.writeNoException();
                    return true;
                case TRANSACTION_isMapNotificationRegistered /*93*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result81 = isMapNotificationRegistered(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result81 ? 1 : 0);
                    return true;
                case TRANSACTION_reqMapDownloadInterrupt /*94*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result82 = reqMapDownloadInterrupt(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result82 ? 1 : 0);
                    return true;
                case 95:
                    data.enforceInterface(DESCRIPTOR);
                    reqMapDatabaseAvailable(data.readString());
                    reply.writeNoException();
                    return true;
                case 96:
                    data.enforceInterface(DESCRIPTOR);
                    reqMapDeleteDatabaseByAddress(data.readString());
                    reply.writeNoException();
                    return true;
                case 97:
                    data.enforceInterface(DESCRIPTOR);
                    reqMapCleanDatabase();
                    reply.writeNoException();
                    return true;
                case 98:
                    data.enforceInterface(DESCRIPTOR);
                    int _result83 = getMapCurrentState(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result83);
                    return true;
                case 99:
                    data.enforceInterface(DESCRIPTOR);
                    int _result84 = getMapRegisterState(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result84);
                    return true;
                case 100:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result85 = registerA2dpAvrcpCallback(nfore.android.bt.servicemanager.IA2dpCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result85 ? 1 : 0);
                    return true;
                case 101:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result86 = unregisterA2dpAvrcpCallback(nfore.android.bt.servicemanager.IA2dpCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result86 ? 1 : 0);
                    return true;
                case 102:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result87 = reqA2dpConnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result87 ? 1 : 0);
                    return true;
                case 103:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result88 = reqA2dpDisconnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result88 ? 1 : 0);
                    return true;
                case 104:
                    data.enforceInterface(DESCRIPTOR);
                    String _result89 = getA2dpConnectedDeviceAddress();
                    reply.writeNoException();
                    reply.writeString(_result89);
                    return true;
                case 105:
                    data.enforceInterface(DESCRIPTOR);
                    int _result90 = getA2dpCurrentState();
                    reply.writeNoException();
                    reply.writeInt(_result90);
                    return true;
                case 106:
                    data.enforceInterface(DESCRIPTOR);
                    int _result91 = getAvrcpCurrentState();
                    reply.writeNoException();
                    reply.writeInt(_result91);
                    return true;
                case 107:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result92 = setA2dpLocalVolume(data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result92 ? 1 : 0);
                    return true;
                case 108:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result93 = setA2dpLocalMute(data.readInt() != 0);
                    reply.writeNoException();
                    reply.writeInt(_result93 ? 1 : 0);
                    return true;
                case 109:
                    data.enforceInterface(DESCRIPTOR);
                    String _result94 = getAvrcpConnectedDeviceAddress();
                    reply.writeNoException();
                    reply.writeString(_result94);
                    return true;
                case 110:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result95 = isAvrcp13Supported(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result95 ? 1 : 0);
                    return true;
                case 111:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result96 = isAvrcp14Supported(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result96 ? 1 : 0);
                    return true;
                case 112:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result97 = reqAvrcpPlay(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result97 ? 1 : 0);
                    return true;
                case 113:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result98 = reqAvrcpStop(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result98 ? 1 : 0);
                    return true;
                case 114:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result99 = reqAvrcpPause(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result99 ? 1 : 0);
                    return true;
                case 115:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result100 = reqAvrcpForward(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result100 ? 1 : 0);
                    return true;
                case 116:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result101 = reqAvrcpBackward(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result101 ? 1 : 0);
                    return true;
                case 117:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result102 = reqAvrcpVolumeUp(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result102 ? 1 : 0);
                    return true;
                case 118:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result103 = reqAvrcpVolumeDown(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result103 ? 1 : 0);
                    return true;
                case 119:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result104 = reqAvrcpStartFastForward(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result104 ? 1 : 0);
                    return true;
                case 120:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result105 = reqAvrcpStopFastForward(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result105 ? 1 : 0);
                    return true;
                case 121:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result106 = reqAvrcpStartRewind(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result106 ? 1 : 0);
                    return true;
                case 122:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result107 = reqAvrcpStopRewind(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result107 ? 1 : 0);
                    return true;
                case 123:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result108 = reqAvrcpGetCapabilitiesSupportEvent(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result108 ? 1 : 0);
                    return true;
                case 124:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result109 = reqAvrcpGetPlayerSettingAttributesList(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result109 ? 1 : 0);
                    return true;
                case 125:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result110 = reqAvrcpGetPlayerSettingValuesList(data.readString(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result110 ? 1 : 0);
                    return true;
                case 126:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result111 = reqAvrcpGetPlayerSettingCurrentValues(data.readString(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result111 ? 1 : 0);
                    return true;
                case 127:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result112 = reqAvrcpSetPlayerSettingValue(data.readString(), data.readByte(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result112 ? 1 : 0);
                    return true;
                case 128:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result113 = reqAvrcpGetElementAttributesPlaying(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result113 ? 1 : 0);
                    return true;
                case 129:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result114 = reqAvrcpGetPlayStatus(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result114 ? 1 : 0);
                    return true;
                case 130:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result115 = reqAvrcpRegisterEventWatcher(data.readString(), data.readByte(), data.readLong());
                    reply.writeNoException();
                    reply.writeInt(_result115 ? 1 : 0);
                    return true;
                case 131:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result116 = reqAvrcpUnregisterEventWatcher(data.readString(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result116 ? 1 : 0);
                    return true;
                case 132:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result117 = reqAvrcpAbortMetadataReceiving(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result117 ? 1 : 0);
                    return true;
                case 133:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result118 = reqAvrcpNextGroup(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result118 ? 1 : 0);
                    return true;
                case 134:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result119 = reqAvrcpPreviousGroup(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result119 ? 1 : 0);
                    return true;
                case 135:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result120 = reqAvrcpSetMetadataCompanyId(data.readString(), data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result120 ? 1 : 0);
                    return true;
                case 136:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result121 = reqAvrcpGetPlayerSettingAttributeText(data.readString(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result121 ? 1 : 0);
                    return true;
                case 137:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result122 = reqAvrcpGetPlayerSettingValueText(data.readString(), data.readByte(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result122 ? 1 : 0);
                    return true;
                case 138:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result123 = reqAvrcpInformDisplayableCharset(data.readString(), data.createIntArray());
                    reply.writeNoException();
                    reply.writeInt(_result123 ? 1 : 0);
                    return true;
                case 139:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result124 = reqAvrcpInformBatteryStatusOfCt(data.readString(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result124 ? 1 : 0);
                    return true;
                case 140:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result125 = reqAvrcpSetAddressedPlayer(data.readString(), data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result125 ? 1 : 0);
                    return true;
                case 141:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result126 = reqAvrcpSetBrowsedPlayer(data.readString(), data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result126 ? 1 : 0);
                    return true;
                case 142:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result127 = reqAvrcpGetFolderItems(data.readString(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result127 ? 1 : 0);
                    return true;
                case 143:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result128 = reqAvrcpChangePath(data.readString(), data.readInt(), data.readLong(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result128 ? 1 : 0);
                    return true;
                case 144:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result129 = reqAvrcpGetItemAttributes(data.readString(), data.readByte(), data.readInt(), data.readLong());
                    reply.writeNoException();
                    reply.writeInt(_result129 ? 1 : 0);
                    return true;
                case 145:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result130 = reqAvrcpPlaySelectedItem(data.readString(), data.readByte(), data.readInt(), data.readLong());
                    reply.writeNoException();
                    reply.writeInt(_result130 ? 1 : 0);
                    return true;
                case 146:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result131 = reqAvrcpSearch(data.readString(), data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result131 ? 1 : 0);
                    return true;
                case 147:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result132 = reqAvrcpAddToNowPlaying(data.readString(), data.readByte(), data.readInt(), data.readLong());
                    reply.writeNoException();
                    reply.writeInt(_result132 ? 1 : 0);
                    return true;
                case 148:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result133 = isAvrcpBrowsingChannelEstablished(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result133 ? 1 : 0);
                    return true;
                case 149:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result134 = reqAvrcpSetAbsoluteVolume(data.readString(), data.readByte());
                    reply.writeNoException();
                    reply.writeInt(_result134 ? 1 : 0);
                    return true;
                case 150:
                    data.enforceInterface(DESCRIPTOR);
                    setA2dpNativeStart();
                    reply.writeNoException();
                    return true;
                case 151:
                    data.enforceInterface(DESCRIPTOR);
                    setA2dpNativeStop();
                    reply.writeNoException();
                    return true;
                case TRANSACTION_isA2dpNativeRunning /*152*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result135 = isA2dpNativeRunning();
                    reply.writeNoException();
                    reply.writeInt(_result135 ? 1 : 0);
                    return true;
                case 153:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result136 = registerPanCallback(nfore.android.bt.servicemanager.IPanCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result136 ? 1 : 0);
                    return true;
                case 154:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result137 = unregisterPanCallback(nfore.android.bt.servicemanager.IPanCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result137 ? 1 : 0);
                    return true;
                case 155:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result138 = reqPanConnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result138 ? 1 : 0);
                    return true;
                case TRANSACTION_reqPanDisconnect /*156*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result139 = reqPanDisconnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result139 ? 1 : 0);
                    return true;
                case 157:
                    data.enforceInterface(DESCRIPTOR);
                    String _result140 = getPanConnectedDeviceAddress();
                    reply.writeNoException();
                    reply.writeString(_result140);
                    return true;
                case 158:
                    data.enforceInterface(DESCRIPTOR);
                    int _result141 = getPanCurrentState();
                    reply.writeNoException();
                    reply.writeInt(_result141);
                    return true;
                case 159:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result142 = registerHidCallback(nfore.android.bt.servicemanager.IHidCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result142 ? 1 : 0);
                    return true;
                case 160:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result143 = unregisterHidCallback(nfore.android.bt.servicemanager.IHidCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result143 ? 1 : 0);
                    return true;
                case 161:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result144 = reqHidConnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result144 ? 1 : 0);
                    return true;
                case 162:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result145 = reqHidDisconnect(data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result145 ? 1 : 0);
                    return true;
                case 163:
                    data.enforceInterface(DESCRIPTOR);
                    int _result146 = sendHidMouseCommand(data.readInt(), data.readInt(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result146);
                    return true;
                case 164:
                    data.enforceInterface(DESCRIPTOR);
                    int _result147 = sendHidVirtualKeyCommand(data.readInt(), data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result147);
                    return true;
                case 165:
                    data.enforceInterface(DESCRIPTOR);
                    int _result148 = getHidCurrentState();
                    reply.writeNoException();
                    reply.writeInt(_result148);
                    return true;
                case 1598968902:
                    reply.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    String getA2dpConnectedDeviceAddress() throws RemoteException;

    int getA2dpCurrentState() throws RemoteException;

    String getAvrcpConnectedDeviceAddress() throws RemoteException;

    int getAvrcpCurrentState() throws RemoteException;

    int getBluetoothCurrentState() throws RemoteException;

    String getBluetoothLocalAdapterAddress() throws RemoteException;

    String getBluetoothLocalAdapterName() throws RemoteException;

    String getBluetoothRemoteDeviceName(String str) throws RemoteException;

    String getHfpConnectedDeviceAddress() throws RemoteException;

    int getHfpCurrentScoState(String str) throws RemoteException;

    int getHfpCurrentState(String str) throws RemoteException;

    int getHfpRemoteDeviceBatteryStatus(String str) throws RemoteException;

    String getHfpRemoteDeviceName(String str) throws RemoteException;

    int getHfpRemoteDeviceSignalStatus(String str) throws RemoteException;

    int getHidCurrentState() throws RemoteException;

    String getHspConnectedDeviceAddress() throws RemoteException;

    int getHspCurrentScoState(String str) throws RemoteException;

    int getHspCurrentState(String str) throws RemoteException;

    int getMapCurrentState(String str) throws RemoteException;

    int getMapRegisterState(String str) throws RemoteException;

    String getPanConnectedDeviceAddress() throws RemoteException;

    int getPanCurrentState() throws RemoteException;

    int getPbapCurrentState(String str) throws RemoteException;

    boolean isA2dpNativeRunning() throws RemoteException;

    boolean isAvrcp13Supported(String str) throws RemoteException;

    boolean isAvrcp14Supported(String str) throws RemoteException;

    boolean isAvrcpBrowsingChannelEstablished(String str) throws RemoteException;

    boolean isBluetoothAutoReconnectEnable() throws RemoteException;

    boolean isBluetoothDiscoverable() throws RemoteException;

    boolean isBluetoothEnable() throws RemoteException;

    boolean isBluetoothScanning() throws RemoteException;

    boolean isHfpRemoteDeviceOnRoaming(String str) throws RemoteException;

    boolean isHfpRemoteDeviceTelecomServiceOn(String str) throws RemoteException;

    boolean isHfpVoiceControlOn(String str) throws RemoteException;

    boolean isMapNotificationRegistered(String str) throws RemoteException;

    boolean isSppConnected(String str) throws RemoteException;

    boolean registerA2dpAvrcpCallback(IA2dpCallback iA2dpCallback) throws RemoteException;

    boolean registerHfpCallback(IHfpCallback iHfpCallback) throws RemoteException;

    boolean registerHidCallback(IHidCallback iHidCallback) throws RemoteException;

    boolean registerHspCallback(IHspCallback iHspCallback) throws RemoteException;

    boolean registerMapCallback(IMapCallback iMapCallback) throws RemoteException;

    boolean registerNforeServiceCallback(IServiceCallback iServiceCallback) throws RemoteException;

    boolean registerPanCallback(IPanCallback iPanCallback) throws RemoteException;

    boolean registerPbapCallback(IPbapCallback iPbapCallback) throws RemoteException;

    boolean registerSppCallback(ISppCallback iSppCallback) throws RemoteException;

    boolean reqA2dpConnect(String str) throws RemoteException;

    boolean reqA2dpDisconnect(String str) throws RemoteException;

    boolean reqAvrcpAbortMetadataReceiving(String str) throws RemoteException;

    boolean reqAvrcpAddToNowPlaying(String str, byte b, int i, long j) throws RemoteException;

    boolean reqAvrcpBackward(String str) throws RemoteException;

    boolean reqAvrcpChangePath(String str, int i, long j, byte b) throws RemoteException;

    boolean reqAvrcpForward(String str) throws RemoteException;

    boolean reqAvrcpGetCapabilitiesSupportEvent(String str) throws RemoteException;

    boolean reqAvrcpGetElementAttributesPlaying(String str) throws RemoteException;

    boolean reqAvrcpGetFolderItems(String str, byte b) throws RemoteException;

    boolean reqAvrcpGetItemAttributes(String str, byte b, int i, long j) throws RemoteException;

    boolean reqAvrcpGetPlayStatus(String str) throws RemoteException;

    boolean reqAvrcpGetPlayerSettingAttributeText(String str, byte b) throws RemoteException;

    boolean reqAvrcpGetPlayerSettingAttributesList(String str) throws RemoteException;

    boolean reqAvrcpGetPlayerSettingCurrentValues(String str, byte b) throws RemoteException;

    boolean reqAvrcpGetPlayerSettingValueText(String str, byte b, byte b2) throws RemoteException;

    boolean reqAvrcpGetPlayerSettingValuesList(String str, byte b) throws RemoteException;

    boolean reqAvrcpInformBatteryStatusOfCt(String str, byte b) throws RemoteException;

    boolean reqAvrcpInformDisplayableCharset(String str, int[] iArr) throws RemoteException;

    boolean reqAvrcpNextGroup(String str) throws RemoteException;

    boolean reqAvrcpPause(String str) throws RemoteException;

    boolean reqAvrcpPlay(String str) throws RemoteException;

    boolean reqAvrcpPlaySelectedItem(String str, byte b, int i, long j) throws RemoteException;

    boolean reqAvrcpPreviousGroup(String str) throws RemoteException;

    boolean reqAvrcpRegisterEventWatcher(String str, byte b, long j) throws RemoteException;

    boolean reqAvrcpSearch(String str, String str2) throws RemoteException;

    boolean reqAvrcpSetAbsoluteVolume(String str, byte b) throws RemoteException;

    boolean reqAvrcpSetAddressedPlayer(String str, int i) throws RemoteException;

    boolean reqAvrcpSetBrowsedPlayer(String str, int i) throws RemoteException;

    boolean reqAvrcpSetMetadataCompanyId(String str, int i) throws RemoteException;

    boolean reqAvrcpSetPlayerSettingValue(String str, byte b, byte b2) throws RemoteException;

    boolean reqAvrcpStartFastForward(String str) throws RemoteException;

    boolean reqAvrcpStartRewind(String str) throws RemoteException;

    boolean reqAvrcpStop(String str) throws RemoteException;

    boolean reqAvrcpStopFastForward(String str) throws RemoteException;

    boolean reqAvrcpStopRewind(String str) throws RemoteException;

    boolean reqAvrcpUnregisterEventWatcher(String str, byte b) throws RemoteException;

    boolean reqAvrcpVolumeDown(String str) throws RemoteException;

    boolean reqAvrcpVolumeUp(String str) throws RemoteException;

    int reqBluetoothConnectAll(String str) throws RemoteException;

    int reqBluetoothDisconnectAll() throws RemoteException;

    boolean reqBluetoothPair(String str) throws RemoteException;

    boolean reqBluetoothPairedDevices() throws RemoteException;

    int reqBluetoothRemoteUuids(String str) throws RemoteException;

    boolean reqBluetoothScanning(boolean z) throws RemoteException;

    boolean reqBluetoothUnpair(String str) throws RemoteException;

    boolean reqBluetoothUnpairAll() throws RemoteException;

    boolean reqBluetoothUnpairAllNextTime() throws RemoteException;

    boolean reqHfpAnswerCall(String str) throws RemoteException;

    boolean reqHfpAtDownloadPhonebook(String str, int i) throws RemoteException;

    boolean reqHfpAtDownloadSMS(String str) throws RemoteException;

    boolean reqHfpAudioTransfer(String str, byte b) throws RemoteException;

    boolean reqHfpConnect(String str) throws RemoteException;

    boolean reqHfpDialCall(String str, String str2) throws RemoteException;

    boolean reqHfpDisconnect(String str) throws RemoteException;

    boolean reqHfpMemoryDial(String str, String str2) throws RemoteException;

    boolean reqHfpMultiCallControl(String str, byte b) throws RemoteException;

    boolean reqHfpReDial(String str) throws RemoteException;

    boolean reqHfpRejectIncomingCall(String str) throws RemoteException;

    void reqHfpRemoteDeviceInfo(String str) throws RemoteException;

    boolean reqHfpRemoteDeviceNetworkOperator(String str) throws RemoteException;

    boolean reqHfpRemoteDevicePhoneNumber(String str) throws RemoteException;

    boolean reqHfpRemoteDeviceSubscriberNumber(String str) throws RemoteException;

    boolean reqHfpRemoteDeviceVolumeSync(String str, byte b, int i) throws RemoteException;

    boolean reqHfpSendDtmf(String str, String str2) throws RemoteException;

    boolean reqHfpTerminateOngoingCall(String str) throws RemoteException;

    boolean reqHfpVoiceDial(String str, boolean z) throws RemoteException;

    boolean reqHidConnect(String str) throws RemoteException;

    boolean reqHidDisconnect(String str) throws RemoteException;

    boolean reqHspAnswerCall(String str) throws RemoteException;

    boolean reqHspAudioTransfer(String str, byte b) throws RemoteException;

    boolean reqHspConnect(String str) throws RemoteException;

    boolean reqHspDisconnect(String str) throws RemoteException;

    boolean reqHspReDial(String str) throws RemoteException;

    boolean reqHspRejectIncomingCall(String str) throws RemoteException;

    boolean reqHspRemoteDeviceVolumeSync(String str, byte b, int i) throws RemoteException;

    boolean reqHspTerminateOngoingCall(String str) throws RemoteException;

    void reqMapCleanDatabase() throws RemoteException;

    void reqMapDatabaseAvailable(String str) throws RemoteException;

    void reqMapDeleteDatabaseByAddress(String str) throws RemoteException;

    boolean reqMapDownloadAllMessages(String str, int i, int i2, boolean z) throws RemoteException;

    boolean reqMapDownloadInterrupt(String str) throws RemoteException;

    boolean reqMapDownloadSingleMessage(String str, int i, String str2) throws RemoteException;

    boolean reqMapRegisterNotification(String str, boolean z) throws RemoteException;

    void reqMapUnregisterNotification(String str) throws RemoteException;

    boolean reqPanConnect(String str) throws RemoteException;

    boolean reqPanDisconnect(String str) throws RemoteException;

    void reqPbapCleanDatabase() throws RemoteException;

    void reqPbapDatabaseAvailable(String str) throws RemoteException;

    void reqPbapDeleteDatabaseByAddress(String str) throws RemoteException;

    boolean reqPbapDownloadAllVcards(String str, int i, int i2, boolean z) throws RemoteException;

    boolean reqPbapDownloadInterrupt(String str) throws RemoteException;

    void reqPbapQueryName(String str, String str2) throws RemoteException;

    boolean reqSppConnect(String str) throws RemoteException;

    void reqSppConnectedDeviceAddressList() throws RemoteException;

    boolean reqSppDisconnect(String str) throws RemoteException;

    void reqSppSendData(String str, byte[] bArr) throws RemoteException;

    int sendHidMouseCommand(int i, int i2, int i3, int i4) throws RemoteException;

    int sendHidVirtualKeyCommand(int i, int i2) throws RemoteException;

    boolean setA2dpLocalMute(boolean z) throws RemoteException;

    boolean setA2dpLocalVolume(int i) throws RemoteException;

    void setA2dpNativeStart() throws RemoteException;

    void setA2dpNativeStop() throws RemoteException;

    void setBluetoothAutoReconnect(boolean z) throws RemoteException;

    void setBluetoothAutoReconnectForceOor(boolean z) throws RemoteException;

    boolean setBluetoothDiscoverableDuration(int i) throws RemoteException;

    boolean setBluetoothDiscoverablePermanent(boolean z) throws RemoteException;

    boolean setBluetoothEnable(boolean z) throws RemoteException;

    boolean setBluetoothLocalAdapterName(String str) throws RemoteException;

    void setHfpAtNewSmsNotify(String str, boolean z) throws RemoteException;

    void setHfpLocalRingEnable(boolean z) throws RemoteException;

    boolean unregisterA2dpAvrcpCallback(IA2dpCallback iA2dpCallback) throws RemoteException;

    boolean unregisterHfpCallback(IHfpCallback iHfpCallback) throws RemoteException;

    boolean unregisterHidCallback(IHidCallback iHidCallback) throws RemoteException;

    boolean unregisterHspCallback(IHspCallback iHspCallback) throws RemoteException;

    boolean unregisterMapCallback(IMapCallback iMapCallback) throws RemoteException;

    boolean unregisterNforeServiceCallback(IServiceCallback iServiceCallback) throws RemoteException;

    boolean unregisterPanCallback(IPanCallback iPanCallback) throws RemoteException;

    boolean unregisterPbapCallback(IPbapCallback iPbapCallback) throws RemoteException;

    boolean unregisterSppCallback(ISppCallback iSppCallback) throws RemoteException;
}
