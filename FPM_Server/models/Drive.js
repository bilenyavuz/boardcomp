let mongoose = require('mongoose');

/*=================================================================
					DATA MODEL Drive
=================================================================*/

let DriveSchema = new mongoose.Schema({
	user: 					{type: mongoose.Schema.Types.ObjectId, ref: 'User'},
	car: 					{type: mongoose.Schema.Types.ObjectId, ref: 'Car'},
	startDate: 				{type: Date, required: true},
	endDate: 				{type: Date, required: true},
	startCoord: 			{
			latitude: 		{type: String, required: true},
			longitude: 		{type: String, required: true}
	},
	endCoord: 				{
			latitude: 		{type: String, required: true},
			longitude: 		{type: String, required: true}
	},
	startAddress: 			{
								country: {type: String},
								city: {type: String},
								zip: {type: String},
								street: {type: String},
							},
	endAddress: 			{
								country: {type: String},
								city: {type: String},
								zip: {type: String},
								street: {type: String},
							},
	startPOI:				{type: String},									//start Point of Interest
	endPOI: 				{type: String},									//end Point Of Interest 
	startMileage: 			{type: Number, required: true},
	endMileage: 			{type: Number, required: true},
	usedkWh: 				{type: Number}
});

mongoose.model('Drive', DriveSchema);