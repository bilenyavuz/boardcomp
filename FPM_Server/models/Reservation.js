let mongoose = require('mongoose');

/*=================================================================
					DATA MODEL Reservation
=================================================================*/

let ReservationSchema = new mongoose.Schema({
    user: 		{type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
    car: 		{type: mongoose.Schema.Types.ObjectId, ref: 'Car', required: true},
    startDate: 	{type: Date, required: true},
    endDate: 	{type: Date, required: true}
});

mongoose.model('Reservation', ReservationSchema);