let mongoose = require('mongoose');
let crypto = require('crypto');
let jwt = require('jsonwebtoken');
let fs = require('fs');

/*=================================================================
                    DATA MODEL Users
 =================================================================*/

let UserSchema = new mongoose.Schema({
	username: 	{type: String, unique: true, required: true},
	firstname: 	{type: String, required: true},
	lastname: 	{type: String, required: true},
	email: 		{type: String, unique: true, required: true},
	phone: 		{type: String, required: false},
	role: 		{type: String, default: 'disabled'},
	hash: 		{type: String, required: false},
	salt: 		{type: String, required: false}
});

UserSchema.methods.setPassword = function (password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'SHA1').toString('hex')
};

UserSchema.methods.validPassword = function (password) {
    let hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'SHA1').toString('hex');

    return this.hash === hash
};

UserSchema.methods.generateJWT = function () {
    //set expiration to 8 hours
    let now = new Date();
    let exp = new Date(now);
    exp.setHours(now.getHours() + 8);

    //Keyfile for signature
    let key = fs.readFileSync('keys/api.key');

    return jwt.sign({
        id: this.id,
        username: this.username,
        role: this.role,
        exp: parseInt(exp.getTime() / 1000)
    }, key, {algorithm: 'RS256'})

};

mongoose.model('User', UserSchema);