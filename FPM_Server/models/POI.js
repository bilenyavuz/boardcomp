let mongoose = require('mongoose');

/*=================================================================
                    DATA MODEL Point Of Interest.
 =================================================================*/

let POISchema = new mongoose.Schema({
    name: {type: String, required: true},
    address: 			{
        country: {type: String, required: true},
        city: {type: String, required: true},
        zip: {type: String, required: true},
        street: {type: String, required: true}
    }
});

mongoose.model('POI', POISchema);