let mongoose = require('mongoose');

/*=================================================================
                    DATA MODEL Car
 =================================================================*/

let CarSchema = new mongoose.Schema({
    vendor:         {type: String, required: true},
    model:          {type: String, required: true},
    licenseNumber:  {type: String, required: true},
    mileage:        {type: Number, required: true},
    battery:        {type: Number, default: 0},
    available:      {type: Boolean, default: false},
    charging:       {type: Boolean, default: false},
    chargedkWh:     {type: Number, default: 0}
});

mongoose.model('Car', CarSchema);