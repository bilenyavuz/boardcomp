let express = require('express');
let path = require('path');
let fs = require('fs');
let favicon = require('serve-favicon');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let passport = require('passport');
let rfs = require('rotating-file-stream');

// Application Base Directory
global.__base = __dirname + '/';

//WebPack
let webpack = require('webpack');

//let webPackConfig = require('./webpack.config')


//WebPack Configuration
webpack({
    entry: './webApp/app/main',
    output: {
        path: __dirname + "/public/webApp",
        filename: "bundle.js"
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel'
            }
        ]
    }
}, (err, stats) => {
    if(err){console.log(err)}
    console.log('[webpack:build]', stats.toString({
        chunks: false,
        colors: true
    }))
});

// Require mongoose, connect and require mongoose models
let mongoose = require('mongoose');

// establishe Database connection -> MongoDB

let mongoConfig = require('./config/mongoDB');

mongoose.Promise = global.Promise;

mongoose.connect(mongoConfig.host, mongoConfig.options, (err) => {
    if (err) {
        console.log(err)
    } else {
        console.log('Connection to ' + mongoConfig.host + ' established')
    }
});

//Entity Schemas Reservation, Drive, Car, POI
require('./models/Reservation');
require('./models/Drive');
require('./models/Car');
require('./models/POI');
//Schema for User and passport config
require('./models/User');
require('./config/passport');

let webApp = require('./routes/webApp');
let apiCars = require('./routes/api/cars');
let apiReservation = require('./routes/api/reservations');
let apiDrive = require('./routes/api/drives');
let apiPOI = require('./routes/api/pois');
let apiUser = require('./routes/api/users');

let app = express();

// Allow Cross-Origin-Resource-Sharing for all Routes
let cors = require('cors');
app.use(cors());

// favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

//Logger Settings

let logDir = path.join(__dirname, 'log');
fs.existsSync(logDir) || fs.mkdirSync(logDir);
let accessLog = rfs('access.log', {interval: '1d', path: logDir});

if (app.get('env') === 'development') {
    app.use(logger('dev'))
} else {
    app.use(logger('combined', {stream: accessLog}))
}


app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: true, limit: '50mb'}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());

// Require JS Files for the …/api/ routes
app.use('/api', apiCars);
app.use('/api', apiDrive);
app.use('/api', apiUser);
app.use('/api', apiPOI);
app.use('/api', apiReservation);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err)
});

// error handler
/*
 app.use(function(err, req, res, next) {
 // set locals, only providing error in development
 res.locals.message = err.message;
 res.locals.error = req.app.get('env') === 'development' ? err : {};

 // render the error page
 res.status(err.status || 500);
 res.json(err)
 });
 */

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {

    app.use(function (err, req, res, next) {

        console.log('_____________________________________________________________________________________________ \n');
        console.log('\x1b[36m' + new Date().toISOString() + '\x1b[0m');
        console.log('RequestBaseURL: ' + '\x1b[36m' + req.originalUrl + '\x1b[0m');
        console.log('Connection form:' + '\x1b[36m', req.connection.remoteAddress + '\x1b[0m');
        console.log(err);
        console.log('_____________________________________________________________________________________________ \n');

        fs.existsSync(logDir) || fs.mkdirSync(logDir);
        let errorLog = rfs('errorLog.log', {interval: '1d', path: logDir});

        errorLog.write(
            '_____________________________________________________________________________________________ \n' +
            new Date().toISOString() + '\n' +
            'RequestBaseURL: ' + req.originalUrl + '\n' +
            'Connection form:' + req.connection.remoteAddress + '\n' +
            err.message + '\n' +
            err +
            '\n_____________________________________________________________________________________________ \n'
        );

        // fs.appendFile(__dirname + '/log/ErrorLog.log',
        //     '_____________________________________________________________________________________________ \n' +
        //     new Date().toISOString() +
        //     '\nConnection form:' + req.connection.remoteAddress + '\n' +
        //     err.message + '\n' +
        //     err +
        //     '\n_____________________________________________________________________________________________ \n', (err) => {
        //     if(err){
        //         return console.log(err);
        //     }
        //
        //     console.log('ErrorLog written');
        // });


        res.status(err.status || 500).json({message: err.message, error: err});

    })
} else {

// production error handler
// no stacktraces leaked to user
    app.use(function (err, req, res, next) {
        res.sendStatus(err.status || 500);
        console.log(err);

    })
}


module.exports = app;