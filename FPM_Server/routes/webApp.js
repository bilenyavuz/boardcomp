let express = require('express');
let router = express.Router();
let path = require('path');

/*=========================================================
 Load HomePage
 =========================================================*/

/* GET home page. */
router.get('/', function(req, res) {
	console.log('Get HomePage');
    res.sendFile(path.join('public/fm_landingpage', 'index.html'));
});

/* GET WebApp. */
router.get('/webApp', (req, res) => {
	console.log('GetWebApp');
    res.sendFile(path.join('/public', 'index.html'));
});

module.exports = router;