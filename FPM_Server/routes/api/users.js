/**
 * Created by Fabian on 15.01.17.
 */
let express = require('express');
let router = express.Router();
let mongoose = require('mongoose');
let passport = require('passport');

let jwt = require('express-jwt');
//let auth = jwt({secret: 'SECRET-SHOULD-BE-NOT-HERE', userProperty: 'user'})
let fs = require('fs');
let cert = fs.readFileSync('keys/api.crt');
let auth = jwt({secret: cert, userPorperty: 'user'});

/*
 ===========================================================
 Load Mongoose Models ./models/
 ===========================================================
 */
let User = mongoose.model('User');

function log(req) {
    console.log('\x1b[36m' + new Date().toISOString() + '\x1b[0m');
    //console.log('Connection form:' + '\x1b[36m', req.connection.remoteAddress + '\x1b[0m')
    console.log('Connection form:' + '\x1b[36m', req.ip + '\x1b[0m');

    if (req.user) {
        console.log('User: ' + '\x1b[36m' + req.user.username + '\x1b[0m');
        console.log('ID: ' + '\x1b[36m' + req.user.id + '\x1b[0m');
        console.log('Role: ' + '\x1b[36m' + req.user.role + '\x1b[0m')
    }
}

/*========================================================
 RESTfulAPI EndPoints
 ========================================================*/

/*------------------Routes for /User--------------------*/
/*
 Routes record:

 HTTP POST 	    /user/login         -> User login
 HTTP POST	    /user/register      -> Creates new User with role: disabled
 HTTP GET       /user               -> Returns all users in the DB (authorization 'Administrator' required)
 HTTP POST      /user               -> Creates new user with given JSON (authorization 'Administrator' required)
 HTTP GET       /user/:user         -> Returns User by ID (authorization 'Administrator' required)
 HTTP PUT       /user/:user         -> Edits User by ID (authorization 'Administrator' required)
 HTTP DELETE    /user/:user         -> Removes User by ID (authorization 'Administrator' required)
 */

router.post('/login', (req, res, next) => {
    log(req);


    // Check for necessary fields username and password
    if (!req.body.username || !req.body.password) {
        return res.status(400).json({message: 'Please fill out all fields'})
    }

    // Authentication with the Passport Framework -> generating JWT
    passport.authenticate('local', (err, user, info) => {
        if (err) {
            return next(err)
        }

        if (user) {
            return res.json({token: user.generateJWT()})
        } else {
            return res.status(401).json(info)
        }
    })(req, res, next)
});


// Returns user data from the logged-in User
router.get('/users/me', auth, (req, res, next) => {
    log(req);

    User.findById(req.user.id)
        .select('firstname lastname email phone')
        .exec((err, data) => {
            if (err) {
                return next(err)
            }

            res.status(200).json(data)
        })
});

router.put('/users/me', auth, (req, res, next) => {
    log(req);

    // User.findByIdAndUpdate(req.user.id, {$set: req.body, new: true}, (err, data) => {
    //     if (err) {
    //         return next(err)
    //     }
    //     if (req.body.password) data.setPassword(req.body.password);
    //
    //     return res.sendStatus(200);
    // })

    if(req.body._id) delete req.body._id;
    if(req.body.role) delete req.body.role;
    if(req.body.hash) delete req.body.hash;
    if(req.body.salt) delete req.body.salt;

    User.findByIdAndUpdate(req.user.id, {$set: req.body}, {new: true})
        .select('firstname lastname email phone')
        .exec((err, data) => {
        if(err) return next(err);

        if(req.body.password) data.setPassword(req.body.password);

        return res.status(200).json(data);
        })
});

router.get('/users', auth, (req, res, next) => {
    log(req);

    if (req.user.role === 'Administrator') {

        if (!req.query.username) {
            User.find((err, data) => {
                if (err) {
                    return next(err)
                }

                res.json(data)
            })
        }
        if (req.query.username) {
            User.find({username: req.query.username}, (err, data) => {
                if (err) {
                    return next(err)
                }

                res.json(data)
            })
        }
    } else if(req.user.role === 'CarDevice'){
        /**
         * Expose just User data with Role 'Driver' to the Android Tab
         * Exposed User data:
         * ID, username, firstname, lastname, hash, salt
        **/

        User.find({role: 'Driver'})
            .select('_id username firstname lastname hash salt')
            .exec((err, data) => {
            if(err){
                return next(err)
            }
            res.json(data);
        })
    } else {
        res.sendStatus(403)
    }
});

router.get('/users/:user', auth, (req, res, next) => {
        log(req);

        if (req.user.role === 'Administrator') {
            User.findById(req.params.user, (err, data) => {
                if (err) {
                    return next(err)
                }

                res.json(data)
            })
        } else {
            res.sendStatus(403)
        }
    }
);

router.put('/users/:user', auth, (req, res, next) => {
    log(req);

    if (req.user.role === 'Administrator') {
        User.findByIdAndUpdate(req.params.user, {$set: req.body, new: true}, (err, data) => {
            if (err) {
                return next(err)
            }
            if (req.body.password) data.setPassword(req.body.password);

            return res.sendStatus(200)

        })
    } else {
        res.sendStatus(403)
    }

});

router.delete('/users/:user', auth, (req, res, next) => {
    log(req);

    if (req.user.role === 'Administrator') {
        User.findByIdAndUpdate({_id: req.params.user}, {$set: {hash: undefined, salt: undefined, role: disabled}}, (err, data) => {
            if (err) {
                return next(err)
            }
            res.sendStatus(200)
        })
    } else {
        res.sendStatus(403)
    }
});


router.post('/users', auth, (req, res, next) => {
    log(req);

    if (req.user.role === 'Administrator') {

        let user = new User(req.body);

        user.setPassword(req.body.password);

        user.save((err) => {
            if (err) {
                return next(err)
            }

            return res.sendStatus(201)
        })
    } else {
        res.sendStatus(403)
    }
});

/**
 * Allow registration of new Users. New User have to be reviewed/authenticated by an userManager or another Administrator
 **/

router.post('/register', (req, res, next) => {
    log(req);

    console.log(req.body);

    //Check for all required fields.
    if (!req.body.username || !req.body.password || !req.body.firstname || !req.body.lastname || !req.body.email) {
        return res.status(400).json({message: 'Please fill out all fields'})
    }

    let user = new User(req.body);
    user.role = 'disabled';

    user.setPassword(req.body.password);

    user.save((err) => {
        if (err) {
            return next(err)
        }

        return res.status(201).json({token: user.generateJWT()})
    })
});

module.exports = router;