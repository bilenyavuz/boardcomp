/**
 * Created by Fabian on 15.01.17.
 */
let express = require('express');
let router = express.Router();
let http = require('http');
let https = require('https');
let mongoose = require('mongoose');

let jwt = require('express-jwt');
//let auth = jwt({secret: 'SECRET-SHOULD-BE-NOT-HERE', userProperty: 'user'})
let fs = require('fs');
let cert = fs.readFileSync('keys/api.crt');
let auth = jwt({secret: cert, userPorperty: 'user'});

//Reverse geoCoding API

let geoCodingAPI = require(__base + 'config/geoCodingAPI');

/*
 ===========================================================
 Load Mongoose Models ./models/
 ===========================================================
 */
let Drive = mongoose.model('Drive');

function log(req) {
    console.log('\x1b[36m' + new Date().toISOString() + '\x1b[0m');
    //console.log('Connection form:' + '\x1b[36m', req.connection.remoteAddress + '\x1b[0m')
    console.log('Connection form:' + '\x1b[36m', req.ip + '\x1b[0m');

    if (req.user) {
        console.log('User: ' + '\x1b[36m' + req.user.username + '\x1b[0m');
        console.log('ID: ' + '\x1b[36m' + req.user.id + '\x1b[0m');
        console.log('Role: ' + '\x1b[36m' + req.user.role + '\x1b[0m')
    }
}

function reverseGeoCoder(latitude, longitude) {

    return new Promise((resolve, reject) => {
        https.get({
            host: geoCodingAPI.host,
            path: geoCodingAPI.path + '?q=' + latitude + ',+' + longitude + '&pretty=1&key=' + geoCodingAPI.key
        }, (response) => {
            console.log('Got Response: ' + response.statusCode);
            let output = [];
            response
                .on('data', (chunk) => {
                    output.push(chunk)
                })
                .on('end', () => {
                    //console.log('Output: ' + output);
                    resolve(output);
                })
        }).on('error', (error) => {
            console.log('Got Error: ' + error);
            reject(error);
        })
    });

    // promise
    //     .then((value) => {
    //     console.log('RESOLVED: ' + value);
    //     result = JSON.parse(value);
    //
    //     let adress = {}
    //     address.country = result.results[0].components.country;
    //     address.city = result.results[0].components.city;
    //     address.zip = result.results[0].components.postcode;
    //     address.street = result.results[0].components.road;
    //
    //     return adress;
    //     })
    //     .catch((reason) => {
    //         console.log('REJECTED: ' + reason);
    //         next(reason);
    //     })
}

//CSV export
let csv = require('json2csv');

//Experimental Export Route - Returns a CSV File with all Drives over all Cars.

router.get('/drives/export', (req, res, next) => {

    // Definition of covered fields in csv file
    let fields = ['_id', 'user._id', 'user.firstname', 'user.lastname', 'startDate', 'endDate', 'startAddress.city',
        'startAddress.zip', 'startAddress.street', 'endAddress.city', 'endAddress.zip', 'endAddress.street', 'startPOI',
        'endPOI', 'startMileage', 'endMileage'];
    // Human readable names for covered fields
    let fieldNames = ['ID', 'UserID', 'Name', '', 'Beginn', 'Ende', 'Start Adresse Stadt', 'Plz', 'Straße',
        'Ziel Adresse Stadt', 'Plz', 'Straße', 'Start POI', 'Ziel POI', 'km Stand(alt)', 'km Stand (neu)'];

    /**
     * Find Drives by Cars (ID) within a specific period of time if start/end Date is given;
     * If startDate is not given it will be set to 0;
     * if endDate is nig given it will be set to the dateTime of requesting;
     */


    if(req.query.startDate == undefined){
        var startDate = new Date(0);
    } else {
        var startDate = new Date(req.query.startDate);

    }

    if(req.query.endDate == undefined){
        var endDate = Date.now();
    } else {
        var endDate = new Date(req.query.endDate);
    }

    if(req.query.car){
        Drive.find({
            'car': req.query.car,
            'startDate': {$gt: startDate, $lt: endDate}
        })
            .populate('user')
            .exec()
            .then((results) => {
                let csvData = csv({data: results, fields: fields, fieldNames: fieldNames});

                res.attachment(new Date().toISOString() + '-Drives.csv');
                res.status(200).send(csvData);
            })
            .catch((err) => {
                return next(err);
            })
    }

    /**
     * Returns all drives within a specific period of time
     */

     else {
        Drive.find({
            'startDate': {$gt: startDate, $lt: endDate}
        })
            .populate('user')
            .exec()
            .then((results) => {
                let csvData = csv({data: results, fields: fields, fieldNames: fieldNames});

                res.attachment(new Date().toISOString() + '-Drives.csv');
                res.status(200).send(csvData);
            })
            .catch((err) => {
                return next(err);
            })
    }
});

/*========================================================
 RESTfulAPI EndPoint DRIVES
 ========================================================*/

/*------------------Routes for /drives-------------------*/

/*
 Routes record:

 HTTP GET 	    /drives 				-> returns all Drives
 HTTP POST 	    /drives 				-> creates a new Drive

 HTTP GET 	    /drives/:drive 		    -> returns a singe Drive (by ID)
 HTTP PUT	    /drives/:drive 		    -> updates a Drive (by ID)
 HTTP DELETE    /drives/:drive 		    -> removes a Drive (by ID)

 */

/*
    HTTP GET /drives

    return all Drives as JSON
 */
router.get('/drives', auth, (req, res, next) => {
    log(req);

    Drive.find((err, data) => {
        if (err) {
            return next(err)
        }
        res.json(data);
    })

});

/*
    HTTP POST /drive

    create a new Drive and return the result as JSON
 */

router.post('/drives', auth, (req, res, next) => {

    log(req);

    if (req.user.role !== 'disabled') {

        console.log('Request is Array: ' + Array.isArray(req.body));

        let promises = [];

        if (Array.isArray(req.body) && req.body.length == !0) {
            for (let i = 0; i < req.body.length; i++) {

                promises.push(new Promise((resolve, reject) => {

                    let drive = new Drive(req.body[i]);

                    drive.save((err, data) => {
                        if (err) {
                            reject(err)
                        } else {
                            resolve(data)
                        }
                    })
                }))
            }
            res.headers('Created-Drives', req.body.length).sendStatus(201)
        } else {

            promises.push(new Promise((resolve, reject) => {

                let promisesGeoCoding = [];

                promisesGeoCoding.push(new Promise((resolve, reject) => {
                    if(!req.body.startAddress || !req.body.startAddress.country || !req.body.startAddress.city
                        || !req.body.startAddress.zip ||
                !req.body.startAddress.street){

                        reverseGeoCoder(req.body.startCoord.latitude, req.body.startCoord.longitude)
                            .then((value) => {
                                result = JSON.parse(value);

                                let startAddress = {};
                                startAddress.country = result.results[0].components.country;
                                if(result.results[0].components.city){
                                    startAddress.city = result.results[0].components.city;
                                } else {
                                    startAddress.city = result.results[0].components.town;
                                }
                                startAddress.zip = result.results[0].components.postcode;
                                startAddress.street = result.results[0].components.road;

                                req.body.startAddress = startAddress;

                                resolve(startAddress);
                            })
                            .catch((err) => {
                            reject(err);
                            })
                    } else {
                        resolve(req.body.startAddress);
                    }
                }));

                promisesGeoCoding.push(new Promise((resolve, reject) => {
                    if(!req.body.endAddress || !req.body.endAddress.country || !req.body.endAddress.city
                        || !req.body.endAddress.zip ||
                    !req.body.endAddress.street){
                        reverseGeoCoder(req.body.endCoord.latitude, req.body.endCoord.longitude)
                            .then((value) => {
                                result = JSON.parse(value);

                                let endAddress = {};
                                endAddress.country = result.results[0].components.country;
                                if(result.results[0].components.city){
                                    endAddress.city = result.results[0].components.city;
                                } else {
                                    endAddress.city = result.results[0].components.town;
                                }
                                endAddress.zip = result.results[0].components.postcode;
                                endAddress.street = result.results[0].components.road;

                                req.body.endAddress = endAddress;

                                resolve(endAddress);
                            })
                            .catch((err) => {
                                reject(err);
                            })
                    } else {
                        resolve(req.body.endAddress);
                    }
                }));

                Promise.all(promisesGeoCoding)
                    .then((result) => {
                        let drive = new Drive(req.body);

                        drive.save((err, data) => {
                            if (err) {
                                reject(err)
                            } else {
                                resolve(data)

                            }
                        })
                    })
                    .catch((err) => {
                        return next(err);
                    })
            }))

        }

        Promise.all(promises)
            .then((values) => {
                res.status(201).json(values);
            })
            .catch((err) => {
                console.log(err);
                next(err);
            })

    } else {
        res.sendStatus(403);
    }

});


//EXPERIMANTAL ROUTE GEOCODING
//TODO: Implement function for geoCoding

router.post('/geoCoding', (req, res, next) => {


    let promise = new Promise((resolve, reject) => {
        https.get({
            host: geoCodingAPI.host,
            path: geoCodingAPI.path + '?q=' + req.body.latitude + ',+' + req.body.longitude + '&pretty=1&key='
                    + geoCodingAPI.key
        }, (response) => {
            console.log('Got Response: ' + response.statusCode);
            let output = [];
            response
                .on('data', (chunk) => {
                    output.push(chunk)
                })
                .on('end', () => {
                    //console.log('Output: ' + output);
                    resolve(output);
                })
        }).on('error', (error) => {
            console.log('Got Error: ' + error);
            reject(error);
        })
    });

    promise.then((value) => {
        console.log('RESOLVED: ' + value);
        result = JSON.parse(value);
        res.status(200).json(result.results[0].components);
    })
        .catch((reason) => {
            console.log('REJECTED: ' + reason);
            next(reason);
        })
});


/*
 HTTP GET /drive/:drive

 return single Drive (ID) as JSON
 */
router.get('/drives/:drive', auth, (req, res, next) => {
    log(req);

    Drive.findById(req.params.drive, (err, drive) => {
        if (err) {
            return next(err)
        }

        res.json(drive)
    })
});

/*
    HTTP PUT /drive/:drive

    updates an existing Drive (by ID) and returns the result as JSON
 */

router.put('/drives/:drive', auth, (req, res, next) => {
    log(req);

    Drive.findByIdAndUpdate(req.params.drive, {$set: req.body, new: true}, (err, drive) => {
        if (err) {
            return next(err)
        }

        res.json(drive)
    })
});

/*
    HTTP DELETE /drive/:drive

    removes an existing Drive (by ID) and returns remaining Drives as JSON
 */

router.delete('/drives/:drive', auth, (req, res, next) => {
    log(req);

    Drive.remove({_id: req.params.drive}, (err) => {
        if (err) {
            return next(err)
        }

        res.sendStatus(200)
    })
});

module.exports = router;