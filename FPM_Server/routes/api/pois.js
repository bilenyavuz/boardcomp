/**
 * Created by Fabian on 15.01.17.
 */
let express = require('express');
let router = express.Router();
let mongoose = require('mongoose');

let jwt = require('express-jwt');
//let auth = jwt({secret: 'SECRET-SHOULD-BE-NOT-HERE', userProperty: 'user'})
let fs = require('fs');
let cert = fs.readFileSync('keys/api.crt');
let auth = jwt({secret: cert, userPorperty: 'user'});

/*
 ===========================================================
 Load Mongoose Models ./models/
 ===========================================================
 */

let POI = mongoose.model('POI');


function log(req) {
    console.log('\x1b[36m' + new Date().toISOString() + '\x1b[0m');
    //console.log('Connection form:' + '\x1b[36m', req.connection.remoteAddress + '\x1b[0m')
    console.log('Connection form:' + '\x1b[36m', req.ip + '\x1b[0m');

    if (req.user) {
        console.log('User: ' + '\x1b[36m' + req.user.username + '\x1b[0m');
        console.log('ID: ' + '\x1b[36m' + req.user.id + '\x1b[0m');
        console.log('Role: ' + '\x1b[36m' + req.user.role + '\x1b[0m')
    }
}

/*========================================================
 RESTfulAPI EndPoint POIS
 ========================================================*/

/*------------------Routes for /pois-------------------*/

/*
 Routes record:

 HTTP GET 	    /pois 				-> returns all Points of Interest
 HTTP GET 	    /pois/:pois 		-> returns a singe POI (by ID)
 HTTP POST 	    /pois 				-> creates a new POI
 HTTP PUT	    /pois/:pois 	    -> updates a POI (by ID)
 HTTP DELETE    /pois/:pois 		-> removes a POI (by ID)

 */


/*
    HTTP GET /pois

    return all POI as JSON
 */
router.get('/pois', (req, res, next) => {
    log(req);

    POI.find((err, data) => {
        if (err) {
            return next(err)
        }

        res.json(data)
    })
});

/*
    HTTP GET /pois/:poi

    return single POI (ID) as JSON
 */
router.get('/pois/:poi', (req, res, next) => {
    log(req);

    poi.findById(req.params.poi, (err, data) => {
        if (err) {
            return next(err)
        }

        res.json(data)
    })
});

/*
    HTTP POST /pois

    create a new poi and return the result as JSON
 */

router.post('/pois', auth, (req, res, next) => {
    log(req);

    let data = new POI(req.body);


    data.save((err, data) => {
        if (err) {
            return next(err)
        }

        res.status(201).json(data)
    })

});

/*
    HTTP PUT /pois/:poi

    updates an existing poi (by ID) and returns the result as JSON
 */

router.put('/pois/:poi', auth, (req, res, next) => {
    log(req);

    POI.findByIdAndUpdate(req.params.poi, {$set: req.body, new: true}, (err, data) => {
        if (err) {
            return next(err)
        }

        res.json(data)

    })
});

/*
    HTTP DELETE /pois/:poi

    removes an existing POI (by ID)
 */

router.delete('/pois/:poi', auth, (req, res, next) => {
    log(req);

    POI.remove({_id: req.params.poi}, (err) => {
        if (err) {
            return next(err)
        }

        res.sendStatus(200)
    })
});

module.exports = router;