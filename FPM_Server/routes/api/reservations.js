/**
 * Created by Fabian on 15.01.17.
 */
let express = require('express');
let router = express.Router();
let mongoose = require('mongoose');

let jwt = require('express-jwt');
let fs = require('fs');
let cert = fs.readFileSync('keys/api.crt');
let auth = jwt({secret: cert, userPorperty: 'user'});

//Reverse geoCoding API

//let geoCodingAPI = require('../config/geoCodingAPI');

/*
 ===========================================================
 Load Mongoose Models ./models/
 ===========================================================
 */
let Reservation = mongoose.model('Reservation');


function log(req) {
    console.log('\x1b[36m' + new Date().toISOString() + '\x1b[0m');
    //console.log('Connection form:' + '\x1b[36m', req.connection.remoteAddress + '\x1b[0m')
    console.log('Connection form:' + '\x1b[36m', req.ip + '\x1b[0m');

    if (req.user) {
        console.log('User: ' + '\x1b[36m' + req.user.username + '\x1b[0m');
        console.log('ID: ' + '\x1b[36m' + req.user.id + '\x1b[0m');
        console.log('Role: ' + '\x1b[36m' + req.user.role + '\x1b[0m')
    }
}

/*========================================================
 RESTfulAPI EndPoint RESERVATION
 ========================================================*/

/*------------------Routes for /Reservation-------------------*/

/*
 Routes record:

 HTTP GET 	    /reservations 						-> returns all Reservations
 HTTP GET 	    /reservations/:reservationID 		-> returns a singe Reservation (by ID)
 HTTP POST 	    /reservations 				        -> creates a new Reservation
 HTTP PUT	    /reservations/:reservationID 	    -> updates a Reservation (by ID)
 HTTP DELETE    /reservations/:reservationID 		-> removes a Reservation (by ID)

 */


/*
    HTTP GET /reservations

    return all Reservations as JSON
 */
router.get('/reservations', (req, res, next) => {
    log(req);

    Reservation.find({})
        .populate({
            path: 'user',
            select: 'firstname lastname -_id'
        })
        .populate({
            path: 'car',
            select: 'vendor model'
        })
        .exec((err, reservation) => {
            if (err) {
                return next(err)
            }

            res.status(200).json(reservation)
        });
});

/*
    HTTP GET /reservations/:car

    return single Reservation (ID) as JSON
 */
router.get('/reservations/:reservation', (req, res, next) => {
    log(req);

    Reservation.findById(req.params.reservation, (err, reservation) => {
        if (err) {
            return next(err)
        }

        res.json(reservation)
    })
});

/*
    HTTP POST /reservations

    create a new Reservations and return the result as JSON
 */

router.post('/reservations', auth, (req, res, next) => {
    log(req);

    let data_reservation = new Reservation(req.body);

    data_reservation.save((err, reservation) => {
        if (err) {
            return next(err)
        }

        res.status(201).json(reservation)
    })

});

/*
    HTTP PUT /reservations/:reservation

    updates an existing Reservation (by ID) and returns the result as JSON
 */

router.put('/reservations/:reservation', auth, (req, res, next) => {
    log(req);

    Reservation.findByIdAndUpdate(req.params.reservation, {$set: req.body, new: true}, (err, reservation) => {
        if (err) {
            return next(err)
        }

        res.json(reservation)
    })
});

/*
    HTTP DELETE /reservations/:reservation

    removes an existing reservation (by ID)
 */

router.delete('/reservations/:reservation', auth, (req, res, next) => {
    log(req);

    Reservation.remove({_id: req.params.reservation}, (err) => {
        if (err) {
            return next(err)
        }

        res.sendStatus(200)
    })
});

module.exports = router;