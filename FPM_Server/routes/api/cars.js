/**
 * Created by Fabian on 15.01.17.
 */
let express = require('express');
let router = express.Router();
let mongoose = require('mongoose');

let jwt = require('express-jwt');
//let auth = jwt({secret: 'SECRET-SHOULD-BE-NOT-HERE', userProperty: 'user'})
let fs = require('fs');
let cert = fs.readFileSync('keys/api.crt');
let auth = jwt({secret: cert, userPorperty: 'user'});

/*
 ===========================================================
 Load Mongoose Models ./models/
 ===========================================================
 */
let Car = mongoose.model('Car');

function log(req) {
    console.log('\x1b[36m' + new Date().toISOString() + '\x1b[0m');
    //console.log('Connection form:' + '\x1b[36m', req.connection.remoteAddress + '\x1b[0m')
    console.log('Connection form:' + '\x1b[36m', req.ip + '\x1b[0m');

    if (req.user) {
        console.log('User: ' + '\x1b[36m' + req.user.username + '\x1b[0m');
        console.log('ID: ' + '\x1b[36m' + req.user.id + '\x1b[0m');
        console.log('Role: ' + '\x1b[36m' + req.user.role + '\x1b[0m')
    }
}

/*========================================================
 RESTfulAPI EndPoint CARS
 ========================================================*/

/*------------------Routes for /Cars-------------------*/

/*
 Routes record:

 HTTP GET 	    /cars 					-> returns all Cars
 HTTP GET 	    /cars/:car 				-> returns a singe Car (by ID)
 HTTP POST 	    /cars 				    -> creates a new Car
 HTTP PUT	    /cars/:car      	    -> updates a Car (by ID)
 HTTP DELETE    /cars/:car 		        -> removes a Car (by ID)

 */


/*
    HTTP GET /cars

    return all Car as JSON
 */
router.get('/cars', (req, res, next) => {
    log(req);

    Car.find((err, car) => {
        if (err) {
            return next(err)
        }

        res.json(car)
    })
});

/*
    HTTP GET /cars/:car

    return single Car (ID) as JSON
 */
router.get('/cars/:car', (req, res, next) => {
    log(req);

    Car.findById(req.params.car, (err, car) => {
        if (err) {
            return next(err)
        }

        res.json(car)
    })
});

/*
    HTTP POST /cars

    create a new Car and return the result as JSON
 */

router.post('/cars', auth, (req, res, next) => {
    log(req);

    let data_car = new Car(req.body);

    data_car.save((err, car) => {
        if (err) {
            return next(err)
        }

        res.status(201).json(car)
    })

});

/*
    HTTP PUT /cars/:car

    updates an existing Car (by ID) and returns the result as JSON
 */

router.put('/cars/:car', auth, (req, res, next) => {
    log(req);

    Car.findByIdAndUpdate(req.params.car, {$set: req.body, new: true}, (err, car) => {
        if (err) {
            return next(err)
        }

        res.json(car)
    })
});

/*
    HTTP DELETE /cars/:car

    removes an existing Car (by ID)
 */

router.delete('/cars/:car', auth, (req, res, next) => {
    log(req);

    Car.remove({_id: req.params.car}, (err) => {
        if (err) {
            return next(err)
        }

        res.sendStatus(200)
    })
});

module.exports = router;