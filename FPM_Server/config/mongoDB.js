let config = {};

config.host =
    'mongodb://localhost/FuhrparkManagement';
config.options =
    {
        server: {
            ssl: true,
            sslValidate: false,
            reconnectTries: Number.MAX_VALUE,
            reconnectInterval: 1000
        },
        auth: {
            authdb: 'admin'
        },
        user: 'FuhrparkManagementUser',
        pass: '153cDc153'
    };

module.exports = config;